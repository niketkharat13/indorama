    <?php $this->load->view('header'); ?>
    <?php $this->load->view('top'); ?>
    <?php $this->load->view('vertical_navbar'); ?>
    <?php $this->load->view('calendar_css') ?>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/standard-table.css">


    <div style="width:103%">
        <div class="col">
            <br>
            <div class="card-yu pt-2 p-1">
                <ul class="nav nav-tabs nav-justified" style="width: 70%;">
                    <li class="nav-item">
                        <a class="nav-link" id="annual-audit-tab" href="<?= base_url() ?>calendar/Calendar_controller/calendar_annual_event" aria-controls="active" aria-expanded="true">Planned Audit</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="regular-audit-tab" href="<?= base_url() ?>calendar/Calendar_controller/calendar_regular_event" aria-controls="active" aria-expanded="true">Full Site</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="desk-audit-tab" href="<?= base_url() ?>calendar/Calendar_controller/calendar_desk_event" aria-controls="active" aria-expanded="true">Desk Audit</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="cluster-audit-tab" href="<?= base_url() ?>calendar/Calendar_controller/calendar_cluster_event" aria-controls="active" aria-expanded="true">Cluster Audit</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="important-link-tab" href="<?= base_url() ?>calendar/Calendar_controller/rescheduled_calendar_event" aria-controls="link" aria-expanded="false">Rescheduled Audits</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="important-link-tab" href="<?= base_url() ?>calendar/Calendar_controller/final_calendar_event" aria-controls="link" aria-expanded="false">Final Audits</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" id="holiday-audit-tab" href="<?= base_url() ?>calendar/Calendar_controller/calendar_holiday_event" aria-controls="active" aria-expanded="true">Holiday</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="important-link-tab" href="<?= base_url() ?>calendar/Calendar_controller/calendar_important_act_event" aria-controls="link" aria-expanded="false">Important Act List</a>
                    </li>
                </ul>
                <style>
                    .filter_div_1 {
                        width: 102%;
                        position: absolute;
                        top: -12px;
                        left: -15px;
                        background-color: #f4f4f4;
                        z-index: 100;
                        box-shadow: 0px 4px 4px #c5c5c5;
                    }
                </style>


                <div class="">
                    <div class="" style="position: relative;">
                        <div class="filter_div_1" id="filter_div_calendar_list" style="display: none;">
                            <?php $this->load->view('calendar_filter_view'); ?>
                        </div>

                        <div class="mt-1 mb-1">
                            <div class="row d-flex align-items-center">
                                <div class="col-3 pr-0 d-flex ">
                                    <a href="<?=base_url()?>calendar/Calendar_controller/calendar_view"><i class="la la-angle-left mr-1"></i>Back</a>
                                </div>

                                <div class="col-5">
                                </div>
                                <div class="col-3 d-flex justify-content-end" style="padding:0;margin-left:4rem">
                                    <button class="btn-yu primary mr-1" data-toggle="tooltip" title="Filter" id="filter_button_1" style="width:35px;border-radius:50%;border:none">
                                        <i class="ft-filter text-white"></i>
                                    </button>



                                    <button id="upload_button" class="btn-yu primary" style="width:35px" data-toggle="modal" data-target="#upload_file">
                                        <i data-toggle="tooltip" title="Upload File" class="ft-upload text-white"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="calendar_list_view_div">
                            <table class="table display" id="calendar_list_grid" width="100%" border="0" cellpadding="0" cellspacing="0">
                                <thead>
                                    <th class="no-sort checkbox adjust_checkbox_column thead-custom" style="padding-left:6px !important;">
                                        <label>
                                            <input type="checkbox" value="" id="select_all">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <th class="thead-custom">#</th>
                                    <th class="thead-custom">Holiday Name</th>
                                    <th class="thead-custom">Holiday Type</th>
                                    <th class="thead-custom">Date</th>
                                    <th class="thead-custom">Year</th>
                                    <th class="thead-custom">Duration <small>(in Days)</small></th>
                                    <th class="thead-custom">State</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th class="checkbox adjust_checkbox_column">
                                            <label>
                                                <input type="checkbox" value="" name="check[]" class="checkbox">
                                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                            </label>
                                        </th>
                                        <td>1</td>
                                        <td>New Year Eve</td>
                                        <td>Government</td>
                                        <td>01 Jan</td>
                                        <td>2020</td>
                                        <td>1</td>
                                        <td>Central</td>
                                    </tr>
                                    <tr>
                                        <th class="checkbox adjust_checkbox_column">
                                            <label>
                                                <input type="checkbox" value="" name="check[]" class="checkbox">
                                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                            </label>
                                        </th>
                                        <td>2</td>
                                        <td>Gudi Padwa</td>
                                        <td>Restricted</td>
                                        <td>25 Mar</td>
                                        <td>2020</td>
                                        <td>1</td>
                                        <td>Maharashtra</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('upload_modal'); ?>
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('calendar_js') ?>


    <!-- upload file -->