  <!-- modal for upload file-->
  <div id="upload_file" class="modal fade" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class>Import Data</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="radio">
                      <div class="row justify-content-between">
                          <div>
                              <label class="mr-1 hl" for="id4">Select File Type</label>
                          </div>
                          <div>
                              <div id="socialShare">
                                  <div class="socialBox pointer">
                                      <span class="ft-download" title="Download" data-toggle='tooltip'></span>
                                      <div id="socialGallery">
                                          <div class="socialToolBox">
                                              <a class="mail excel_icon" style=" background-color:#0f9d58;" title="Download Excel" data-toggle='tooltip' onclick="downloadExcel()"><i class="fas fa-file-excel"></i></a>
                                              <a class="link csv_icon" style="background-color:#f3b509;" onclick="downloadCsv()" title="Download CSV" data-toggle='tooltip'><i class="fas fa-file-csv"></i></a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row jcontent justify-content-center">
                          <div class="radio_box-style">
                              <input type="radio" name="filetype" id="upload_excel">
                              <label for="upload_excel" class="upload_modal_label">
                                  <span class=""></span>
                                  <i class='ft-file font-size-46'></i><br>
                                  <span>XLS/XLSX</span>
                              </label>
                          </div>
                          <div class="radio_box-style">
                              <input type="radio" name="filetype" id="upload_csv">
                              <label for="upload_csv" class="upload_modal_label">
                                  <span></span>
                                  <i class='ft-file font-size-46'></i><br>
                                  <span>CSV</span>
                              </label>
                          </div>
                      </div>
                  </div>
                  <div class="col-12">
                      <fieldset class="form-group">
                          <form action="#" class="dropzone dropzone-area dropht" id="newvmdemailform">
                          </form>
                      </fieldset>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-toggle="modal" data-target="#uploadfile2" class="btn-width btn-yu primary text-white" id="importfile">Import</button>
                  <button type="button" data-dismiss="modal" class="btn-width btn-yu primary text-white">Cancel</button>
              </div>
          </div>
      </div>
  </div>

  <!-- modal for upload file 2-->
  <div id="uploadfile2" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title">Map Fields</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                  </button>
                  <!-- <h6>Default Fields</h6> -->

              </div>
              <div class="modal-body">
                  <table class=" table p ">
                      <tr>
                          <th class="c1 bg border-0">LIST OF FIELDS IN TABLE</th>
                          <th class="c2 bg border-0">COLUMNS IN FILE</th>
                          <th class="c2 bg border-0"></th>
                      </tr>
                      <tr>
                          <td class="tc">Table Header1</td>
                          <td>
                              <div class="search_dropdown_div">
                                  <select class="selectivity_dropdown sb" id="table_header1">
                                      <option value="10"> Header 1</option>
                                      <option value="25"> Header 2</option>
                                      <option value="50">Header 3</option>
                                      <option value="100"> Header 4</option>
                                  </select>
                              </div>
                          </td>

                      </tr>
                      <tr>
                          <td>Table Date Header2</td>
                          <td>
                              <div class="search_dropdown_div">
                                  <select class="selectivity_dropdown sb" id="table_header2">
                                      <option value="10"> Header 1</option>
                                      <option value="25"> Header 2</option>
                                      <option value="50"> Header 3</option>
                                      <option value="100"> Header 4</option>
                                  </select>
                              </div>
                          </td>
                          <td>
                              <div class="search_dropdown_div">
                                  <select class="selectivity_dropdown sb" id="table_header2_date_format">
                                      <option value="10">dd.MM.yyyy</option>
                                      <option value="25">dd/MM/yyyy</option>
                                      <option value="50">MM.dd.yyyy</option>
                                      <option value="100">yyyy-MM-dd</option>
                                  </select>
                              </div>
                          </td>
                      </tr>
                      <tr>
                          <td>Table Date Header3</td>
                          <td>
                              <div class="search_dropdown_div">
                                  <select class="selectivity_dropdown sb" id="table_header3">
                                      <option value="10"> Header 1</option>
                                      <option value="25">Header 2</option>
                                      <option value="50">Header 3</option>
                                      <option value="100">Header 4</option>
                                  </select>
                              </div>
                          </td>
                          <td>
                              <div class="search_dropdown_div">
                                  <select class="selectivity_dropdown sb" id="table_header3_date_format">
                                      <option value="10">dd.MM.yyyy</option>
                                      <option value="25">dd/MM/yyyy</option>
                                      <option value="50">MM.dd.yyyy</option>
                                      <option value="100">yyyy-MM-dd</option>
                                  </select>
                              </div>
                          </td>
                      </tr>
                      <tr>
                          <td>Table Header4</td>
                          <td>
                              <div class="search_dropdown_div">
                                  <select class="selectivity_dropdown sb" id="table_header4">
                                      <option value="10"> Header 1</option>
                                      <option value="25"> Header 2</option>
                                      <option value="50"> Header 3</option>
                                      <option value="100">Header 4</option>
                                  </select>
                              </div>
                          </td>
                      </tr>
                      <tr>
                          <td>Table Header5</td>
                          <td>
                              <div class="search_dropdown_div">

                                  <select class="selectivity_dropdown sb" id="table_header5">
                                      <option value="10"> Header 1</option>
                                      <option value="25"> Header 2</option>
                                      <option value="50"> Header 3</option>
                                      <option value="100">Header 4</option>
                                  </select>
                              </div>
                          </td>
                      </tr>
                      <tr>
                          <td>Table Header6</td>
                          <td>
                              <div class="search_dropdown_div">
                                  <select class="selectivity_dropdown sb" id="table_header6">
                                      <option value="10"> Header 1</option>
                                      <option value="25"> Header 2</option>
                                      <option value="50">Header 3</option>
                                      <option value="100">Header 4</option>
                                  </select>
                              </div>
                          </td>
                      </tr>
                  </table>
              </div>
              <div class="modal-footer">
                  <div class="bg">
                      <button type="button" data-dismiss="modal" class="btn-width btn-yu primary text-white" id="continuebtn">Continue</button>
                      <button type="button" data-dismiss="modal" class="btn-width btn-yu primary text-white" id="cancelbtn">Back</button>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- modal for upload file 2-->