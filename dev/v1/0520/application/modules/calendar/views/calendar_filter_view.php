	<style type="text/css">
		.tdWidth {
			min-width: 180px !important;
		}

		.default_filter_element_div .table-responsive {
			height: auto;
		}

		.calendar_annual_event_filter,
		.calendar_regular_event,
		.calendar_desk_event,
		.calendar_holiday_event,
		.calendar_important_act_event,
		.rescheduled_calendar_event {
			display: none;
		}
	</style>

	<div class="row">
		<div class="default_filter_element_div col-lg-12" style="white-space: nowrap;overflow-x: auto !important;overflow-y: auto !important;">
			<table class="table table-responsive" style="padding: 1%;margin-bottom: 0%;">
				<tr>
					<form id="calendar_filter_form">
						<!-- calendar annual filter view -->
						<td class=" calendar_annual_event_filter">
							<div>
								<h4>By First Audit Month</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Jan
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Feb
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Mar
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_annual_event_filter">
							<div>
								<h4>By Client</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											JLL
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											HDFC
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_annual_event_filter">
							<div>
								<h4>By Account</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											HDFC
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											SBI
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_annual_event_filter">
							<div>
								<h4>By Region</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											East
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											West
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											North
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											South
										</label>
									</div>
								</div>
							</div>
						</td>
						<td class=" calendar_annual_event_filter">
							<div>
								<h4>By Location</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Kandivali
										</label>
									</div>
								</div>
							</div>
						</td>
						<td class=" calendar_annual_event_filter">
							<div>
								<h4>By Site</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Charkop
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Andheri
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_annual_event_filter">
							<div>
								<h4>By Vendor</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Chroma
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Bosch Ltd
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_annual_event_filter">
							<div>
								<h4>By Auditor</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Ami Sheth
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Soham Patil
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class="tdWidth calendar_annual_event_filter">
							<div>
								<h4>By Week Day</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Monday
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Tuesday
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Wednesday
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Thursday
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Friday
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Saturday
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class="tdWidth calendar_annual_event_filter">
							<div>
								<h4>By Grace Day</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											0
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											1
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											2
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											3
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											4
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											5
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											6
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											7
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class="tdWidth calendar_annual_event_filter">
							<div>
								<h4>By Schedule for # of Month</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											6
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											12
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class="tdWidth calendar_annual_event_filter">
							<div>
								<h4>By Audit Service Type</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Online (AI)
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Online
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Physical
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class="tdWidth calendar_annual_event_filter">
							<div>
								<h4>By Audit Categorisation</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Full Site
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Desk
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Cluster
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_annual_event_filter">
							<h4>By Frequency</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Monthly
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Quarterly
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Alternate Month
									</label>
								</div>
							</div>
						</td>
						<!-- calendar regular filter -->
						<td class=" calendar_regular_event">
							<div>
								<h4>By Audit Start Date</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											03 Mar
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											04 Mar
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_regular_event">
							<div>
								<h4>By Start Time</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											11:00 am
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											12:00 pm
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_regular_event">
							<div>
								<h4>By Audit Month</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Jan 2020
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Jan 2019
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_regular_event">
							<h4>By Audit Type</h4>
							<div class="option_style" id="document_filter_element_audit_type">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Original
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Remediation
									</label>
								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_regular_event">
							<h4>By Status</h4>
							<div class="option_style" id="document_filter_element_audit_type">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Scheduled
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Auto-close
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Audited
									</label>
								</div>
							</div>
						</td>
						<td class=" calendar_regular_event">
							<div>
								<h4>By Client</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											JLL
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											HDFC
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_regular_event">
							<div>
								<h4>By Account</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											HDFC
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											SBI
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_regular_event">
							<div>
								<h4>By Region</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											East
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											West
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											North
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											South
										</label>
									</div>
								</div>
							</div>
						</td>
						<td class=" calendar_regular_event">
							<div>
								<h4>By Location</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Kandivali
										</label>
									</div>
								</div>
							</div>
						</td>
						<td class=" calendar_regular_event">
							<div>
								<h4>By Site</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Charkop
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Andheri
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_regular_event">
							<div>
								<h4>By Vendor</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Chroma
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Bosch Ltd
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_regular_event">
							<div>
								<h4>By Auditor</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Ami Sheth
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Soham Patil
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class="tdWidth calendar_regular_event">
							<div>
								<h4>By Audit Service Type</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Online (AI)
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Online
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Physical
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_regular_event">

							<h4>By Frequency</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Monthly
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Quarterly
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Alternate Month
									</label>
								</div>


							</div>

						</td>


						<!-- calendar Desk filter -->
						<td class=" calendar_desk_event">
							<div>
								<h4>By Audit Start Date</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											03 Mar
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											04 Mar
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_desk_event">
							<div>
								<h4>By Start Time</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											11:00 am
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											12:00 pm
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_desk_event">
							<div>
								<h4>By Audit Month</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Jan 2020
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Jan 2019
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_desk_event">
							<h4>By Audit Type</h4>
							<div class="option_style" id="document_filter_element_audit_type">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Original
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Remediation
									</label>
								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_desk_event">
							<h4>By Status</h4>
							<div class="option_style" id="document_filter_element_audit_type">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Scheduled
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Auto-close
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Audited
									</label>
								</div>
							</div>
						</td>
						<td class=" calendar_desk_event">
							<div>
								<h4>By Client</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											JLL
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											HDFC
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_desk_event">
							<div>
								<h4>By Account</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											HDFC
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											SBI
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_desk_event">
							<div>
								<h4>By Region</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											East
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											West
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											North
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											South
										</label>
									</div>
								</div>
							</div>
						</td>
						<td class=" calendar_desk_event">
							<div>
								<h4>By Site</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Charkop
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Andheri
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_desk_event">
							<div>
								<h4>By Vendor</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Chroma
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Bosch Ltd
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_desk_event">
							<div>
								<h4>By Auditor</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Ami Sheth
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Soham Patil
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class="tdWidth calendar_desk_event">
							<div>
								<h4>By Audit Service Type</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Online (AI)
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Online
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Physical
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_desk_event">

							<h4>By Frequency</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Monthly
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Quarterly
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Alternate Month
									</label>
								</div>


							</div>

						</td>

						<!-- calendar cluster filter -->
						<td class=" calendar_cluster_event">
							<div>
								<h4>By Audit Start Date</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											03 Mar
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											04 Mar
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_cluster_event">
							<div>
								<h4>By Start Time</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											11:00 am
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											12:00 pm
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_cluster_event">
							<div>
								<h4>By Audit Month</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Jan 2020
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Jan 2019
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_cluster_event">
							<h4>By Audit Type</h4>
							<div class="option_style" id="document_filter_element_audit_type">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Original
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Remediation
									</label>
								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_cluster_event">
							<h4>By Status</h4>
							<div class="option_style" id="document_filter_element_audit_type">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Scheduled
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Auto-close
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Audited
									</label>
								</div>
							</div>
						</td>
						<td class=" calendar_cluster_event">
							<div>
								<h4>By Client</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											JLL
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											HDFC
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_cluster_event">
							<div>
								<h4>By Account</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											HDFC
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											SBI
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_cluster_event">
							<div>
								<h4>By Region</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											East
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											West
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											North
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											South
										</label>
									</div>
								</div>
							</div>
						</td>
						<td class=" calendar_cluster_event">
							<div>
								<h4>By Location</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Kandivali
										</label>
									</div>
								</div>
							</div>
						</td>
						<td class=" calendar_cluster_event">
							<div>
								<h4>By Cluster</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Cluster 1
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Cluster 2
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_cluster_event">
							<div>
								<h4>By Vendor</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Chroma
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Bosch Ltd
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_cluster_event">
							<div>
								<h4>By Auditor</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Ami Sheth
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Soham Patil
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class="tdWidth calendar_cluster_event">
							<div>
								<h4>By Audit Service Type</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Online (AI)
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Online
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Physical
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_cluster_event">

							<h4>By Frequency</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Monthly
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Quarterly
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Alternate Month
									</label>
								</div>


							</div>

						</td>

						<!-- holiday filter  -->
						<td class=" tdWidth calendar_holiday_event">

							<h4>By Holiday Name</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										New Year Eve
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Gudi Padwa
									</label>
								</div>

							</div>

						</td>
						<td class=" tdWidth calendar_holiday_event">

							<h4>By Holiday Type</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Government
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Restricted
									</label>
								</div>

							</div>

						</td>
						<td class=" tdWidth calendar_holiday_event">

							<h4>By Date</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										01 Jan
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										25 Mar
									</label>
								</div>

							</div>

						</td>
						<td class=" tdWidth calendar_holiday_event">

							<h4>By Year</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										2020
								</div>

							</div>

						</td>
						<td class=" tdWidth calendar_holiday_event">

							<h4>By Duration(in days)</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										1
								</div>

							</div>

						</td>

						<td class=" tdWidth calendar_holiday_event">

							<h4>By State</h4>
							<div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Central
								</div>
							</div>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Maharashtra
								</div>
							</div>

						</td>

						<!-- important act date -->
						<td class=" tdWidth calendar_important_act_event">
							<h4>By Event Date</h4>
							<div class="" id="">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										03 Mar 2020
								</div>

							</div>
							<div class="option_style" id="">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										04 Mar 2020
								</div>

							</div>
						</td>
						<td class=" tdWidth calendar_important_act_event">
							<h4>By Appropriate Government</h4>
							<div class="">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										State
								</div>
							</div>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Central
								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_important_act_event">
							<h4>By State</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Maharashtra
								</div>
							</div>
						</td>
						<td class="tdWidth calendar_important_act_event">
							<h4>By Act</h4>
							<div class="option_style" id="document_filter_element_acts">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Bonus
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										ESI
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										PF
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										CLRA
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										MBenefit
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										MWages
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										HRA
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										PWages
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										PTax
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										LWF
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										S&E
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Others
									</label>
								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_important_act_event">
							<h4>By Title</h4>
							<div class="option_style" id="document_filter_element_title">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Bonus Register
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Bonus Returns
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										ESI Registration Code
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										ESI challan
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										ESI challan date of payment
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										ESIC ECR
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										ESI Cards
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										PF Registration Code
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										PF Challan
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										PF challan date of payment
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										PF ECR
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										EDLI Exemption
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										EDLI Monthly Return
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										PF Declaration Form 11 / UAN
									</label>
								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_important_act_event">
							<h4>By Document Type</h4>
							<div class="option_style" id="document_filter_element_doc_type">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Register
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Returns
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Challan
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Preliminary
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										License
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Head Count
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Bank statement/Mini statement
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Document
									</label>
								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_important_act_event">

							<h4>By Frequency</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Monthly
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Quarterly
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Alternate Month
									</label>
								</div>


							</div>

						</td>

						<!-- reschedule Audit filter -->
						<td class=" tdWidth rescheduled_calendar_event">
							<h4>By Requestor</h4>
							<div class="">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Client
								</div>
							</div>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Audit Manager
								</div>
							</div>
						</td>
						<td class=" tdWidth rescheduled_calendar_event">
							<h4>By Requestor Name</h4>
							<div class="">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Aniket Y
								</div>
							</div>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Bhavana N
								</div>
							</div>
						</td>
						<td class=" tdWidth rescheduled_calendar_event">
							<h4>By Approver Name</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Bhavana N
								</div>
							</div>
						</td>
						<td class=" tdWidth rescheduled_calendar_event">
							<h4>By Approval Status</h4>
							<div class="">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										On Hold
								</div>
							</div>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Decision Pending
								</div>
							</div>
						</td>
						<td class=" rescheduled_calendar_event">
							<div>
								<h4>By Audit Start Date</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											03 Mar
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											04 Mar
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" rescheduled_calendar_event">
							<div>
								<h4>By Start Time</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											11:00 am
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											12:00 pm
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" rescheduled_calendar_event">
							<div>
								<h4>By Audit Month</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Jan 2020
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Jan 2019
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" tdWidth rescheduled_calendar_event">
							<h4>By Audit Type</h4>
							<div class="option_style" id="document_filter_element_audit_type">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Original
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Remediation
									</label>
								</div>


							</div>

						</td>
						<td class=" rescheduled_calendar_event">
							<div>
								<h4>By Client</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											JLL
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											HDFC
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" rescheduled_calendar_event">
							<div>
								<h4>By Account</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											HDFC
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											SBI
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" rescheduled_calendar_event">
							<div>
								<h4>By Region</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											East
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											West
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											North
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											South
										</label>
									</div>
								</div>
							</div>
						</td>
						<td class=" rescheduled_calendar_event">
							<div>
								<h4>By Location</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Kandivali
										</label>
									</div>


								</div>
							</div>
						</td>
						<td class=" rescheduled_calendar_event">
							<div>
								<h4>By Cluster</h4>
								<div class="">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Cluster 1
										</label>
									</div>

								</div>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Cluster 2
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" rescheduled_calendar_event">
							<div>
								<h4>By Vendor</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Croma
										</label>
									</div>

								</div>

							</div>
						</td>
						<td class=" rescheduled_calendar_event">
							<div>
								<h4>By Auditor</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Ami Sheth
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Soham Patil
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class="tdWidth rescheduled_calendar_event">
							<div>
								<h4>By Audit Service Type</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Online (AI)
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Online
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Physical
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" tdWidth rescheduled_calendar_event">

							<h4>By Frequency</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Monthly
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Quarterly
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Alternate Month
									</label>
								</div>


							</div>

						</td>


						<!-- calendar final event -->
						<td class=" calendar_final_event">
							<div>
								<h4>By Audit Start Date</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											03 Mar
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											04 Mar
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_final_event">
							<div>
								<h4>By Audit Month</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Jan 2020
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Jan 2019
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_final_event">
							<h4>By Audit Type</h4>
							<div class="option_style" id="document_filter_element_audit_type">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Original
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Remediation
									</label>
								</div>
							</div>
						</td>
						<td class=" calendar_final_event">
							<div>
								<h4>By Client</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											JLL
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											HDFC
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_final_event">
							<div>
								<h4>By Account</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											HDFC
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											SBI
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_final_event">
							<div>
								<h4>By Region</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											East
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											West
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											North
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											South
										</label>
									</div>
								</div>
							</div>
						</td>
						<td class=" calendar_final_event">
							<div>
								<h4>By Location</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Kandivali
										</label>
									</div>
								</div>
							</div>
						</td>
						<td class=" calendar_final_event">
							<div>
								<h4>By Site</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Charkop
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Andheri
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_final_event">
							<div>
								<h4>By Vendor</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Chroma
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Bosch Ltd
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" calendar_final_event">
							<div>
								<h4>By Auditor</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Ami Sheth
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Soham Patil
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class="tdWidth calendar_final_event">
							<div>
								<h4>By Audit Service Type</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Online (AI)
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Online
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											Physical
										</label>
									</div>

								</div>
							</div>
						</td>
						<td class=" tdWidth calendar_final_event">

							<h4>By Frequency</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Monthly
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Quarterly
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Alternate Month
									</label>
								</div>


							</div>

						</td>
					</form>
				</tr>
			</table>
		</div>
	</div>
	<hr>
	<div style="padding: 10px;">
		<button class="btn btn-sm primary" style="width:135px" id="filter_div_save">Apply and Export</button>
		<button class="btn btn-sm primary" style="width:135px" id="filter_div_apply">Apply</button>
		<button class="btn btn-sm btn-dark" style="width:100px" id="filter_div_close">Close</button>
	</div>


	<script type="text/javascript">
		$(document).ready(function() {
			var value = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
			if (value == "calendar_event" || value == "calendar_view") {
				$(".calendar_filter_element").show();
				$(".dashboard_filter_element").hide();
				$(".default_filter_element").hide();
				$(".document_filter_element").hide();
				$(".report_filter_element").hide();
				$(".audit_table_filter_element").hide();
				$(".site_filter_element").hide();
				$(".audit_filter_element").hide();
				$(".calendar_important_act_event").hide();
				$(".calendar_regular_event").hide();
				$(".calendar_holiday_event").hide();
				$(".calendar_annual_event_filter").hide();
				$(".rescheduled_calendar_event").hide();
				$(".calendar_desk_event").hide();
				$(".calendar_cluster_event").hide();
				$(".calendar_final_event").hide();


			} else if (value == "calendar_annual_event" || value == "calendar_annual_event?filetype=mpp") {
				$(".calendar_annual_event_filter").show();
				$(".calendar_regular_event").hide();
				$(".calendar_holiday_event").hide();
				$(".calendar_important_act_event").hide();
				$(".rescheduled_calendar_event").hide();
				$(".calendar_desk_event").hide();
				$(".calendar_cluster_event").hide();
				$(".calendar_final_event").hide();



			} else if (value == "calendar_regular_event") {
				$(".calendar_regular_event").show();
				$(".calendar_annual_event_filter").hide();
				$(".calendar_holiday_event").hide();
				$(".calendar_important_act_event").hide();
				$(".rescheduled_calendar_event").hide();
				$(".calendar_desk_event").hide();
				$(".calendar_cluster_event").hide();
				$(".calendar_final_event").hide();


			} else if (value == "calendar_holiday_event") {
				$(".calendar_holiday_event").show();
				$(".calendar_regular_event").hide();
				$(".calendar_annual_event_filter").hide();
				$(".calendar_important_act_event").hide();
				$(".rescheduled_calendar_event").hide();
				$(".calendar_desk_event").hide();
				$(".calendar_cluster_event").hide();
				$(".calendar_final_event").hide();


			} else if (value == "calendar_important_act_event") {
				$(".calendar_important_act_event").show();
				$(".calendar_holiday_event").hide();
				$(".calendar_regular_event").hide();
				$(".calendar_annual_event_filter").hide();
				$(".rescheduled_calendar_event").hide();
				$(".calendar_desk_event").hide();
				$(".calendar_cluster_event").hide();
				$(".calendar_final_event").hide();


			} else if (value == "rescheduled_calendar_event") {
				$(".calendar_important_act_event").hide();
				$(".calendar_holiday_event").hide();
				$(".calendar_regular_event").hide();
				$(".calendar_annual_event_filter").hide();
				$(".rescheduled_calendar_event").show();
				$(".calendar_desk_event").hide();
				$(".calendar_cluster_event").hide();
				$(".calendar_final_event").hide();


			} else if (value == "rescheduled_calendar_event") {
				$(".calendar_important_act_event").hide();
				$(".calendar_holiday_event").hide();
				$(".calendar_regular_event").hide();
				$(".calendar_annual_event_filter").hide();
				$(".rescheduled_calendar_event").show();
				$(".calendar_desk_event").hide();
				$(".calendar_cluster_event").hide();
				$(".calendar_final_event").hide();


			} else if (value == "calendar_desk_event") {
				$(".calendar_important_act_event").hide();
				$(".calendar_holiday_event").hide();
				$(".calendar_regular_event").hide();
				$(".calendar_annual_event_filter").hide();
				$(".rescheduled_calendar_event").hide();
				$(".calendar_desk_event").show();
				$(".calendar_cluster_event").hide();
				$(".calendar_final_event").hide();


			} else if (value == "calendar_cluster_event") {
				$(".calendar_important_act_event").hide();
				$(".calendar_holiday_event").hide();
				$(".calendar_regular_event").hide();
				$(".calendar_annual_event_filter").hide();
				$(".rescheduled_calendar_event").hide();
				$(".calendar_desk_event").hide();
				$(".calendar_final_event").hide();
				$(".calendar_cluster_event").show();
			} else if (value == "final_calendar_event") {
				$(".calendar_important_act_event").hide();
				$(".calendar_holiday_event").hide();
				$(".calendar_regular_event").hide();
				$(".calendar_annual_event_filter").hide();
				$(".rescheduled_calendar_event").hide();
				$(".calendar_desk_event").hide();
				$(".calendar_cluster_event").hide();
				$(".calendar_final_event").show();

			}


		});
	</script>