        <style type="text/css">
            /*.selectdiv {
                position: relative;
                
                
                float: left;
                min-width: 20% !important;
                margin-bottom: 0% !important 
                
                }         

                
                select::-ms-expand {
                    display: none;
                }

                .selectdiv:after {
                content: '>';
                font: 17px "Consolas", monospace;
                color: #333;
                -webkit-transform: rotate(90deg);
                -moz-transform: rotate(90deg);
                -ms-transform: rotate(90deg);
                transform: rotate(90deg);
                right: 11px;
            
                top: 18px;
                padding: 0 0 2px;
                border-bottom: 1px solid #999;
                
                position: absolute;
                pointer-events: none;
                }

                .selectdiv select {
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                
                
                display: block;
                width: 100%;
                max-width: 320px;
                height: 31px;
                float: left;
                margin: 10% -4% !important;
                padding: 0px 24px;
                font-size: 16px;
                line-height: 1.75;
                color: #333;
                background-color: #ffffff;
                background-image: none;
                border: 1px solid #cccccc;
                -ms-word-break: normal;
                word-break: normal;
                border-color: #00517cf5 !important;
                }*/
            .line {
                height: 65px;
                border: 1px #dbd6d6 solid;
            }


            .line-arrange {
                margin-left: -1%
            }

            .min-max-3 {
                max-width: 1213px;
                min-width: 1231px;
                margin-left: 27px;
                margin-bottom: 15px;

            }


            select {

                /* styling */
                background-color: white;
                border: thin solid #0a5881;
                border-radius: 4px;
                display: inline-block;

                line-height: 1.5em;
                padding: 0.5em 3.5em 0.5em 1em;

                /* reset */

                margin: 0;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
                -webkit-appearance: none;
                -moz-appearance: none;
            }

            select.minimal {
                background-image:
                    linear-gradient(45deg, transparent 50%, #0a5881 50%),
                    linear-gradient(135deg, #0a5881 50%, transparent 50%),
                    linear-gradient(to right, #ccc, #ccc);
                background-position:
                    calc(100% - 20px) calc(1em + 2px),
                    calc(100% - 15px) calc(1em + 2px),
                    calc(100% - 2.5em) 0.5em;
                background-size:
                    5px 5px,
                    5px 5px,
                    1px 1.5em;
                background-repeat: no-repeat;
            }

            select.minimal:focus {
                background-image:
                    linear-gradient(45deg, #0a5881 50%, transparent 50%),
                    linear-gradient(135deg, transparent 50%, #0a5881 50%),
                    linear-gradient(to right, #ccc, #ccc);
                background-position:
                    calc(100% - 15px) 1em,
                    calc(100% - 20px) 1em,
                    calc(100% - 2.5em) 0.5em;
                background-size:
                    5px 5px,
                    5px 5px,
                    1px 1.5em;
                background-repeat: no-repeat;
                border-color: #0a5881;
                outline: 0;
            }


            select:-moz-focusring {
                color: transparent;
                text-shadow: 0 0 0 #000;
            }

            .size-star {
                font-size: 76px;
                margin-bottom: -56px;
                margin-top: -14px;
                color: #f15561;
            }

            .widget_div {
                justify-content: space-evenly;
            }

            .scheduled_color{
                color: #f5ad5f;
                font-size: 25px !important;
                margin-top: 14px;
            }
            .audited_color{
                color: #a1d36e;
            }
            .autoclose_color{
                color: #fa6f57;
                font-size: 25px !important;
                margin-top: 14px;
            }
            .reopen_color{
                color: #568dd5;
                font-size: 25px !important;
                margin-top: 14px;
            }
            .onhold_color{
                color: #935b1a;
                font-size: 25px !important;
                margin-top: 14px;
            }
            .rescheduled_color{
                color: #3cccc6;
                font-size: 25px !important;
                margin-top: 14px;
            }
        </style>


        <section id="basic" class="mt-1">
            <div>
                <div class="row ">
                    <div class="col-lg-12 col-md-12">
                        <div class="row widget_div">
                            <div class="col" style="justify-items: center;display: grid;">

                                <div class="">
                                    <div class="align-self-center d-flex">
                                        <i class="ft-check-circle font-large-2 scheduled_color"></i>&nbsp;
                                        <h2 class="text-bold-600 full_score" style="font-size: 2.74rem;margin-bottom: 0;">
                                            50</h2>
                                    </div>
                                    <div class="text-center">
                                        <h5 class="text-muted text-bold-500">Scheduled</h5>
                                    </div>
                                </div>

                            </div>
                            <div class="col" style="justify-items: center;display: grid;">

                                <div class="">
                                    <div class="align-self-center d-flex">

                                        <p class="size-star audited_color"> *</p>
                                        &nbsp;
                                        <h2 class="text-bold-600 critical_score" style="font-size: 2.74rem;margin-bottom: 0;">
                                            25</h2>
                                    </div>
                                    <div class="text-center">
                                        <h5 class="text-muted text-bold-500">Audited</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col" style="justify-items: center;display: grid;">

                                <div class="">
                                    <div class="align-self-center d-flex">
                                        <i class="mbri-clock autoclose_color"></i>
                                        &nbsp;
                                        <h2 class="text-bold-600 critical_score" style="font-size: 2.74rem;margin-bottom: 0;">
                                            5</h2>
                                    </div>
                                    <div>
                                        <h5 class="text-muted text-bold-500">Auto-closed</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col" style="justify-items: center;display: grid;">
                                <div class="">
                                    <div class="align-self-center d-flex">
                                        <i class="mbri-unlock reopen_color"></i>
                                        &nbsp;
                                        <h2 class="text-bold-600 critical_score" style="font-size: 2.74rem;margin-bottom: 0;">
                                            5</h2>
                                    </div>
                                    <div class="text-center">
                                        <h5 class="text-muted text-bold-500">Reopen</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col" style="justify-items: center;display: grid;">

                                <div class="">
                                    <div class="align-self-center d-flex">
                                    <i class="ft-pause-circle onhold_color"></i>
                                        &nbsp;
                                        <h2 class="text-bold-600 critical_score" style="font-size: 2.74rem;margin-bottom: 0;">
                                            5</h2>
                                    </div>
                                    <div class="text-center">
                                        <h5 class="text-muted text-bold-500">On Hold</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col" style="justify-items: center;display: grid;">

                                <div class="">
                                    <div class="align-self-center d-flex ml-1">
                                        <i class="ft-rotate-ccw font-large-2 rescheduled_color"></i>
                                        &nbsp;
                                        <h2 class="text-bold-600" style="font-size: 2.74rem;margin-bottom: 0;" id="client-site">
                                            5</h2>
                                    </div>
                                    <div>
                                        <h5 class="text-muted text-bold-500" style="margin-left: 18px;">Rescheduled</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </section>