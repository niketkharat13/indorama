    <!-- Status Change CSS -->
    <style type="text/css">
        .dropdown-menu>li {
            position: relative;
            margin-bottom: 1px;
        }

        .dropdown-menu>li>a {
            padding: 8px 10px;
            outline: 0;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .dropdown-menu>li>a {
            display: block;
            padding: 3px 20px;
            clear: both;
            font-weight: normal;
            line-height: 1.5384616;
            color: #333333;
            white-space: nowrap;
        }

        .btn-group,
        .btn-group-vertical {
            position: relative;
            display: inline-block;
            vertical-align: middle;
        }

        .bg-blue {
            background-color: #03A9F4;
            border-color: #03A9F4;
            color: #fff;
        }

        .bg-green {
            background-color: #8BC34A;
            border-color: #8BC34A;
            color: #fff;
        }

        .bg-orange {
            background-color: #FF9800;
            border-color: #FF9800;
            color: #fff;
        }

        .bg-danger {
            background-color: #F44336;
            border-color: #F44336;
            color: #fff;
        }

        .bg-success {
            background-color: #4CAF50 !important;
            border-color: #4CAF50;
            color: #fff;
        }

        .calendar_list_grid_plan .status-label {
            font-weight: 500;
            padding: 2px 5px 1px 5px;
            line-height: 1.5384616;
            border: 1px solid transparent;
            /*text-transform: uppercase;*/
            font-size: 12px;
            letter-spacing: 0.1px;
            border-radius: 0.25rem;
            width: 140px;
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 2px;
        }

        .status-label {
            font-weight: 500;
            padding: 2px 5px 1px 5px;
            line-height: 1.5384616;
            border: 1px solid transparent;
            /*text-transform: uppercase;*/
            font-size: 12px;
            letter-spacing: 0.1px;
            border-radius: 0.25rem;
            width: 100px;
            display: flex;
            justify-content: flex-end;
            align-items: center;
            padding: 2px;
        }

        .status-label:hover {
            color: #fff;
        }

        /*Dropdown Option*/
        .border-danger {
            border-color: #F44336;
        }

        .border-success {
            border-color: #4CAF50;
        }

        .border-warning {
            border-color: #FF5722;
        }

        .border-blue {
            border-color: #03A9F4;
        }

        .position-left {
            margin-right: 7px;
        }

        .status-mark {
            width: 8px;
            height: 8px;
            display: inline-block;
            border-radius: 50%;
            border: 2px solid;
        }

        .bg-rescheduled {
            background-color: #3cccc6;
            border-color: #3cccc6;
            color: white;
        }

        .border-rescheduled {
            border: 1px solid #3cccc6;
        }


        .bg-onhold {
            background-color: #935b1a;
            border-color: #935b1a;
            color: white;
        }

        .border-onhold {
            border: 1px solid #935b1a;
        }

        .bg-audited {
            background-color: #a1d36e;
            border-color: #a1d36e;
            color: white;
        }

        .border-audited {
            border: 1px solid #a1d36e;
        }

        .bg-scheduled {
            background-color: #f5ad5f;
            border-color: #f5ad5f;
            color: white;
        }

        .border-scheduled {
            border: 1px solid #f5ad5f;
        }

        .bg-converted {
            background-color: #a1d36e;
            border-color: #a1d36e;
            color: white;
        }

        .border-converted {
            border: 1px solid #a1d36e;
        }

        .bg-pending {
            background-color: #568dd5;
            border-color: #568dd5;
            color: white;
        }

        .border-pending {
            border: 1px solid #568dd5;
        }

        .bg-missed {
            background-color: #fa6f57;
            border-color: #fa6f57;
            color: white;
        }

        .border-missed {
            border: 1px solid #fa6f57;
        }

        .bg-manual {
            background-color: #f5ad5f;
            border-color: #f5ad5f;
            color: white;
        }

        .border-manual {
            border: 1px solid #f5ad5f;
        }

        .bg-deleted {
            background-color: #935b1a;
            border-color: #935b1a;
            color: white;
        }

        .border-deleted {
            border: 1px solid #935b1a;
        }

        .bg-auto-close {
            background-color: #fa6f57;
            border-color: #fa6f57;
            color: white;
        }

        .border-auto-close {
            border: 1px solid #fa6f57;
        }

        .bg-reopen {
            background-color: #568dd5;
            border-color: #568dd5;
            color: white;
        }

        .border-reopen {
            border: 1px solid #568dd5;
        }
    </style>
    <!-- END Status Change CSS -->

    <!-- calender css -->

    <style>
        .ft-minus-circle {
            font-size: 15px;
        }

        .ft-info {
            color: #008FA1;
        }

        .disable_pointer {
            cursor: no-drop;
        }

        /* weekend background color */
        .fc-sun {
            background-color: #f3f3f3;
        }

        /* reposnsive  */
        @media (min-width: 538px) and (max-width: 768px) {

            .side_list_view {
                margin-top: 2rem;
            }
        }

        @media (min-width: 333px) and (max-width: 425px) {
            .title_legend_div {
                display: flex;
                flex-direction: column;
            }

            .fc-toolbar {
                display: flex;
                flex-direction: column-reverse;
            }

            .fc-left,
            .fc-right {
                margin-top: 10px;
            }

            .legend_div {
                margin-top: 10px;
                display: flex;
                flex-direction: column;
            }

            .legend_icon {
                float: left;
            }

            .icon-btn {
                margin: 2px;
            }

            .side_list_view {
                margin-top: 2rem;
            }
        }
    </style>

    <style type="text/css">
        /*LEGEND POINTER*/
        .normal_pointer {
            cursor: default !important;
        }

        .audit_badge {
            background-color: #fff;
            border: 1px solid #c5c5c5;
        }

        .search_dropdown_div {
            position: relative !important;
        }

        .selectivity_dropdown {
            position: relative !important;
        }

        .icon-btn {
            padding: 3px 5px 3px 5px;
            border-radius: 50px;
        }

        .legend_icon {
            height: 14px;
            width: 14px;
            display: inline-block;
            border-radius: 50%;
            vertical-align: text-bottom;
        }

        .legend_account_color_1 {
            background-color: #da4453;
        }

        .legend_account_color_2 {
            background-color: #37bc9b;
        }

        .legend_account_color_3 {
            background-color: #967adc;
        }

        .legend_account_color_4 {
            background-color: #ff0000;
        }

        .legend_account_color_5 {
            background-color: #ffa500;
        }

        .legend_account_color_6 {
            background-color: #8bc34a;
        }

        .legend_account_color_7 {
            background-color: #018ee0;
        }

        /* .legend_div {
            margin-top: -9px;
        } */
    </style>

    <style type="text/css">
        /*Blinker CSS*/
        .blink_component {
            animation: blinker 2s linear infinite;
        }

        @keyframes blinker {
            50% {
                opacity: 0;
            }
        }

        .status_icon_accpet {
            font-size: 18px;
            color: green;
        }

        .status_icon_reject {
            font-size: 18px;
            color: red;
        }
    </style>

    <style>
        .date_div {
            position: absolute;
            width: 100%;
            top: -1rem;
            right: 0px;
            background: #00517cf5;
            color: white;
            text-align: center;
            border-radius: 4px;
        }

        .regular_audit_color {
            background: #37bc9b !important;
        }

        .cluster_audit_color {
            background: #967ADC !important;
        }

        .annual_plan_color {
            background: #da4453 !important;
        }

        .imp_act_color {
            background: orange !important;
        }

        .holiday_event_color {
            background: red !important;
        }

        .event_icon_imp_act {
            margin-top: 6px;
        }

        .final_audit_event_color {
            background-color: #8bc34a !important;
        }

        .desk_event_color {
            background: #018ee0 !important;
        }
    </style>

    <style>
        .selectivity-caret {
            position: absolute;
            right: 5px;
            top: 8px;
        }

        .modal-footer-annual {
            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -moz-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: end;
            -webkit-justify-content: flex-end;
            -moz-box-pack: end;
            -ms-flex-pack: end;
            justify-content: space-between;
            padding: 1rem;
            border-top: 1px solid #626E82;
            border-bottom-right-radius: 0.35rem;
            border-bottom-left-radius: 0.35rem;
        }

        .search_dropdown_div .selectivity-input {
            border: 1px solid #BABFC7;
            width: 100%;
            border-radius: 0.25rem !important;
        }

        .search_dropdown_div .selectivity-single-select {
            border-radius: 0.25rem !important;
        }

        .search_dropdown_div .selectivity-load-more.highlight,
        .selectivity-result-item.highlight {
            background: #566269 !important;
        }

        .icon {
            border-radius: 50%;
            color: white;
            background-color: green;
        }

        .fc-center {
            padding-right: 0 !important;
        }

        /* .search_dropdown_div .selectivity-single-result-container {
            right:12.5em !important;} */
    </style>

    <style>
        .margin-top-20 {
            margin-top: -20px;
        }

        .nav-tabs {
            z-index: 50;
        }

        .calendar_view {
            position: relative;
            top: 9px;
        }

        .calendar_list_view_div .table-responsive {
            height: 300px !important;
            overflow-y: scroll;
        }

        .nav.nav-tabs.nav-justified {
            width: 100% !important;
        }

        .JColResizer>tbody>tr>td,
        .JColResizer>tbody>tr>th {
            overflow: hidden;
            padding-left: 5px !important;
            padding-right: 5px !important;
        }

        div.dt-button-background {
            background: none !important;
        }

        .filter_div_calendar {
            width: 100%;
            position: absolute;
            background-color: #f4f4f4;
            z-index: 100;
            box-shadow: 0px 4px 4px #c5c5c5;
            /* top: 7rem; */
            margin-left: 14px;
        }

        .fc-content {
            color: #fff !important;
        }

        .font12 {
            font-size: 12px
        }


        .calendar {
            /* min-width: 350px;
            max-width: 50%; */
            margin: 1em auto;
            padding: 1em;
            background: #eee;
            font-family: arial, helvetica, san-serif;
            box-shadow: 0 0 0.1em rgba(0, 0, 0, 0.5);
            border-radius: 0.2em;
        }

        /* .calendar:last-child {
            border-top: 6px solid #00517cf5;
        } */

        .calendar h1 {
            margin: 0 0 0.4em;
            font-weight: bold;
        }

        .calendar .event {
            color: #333;
            display: block;
            padding: 0.1em;
            transition: all 0.25s ease;
            margin-bottom: 0.5em;
        }

        .calendar .event:hover {
            background: #d5d5d5;
            text-decoration: none;
            color: black;
        }

        .calendar .event_icon {
            width: 3em;
            float: left;
            margin-right: 0.75em;
        }

        .calendar .event_month,
        .calendar .event_day {
            text-align: center;
        }

        .calendar .event_month {
            padding: 0.1em;
            margin-bottom: 0.15em;
            background: #00517cf5;
            font-size: 0.75em;
            color: white;
            border-top-left-radius: 0.3em;
            border-top-right-radius: 0.3em;
        }

        .calendar .event_day {
            border: 1px solid #999;
            background: white;
            color: black;
            font-size: 1.25em;
            font-weight: bold;
            border-bottom-left-radius: 0.1em;
            border-bottom-right-radius: 0.1em;
        }

        .calendar .event_title {
            font-size: 1.1em;
            height: 3em;
            display: table-cell;
        }

        .calendar .btn {
            margin-top: 0.5em;
            width: 100%;
            font-size: 1.5em;
        }
    </style>

    <!-- ADD Event Module CSS -->
    <style>
        .head_form_title {
            text-align: center;
            margin-bottom: 18px;
            margin-top: -13px;
        }

        .p_cls {

            margin-top: 5px;
            margin-bottom: 0;
        }
    </style>

    <!-- CALENDAR.PHP CSS -->
    <style>
        .grey {
            color: grey !important
        }

        #socialShare {
            /* width: 100%; */
            margin-top: -4px;
            text-align: center;
            /* margin-right: 18px; */
        }

        #socialShare a,
        #socialShare>.socialBox {
            position: relative;
            float: none;
            display: inline-block;
            color: #fff;
            font-size: 20px;
            padding: 5px;
            background-color: transparent;
            width: 35px;
            height: 35px;
            /* line-height: 40px; */
            text-align: center;
            border-radius: 50%;
        }

        #socialShare a {
            background-color: rgba(0, 0, 0, 0.2);
        }

        #socialShare>*>span {
            background-color: #00517cf5;
            box-shadow: 0 0 0 #00517cf5;
            display: block;
            color: #fff;
            font-size: 16px;
            padding: 8px;
            width: 35px;
            height: 35px;
            line-height: 18px;
            text-align: center;
            border-radius: 50%;
            -webkit-transform: scale(1);
            -moz-transform: scale(1);
            transform: scale(1);
            -webkit-transition: all .35s ease-in-out;
            -moz-transition: all .35s ease-in-out;
            transition: all .35s ease-in-out;

            /* margin-top: -4px; */
        }

        #socialGallery {
            /* left: -8%; */
            margin: 0 auto 0;
            position: absolute;
            top: 0;
            transform: translate(-50%, 0);
            visibility: hidden;
            width: 400px;
            left: -45px;
        }

        #socialGallery a {
            visibility: hidden;
            opacity: 0;
            margin: 5px 2px;
            background-color: #00517cf5;
            position: relative;
            top: 10px;
        }

        #socialGallery a>span {
            position: relative;
            top: 4px;
            left: 4px;
            border-radius: 50%;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
        }

        .socialToolBox {
            cursor: default;
        }

        .pointer {
            cursor: pointer
        }

        @media only screen and (max-width: 480px) {
            #socialGallery {
                width: 120px;
            }
        }

        /* legend styles */
        .red-color {
            background-color: #da4453;
            height: 14px;
            width: 14px;
            display: block;
            border-radius: 50%;
            /* margin-left: 7px; */
            margin-left: 7px;
        }

        .purpel-color {
            background-color: #967adc;
            height: 14px;
            width: 14px;
            display: block;
            border-radius: 50%;
            /* margin-left: 7px; */
            margin-left: 7px;
        }

        .holiday-color {
            background-color: red;
            height: 14px;
            width: 14px;
            display: block;
            border-radius: 50%;
            /* margin-left: 7px; */
            margin-left: 7px;
        }

        .fc-day-header {
            color: black !important;
        }

        .orange-color {
            background-color: orange;
            height: 14px;
            width: 14px;
            display: block;
            border-radius: 50%;
            /* margin-left: 7px; */
            margin-left: 7px;
        }

        .green-color {
            background-color: #37bc9b;
            height: 14px;
            width: 14px;
            display: block;
            border-radius: 50%;
            /* margin-left: 7px; */
            margin-left: 7px;
        }

        .row-arrange {
            padding-left: 3%;
            margin-top: -1%;
        }

        table {
            border-collapse: collapse;

        }

        table,
        th,
        td {
            border: 1px solid #b9b9b9;
        }

        th,
        td {
            padding: 5px;
            text-align: left;
        }
    </style>
    <!-- CALENDAR.PHP CSS END-->

    <!-- CALENDAR LIST VIEW CSS -->
    <style>
        .filter_div_1 {
            width: 102%;
            position: absolute;
            top: -12px;
            left: -15px;
            background-color: #f4f4f4;
            z-index: 100;
            box-shadow: 0px 4px 4px #c5c5c5;
        }
    </style>
    <!-- CALENDAR LIST VIEW CSS END-->

    <style type="text/css">
        .c1 {
            width: 200px;
        }

        .c2 {
            width: 400px;
        }

        .upload_file_2_modal .bg {
            background-color: #FFF !important;
            padding-bottom: 0px;
            padding-top: 10px;
            position: sticky;
            bottom: 0px;
            border: none !important;
            /* left: 100px; */

        }

        .upload_file_2_modal table {
            border: none !important;
            border-bottom: none !important;
        }

        .upload_file_2_modal table th,
        table td,
        .upload_file_2_modal tr,
        .upload_file_2_modal table td {
            border-bottom: none !important;
        }

        .thead-custom {
            background-color: #0a5881 !important;
            color: white;
        }

        .upload_file_2_modal td,
        .upload_file_2_modal tr {
            border: none !important;
        }

        thead th:first-child {
            left: 0;
            z-index: 1;
        }

        .table thead th {
            border-right: 1px dotted #c5d5ec;
            padding-left: 20px !important;
            padding-right: 5px !important;
        }

        thead th {
            position: -webkit-sticky;
            position: sticky;
            top: 0;
            background: #fff;
            color: #fff;
        }

        .legend_table,
        th,
        td {
            border: 1px solid #c5c5c5;
        }

        .JColResizer>tbody>tr>td,
        .JColResizer>tbody>tr>th {
            overflow: hidden;
            padding-left: 5px !important;
            padding-right: 5px !important;
        }

        tbody th {
            position: -webkit-sticky;
            position: sticky;
            left: 0;
            top: 0;
            background: #d5e1e7;
            border-right: 1px solid #CCC;
        }

        .icon {
            border-radius: 50%;
            color: white;
            background-color: green;
        }

        .ft-plus,
        .ft-minus {
            cursor: pointer;
        }

        .radio {
            margin: 0.5rem;
        }

        .radio_box-style input[type='radio']:checked+label {
            border: 1px solid #2e71d9;
            color: #2e71d9;
        }

        .hl {
            margin-left: 9px
        }

        #socialShare {
            /* width: 100%; */
            margin-top: -4px;
            text-align: center;
            margin-right: 12px;
        }

        .font-size-46 {
            font-size: 46px;
        }

        .upload_modal_label {
            margin-bottom: 10px
        }

        .radio_box-style label {
            width: 100px;
            float: left;
            border: 1px solid #eee;
            margin-right: 20px;
            padding: 24px 5px 17px;
            text-align: center;
            margin-top: 10px;
            cursor: pointer;
            border-radius: 2px;
            font-size: var(--fs13px);
            position: relative;
            overflow: hidden;
        }

        .radio_box-style label:before {
            content: "";
            width: 13px;
            height: 13px;
            border: 1px solid #ccc;
            float: left;
            position: absolute;
            top: 7px;
            left: 7px;
            border-radius: 50%;
        }

        .radio_box-style input[type='radio']:checked+label:after {
            background: #2e71d9;
        }

        .radio_box-style label:after {
            content: "";
            width: 7px;
            height: 7px;
            float: left;
            position: absolute;
            top: 10px;
            left: 10px;
            border-radius: 50%;
        }

        .dropzone,
        .dropzone * {
            box-sizing: border-box;
        }

        .dropzone {
            min-height: 95px;
            border: 2px dashed rgb(10, 88, 129);
            background: white;
            padding: 20px 20px;
        }

        .dropzone .dz-message {
            font-size: 20px;
            position: absolute;
            top: 60%;
            left: 0;
            width: 100%;
            height: 60px;
            margin-top: -30px;
            color: #0a5881;
            text-align: center;
        }

        .dropzone .dz-message {
            font-size: 20px;
            position: absolute;
            top: 60%;
            left: 0;
            width: 100%;
            height: 60px;
            margin-top: -30px;
            color: #0a5881;
            text-align: center;
        }

        .dropzone .dz-message:before {
            content: '\e94b';
            font-family: 'feather';
            font-size: 20px;
            position: absolute;
            top: 30px;
            width: 80px;
            height: 80px;
            display: inline-block;
            left: 50%;
            margin-left: -40px;
            line-height: 1;
            z-index: 2;
            color: #0a5881;
            text-indent: 0;
            font-weight: normal;
        }
    </style>