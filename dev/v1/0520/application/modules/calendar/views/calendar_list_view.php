    <?php $this->load->view('header'); ?>
    <?php $this->load->view('top'); ?>
    <?php $this->load->view('vertical_navbar'); ?>
    <?php $this->load->view('calendar_css') ?>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/standard-table.css">


    <div style="width:103%">
        <div class="col">
            <br>
            <div class="card-yu pt-2 p-1">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a class="nav-link " id="active-tab" href="<?= base_url() ?>calendar/Calendar_controller/calendar_view" aria-controls="active" aria-expanded="true">Calendar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" id="link-tab" href="<?= base_url() ?>calendar/Calendar_controller/calendar_event" aria-controls="link" aria-expanded="false">Event List</a>
                    </li>

                </ul>

                <style>
                    .filter_div_1 {
                        width: 102%;
                        position: absolute;
                        top: -12px;
                        left: -15px;
                        background-color: #f4f4f4;
                        z-index: 100;
                        box-shadow: 0px 4px 4px #c5c5c5;
                    }
                </style>


                <div class="">
                    <div class="" style="position: relative; top:-50px;">
                        <div class="filter_div_1" id="filter_div_calendar_list" style="display: none;">
                            <?php $this->load->view('calendar_filter_view'); ?>
                        </div>

                        <div class="mt-1 mb-1">
                            <div class="row d-flex align-items-center">
                                <div class="col-3 pr-0 d-flex ">
                                </div>

                                <div class="col-5">
                                </div>
                                <div class="col-3 d-flex justify-content-end" style="padding:0;margin-left:4rem">
                                    <button class="btn-yu primary mr-1" id="filter_button_1" style="width:35px;border-radius:50%;border:none">
                                        <i class="ft-filter text-white"></i>
                                    </button>

                                    <div class="">
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#"><i class="la la-list"></i></a>
                                            <a class="dropdown-item" href="#"> <i class="la la-th-large"></i></a>
                                        </div>
                                    </div>

                                    <button id="upload_button" class="btn-yu primary" style="width:35px" title="Upload File" data-toggle="modal" data-target="#upload_file">
                                        <i class="ft-upload text-white"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="calendar_list_view_div">
                            <table class="table display" id="calendar_list_grid" width="100%" border="0" cellpadding="0" cellspacing="0">
                                <thead>
                                    <th class="no-sort checkbox adjust_checkbox_column thead-custom" style="padding-left:6px !important;">
                                        <label>
                                            <input type="checkbox" value="" id="select_all">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <th class="thead-custom">#</th>
                                    <th class="thead-custom" style="min-width: 175px;">Audit Start Time</th>
                                    <th class="thead-custom" style="min-width: 175px;">Audit End Time</th>
                                    <th class="thead-custom">Vendor</th>
                                    <th class="thead-custom">Client</th>
                                    <th class="thead-custom">Account</th>
                                    <th class="thead-custom">Site</th>
                                    <th class="thead-custom">Auditor</th>
                                    <th class="thead-custom">Audit Type</th>
                                    <th class="thead-custom">Type</th>
                                    <th class="thead-custom" style="min-width: 45px;">Month</th>
                                    <th class="thead-custom">Audit</th>
                                    <th class="thead-custom">Site Audit Start Date</th>
                                    <th class="thead-custom">Site Audit End Date</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th class="checkbox adjust_checkbox_column">
                                            <label>
                                                <input type="checkbox" value="" name="check[]" class="checkbox">
                                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                            </label>
                                        </th>
                                        <td>1</td>
                                        <td><b>Mar 3, 2020</b> Monday 11.00 AM</td>
                                        <td><b>Mar 3, 2020</b> Monday 11.15 AM</td>
                                        <td>Chroma</td>
                                        <td>JLL</td>
                                        <td>HDFC</td>
                                        <td>Charkop</td>
                                        <td>Ami Sheth</td>
                                        <td>Online</td>
                                        <td>O</td>
                                        <td>Jan 2020</td>
                                        <td>Regular</td>
                                        <td><b>Mar 3, 2020</b> 11.00 AM</td>
                                        <td><b>Mar 3, 2020</b> 11.15 AM</td>
                                    </tr>
                                    <tr>
                                        <th class="checkbox adjust_checkbox_column">
                                            <label>
                                                <input type="checkbox" value="" name="check[]" class="checkbox">
                                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                            </label>
                                        </th>
                                        <td>2</td>
                                        <td><b>Mar 3, 2020</b> Monday 11.00 AM</td>
                                        <td><b>Mar 3, 2020</b> Monday 11.15 AM</td>
                                        <td>Bosch Ltd</td>
                                        <td>HDFC</td>
                                        <td>HDFC</td>
                                        <td>Charkop</td>
                                        <td>Ami Sheth</td>
                                        <td>Physical</td>
                                        <td>R</td>
                                        <td>Dec 2019</td>
                                        <td>Final</td>
                                        <td><b>Mar 3, 2020</b> 11.00 AM</td>
                                        <td><b>Mar 3, 2020</b> 11.15 AM</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                        <table class="legend_table mr-3" style="width: 200px;">
                            <thead class="heading">
                                <th class="thead-custom">Sign</th>
                                <th class="discription thead-custom">Description</th>
                            </thead>
                            <tbody class="tbody">
                                <tr>
                                    <td class="sign">O</td>
                                    <td>Original</td>
                                </tr>
                                <tr>
                                    <td class="sign">R</td>
                                    <td>Remediation</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('calendar_js') ?>


    <!-- form modal -->
    <div class="modal fade text-left datamodal" id="add_calendar_event_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel16">Add Event</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <?php $this->load->view('add_calendar_event') ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-yu btn-width primary text-white" data-dismiss="modal">Reset</button>
                    <button type="button" class="btn-yu btn-width primary text-white" data-dismiss="modal">Submit Form</button>
                </div>
            </div>
        </div>
    </div>

    <!-- modal for upload file-->
    <div id="upload_file" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class>Import Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="radio">
                        <div class="row d-flex  justify-content-between">
                            <label class="mr-1 hl" for="id4">Select File Type</label>
                            <button class="btn-yu primary mr-2" data-toggle="tooltip" title="Download Template" style="width:35px"><i class="ft-download"></i></button>
                        </div>
                        <div class="row justify-content-center">
                            <div class="radio_box-style fl">
                                <input type="radio" name="filetype" checked="checked" value="mpp" id="label_mpp" class="p0 fl mt2" onchange="mppselectdeselect(true)">
                                <label class="fl pl20" for="label_mpp">
                                    <span class="zoho-mpp-import block fs30px mB15"></span>
                                    <i class='ft-file' style='font-size:48px'></i><br>
                                    <span>XLS/XLSX</span>
                                </label>
                            </div>
                            <div class="radio_box-style fl">
                                <input type="radio" name="filetype" value="mpp" id="label_mpp1" class="p0 fl mt2" onchange="mppselectdeselect(true)">
                                <label class="fl pl20" for="label_mpp1">
                                    <span class="zoho-mpp-import block fs30px mB15"></span>
                                    <i class='ft-file' style='font-size:48px'></i><br>
                                    <span>CSV</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <fieldset class="form-group">
                            <form action="#" class="dropzone dropzone-area dropht" id="newvmdemailform">

                            </form>
                        </fieldset>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-toggle="modal" data-target="#uploadfile2" class="btn-width btn-yu primary text-white" id="importfile">Import</button>
                    <button type="button" data-dismiss="modal" class="btn-width btn-yu primary text-white">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <!-- modal for upload file 2-->
    <div id="uploadfile2" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Map Fields</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <!-- <h6>Default Fields</h6> -->

                </div>
                <div class="modal-body">
                    <table class=" table p ">
                        <tr>
                            <th class="c1 bg">LIST OF FIELDS IN TABLE</th>
                            <th class="c2 bg">COLUMNS IN FILE</th>
                            <th class="c2 bg"></th>
                        </tr>
                        <tr>
                            <td class="tc">Table Header1</td>
                            <td>
                                <select class="form-control sb">
                                    <option value="10" selected> Header 1</option>
                                    <option value="25"> Header 2</option>
                                    <option value="50">Header 3</option>
                                    <option value="100"> Header 4</option>
                                </select>
                            </td>

                        </tr>
                        <tr>
                            <td>Table Date Header2</td>
                            <td>
                                <select class="form-control sb">
                                    <option value="10"> Header 1</option>
                                    <option value="25" selected> Header 2</option>
                                    <option value="50"> Header 3</option>
                                    <option value="100"> Header 4</option>
                                </select>
                            </td>
                            <td>
                                <select class="form-control sb">
                                    <option value="10">dd.MM.yyyy</option>
                                    <option value="25">dd/MM/yyyy</option>
                                    <option value="50">MM.dd.yyyy</option>
                                    <option value="100">yyyy-MM-dd</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Table Date Header3</td>
                            <td>
                                <select class="form-control sb">
                                    <option value="10"> Header 1</option>
                                    <option value="25">Header 2</option>
                                    <option value="50" selected>Header 3</option>
                                    <option value="100">Header 4</option>
                                </select>
                            </td>
                            <td>
                                <select class="form-control sb">
                                    <option value="10">dd.MM.yyyy</option>
                                    <option value="25">dd/MM/yyyy</option>
                                    <option value="50">MM.dd.yyyy</option>
                                    <option value="100">yyyy-MM-dd</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Table Header4</td>
                            <td>
                                <select class="form-control sb">
                                    <option value="10"> Header 1</option>
                                    <option value="25"> Header 2</option>
                                    <option value="50"> Header 3</option>
                                    <option value="100" selected>Header 4</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Table Header5</td>
                            <td>
                                <select class="form-control sb">
                                    <option value="10" selected> Header 1</option>
                                    <option value="25"> Header 2</option>
                                    <option value="50"> Header 3</option>
                                    <option value="100">Header 4</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Table Header6</td>
                            <td>
                                <select class="form-control sb">
                                    <option value="10"> Header 1</option>
                                    <option value="25" selected> Header 2</option>
                                    <option value="50">Header 3</option>
                                    <option value="100">Header 4</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <div class="bg">
                        <button type="button" data-dismiss="modal" class="btn-width btn-yu primary text-white" id="continuebtn">Continue</button>
                        <button type="button" data-dismiss="modal" class="btn-width btn-yu primary text-white" id="cancelbtn">Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>