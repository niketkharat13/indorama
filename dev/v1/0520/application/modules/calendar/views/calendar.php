    <?php $this->load->view('header'); ?>
    <?php $this->load->view('top'); ?>
    <?php $this->load->view('vertical_navbar'); ?>
    <?php $this->load->view('calendar_css') ?>

    <div style="width:103%">
        <div class="col">
            <br>
            <div class="card-yu pt-2 p-1">
                <!-- Calendar Legends -->
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 title_legend_div">
                        <div class="row align-items-baseline">
                            <div class="col-lg-2 col-md-3 col-sm-12">
                                <h4 class="ml-2">Calendar</h4>
                            </div>
                            <div class="col-lg-10 col-md-12 col-sm-12 container text-right legend_div">
                                <a class="btn icon-btn audit_badge normal_pointer">
                                    <span class="legend_icon legend_account_color_1"></span>&nbsp;&nbsp;Schedule 1
                                </a>
                                <a class="btn icon-btn audit_badge normal_pointer">
                                    <span class="legend_icon legend_account_color_2"></span>&nbsp;&nbsp;Schedule 2
                                </a>
                                <a class="btn icon-btn audit_badge normal_pointer">
                                    <span class="legend_icon legend_account_color_7"></span>&nbsp;&nbsp;Schedule 3
                                </a>
                                <a class="btn icon-btn audit_badge normal_pointer">
                                    <span class="legend_icon legend_account_color_3"></span>&nbsp;&nbsp;Schedule 4
                                </a>
                                <a class="btn icon-btn audit_badge normal_pointer">
                                    <span class="legend_icon legend_account_color_6"></span>&nbsp;&nbsp;Schedule 5
                                </a>
                                <a class="btn icon-btn audit_badge normal_pointer">
                                    <span class="legend_icon legend_account_color_4"></span>&nbsp;&nbsp;Schedule 6
                                </a>
                                <a class="btn icon-btn audit_badge normal_pointer">
                                    <span class="legend_icon legend_account_color_5"></span>&nbsp;&nbsp;Schedule 7
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Calendar Legends -->
                <div class="calendar_view">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">

                            <!-- Share and Listt View Button -->
                            <!-- <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="d-flex justify-content-end">
                                        <button class="btn-yu primary ml-1" data-toggle="tooltip" title="Scheduled list view" style="width:35px;height:35px;border-radius:50%" onclick="annual_list_view()"><i class="la la-list-alt"></i></button>
                                    </div>
                                </div>
                            </div> -->
                            <!-- END Share and List View BButton -->

                            <div class="row mt-1">
                                <!-- Calendar view  -->
                                <div class="col-lg-8 col-md-12 col-sm-12">
                                    <div id='fc-event-colors-2'></div>
                                </div>
                                <!-- END Calendar View -->

                                <!-- Calendar Side list-->
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="side_list_view">
                                        <!-- Actionable List View -->
                                        <!-- <div class="calendar" style="height:175px; position:relative;">
                                            <div class="date_div">
                                                <label style="margin-top:5px;margin-bottom:5px;">Today's Date : <span id="t_date"></span></label>
                                            </div>

                                            <h4 class="font600 mt-1">Actionables</h4>

                                            <span style="float: right; font-weight: 700; cursor: pointer;height:   47px;display:flex;align-items:center">
                                                <i class="la la-hand-paper-o warning blink_component" style="font-size: 21px;" data-toggle="tooltip" title="Schedule On Hold"></i>

                                            </span>
                                            <div class="event clearfix regular_event">
                                                <div class="event_icon">
                                                    <div class="event_month regular_audit_color">MAY</div>
                                                    <div class="event_day">2</div>
                                                </div>
                                                <div class="event_title">
                                                    <span>
                                                        <b>Full Site <i data-toggle="tooltip" title="Online" class="la la-globe success"></i></b><br>
                                                        <span class="d-flex align-items-center font12"><i class="ft-clock" style="margin-right:5px"></i>From: 11 AM to 2:00 PM</span>
                                                    </span>
                                                </div>
                                            </div>

                                            <span style="float: right; font-weight: 700; cursor: pointer;height:   47px;display:flex;align-items:center">
                                                <i class="ft-alert-circle danger blink_component" data-toggle="tooltip" title="Decision Pending" onclick="audit_manager_edit_modal()" style="font-size: 21px;"></i>

                                            </span>
                                            <div class="event clearfix cluster_event">
                                                <div class="event_icon">
                                                    <div class="event_month cluster_audit_color">MAY</div>
                                                    <div class="event_day">6</div>
                                                </div>
                                                <div class="event_title">
                                                    <span><b>Schedule 4</b><br>
                                                        <span class="d-flex align-items-center font12"><i class="ft-clock" style="margin-right:5px"></i>From: 11 AM to 2:00 PM</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div> -->
                                        <!-- END Actionable List View -->

                                        <!-- Upcoming Events List View  -->
                                        <div class="calendar" style="margin-top:63px;height:205px; position:relative;">
                                            <div class="date_div">
                                                <label style="margin-top:5px;margin-bottom:5px;">Upcoming Event (This Week)</label>
                                            </div>
                                            <div style="height:160px;overflow-y:scroll" class="mt-2">
                                                <!-- =================EVENT 1============================= -->
                                                    <span style="float: right; font-weight: 700; cursor: pointer;height:   47px;display:flex;align-items:center">
                                                        <i class="ft-info info" style="font-size: 21px;" data-toggle="popover" data-trigger="hover" title="Full Site  " data-content="All day long meeting"></i>
                                                    </span>
                                                    <a class="event clearfix regular_event">
                                                        <div class="event_icon">
                                                            <div class="event_month regular_audit_color">MAY</div>
                                                            <div class="event_day">2</div>
                                                        </div>
                                                        <div class="event_title">
                                                            <span>
                                                                <b>Schedule 2</b><br>
                                                                <span class="d-flex align-items-center font12"><i class="ft-clock" style="margin-right:5px"></i>From: 11 AM to 2:00 PM</span>
                                                            </span>
                                                        </div>
                                                    </a>
                                                <!-- =================END EVENT 1============================= -->

                                                <!-- =================EVENT 2============================= -->
                                                    <span style="float: right; font-weight: 700; cursor: pointer;height:   47px;display:flex;align-items:center">
                                                        <i class="ft-info info" style="font-size: 21px;" data-toggle="popover" data-trigger="hover" title="Schedule 4" data-content="All day long meeting with CEO"></i>
                                                    </span>
                                                    <a class="event clearfix cluster_event">
                                                        <div class="event_icon">
                                                            <div class="event_month cluster_audit_color">MAY</div>
                                                            <div class="event_day">6</div>
                                                        </div>
                                                        <div class="event_title">
                                                            <span><b>Schedule 4</b><br>
                                                                <span class="d-flex align-items-center font12"><i class="ft-clock" style="margin-right:5px"></i>From: 11 AM to 2:00 PM</span>
                                                            </span>
                                                        </div>
                                                    </a>
                                                <!-- =================END EVENT 2============================= -->

                                                <!-- =================EVENT 3============================= -->
                                                    <span style="float: right; font-weight: 700; cursor: pointer;height:   47px;display:flex;align-items:center">
                                                        <i class="ft-info info" style="font-size: 21px;" data-toggle="popover" data-trigger="hover" title="Schedule 5" data-content="All Day-Schedule 5"></i>
                                                    </span>
                                                    <a class="event clearfix final_audit_event">
                                                        <div class="event_icon ">
                                                            <div class="event_month final_audit_event_color">MAY</div>
                                                            <div class="event_day">7</div>
                                                        </div>
                                                        <div class="event_title">
                                                            <span><b>Schedule 5</b><br>
                                                                <span class="d-flex align-items-center font12"><i class="ft-clock" style="margin-right:5px"></i>All Day</span>
                                                            </span>
                                                        </div>
                                                    </a>
                                                <!-- =================END EVENT 3============================= -->

                                                <!-- =================EVENT 4============================= -->
                                                    <span style="float: right; font-weight: 700; cursor: pointer;height:   47px;display:flex;align-items:center">
                                                        <i class="ft-info info" style="font-size: 21px;" data-toggle="popover" data-trigger="hover" title="Schedule 3" data-content="Schedule 3"></i>
                                                    </span>
                                                    <a class="event clearfix desk_audit_event">
                                                        <div class="event_icon ">
                                                            <div class="event_month desk_event_color">MAY</div>
                                                            <div class="event_day">15</div>
                                                        </div>
                                                        <div class="event_title">
                                                            <span><b>Schedule 3</b><br>
                                                                <span class="d-flex align-items-center font12"><i class="ft-clock" style="margin-right:5px"></i>From: 3:00 AM to 4:00 PM</span>
                                                            </span>
                                                        </div>
                                                    </a>
                                                <!-- =================END EVENT 4============================= -->

                                                <!-- =================EVENT 5============================= -->
                                                    <span style="float: right; font-weight: 700; cursor: pointer;height:   47px;display:flex;align-items:center">
                                                        <i class="ft-info info" style="font-size: 21px;" data-toggle="popover" data-trigger="hover" title="Schedule 1" data-content="All day long meeting with CEO"></i>
                                                    </span>
                                                    <a class="event clearfix annual_event">
                                                        <div class="event_icon">
                                                            <div class="event_month annual_plan_color">MAY</div>
                                                            <div class="event_day">18</div>
                                                        </div>
                                                        <div class="event_title">
                                                            <span><b>Schedule 1</b><br>
                                                                <span class="d-flex align-items-center font12"><i class="ft-clock" style="margin-right:5px"></i>To be Scheduled</span>
                                                            </span>
                                                        </div>
                                                    </a>
                                                <!-- =================END EVENT 5============================= -->

                                                <!-- =================EVENT 6============================= -->
                                                    <span style="float: right; font-weight: 700; cursor: pointer;height:   47px;display:flex;align-items:center">
                                                        <i class="ft-info info" style="font-size: 21px;" data-toggle="popover" data-trigger="hover" title="Schedule 7" data-content="All day long meeting with CEO"></i>
                                                    </span>
                                                    <a class="event clearfix imp_act_event">
                                                        <div class="event_icon ">
                                                            <div class="event_month imp_act_color">MAY</div>
                                                            <div class="event_day">20</div>
                                                        </div>
                                                        <div class="event_title">
                                                            <span><b>Schedule 7</b><br>
                                                                <span class="d-flex align-items-center font12"><i class="ft-clock" style="margin-right:5px"></i>From: 11 AM to 2:00 PM</span>
                                                            </span>
                                                        </div>
                                                    </a>
                                                <!-- =================END EVENT 6============================= -->

                                                <!-- =================EVENT 7============================= -->
                                                    <span style="float: right; font-weight: 700; cursor: pointer;height:   47px;display:flex;align-items:center">
                                                       
                                                    </span>
                                                    <a class="event clearfix annual_event">
                                                        <div class="event_icon">
                                                            <div class="event_month annual_plan_color">MAY</div>
                                                            <div class="event_day">21</div>
                                                        </div>
                                                        <div class="event_title">
                                                            <span><b>Schedule 1</b><br>
                                                                <span class="d-flex align-items-center font12"><i class="ft-clock" style="margin-right:5px"></i>To be Scheduled</span>
                                                            </span>
                                                        </div>
                                                    </a>
                                                <!-- =================END EVENT 7============================= -->

                                                <!-- =================EVENT 8============================= -->
                                                    <span style="float: right; font-weight: 700; cursor: pointer;height:   47px;display:flex;align-items:center">
                                                        <i class="ft-info info" style="font-size: 21px;" data-toggle="popover" data-trigger="hover" title="Schedule 1 " data-content="All day long meeting with CEO"></i>
                                                    </span>
                                                    <a class="event clearfix annual_event">
                                                        <div class="event_icon">
                                                            <div class="event_month annual_plan_color">MAY</div>
                                                            <div class="event_day">22</div>
                                                        </div>
                                                        <div class="event_title">
                                                            <span><b>Schedule 1</b><br>
                                                                <span class="d-flex align-items-center font12"><i class="ft-clock" style="margin-right:5px"></i>To be Scheduled</span>
                                                            </span>
                                                        </div>
                                                    </a>
                                                <!-- =================END EVENT 8============================= -->

                                                <!-- =================EVENT 9============================= -->
                                                    <span style="float: right; font-weight: 700; cursor: pointer;height:   47px;display:flex;align-items:center">
                                                        <i class="ft-info info" style="font-size: 21px;" data-toggle="popover" data-toggle="popover" data-trigger="hover" title="Schedule 1" data-content="All day long meeting"></i>
                                                    </span>
                                                    <a class="event clearfix annual_event">
                                                        <div class="event_icon">
                                                            <div class="event_month annual_plan_color">MAY</div>
                                                            <div class="event_day">25</div>
                                                        </div>
                                                        <div class="event_title">
                                                            <span><b>Schedule 1</b><br>
                                                                <span class="d-flex align-items-center font12"><i class="ft-clock" style="margin-right:5px"></i>To be Scheduled</span>
                                                            </span>
                                                        </div>
                                                    </a>
                                                <!-- =================END EVENT 9============================= -->

                                                <!-- =================EVENT 10============================= -->
                                                    <span style="float: right; font-weight: 700; cursor: pointer;height:   47px;display:flex;align-items:center">
                                                        <i class="ft-info info" style="font-size: 21px;" data-toggle="popover" data-trigger="hover" title="Schedule 6" data-content="All day long meeting"></i>
                                                    </span>
                                                    <a class="event clearfix holiday_event">
                                                        <div class="event_icon ">
                                                            <div class="event_month holiday_event_color">MAY</div>
                                                            <div class="event_day">30</div>
                                                        </div>
                                                        <div class="event_title">
                                                            <span><b>Schedule 6</b><br>
                                                                <span class="d-flex align-items-center font12"><i class="ft-clock" style="margin-right:5px"></i>From: 11 AM to 2:00 PM</span>
                                                            </span>
                                                        </div>
                                                    </a>
                                                <!-- =================END EVENT 10============================= -->
                                            </div>
                                        </div>
                                        <!--END Upcoming Events List View  -->

                                        <!-- Calendar Icon Legend -->
                                        <!-- <div class="col-lg-12 col-md-12 col-sm-12 container text-left legend_div" style="padding-left: 0px;">
                                            <a class="btn icon-btn audit_badge normal_pointer" style="font-size: 12px;">
                                                <i data-toggle="tooltip" title="Online" class="la la-globe success"></i>&nbsp;Online Audit
                                            </a>
                                            <a class="btn icon-btn audit_badge normal_pointer" style="font-size: 12px;">
                                                <i data-toggle="tooltip" title="Physical" class="la la-user danger"></i>&nbsp;Physical Audit
                                            </a>
                                            <a class="btn icon-btn audit_badge normal_pointer" style="font-size: 12px;">
                                                <i data-toggle="tooltip" title="On Hold" class="la la-hand-paper-o warning"></i>&nbsp;On Hold
                                            </a>
                                            <a class="btn icon-btn audit_badge normal_pointer" style="font-size: 12px;">
                                                <i data-toggle="tooltip" title="Decision Pending" class="ft-alert-circle danger"></i>&nbsp;Decision Pending
                                            </a>
                                        </div> -->
                                        <!-- END Calendar Icon Legend -->
                                    </div>
                                </div>
                                <!--END Calendar Side list-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 1. Add Event Modal----- -->
    <!-- <div class="modal fade text-left datamodal" id="add_calendar_event_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel16">Create Audit Schedule</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-8 col-md-8  col-sm-12 mt-1 add_event_audit_selection">
                        <div class="row">
                            <div class="col-md-12  col-sm-12  ">
                                <div class="row">
                                    <div class="col-4 d-flex align-items-center">
                                        <h6 class="" for="select_event_dropdown">Audit Schedule Type</h6>
                                    </div>
                                    <div class="col-8 search_dropdown_div">
                                        <select class="selectivity_dropdown" id="select_event_dropdown">
                                            <option selected value="annual_audit_selected">Schedule 1</option>
                                            <option value="regular_audit_selected">Schedule 2</option>
                                            <option value="desk_audit_selected">Schedule 3</option>
                                            <option value="cluster_audit_selected">Schedule 4</option>
                                            <option value="holiday_audit_selected">Schedule 6</option>
                                            <option value="act_audit_selected">Schedule 7</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-yu btn-width primary text-white" onclick="calendar_reset_confirm()">Reset</button>
                    <input type="Submit" class="btn-yu btn-width primary text-white btn-submit-form" data-dismiss="modal" value="Submit Form" id="calendar_submit_button_event">
                </div>
            </div>
        </div>
    </div> -->
    <!-- 1. END Add Event Modal----- -->

    <!-- 2. Regular Event Modal----- -->
    <div id="regular_edit_modal" class="modal fade ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="regular_event_modalTitle" class="modal-title"><span>JLL</span>&nbsp;>&nbsp;&nbsp;<span>HDFC Bank</span>&nbsp;>&nbsp;&nbsp;<span>Charkop 2</span>&nbsp;>&nbsp;&nbsp;<span>Feb 2020</span>&nbsp;>&nbsp;&nbsp;<span>Original</span></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div  class="modal-body p-1">
                    <div class="row " id="regular_edit_form">

                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="mb-0" for="event_start_time_edit_regular">Date & Time</label>
                                </div>
                                <div class="col-9 ">
                                    <div class="d-flex date_time_div_regular">
                                        <input type="text" class="form-control " value="2 May,2020" readonly>
                                        <input type="text" class="form-control " style="width: 100px;" value="10:00 am" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">Auditor</div>
                                <div class="col-9 search_dropdown_div">
                                    <input type="text" class="form-control" value="Soham P" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mt-1">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="thead-custom">Vendor</th>
                                        <th class="thead-custom">Audit Start Time</th>
                                        <th class="thead-custom">Duration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Hewitt 1</td>
                                        <td><span class="vendor1_regular_time">10:00 am</span></td>
                                        <td>1 hr</td>
                                    </tr>
                                    <tr>
                                        <td>Croma 2</td>
                                        <td><span class="vendor2_regular_time">11:00 am</span></td>
                                        <td>1 hr</td>
                                    </tr>
                                    <tr>
                                        <td>Hotel Hyatt 3</td>
                                        <td><span class="vendor3_regular_time">12:00 pm</span></td>
                                        <td>1 hr</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row " id="reschedule_client_modal" style="display: none;">
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="mb-0" for="event_start_time_edit_regular">Date & Time</label>
                                </div>
                                <div class="col-9 ">
                                    <div class="d-flex date_time_div_regular">
                                        <input type="text" class="form-control date_time_background" id="client_date" value="2 May,2020" style="color: #98A4B8;">
                                        <input type="text" class="form-control date_time_background" id="client_time" placeholder="10:00 am" style="width: 100px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center margin-top-20">Auditor</div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="selectivity_dropdown mb-0" id="auditor_edit_modal_regular">
                                        <option value="" selected disabled>Select Auditor</option>
                                        <option value="auditor_1">Niket K</option>
                                        <option value="auditor_2">Soham P</option>
                                        <option value="auditor_3">Aniket Y</option>
                                    </select>
                                    <small>Only editable by Audit Manager</small>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 mt-1">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="thead-custom">#</th>
                                        <th class="thead-custom">Time Slot</th>
                                        <th class="thead-custom">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <div class="d-flex date_time_div_regular justify-content-between">
                                                <div class="d-flex">
                                                    <input type="text" class="form-control" id="client_date_slot_1" placeholder="Select Date" style="color: #98A4B8; width:200px;">
                                                    <input type="text" class="form-control" id="client_time_slot_1" placeholder="Select Time" style="width: 100px;">
                                                </div>
                                                <div>
                                                    <textarea placeholder="Comment" class="form-control"></textarea>
                                                </div>
                                            </div>


                                        </td>
                                        <td>
                                            <span class="ft-plus icon next_slot"></span>
                                        </td>
                                    </tr>
                                    <tr class="next_time_slot" style="display: none;">
                                        <td>2</td>
                                        <td>
                                            <div class="d-flex date_time_div_regular justify-content-between">
                                                <div class="d-flex">
                                                    <input type="text" class="form-control" id="client_date_slot_2" placeholder="Select Date" style="color: #98A4B8; width:200px;">
                                                    <input type="text" class="form-control" id="client_time_slot_2" placeholder="Select Time" style="width: 100px;">
                                                </div>
                                                <div>
                                                    <textarea placeholder="Comment" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </td>

                                    </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>

                    <div class="col-9" style="display: none;">
                        <input type="remark" id="hidden_text" placeholder="" name="" value="" class="form-control col reset-cls">
                    </div>

                </div>
                <div class="modal-footer-annual">
                    <div>
                        <small>*if any 1 slot accepted other rejected*</small>
                    </div>

                    <div>
                        <button type="button" class="btn-yu primary" data-dismiss="modal" style="width:80px">Close</button>
                        <button class="btn-yu primary" id="reschedule_client_btn" style="width:150px">Reschedule Audit</button>

                        <button class="btn-yu primary" data-dismiss="modal" id="update_event_btn_submit_regular" style="width:80px; display:none;">Submit</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--2. END Regular Event Modal----- -->

    <!--3. Cluster Event Modal----- -->
    <div id="cluster_edit_modal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="cluster_event_modalTitle" class="modal-title"><span>HDFC Bank</span>&nbsp;>&nbsp;&nbsp;<span>HDFC Bank</span>&nbsp;>&nbsp;&nbsp;<span>Cluster 2</span>&nbsp;>&nbsp;&nbsp;<span>Vendor 1</span>&nbsp;>&nbsp;&nbsp;<span>Original</span></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div  class="modal-body p-1">
                    <div class="row cluster_audit_modal_edit">
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="mb-0" for="event_start_time_edit_cluster">Date & Time</label>
                                </div>
                                <div class="col-9 ">
                                    <div class="d-flex date_time_div_regular">
                                        <input type="text" class="form-control" readonly style="color: #98A4B8;" value="6 May,2020">
                                        <input type="text" class="form-control" readonly value="10:00 am" style="width: 100px;">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6  col-sm-12 mt-1 ">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center margin-top-20">Auditor</div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="form-control mb-0" id="auditor_edit_modal_cluster" disabled>
                                        <option value="" selected disabled>Select Auditor</option>
                                        <option value="auditor_1">Niket K</option>
                                        <option value="auditor_2" selected="">Soham P</option>
                                        <option value="auditor_3">Aniket Y</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 mt-1 ">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center margin-top-20">Location</div>
                                <div class="col-9 search_dropdown_div">
                                    <input type="text" name="" class="form-control" id="location_edit_modal_cluster" value="Charkop" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 mt-1 ">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="thead-custom">Site</th>
                                        <th class="thead-custom">Audit Start Time</th>
                                        <th class="thead-custom">Duration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Charkop 2</td>
                                        <td><span class="vendor1_regular_time">10:00 am</span></td>
                                        <td>1 hr</td>
                                    </tr>
                                    <tr>
                                        <td>Sion</td>
                                        <td><span class="vendor2_regular_time">11:00 am</span></td>
                                        <td>1 hr</td>
                                    </tr>
                                    <tr>
                                        <td>Virar</td>
                                        <td><span class="vendor3_regular_time">12:00 pm</span></td>
                                        <td>1 hr</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row " id="cluster_audit_manager_modal" style="display: none;">
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="mb-0" for="event_start_time_edit_regular">Date & Time</label>
                                </div>
                                <div class="col-9 ">
                                    <div class="d-flex date_time_div_regular">
                                        <input type="text" class="form-control date_time_background" placeholder="Select Date" style="color: #98A4B8;" value="4 May,2020">
                                        <input type="text" class="form-control date_time_background" placeholder="Select Time" style="width: 100px;" value="10:00 am">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center" style="margin-top:-20px;">Auditor</div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="selectivity_dropdown mb-0" id="cluster_audit_manager_dropdown">
                                        <option value="" selected disabled>Select Auditor</option>
                                        <option value="auditor_1">Niket K</option>
                                        <option value="auditor_2">Soham P</option>
                                        <option value="auditor_3">Aniket Y</option>
                                    </select>
                                    <small>Only editable by Audit Manager</small>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center" style="margin-top:-20px;">Location</div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="selectivity_dropdown mb-0" id="cluster_reschedule_location">
                                        <option value="" selected disabled>Select Auditor</option>
                                        <option value="location_1">Charkop</option>
                                        <option value="location_2">Andheri</option>
                                        <option value="location_3">Virar</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 mt-1">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="thead-custom">#</th>
                                        <th class="thead-custom">Time Slot</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="">
                                        <td>
                                            <span>1</span>
                                        </td>
                                        <td>
                                            <div class="d-flex  justify-content-between">
                                                <div class="d-flex">
                                                    <input type="text" class="form-control" id="audit_manager_date" placeholder="Select Date" style="color: #98A4B8; width:200px;">
                                                    <input type="text" class="form-control" id="audit_manager_time" placeholder="Select Time" style="width: 100px;">
                                                </div>
                                                <div>
                                                    <textarea class="form-control" style="height: 40px;">Comment</textarea>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>

                    </div>

                    <div class="col-9" style="display: none;">
                        <input type="remark" id="hidden_text" placeholder="" name="" value="" class="form-control col reset-cls">
                    </div>
                </div>
                <div class="modal-footer-annual">
                    <div>
                        <small>Note: To schedule Schedule 4 select time slot</small>
                        <br>
                        <!-- <small>*if any 1 slot accepted other rejected*</small> -->

                    </div>
                    <div>
                        <button type="button" class="btn-yu primary" data-dismiss="modal" style="width:80px">Close</button>
                        <button class="btn-yu primary" id="reschedule_audit_manager_btn" style="width:150px">Reschedule Audit</button>
                        <button class="btn-yu primary" data-dismiss="modal" id="update_event_btn_submit_cluster" style="width:80px; display:none;">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--3. END Cluster Event Modal----- -->

    <!--4. Desk Event Modal----- -->
    <div id="desk_edit_modal" class="modal fade ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="desk_event_modalTitle" class="modal-title"><span>JLL</span>&nbsp;>&nbsp;&nbsp;<span>HDFC Bank</span>&nbsp;>&nbsp;&nbsp;<span>Charkop 2</span>&nbsp;>&nbsp;&nbsp;<span>Feb 2020</span>&nbsp;>&nbsp;&nbsp;<span>Original</span></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div  class="modal-body p-1">
                    <div class="row " id="desk_edit_form">

                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="mb-0" for="event_start_time_edit_regular">Date & Time</label>
                                </div>
                                <div class="col-9 ">
                                    <div class="d-flex date_time_div_regular">
                                        <input type="text" class="form-control" readonly style="color: #98A4B8;" value="15 May,2020">
                                        <input type="text" class="form-control " readonly style="width: 100px;" value="10:00 am">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center margin-top-20">Auditor</div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="form-control mb-0" id="auditor_edit_modal_regular" disabled>
                                        <option value="" disabled>Select Auditor</option>
                                        <option value="auditor_1" selected>Niket K</option>
                                        <option value="auditor_2">Soham P</option>
                                        <option value="auditor_3">Aniket Y</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mt-1">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="thead-custom">Vendor</th>
                                        <th class="thead-custom">Audit Start Time</th>
                                        <th class="thead-custom">Duration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Hewitt 1</td>
                                        <td><span class="vendor1_regular_time">10:00 am</span></td>
                                        <td>1 hr</td>
                                    </tr>
                                    <tr>
                                        <td>Croma 2</td>
                                        <td><span class="vendor2_regular_time">11:00 am</span></td>
                                        <td>1 hr</td>
                                    </tr>
                                    <tr>
                                        <td>Hotel Hyatt 3</td>
                                        <td><span class="vendor3_regular_time">12:00 pm</span></td>
                                        <td>1 hr</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-9" style="display: none;">
                        <input type="remark" id="hidden_text" placeholder="" name="" value="" class="form-control col reset-cls">
                    </div>

                </div>
                <div class="modal-footer-annual">
                    <div>
                        <small>*Rescheduling flow is similar to Schedule 4*</small>
                    </div>

                    <div>
                        <button type="button" class="btn-yu primary" data-dismiss="modal" style="width:80px">Close</button>
                        <button class="btn-yu primary" id="reschedule_client_btn_desk" data-dismiss="modal" style="width:150px">Reschedule Audit</button>
                        <button class="btn-yu primary" data-dismiss="modal" id="update_event_btn_submit_regular" style="width:80px; display:none;">Submit</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--4.END Desk Event Modal----- -->

    <!--5. Planned Physical Full Site Event Modal----- -->
    <div id="annual_edit_physical_modal" class="modal fade ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="planned_physical_fullsite_modalTitle" class="modal-title"><span>JLL</span>&nbsp;>&nbsp;&nbsp;<span>HDFC Bank</span>&nbsp;>&nbsp;&nbsp;<span>Charkop 2</span>&nbsp;>&nbsp;&nbsp;<span>Jan 2020</span>&nbsp;(Physical Audit - Full Site)</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div  class="modal-body p-1">
                    <div class="row prelimenary_audit_form">

                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="mb-0" for="event_start_date_edit_annual_physical">Date & Time</label>
                                </div>
                                <div class="col-9">
                                    <div class="d-flex">
                                        <input type="text" class="form-control date_time_background" id="event_start_date_edit_annual_physical" placeholder="Select Date" style="color: #98A4B8;" readonly>
                                        <input type="text" class="form-control date_time_background" id="event_start_time_edit_annual_physical" placeholder="Select Time" style="width: 100px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center margin-top-20">Auditor</div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="selectivity_dropdown mb-0" id="auditor_edit_modal_physical_edit">
                                        <option value="" selected disabled>Select Auditor</option>
                                        <option value="auditor_1">Niket K</option>
                                        <option value="auditor_2">Soham P</option>
                                        <option value="auditor_3">Aniket Y</option>
                                    </select>
                                    <small>Only editable by Audit Manager</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center" for="full_site_planned_location">Location</div>
                                <div class="col-9 search_dropdown_div">
                                    <input type="text" name="" class="form-control" value="Andheri" id="full_site_planned_location" readonly>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 mt-1">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="thead-custom">Vendor</th>
                                        <th class="thead-custom">Audit Start Time</th>
                                        <th class="thead-custom">Duration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Hewitt 1</td>
                                        <td><span class="vendor1_time"></span></td>
                                        <td>1 hr</td>
                                    </tr>
                                    <tr>
                                        <td>Croma 2</td>
                                        <td><span class="vendor2_time"></span></td>
                                        <td>1 hr</td>
                                    </tr>
                                    <tr>
                                        <td>Hotel Hyatt 3</td>
                                        <td><span class="vendor3_time"></span></td>
                                        <td>1 hr</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="col-9" style="display: none;">
                        <input type="remark" id="physical_hidden_text" placeholder="" name="" value="" class="form-control col reset-cls">
                    </div>
                </div>
                <div class="modal-footer-annual">
                    <div>
                        <small>Note: To schedule Full Site physical select time slot</small>
                    </div>
                    <div>
                        <button type="button" class="btn-yu primary" data-dismiss="modal" style="width:80px">Close</button>
                        <button class="btn-yu primary" data-dismiss="modal" id="update_event_btn_annual_physical" style="width:120px">Schedule Audit </button>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--5. END Planned Physical Full Site Event Modal----- -->

    <!-- 6. Important Act Event Modal -->
    <div id="imp_act_edit_modal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Important Act Schedule</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div  class="modal-body p-1">
                    <div class="row">
                        <div class="col-md-6  col-sm-12 ">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="" for="imp_act_date_selection">Date</label>
                                </div>
                                <div class="col-9">
                                    <div class="form-group">
                                        <div class="d-flex">
                                            <input type="text" class="form-control " id="imp_act_date_edit_modal" placeholder="Select Date" value="20 May,2020" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12  add_event_type_selection">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="" for="type_selection">Appropriate Government</label>
                                </div>
                                <div class="col-9">
                                    <div class="form-group">
                                        <div class="d-flex">
                                            <select class="form-control" id="type_selection" disabled>
                                                <option value="-1">Select </option>
                                                <option value="type_1" disabled selected>State</option>
                                                <option value="type_2">Central</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 ">
                            <div class=" add_event_state_selection">
                                <div class="row">
                                    <div class="col-3 d-flex align-items-center">
                                        <label class="state_selection_dropdown" for="site_selection">State</label>
                                    </div>
                                    <div class="col-9">
                                        <div class="form-group">
                                            <div class="d-flex">
                                                <select class="form-control state_selection_dropdown" id="state_selection" disabled>
                                                    <option value="-1">Select state</option>
                                                    <option value="type_1" disabled selected>Maharashtra</option>
                                                    <option value="type_2">Goa</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="add_event_central_selection" style="display: none;">
                                <div class="row">
                                    <div class="col-3 d-flex align-items-center">
                                        <label class="" for="site_selection">Central</label>
                                    </div>
                                    <div class="col-9">
                                        <div class="form-group">
                                            <div class="d-flex">
                                                <input type="text" value="Central" class="form-control" readonly='true'>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 add_event_act_selection">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="" for="act_selection">Act</label>
                                </div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="form-control" id="act_selection" disabled>
                                        <option value="act_0">Select Act</option>

                                        <option value="act_1" disabled selected>Bonus</option>
                                        <option value="act_2">ESI</option>
                                        <option value="act_4">PF</option>
                                        <option value="act_5">CLRA</option>
                                        <option value="act_6">MBenefit</option>
                                        <option value="act_7">MWages</option>
                                        <option value="act_8">HRA</option>
                                        <option value="act_9">PWages</option>
                                        <option value="act_10">PTax</option>
                                        <option value="act_11">S&E</option>
                                        <option value="act_12">Other</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12  add_event_title_selection">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="" for="title_selection">Title</label>
                                </div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="form-control" id="title_selection" disabled>
                                        <option value="title_0">Select Title</option>
                                        <option value="title_1" class="act_1_title_1 act_1_title">Bonus Register</option>
                                        <option value="title_2" class="act_1_title_2 act_1_title" disabled selected>Bonus Returns</option>
                                        <option value="title_7" class="act_2_title">ESI Registration Code</option>
                                        <option value="title_3" class="act_2_title_1 act_2_title">ESI challan </option>
                                        <option value="title_4" class="act_2_title_2 act_2_title">ESI challan date of payment
                                        </option>
                                        <option value="title_5" class="act_2_title_3 act_2_title">ESIC ECR</option>
                                        <option value="title_6" class="act_2_title_4 act_2_title">ESI Cards</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12  add_event_type_doc_selection">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="" for="doc_selection">Type of Document</label>
                                </div>
                                <div class="col-9">
                                    <textarea type="text" class="doc_text_input form-control" disabled>Returns</textarea>
                                    <small>*Based on Act & Title*</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 mt-1 add_event_type_description_selection">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="" for="description_selection">Description</label>
                                </div>
                                <div class="col-9">
                                    <textarea type="text" class="description_text_input form-control" disabled>Bonus Annual Returns in Form D</textarea>
                                    <small>*Based on Act & Title*</small>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer-annual">
                    <div>
                        <small>Note: To schedule Audit select time slot</small>
                    </div>
                    <div>
                        <button type="button" class="btn-yu primary" data-dismiss="modal" style="width:100px">Close</button>
                        <!-- <button class="btn-yu primary" data-dismiss="modal" id="update_event_btn_imp_act" style="width:120px">Schedule Act</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 6. END Important Act Event Modal -->

    <!-- 7. Planned Cluster Event Modal -->
    <div id="annual_cluster_edit_modal" class="modal fade ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="planned_cluster_modalTitle" class="modal-title"><span>JLL</span>&nbsp;>&nbsp;&nbsp;<span>HDFC Bank</span>&nbsp;>&nbsp;&nbsp;<span>Charkop 2</span>&nbsp;>&nbsp;&nbsp;<span>Jan 2020</span>&nbsp;(Schedule 4)</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div  class="modal-body p-1">
                    <div class="row prelimenary_audit_form">

                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="mb-0" for="event_start_time_edit_annual">Date & Time</label>
                                </div>
                                <div class="col-9">
                                    <div class="d-flex">
                                        <input type="text" class="form-control date_time_background" id="event_start_date_edit_annual_cluster" placeholder="Select Date" style="color: #98A4B8;" readonly>
                                        <input type="text" class="form-control date_time_background" id="event_start_time_edit_annual_cluster" placeholder="Select Time" style="width: 100px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center margin-top-20">Auditor</div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="selectivity_dropdown mb-0" id="auditor_edit_modal_planned_cluster">
                                        <option value="" selected disabled>Select Auditor</option>
                                        <option value="auditor_1">Niket K</option>
                                        <option value="auditor_2">Soham P</option>
                                        <option value="auditor_3">Aniket Y</option>
                                    </select>
                                    <small>Only editable by Audit Manager</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center" for="location_edit_modal_planned_cluster">Location</div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="selectivity_dropdown mb-0" id="location_edit_modal_planned_cluster">
                                        <option value="site_1">Charkop</option>
                                        <option value="site_2">Andheri</option>
                                        <option value="site_3">Dadar</option>
                                        <option value="site_4">Virar</option>
                                        <option value="site_5">
                                            <-- Site Master -->
                                        </option>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 mt-1">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="thead-custom">Vendor</th>
                                        <th class="thead-custom">Audit Start Time</th>
                                        <th class="thead-custom">Duration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Hewitt 1</td>
                                        <td><span class="vendor1_time"></span></td>
                                        <td>1 hr</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="col-9" style="display: none;">
                        <input type="remark" id="cluster_hidden_text" placeholder="" name="" value="" class="form-control col reset-cls">
                    </div>
                </div>
                <div class="modal-footer-annual">
                    <div>
                        <small>Note: To schedule Schedule 4 select time slot</small>
                    </div>
                    <div>
                        <button type="button" class="btn-yu primary" data-dismiss="modal" style="width:80px">Close</button>
                        <button class="btn-yu primary" data-dismiss="modal" id="update_event_btn_annual_cluster" style="width:190px">Schedule Audit </button>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- 7. END Planned Cluster Event Modal -->

    <!--8. Planned Online Full Site Event Modal---- -->
    <div id="annual_edit_online_modal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="planned_online_fullsite_modalTitle" class="modal-title"><span>JLL</span>&nbsp;>&nbsp;&nbsp;<span>HDFC Bank</span>&nbsp;>&nbsp;&nbsp;<span>Charkop 2</span>&nbsp;>&nbsp;&nbsp;<span>Jan 2020</span>&nbsp;(Online Audit - Full Site)</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div id="planned_online_fullsite_modalBody" class="modal-body p-1">
                    <div class="row prelimenary_audit_form">

                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="mb-0" for="event_start_time_edit_annual">Date & Time</label>
                                </div>
                                <div class="col-9">
                                    <div class="d-flex">
                                        <input type="text" class="form-control date_time_background" id="event_start_date_edit_annual" placeholder="Select Date" style="color: #98A4B8;" readonly>
                                        <input type="text" class="form-control date_time_background" id="event_start_time_edit_annual" placeholder="Select Time" style="width: 100px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center margin-top-20">Auditor</div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="selectivity_dropdown mb-0" id="auditor_edit_modal_annual_edit">
                                        <option value="" selected disabled>Select Auditor</option>
                                        <option value="auditor_1">Niket K</option>
                                        <option value="auditor_2">Soham P</option>
                                        <option value="auditor_3">Aniket Y</option>
                                    </select>
                                    <small>Only editable by Audit Manager</small>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 mt-1">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="thead-custom">Vendor</th>
                                        <th class="thead-custom">Audit Start Time</th>
                                        <th class="thead-custom">Duration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Hewitt 1</td>
                                        <td><span class="vendor1_time"></span></td>
                                        <td>1 hr</td>
                                    </tr>
                                    <tr>
                                        <td>Croma 2</td>
                                        <td><span class="vendor2_time"></span></td>
                                        <td>1 hr</td>
                                    </tr>
                                    <tr>
                                        <td>Hotel Hyatt 3</td>
                                        <td><span class="vendor3_time"></span></td>
                                        <td>1 hr</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="col-9" style="display: none;">
                        <input type="remark" id="hidden_text" placeholder="" name="" value="" class="form-control col reset-cls">
                    </div>
                </div>
                <div class="modal-footer-annual">
                    <div>
                        <small>Note: To schedule Full Site online select time slot</small>
                    </div>
                    <div>
                        <button type="button" class="btn-yu primary" data-dismiss="modal" style="width:80px">Close</button>
                        <button class="btn-yu primary" data-dismiss="modal" id="update_event_btn_annual" style="width:120px">Schedule Audit </button>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--8. END Planned Online Full Site Event Modal----- -->

    <!--9. Planned Desk Event Modal----- -->
    <div id="annual_desk_edit_modal" class="modal fade ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="planned_desk_event_modalTitle" class="modal-title"><span>JLL</span>&nbsp;>&nbsp;&nbsp;<span>HDFC Bank</span>&nbsp;>&nbsp;&nbsp;<span>Charkop 2</span>&nbsp;>&nbsp;&nbsp;<span>Jan 2020</span>&nbsp;(Schedule 3)</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div id="planned_desk_event_modalBody" class="modal-body p-1">
                    <div class="row prelimenary_audit_form">

                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="mb-0" for="event_start_time_edit_annual">Date & Time</label>
                                </div>
                                <div class="col-9">
                                    <div class="d-flex">
                                        <input type="text" class="form-control date_time_background" id="event_start_date_edit_annual_desk" placeholder="Select Date" style="color: #98A4B8;" readonly>
                                        <input type="text" class="form-control date_time_background" id="event_start_time_edit_annual_desk" placeholder="Select Time" style="width: 100px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center margin-top-20">Auditor</div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="selectivity_dropdown mb-0" id="auditor_edit_modal_desk">
                                        <option value="" selected disabled>Select Auditor</option>
                                        <option value="auditor_1">Niket K</option>
                                        <option value="auditor_2">Soham P</option>
                                        <option value="auditor_3">Aniket Y</option>
                                    </select>
                                    <small>Only editable by Audit Manager</small>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center" for="location_edit_modal_desk">Desk Name</div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="selectivity_dropdown mb-0" id="location_edit_modal_desk">
                                        <option value="desk_1">Desk - Charkop</option>
                                        <option value="desk_2">Desk - Andheri</option>
                                        <option value="desk_3">Desk - Dadar</option>
                                        <option value="desk_4">Desk - Virar</option>
                                        <option value="desk_5">
                                            <-- Desk Master -->
                                        </option>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 mt-1">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="thead-custom">Vendor</th>
                                        <th class="thead-custom">Audit Start Time</th>
                                        <th class="thead-custom">Duration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Hewitt 1</td>
                                        <td><span class="vendor1_time"></span></td>
                                        <td>1 hr</td>
                                    </tr>
                                    <tr>
                                        <td>Croma 2</td>
                                        <td><span class="vendor2_time"></span></td>
                                        <td>1 hr</td>
                                    </tr>
                                    <tr>
                                        <td>Hotel Hyatt 3</td>
                                        <td><span class="vendor3_time"></span></td>
                                        <td>1 hr</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="col-9" style="display: none;">
                        <input type="remark" id="desk_hidden_text" placeholder="" name="" value="" class="form-control col reset-cls">
                    </div>
                </div>
                <div class="modal-footer-annual">
                    <div>
                        <small>Note: To schedule Schedule 3 select time slot</small>
                    </div>
                    <div>
                        <button type="button" class="btn-yu primary" data-dismiss="modal" style="width:80px">Close</button>
                        <button class="btn-yu primary" data-dismiss="modal" id="update_event_btn_annual_desk" style="width:165px">Schedule Audit </button>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--9. END Planned Desk Event Modal----- -->

    <!-- 10. Holiday Event Modal----- -->
    <div id="holiday_edit_modal" class="modal fade ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Holiday</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div  class="modal-body p-1">
                    <div class="row " id="">
                        <div class="col-md-6  col-sm-12 mt-1 add_event_holiday_selection">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="" for="holiday_selection">Holiday Name</label>
                                </div>
                                <div class="col-9">
                                    <input type="text" name="" id="" placeholder="Enter Holiday Name" class="form-control" value="Festive Holiday" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 mt-1 add_event_holiday_selection_type">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="" for="holiday_selection">Holiday Type</label>
                                </div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="form-control" id="holiday_selection" disabled>
                                        <option value="account_0" selected>Mandatory Holiday</option>
                                        <option value="account_1">Restricted Holiday</option>
                                        <option value="account_2">Holiday Type 2</option>
                                        <option value="account_3">Holiday Type 3</option>
                                        <option value="account_4">Holiday Type 4</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 mt-1 add_event_start_date_selection">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="" for="holiday_date_selection">Date</label>
                                </div>
                                <div class="col-9">
                                    <input type="text" class="form-control " id="holiday_date_selection" placeholder="Select Date" value="30 May, 2020" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="" for="state_add_schedule_holiday">State</label>
                                </div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="form-control mb-0" id="state_add_schedule_holiday" disabled>

                                        <option value="type_1" selected>Maharashtra</option>
                                        <option value="type_2">Goa</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer-annual">
                    <div>
                        <small>Note: To schedule Holiday select time slot</small>
                    </div>
                    <div>
                        <button type="button" class="btn-yu primary" data-dismiss="modal" style="width:100px">Close</button>
                        <!-- <button class="btn-yu primary" data-dismiss="modal" id="update_event_btn_holiday" style="width:120px">Schedule Holiday</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 10. END Holiday Event Modal----- -->


    <!-- 11. Audit Manager Decision Modal----- -->
    <div id="audit_manager_edit_modal" class="modal fade ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="audit_manager_decision_modalTitle" class="modal-title"><span>HDFC Bank</span>&nbsp;>&nbsp;&nbsp;<span>HDFC Bank</span>&nbsp;>&nbsp;&nbsp;<span>Cluster 2</span>&nbsp;>&nbsp;&nbsp;<span>Vendor 1</span></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div  class="modal-body p-1">

                    <div class="row " id="">
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="mb-0" for="">Date & Time</label>
                                </div>
                                <div class="col-9 ">
                                    <div class="d-flex date_time_div_regular">
                                        <input type="text" class="form-control  date_time_background" placeholder="Select Date" style="color: #98A4B8;" value="16 May,2020">
                                        <input type="text" class="form-control date_time_background" placeholder="Select Time" style="width: 100px;" value="6:20">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center" style="margin-top:-20px;">Auditor</div>
                                <div class="col-9 search_dropdown_div">
                                    <select class="selectivity_dropdown mb-0" id="audit_manager_dropdown">
                                        <option value="" selected disabled>Select Auditor</option>
                                        <option value="auditor_1">Niket K</option>
                                        <option value="auditor_2">Soham P</option>
                                        <option value="auditor_3">Aniket Y</option>
                                    </select>
                                    <small>Only editable by Audit Manager</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mt-1">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="thead-custom">#</th>
                                        <th class="thead-custom">Schedule Audit</th>
                                        <th class="thead-custom">Action</th>
                                        <th class="thead-custom" style="min-width: 35px;"></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <div class="d-flex justify-content-between">
                                                <div class="d-flex">
                                                    <input type="text" class="form-control date_time_background" id="" placeholder="Select Date" style="color: #98A4B8; width:200px;" value="16 May,2020">
                                                    <input type="text" class="form-control date_time_background" id="" placeholder="Select Time" style="width: 100px;" value="11:00 am">
                                                </div>
                                                <div>
                                                    <textarea class="form-control" placeholder="Comment" style="height: 40px;"></textarea>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-around">
                                                <span class="text-success accept_1" style="cursor: pointer;">Accept</span>
                                                <span class="text-danger reject_1" style="cursor: pointer;">Reject</span>
                                                <input type="hidden" id="reject_1_hidden">
                                            </div>
                                        </td>
                                        <td>
                                            <span id="status_icon_accpet_1" class="status_icon_accpet" style="display: none">
                                                <i class="ft-check"></i></span>
                                            <span id="status_icon_reject_1" class="status_icon_reject" style="display: none">
                                                <i class="ft-x"></i></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>
                                            <div class="d-flex justify-content-between">
                                                <div class="d-flex">
                                                    <input type="text" class="form-control date_time_background" id="" placeholder="Select Date" style="color: #98A4B8; width:200px;" value="17 May,2020">
                                                    <input type="text" class="form-control date_time_background" id="" placeholder="Select Time" style="width: 100px;" value="12:00 pm">
                                                </div>
                                                <div>
                                                    <textarea class="form-control" placeholder="Comment" style="height: 40px;"></textarea>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-around">
                                                <span class="text-success accept_2" style="cursor: pointer;">Accept</span>
                                                <span class="text-danger reject_2" style="cursor: pointer;">Reject</span>

                                                <input type="hidden" id="reject_2_hidden">

                                            </div>
                                        </td>
                                        <td>
                                            <span id="status_icon_accpet_2" class="status_icon_accpet" style="display: none"><i class="ft-check"></i></span>
                                            <span id="status_icon_reject_2" class="status_icon_reject" style="display: none"><i class="ft-x"></i></span>


                                        </td>
                                    </tr>
                                    <tr class="audit_manager_reschedule_modal" style="display: none;">

                                        <td>
                                            <span>3</span>
                                        </td>
                                        <td>
                                            Enter Your Date & Time Slot :
                                            <div class="d-flex justify-content-between mt-1">
                                                <div class="d-flex">
                                                    <input type="text" class="form-control" id="audit_manager_date_2" placeholder="Enter Your Date" style="color: #98A4B8; width:200px;">
                                                    <input type="text" class="form-control" id="audit_manager_time_2" placeholder="Time" style="width: 100px;">
                                                </div>
                                                <div>
                                                    <textarea class="form-control" placeholder="Comment" style="height: 40px;"></textarea>
                                                </div>
                                            </div>
                                        </td>
                                        <td></td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
                <div class="modal-footer-annual">
                    <div>
                        <small>Note: To schedule Schedule 4 select time slot</small>
                        <br>
                        <small>*if any 1 slot accepted other rejected*</small>
                    </div>
                    <div>
                        <button type="button" class="btn-yu primary" data-dismiss="modal" style="width:80px">Close</button>
                        <button class="btn-yu primary" data-dismiss="modal" id="reschedule_audit_manager" style="width:150px">Reschedule Audit</button>
                        <button class="btn-yu primary" data-dismiss="modal" id="update_event_btn_audit_manager" style="width:80px; display:none;">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 11. Audit Manager Decision Modal----- -->

    <!-- 12. Final Event Modal----- -->
    <div id="final_edit_modal" class="modal fade ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="regular_event_modalTitle" class="modal-title"><span>JLL</span>&nbsp;>&nbsp;&nbsp;<span>HDFC Bank</span>&nbsp;>&nbsp;&nbsp;<span>Charkop 2</span>&nbsp;>&nbsp;&nbsp;<span>Feb 2020</span>&nbsp;>&nbsp;&nbsp;<span>Original
                        </span></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div  class="modal-body p-1">
                    <div class="row " id="final_edit_form">
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="mb-0">Date & Time</label>
                                </div>
                                <div class="col-9 ">
                                    <div class="d-flex date_time_div_final">
                                        <input type="text" class="form-control" value="7 May,2020" readonly>
                                        <input type="text" class="form-control" style="width: 100px;" value="10:00 am" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">Auditor</div>
                                <div class="col-9 search_dropdown_div">
                                    <input type="text" class="form-control" id="text-field-final-read-only" value="Soham P" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">Location</div>
                                <div class="col-9 search_dropdown_div">
                                    <input type="text" class="form-control" id="text-field-final-read-only-location" value="Virar" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mt-1">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="thead-custom">Vendor</th>
                                        <th class="thead-custom">Audit Start Time</th>
                                        <th class="thead-custom">Duration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Hewitt 1</td>
                                        <td><span class="vendor1_regular_time">10:00 am</span></td>
                                        <td>1 hr</td>
                                    </tr>
                                    <tr>
                                        <td>Croma 2</td>
                                        <td><span class="vendor2_regular_time">11:00 am</span></td>
                                        <td>1 hr</td>
                                    </tr>
                                    <tr>
                                        <td>Hotel Hyatt 3</td>
                                        <td><span class="vendor3_regular_time">12:00 pm</span></td>
                                        <td>1 hr</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row " id="final_reschedule_row" style="display: none;">
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center">
                                    <label class="mb-0" for="event_start_time_edit_final">Date & Time</label>
                                </div>
                                <div class="col-9 ">
                                    <div class="d-flex date_time_div_regular">
                                        <input type="text" class="form-control date_time_background" id="event_date_edit_final" placeholder="Select Date" style="color: #98A4B8;">
                                        <input type="text" class="form-control date_time_background" id="event_time_edit_final" placeholder="Select Time" style="width: 100px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center" style="margin-top:-20px;">Auditor</div>
                                <div class="col-9 search_dropdown_div">
                                    <input type="text" value="Soham P" class="form-control" id="auditor_name_final_readonly" readonly>
                                    <small>Only editable by Audit Manager</small>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6  col-sm-12 mt-1">
                            <div class="row">
                                <div class="col-3 d-flex align-items-center" style="margin-top:-20px;">Location</div>
                                <div class="col-9 search_dropdown_div">
                                    <input type="text" value="Virar" class="form-control" id="auditor_location_final_readonly" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer-annual">
                    <div>
                        <small>*if any 1 slot accepted other rejected*</small>
                    </div>
                    <div>
                        <button type="button" class="btn-yu primary" data-dismiss="modal" style="width:80px">Close</button>
                        <button class="btn-yu primary" id="final_reschedule_client_btn" style="width:150px">Reschedule Audit</button>
                        <button class="btn-yu primary" data-dismiss="modal" id="update_event_btn_submit_final" style="width:80px; display:none;">Submit</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--12. END Final Event Modal----- -->


    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('calendar_js'); ?>
    <?php $this->load->view('calendar_plugin_js'); ?>