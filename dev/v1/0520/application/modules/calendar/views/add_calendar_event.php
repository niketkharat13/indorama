	<style>
		.margin-top-20 {
			margin-top: -20px;
		}

		.margin-top-1rem {
			margin-top: 1rem;
		}
	</style>
	<div class="row">
		<div class="col-12 mt-1">
			<div class="form-group ">
				<h4 class="head_form_title">Planned Audit</h4>

				<div class="row">
					<div class="col text-center">
					</div>
				</div>

				<!-- regular audit form -->
				<form class="regular_audit_form" style="display: none;">
					<div class="row">
						<div class="col">
							<div class="d-flex">
								<div class="radio">
									<input id="original_audit_full" name="radio_horizontal" type="radio" class="radioButtons" checked onchange="show_vendor_full()">
									<label for="original_audit_full" class="radio-label">Original Audit</label>
								</div>
								<div class="radio">
									<input id="remediation_audit_full" name="radio_horizontal" type="radio" class="radioButtons" onchange="show_vendor_full()">
									<label for="remediation_audit_full" class="radio-label">Remediation Audit</label>
								</div>
							</div>
						</div>
					</div>
					<!-- form is same for both -->
					<div class="row">
						<div class="col-md-6  col-sm-12 mt-1 ">
							<div class="row">
								<div class="col-3 d-flex ">
									<label class="mb-0" for="">Date & Time</label>
								</div>
								<div class="col-9">
									<div class="form-group mb-0">
										<div class="d-flex">
											<input type="text" class="form-control date_time_background" id="format_1" placeholder="Select Date">
											<input type="text" class="form-control date_time_background" id="autoswitch" placeholder="Select Time" style="width: 100px;">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1"></div>
						<div class="col-md-6  col-sm-12 mt-2">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="prelimenary_client_selection">Client</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown mb-0" id="prelimenary_client_selection">
										<option value="">JP Morgan Services India Pvt Ltd</option>
										<option value="">Lehman Brothers</option>
										<option value="">JLL</option>
										<option value="">Accenture</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-2">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="prelimenary_account_selection">Account</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown mb-0" id="prelimenary_account_selection">
										<option value="">JP Morgan Services India Pvt Ltd</option>
										<option value="">Lehman Brothers</option>
										<option value="">JLL</option>
										<option value="">Accenture</option>
									</select>
									<small>Based on client</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="region_add_schedule_regular">Region</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown mb-0" id="region_add_schedule_regular">
										<option value="">East</option>
										<option value="">West</option>
										<option value="">South</option>
										<option value="">North</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1 ">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="prelimenary_site_selection">Site</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown mb-0" id="prelimenary_site_selection">
										<option value="site_1">Andheri</option>
										<option value="site_2">Sion</option>
										<option value="site_3">Charkop</option>
										<option value="site_4">Virar</option>
									</select>
									<small>*Based on Account and Region*</small>
								</div>
							</div>
						</div>

						<div class="col-md-6  col-sm-12 mt-1 ">
							<div class="row">
								<div class="col-3 d-flex">
									<label class="" for="site_address_full_site">Site Address</label>
								</div>
								<div class="col-9">
									<input type="text" class="form-control" id="site_address_full_site" readonly>
									<small>Based on site address will be displayed</small>

								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1 ">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="prelimenary_auditor_selection">Auditor</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown mb-0" id="prelimenary_auditor_selection">
										<option value="auditor_1">Niket K</option>
										<option value="auditor_2">Soham P</option>
										<option value="auditor_3">Aniket Y</option>
									</select>
									<small>*Auto select from site*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1 ">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="regular_audit_frequency_selection">Frequency</label>
								</div>
								<div class="col-9 search_dropdown_div">

									<select class="selectivity_dropdown mb-0" id="regular_audit_frequency_selection">
										<option value="monthly">Monthly</option>
										<option value="bi-monthly">Bi-Monthly</option>
										<option value="quarterly">Quarterly</option>
										<option value="half_yearly">Half Yearly</option>
										<option value="yearly">Yearly</option>
									</select>
									<small>*Based on Account*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1" style="display:none" id="regular_audit_layout">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="regular_month_selection">Audit Month </label>
								</div>
								<div class="col-9 search_dropdown_div" id="regular_audit_col">

								</div>
								<sub id="regular_bi_month_sub" style="display: none;margin-left:117px">
									<div class="checkbox">
										<label class="d-flex align-items-center">

											Every alternate month same date
										</label>
									</div>
								</sub>
								<sub id="regular_year_sub" style="display: none;margin-left:117px">
									<div class="checkbox">
										<label class="d-flex align-items-center">

											Every year same month and date
										</label>
									</div>
								</sub>
								<sub id="regular_month_sub" style="display: none;margin-left:117px">
									<div class="checkbox">
										<label class="d-flex align-items-center">

											Every Month same date
										</label>
									</div>
								</sub>
								<sub id="regular_qtr_sub" style="display: none;margin-left:117px">
									<div class="checkbox">
										<label class="d-flex align-items-center">

											Every Quarter same date
										</label>
									</div>
								</sub>
								<sub id="regular_hy_sub" style="display: none;margin-left:117px">
									<div class="checkbox">
										<label class="d-flex align-items-center">

											Every Half Yearly same date
										</label>
									</div>
								</sub>
							</div>
						</div>
						<div class="col-md-6  col-sm-12  mt-1">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="mb-0" for="audit_type_selection">Audit Type</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown mb-0" id="audit_type_selection">
										<option value="">Online</option>
										<option value="">Physical</option>
										<option value="">Online (AI)</option>
									</select>
									<small>*Based on Account-auto select from site*</small><br>
									<small>*Editable only if "(Physical + Online)" is selected in Account*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12  mt-1" id="vendor_div_full_site" style="display: none">
							<div class="row">
								<div class="col-3 d-flex">
									<label class="mt-1" for="vendor_selection_cluster_full">Vendor</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown" id="vendor_selection_cluster_full">
										<option value="vendor_1">Ami Sheth and Company</option>
										<option value="vendor_2">Niket & Sons Company</option>
										<option value="vendor_3">Soham Ui Design Solution</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- regular audit form Ends-->
				<!-- annual audit form -->
				<form class="annual_audit_form">
					<small style="text-align: center;display: block;margin-bottom: 1rem;"><i class="ft-info"></i>&nbsp;Ony for Original Audit</small>
					<div class="row">

						<div class="col-md-6  col-sm-12  add_event_client_selection">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="client_selection_annual">Client</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown selectivity-input" id="client_selection_annual">
										<option value="client_1">HDFC</option>
										<option value="client_2">RBI</option>
										<option value="client_3">SBI</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12  add_event_account_selection">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="annual_account_selection">Account</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown selectivity-input" id="annual_account_selection">

										<option value="account_1">JP Morgan Services India Pvt Ltd</option>
										<option value="account_2">Lehman Brothers</option>
										<option value="account_3">JLL</option>
										<option value="account_4">Accenture</option>
									</select>
									<div class="cluster_annual_account_selection_div" style="display:none">
										<select class="multiple-input" id="cluster_annual_account_selection" name="account[name]">

											<option value="account_1">JP Morgan Services India Pvt Ltd</option>
											<option value="account_2">Lehman Brothers</option>
											<option value="account_3">JLL</option>
											<option value="account_4">Accenture</option>
										</select>
									</div>
									<small>*Based on Client*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12  add_event_client_selection mt-1">
							<div class="row">
								<div class="col-3 d-flex">
									<label class="" for="client_audit_categorisation">Audit Categorisation</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown selectivity-input" id="client_audit_categorisation">
										<option value="category_1">Full Site</option>
										<option value="category_2">Desk</option>
										<option value="category_3">Cluster</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="region_add_schedule">Region</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<!-- <select class="selectivity_dropdown selectivity-input mb-0" id="region_add_schedule">
										<option value="region_1">East</option>
										<option value="region_2">West</option>
										<option value="region_3">South</option>
										<option value="region_4">North</option>
									</select> -->
									<input type="text" class="form-control" value="East" readonly>
									<small>*Based on account*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-2" id="cluster_name_selection_div" style="display: none;">
							<div class="row">
								<div class="col-3 d-flex">
									<label class="" for="cluster_name_selection">Cluster Name</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown selectivity-input" id="cluster_name_selection">
										<option value="cluster_1">Cluster 1</option>
										<option value="cluster_2">Cluster 2</option>
										<option value="cluster_3">Cluster 3</option>
									</select>
								</div>
							</div>
						</div>

						<div class="col-md-6  col-sm-12 mt-2" id="desk_name_selection_div" style="display: none;">
							<div class="row">
								<div class="col-3 d-flex">
									<label class="mt-1" for="desk_name_selection">Desk Name</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown selectivity-input" id="desk_name_selection">
										<option value="desk_1">Desk - Andheri</option>
										<option value="desk_2">Desk - Sion</option>
										<option value="desk_3">Desk - Charkop</option>
										<option value="desk_4">Desk - Virar</option>
									</select>
								</div>
							</div>
						</div>

						<div class="col-md-6  col-sm-12 mt-2 add_event_site_selection">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="annual_site_selection">Site</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown selectivity-input" id="annual_site_selection">
										<option value="site_2">Andheri</option>
										<option value="site_3">Sion</option>
										<option value="site_4">Charkop</option>
										<option value="site_5">Virar</option>
									</select>

									<div class="cluster_annual_site_selection_div" style="display:none">
										<select class="multiple-input" id="cluster_annual_site_selection" name="account[name]">
											<option value="site_2">Andheri</option>
											<option value="site_3">Sion</option>
											<option value="site_4">Charkop</option>
											<option value="site_5">Virar</option>
										</select>
									</div>
									<small>*Based on Account and Region*</small>
									<small>*Based on Cluster name*</small>
								</div>
							</div>
						</div>

						<div class="col-md-6  col-sm-12 mt-1">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="annual_auditor_selection">Auditor</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown selectivity-input" id="annual_auditor_selection">
										<option value="auditor_1">Niket K</option>
										<option value="auditor_2">Soham P</option>
										<option value="auditor_3">Aniket Y</option>
									</select>
									<small>*Auto select from site*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1 add_event_tat">
							<div class="row align-items-center">
								<div class="col-3 d-flex ">
									<label class="" for="audit_end_date_selection">Grace Period</label>
								</div>
								<div class="col-9">
									<p class="p_cls">
										<b>3 Days</b>
										<small>(between Regular & Final)</small>
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1">
							<div class="row">
								<div class="col-3 d-flex ">
									<label for="planned_regular_audit_date">Regular Audit Date</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<input type="text" class="form-control date_time_background" id="planned_regular_audit_date" placeholder="Select Date">
									<small><i class="ft-info"></i>&nbsp;Audit will be scheduled on same date based on frequency</small>
								</div>

							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1 ">
							<div class="row">
								<div class="col-3 d-flex ">
									<label class="" for="audit_number_of_month_selection">Schedule for # of Month</label>
								</div>
								<div class="col-9">
									<input type="number" name="" id="audit_number_of_month_selection" class="form-control">
									<small><i class="ft-info"></i>&nbsp;Min 3 months to Max 36 months</small>
								</div>

							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="audit_type_selection_annual">Audit Type</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown" id="audit_type_selection_annual">
										<option value="online">Online</option>
										<option value="physical">Physical</option>
										<option value="ai">Online (AI)</option>
									</select>
									<small>*Based on Account-auto select from site*</small><br>
									<small>*Editable only if "(Physical + Online)" is selected in Account*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="audit_frequency_selection_annual">Frequency</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown" id="audit_frequency_selection_annual">
										<option value="monthly">Monthly</option>
										<option value="bi-monthly">Bi-Monthly</option>
										<option value="quarterly">Quarterly</option>
										<option value="half_yearly">Half Yearly</option>
										<option value="yearly">Yearly</option>
									</select>
									<small>*Based on Account*</small>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- annual audit form Ends-->

				<!-- imp act  audit form -->
				<form class="act_audit_form" style="display: none;">
					<div class="row">

						<div class="col-md-6  col-sm-12 ">
							<div class="row">
								<div class="col-3 d-flex ">
									<label class="" for="site_selection">Date</label>
								</div>
								<div class="col-9">
									<div class="form-group">
										<div class="d-flex">
											<input type="text" class="form-control date_time_background" id="format_7" placeholder="Select Date">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12  add_event_type_selection">
							<div class="row">
								<div class="col-3 d-flex ">
									<label class="" for="type_selection">Appropriate Government</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown" id="type_selection">
										<option value="type_1">State</option>
										<option value="type_2">Central</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 ">
							<div class=" add_event_state_selection">
								<div class="row">
									<div class="col-3 d-flex margin-top-1rem">
										<label class="state_selection_dropdown" for="site_selection">State</label>
									</div>
									<div class="col-9 search_dropdown_div">
										<select class="multiple-input state_selection_dropdown" name="[state][]" id="state_selection">
											<option value="type_1">Maharashtra</option>
											<option value="type_2">Goa</option>
										</select>
										<small>*If Appropriate Government is central then dropdown will be central and disabled*</small>
									</div>
								</div>
							</div>
							<div class="add_event_central_selection" style="display: none;">
								<div class="row">
									<div class="col-3 d-flex margin-top-1rem">
										<label class="" for="site_selection">Central</label>
									</div>
									<div class="col-9">
										<div class="form-group">
											<div class="d-flex">
												<input type="text" value="Central" class="form-control" readonly='true'>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 add_event_act_selection">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="act_selection">Act</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown" id="act_selection">
										<option value="act_1">Bonus</option>
										<option value="act_2">ESI</option>
										<option value="act_4">PF</option>
										<option value="act_5">CLRA</option>
										<option value="act_6">MBenefit</option>
										<option value="act_7">MWages</option>
										<option value="act_8">HRA</option>
										<option value="act_9">PWages</option>
										<option value="act_10">PTax</option>
										<option value="act_11">S&E</option>
										<option value="act_12">Other</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-2 add_event_title_selection">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="title_selection">Title</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown" id="title_selection">
										<option value="title_1" class="act_1_title_1 act_1_title">Bonus Register</option>
										<option value="title_2" class="act_1_title_2 act_1_title">Bonus Returns</option>
										<option value="title_7" class="act_2_title">ESI Registration Code</option>
										<option value="title_3" class="act_2_title_1 act_2_title">ESI challan </option>
										<option value="title_4" class="act_2_title_2 act_2_title">ESI challan date of payment
										</option>
										<option value="title_5" class="act_2_title_3 act_2_title">ESIC ECR</option>
										<option value="title_6" class="act_2_title_4 act_2_title">ESI Cards</option>
									</select>
									<small>*Based on act*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-2 add_event_type_doc_selection">
							<div class="row">
								<div class="col-3 d-flex ">
									<label class="" for="doc_selection">Type of Document</label>
								</div>
								<div class="col-9">
									<textarea type="text" class="doc_text_input form-control"></textarea>
									<small>*Based on Act & Title*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1 add_event_type_description_selection">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="description_selection">Description</label>
								</div>
								<div class="col-9">
									<textarea type="text" class="description_text_input form-control"></textarea>
									<small>*Based on Act & Title*</small>

								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- imp act  audit form Ends-->

				<!-- holiday audit form-- -->
				<form class="holiday_audit_form" style="display: none;">
					<div class="row " id="">
						<div class="col-md-6  col-sm-12 mt-1 add_event_holiday_selection">
							<div class="row">
								<div class="col-3 d-flex ">
									<label class="" for="holiday_selection">Holiday Name</label>
								</div>
								<div class="col-9">
									<input type="text" name="" id="" placeholder="Enter Holiday Name" class="form-control">
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1 add_event_holiday_selection_type">
							<div class="row">
								<div class="col-3 d-flex ">
									<label class="" for="holiday_selection_type">Holiday Type</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown" id="holiday_type_selection">
										<option value="holiday_1">Mandatory Holiday</option>
										<option value="holiday_2">Restricted Holiday</option>
										<option value="holiday_3">Holiday Type 2</option>
										<option value="holiday_4">Holiday Type 3</option>
										<option value="holiday_5">Holiday Type 4</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1 add_event_start_date_selection">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="start_date_selection">Date</label>
								</div>
								<div class="col-9">
									<input type="text" class="form-control date_time_background" id="format_4" placeholder="Select Date" readonly>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="state_add_schedule">State</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="multiple-input  mb-0" name="[state][]" id="state_add_schedule">
										<option value="type_1">Maharashtra</option>
										<option value="type_2">Goa</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- holiday audit form-- Ends-->

				<!-- cluster form -->
				<form class="cluster_audit_form" style="display: none;">
					<div class="row">
						<div class="col">
							<div class="d-flex">
								<div class="radio">
									<input id="cluster_original_audit" name="cluster_radio_horizontal" type="radio" class="radioButtons" checked onchange="show_vendor_cluster()">
									<label for="cluster_original_audit" class="radio-label">Original Audit</label>
								</div>
								<div class="radio">
									<input id="cluster_remediation_audit" name="cluster_radio_horizontal" value="remediation" type="radio" class="radioButtons" onchange="show_vendor_cluster()">
									<label for="cluster_remediation_audit" class="radio-label">Remediation Audit</label>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6  col-sm-12 mt-1 ">
							<div class="row">
								<div class="col-3 d-flex ">
									<label class="" for="site_selection">Date & Time</label>
								</div>
								<div class="col-9">
									<div class="form-group">
										<div class="d-flex">
											<input type="text" class="form-control  date_time_background" id="format_6" placeholder="Select Date" readonly>
											<input type="text" class="form-control date_time_background " id="autoswitch_3" placeholder="" style="width: 100px;">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1 "></div>
						<div class="col-md-6  col-sm-12 mt-1">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="cluster_client_selection">Client</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown" id="cluster_client_selection">
										<option value="client_1">HDFC</option>
										<option value="client_2">RBI</option>
										<option value="client_3">SBI</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="cluster_account_selection">Account</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="multiple-input" id="multi_select_account_cluster_audit">
										<option value="1">JP Morgan Services India Pvt Ltd</option>
										<option value="1">Lehman Brothers</option>
										<option value="1">JLL</option>
										<option value="1">Accenture</option>
									</select>
									<small>Based on client</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-2">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="region_add_schedule_cluster">Region</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown mb-0" id="region_add_schedule_cluster">
										<option value="">East</option>
										<option value="">West</option>
										<option value="">South</option>
										<option value="">North</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-2">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="cluster_selection">Cluster</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown" id="cluster_selection">
										<option value="site_2">Cluster 2</option>
										<option value="site_3">Cluster 3</option>
										<option value="site_4">Cluster 4</option>
									</select>
									<small>*Based on Account*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1 ">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="cluster_address_selection">Cluster Address</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown" id="cluster_address_selection">
										<option value="address_1">Andheri</option>
										<option value="address_2">Charkop</option>
										<option value="address_3">Virar</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1 ">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="cluster_auditor_selection">Auditor</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown" id="cluster_auditor_selection">
										<option value="auditor_1">Niket K</option>
										<option value="auditor_2">Soham P</option>
										<option value="auditor_3">Aniket Y</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12  mt-1">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="audit_frequency_selection_regular">Frequency</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown" id="audit_frequency_selection_cluster">
										<option value="monthly">Monthly</option>
										<option value="bi-monthly">Bi-Monthly</option>
										<option value="quarterly">Quarterly</option>
										<option value="half_yearly">Half Yearly</option>
										<option value="yearly">Yearly</option>
									</select>
									<small>*Based on Account*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1" style="display:none" id="regular_audit_layout_cluster">
							<div class="row">
								<div class="col-3 d-flex ">
									<label class="" for="regular_month_selection">Audit Month </label>
								</div>
								<div class="col-9 search_dropdown_div" id="regular_audit_col_cluster">

								</div>
								<sub id="regular_bi_month_sub_cluster" style="display: none;margin-left:117px">
									<div class="checkbox">
										<label class="d-flex align-items-center">

											Every alternate month same date
										</label>
									</div>
								</sub>
								<sub id="regular_year_sub_cluster" style="display: none;margin-left:117px">
									<div class="checkbox">
										<label class="d-flex align-items-center">

											Every year same month and date
										</label>
									</div>
								</sub>
								<sub id="regular_month_sub_cluster" style="display: none;margin-left:117px">
									<div class="checkbox">
										<label class="d-flex align-items-center">

											Every Month same date
										</label>
									</div>
								</sub>
								<sub id="regular_qtr_sub_cluster" style="display: none;margin-left:117px">
									<div class="checkbox">
										<label class="d-flex align-items-center">

											Every Quarter same date
										</label>
									</div>
								</sub>
								<sub id="regular_hy_sub_cluster" style="display: none;margin-left:117px">
									<div class="checkbox">
										<label class="d-flex align-items-center">

											Every Half Yearly same date
										</label>
									</div>
								</sub>
							</div>
						</div>
						<div class="col-md-6  col-sm-12  mt-1">
							<div class="row">
								<div class="col-3 d-flex">
									<label class="mt-1" for="audit_type_selection_cluster">Audit Type</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown" id="audit_type_selection_cluster">
										<option value="">Online</option>
										<option value="">Physical</option>
										<option value="">Online (AI)</option>
									</select>
									<small>*Based on Account*</small><br>
									<small>*Editable only if "(Physical + Online)" is selected in Account*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12  mt-1" id="vendor_div" style="display: none">
							<div class="row">
								<div class="col-3 d-flex">
									<label class="mt-1" for="vendor_selection_cluster">Vendor</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown" id="vendor_selection_cluster">
										<option value="vendor_1">Ami Sheth and Company</option>
										<option value="vendor_2">Niket & Sons Company</option>
										<option value="vendor_3">Soham Ui Design Solution</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- Cluster Form END -->

				<!-- desk audit form -->
				<form class="desk_audit_form" style="display: none;">
					<div class="row">
						<div class="col">
							<div class="d-flex">
								<div class="radio">
									<input id="original_desk_audit" name="audit_radio_horizontal" type="radio" class="audit_radioButtons" checked onchange="show_vendor_desk()">
									<label for="original_desk_audit" class="radio-label">Original Audit</label>
								</div>
								<div class="radio">
									<input id="remediation_desk_audit" name="audit_radio_horizontal" type="radio" class="audit_radioButtons" onchange="show_vendor_desk()">
									<label for="remediation_desk_audit" class="radio-label">Remediation Audit</label>
								</div>
							</div>
						</div>
					</div>
					<!-- form is same for both -->
					<div class="row">
						<div class="col-md-6  col-sm-12 mt-1 ">
							<div class="row">
								<div class="col-3 d-flex ">
									<label class="mb-0" for="">Date & Time</label>
								</div>
								<div class="col-9">
									<div class="form-group mb-0">
										<div class="d-flex">
											<input type="text" class="form-control date_time_background" id="desk_format_1" placeholder="Select Date">
											<input type="text" class="form-control date_time_background" id="desk_autoswitch" placeholder="Select Time" style="width: 100px;">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1"></div>
						<div class="col-md-6  col-sm-12 mt-2">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="desk_client_selection">Client</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown mb-0" id="desk_client_selection">
										<option value="">JP Morgan Services India Pvt Ltd</option>
										<option value="">Lehman Brothers</option>
										<option value="">JLL</option>
										<option value="">Accenture</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-2">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="desk_account_selection">Account</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown mb-0" id="desk_account_selection">
										<option value="">JP Morgan Services India Pvt Ltd</option>
										<option value="">Lehman Brothers</option>
										<option value="">JLL</option>
										<option value="">Accenture</option>
									</select>
									<small>Based on client</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="region_add_schedule_desk">Region</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown mb-0" id="region_add_schedule_desk">
										<option value="">East</option>
										<option value="">West</option>
										<option value="">South</option>
										<option value="">North</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1 ">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="desk_site_selection">Site</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown mb-0" id="desk_site_selection">
										<option value="site_1">Andheri</option>
										<option value="site_2">Sion</option>
										<option value="site_3">Charkop</option>
										<option value="site_4">Virar</option>
									</select>
									<small>*Based on Account and Region*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1 ">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="desk_name">Desk Name</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown mb-0" id="desk_name">
										<option value="desk_1">Desk - Andheri</option>
										<option value="desk_2">Desk - Sion</option>
										<option value="desk_3">Desk - Charkop</option>
										<option value="desk_4">Desk - Virar</option>
									</select>
									<small>*Based on Desk Master for that Region*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1 ">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="desk_auditor_selection">Auditor</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown mb-0" id="desk_auditor_selection">
										<option value="auditor_1">Niket K</option>
										<option value="auditor_2">Soham P</option>
										<option value="auditor_3">Aniket Y</option>
									</select>
									<small>*Auto select from site*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1 ">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="desk_audit_frequency_selection">Frequency</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown mb-0" id="desk_audit_frequency_selection">
										<option value="monthly">Monthly</option>
										<option value="bi-monthly">Bi-Monthly</option>
										<option value="quarterly">Quarterly</option>
										<option value="half_yearly">Half Yearly</option>
										<option value="yearly">Yearly</option>
									</select>
									<small>*Based on Account*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12 mt-1" style="display:none" id="desk_audit_layout">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="" for="desk_month_selection">Audit Month </label>
								</div>
								<div class="col-9 search_dropdown_div" id="desk_audit_col">

								</div>
								<sub id="desk_bi_month_sub" style="display: none;margin-left:117px">
									<div class="checkbox">
										<label class="d-flex align-items-center">

											Every alternate month same date
										</label>
									</div>
								</sub>
								<sub id="desk_year_sub" style="display: none;margin-left:117px">
									<div class="checkbox">
										<label class="d-flex align-items-center">

											Every year same month and date
										</label>
									</div>
								</sub>
								<sub id="desk_month_sub" style="display: none;margin-left:117px">
									<div class="checkbox">
										<label class="d-flex align-items-center">

											Every Month same date
										</label>
									</div>
								</sub>
								<sub id="desk_qtr_sub" style="display: none;margin-left:117px">
									<div class="checkbox">
										<label class="d-flex align-items-center">

											Every Quarter same date
										</label>
									</div>
								</sub>
								<sub id="desk_hy_sub" style="display: none;margin-left:117px">
									<div class="checkbox">
										<label class="d-flex align-items-center">

											Every Half Yearly same date
										</label>
									</div>
								</sub>
							</div>
						</div>
						<div class="col-md-6  col-sm-12  mt-1">
							<div class="row">
								<div class="col-3 d-flex margin-top-1rem">
									<label class="mb-0" for="audit_type_selection">Audit Type</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown mb-0" id="desk_audit_type_selection">
										<option value="online">Online</option>
										<option value="physical">Physical</option>
										<option value="online_ai">Online (AI)</option>
									</select>
									<small>*Based on Account-auto select from site*</small><br>
									<small>*Editable only if "(Physical + Online)" is selected in Account*</small>
								</div>
							</div>
						</div>
						<div class="col-md-6  col-sm-12  mt-1" id="vendor_div_desk" style="display: none">
							<div class="row">
								<div class="col-3 d-flex">
									<label class="mt-1" for="vendor_selection_desk">Vendor</label>
								</div>
								<div class="col-9 search_dropdown_div">
									<select class="selectivity_dropdown" id="vendor_selection_desk">
										<option value="vendor_1">Ami Sheth and Company</option>
										<option value="vendor_2">Niket & Sons Company</option>
										<option value="vendor_3">Soham Ui Design Solution</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- desk audit form Ends-->
			</div>
		</div>
	</div>