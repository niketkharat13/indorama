<script>
    /********************************************
     *				Events Colors				*
     ********************************************/

    // calendar js plugin
    $(function() {
        $('#fc-event-colors-2').fullCalendar({
            defaultView: 'month',
            eventRender: function(eventObj, $el) {
                $el.popover({
                    title: eventObj.title,
                    content: eventObj.description,
                    trigger: 'hover',
                    placement: 'top',
                    container: 'body'
                });
            },
            buttonText: {
                today: 'Today',
                month: 'Month',
                week: 'Week',
                day: 'Day',

            },
            header: {

                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },

            selectable: true,
            selectHelper: true,
            // select: function(start, end, allDay) {
            //     $("#add_calendar_event_modal").modal('show');

            // },
            select: (start, end, allDay) => {
                var startDate = moment(start),
                    endDate = moment(end),
                    date = startDate.clone(),
                    isWeekend = false;

                while (date.isBefore(endDate)) {
                    if (date.isoWeekday() == 6 || date.isoWeekday() == 7) {
                        isWeekend = true;
                    }
                    date.add(1, 'day');
                }

                // if (isWeekend) {
                //     $.confirm({
                //         title: '',
                //         type: 'orange',
                //         content: 'Do you want to schedule an audit on Holiday ?',
                //         buttons: {
                //             text: 'Done!',
                //             btnClass: 'btn-yu',
                //             Yes: function() {
                //                 $("#add_calendar_event_modal").modal('show');

                //             },
                //             No: function() {
                //                 $.alert({
                //                     title: '',
                //                     content: 'Schedule canceled',
                //                     type: 'red',
                //                 });
                //             }
                //         }
                //     });
                //     return false;

                // } else {
                //     $("#add_calendar_event_modal").modal('show');
                // }
                this.startDate = startDate.format("YYYY-MM-DD");
                this.endDate = endDate.format("YYYY-MM-DD");
            },
            businessHours: false, // display business hours
            editable: false, //Avoid Event from Dragging
            timeFormat: 'hh:mm a',
            events: [{
                    title: 'Schedule 4',
                    start: '2020-05-06T11:30',
                    end: '2020-05-06T12:30',
                    color: '#967ADC',
                    description: '',
                },
                {
                    title: 'Schedule 2',
                    start: '2020-05-02',
                    end: '2020-05-02T15:30:00',
                    color: '#37BC9B',
                    description: '',
                    meridiem: 'short'
                },
                {
                    title: 'Schedule 3',
                    start: '2020-05-15T15:00:00',
                    end: '2020-05-15T16:00:00',
                    color: '#018ee0',
                    description: 'Desk Audit',
                    meridiem: 'short'

                },
                {

                    title: 'Schedule 5',
                    start: '2020-05-07',
                    end: '2020-05-07',
                    color: '#8bc34a',
                    description: '',

                },
                {

                    title: 'Schedule 1',
                    start: '2020-05-18',
                    end: '2020-05-18',
                    color: '#DA4453',
                    description: '',
                    hour: 'numeric',
                    minute: '2-digit',
                    meridiem: false

                },
                {

                    title: 'Schedule 1',
                    start: '2020-05-22',
                    end: '2020-05-22',
                    color: '#DA4453',
                    description: '',
                    hour: 'numeric',
                    minute: '2-digit',
                    meridiem: false
                },
                {

                    title: 'Schedule 1',
                    start: '2020-05-21',
                    end: '2020-05-21',
                    color: '#DA4453',
                    description: '',
                    hour: 'numeric',
                    minute: '2-digit',
                    meridiem: false

                },
                {

                    title: 'Schedule 7',
                    start: '2020-05-20T10:30:00',
                    end: '2020-05-20T12:30:00',
                    color: 'orange',
                    description: '',

                },
                {

                    title: 'Schedule 1',
                    start: '2020-05-25',
                    end: '2020-05-25',
                    color: '#DA4453',
                    description: '',
                    hour: 'numeric',
                    minute: '2-digit',
                    meridiem: false

                },
                {

                    title: 'Schedule 6',
                    start: '2020-05-30',
                    end: '2020-05-30',
                    color: 'red',
                    description: '',

                },


            ],
            customButtons: {
                addScheduleButton: {
                    text: 'Create Schedule',
                    click: function() {
                        $("#add_calendar_event_modal").modal('show');
                        var eventName = $('#event_name').val();
                    }
                }
            },
            // eventClick: function(event, jsEvent, view) {
            //     event_modal_trigger(event);
            // },
        });

    });


    function close_filter_div() {
        $("#filter_div_calendar").hide();
    }

    function filter_div_reset() {
        $(".filter_reset").prop("checked", false);
    }

    function filter_div_open() {
        $("#filter_div").show();
    }

    // function event_modal_trigger(event) {
    //     var update_event_name = $("#modalTitle").html(event.title);
    //     $("#hidden_text").val(event.start);
    //     $("#hidden_text").val();
    //     var dateTime = $("#hidden_text").val();
    //     var selectedDate = dateTime.split(' ', 4).join(' ');
    //     var editTime = $("#event_start_date_edit").val(selectedDate);

    //     // to show annual edit form
    //     if (event.title == "Planned Full Site Online Audit") {
    //         $("#annual_edit_online_modal").modal();
    //     } else if (event.title == "Planned Cluster Audit") {
    //         $("#annual_cluster_edit_modal").modal();
    //     } else if (event.title == "Planned Full Site Physical Audit") {
    //         $("#annual_edit_physical_modal").modal();
    //     } else if (event.title == "Planned Desk Audit") {
    //         $("#annual_desk_edit_modal").modal();
    //     }
    //     // to show annual edit form
    //     else if (event.title == "Full Site") {


    //         $("#regular_edit_modal").modal();
    //     }
    //     // to show cluster edit form
    //     else if (event.title == "Cluster Audit") {
    //         $("#cluster_edit_modal").modal();

    //     } // to show annual edit form
    //     else if (event.title == "Desk Audit") {
    //         $("#desk_edit_modal").modal();

    //     } else if (event.title == "Holidays") {
    //         $("#holiday_edit_modal").modal();

    //     } else if (event.title == "Important Act Schedule") {
    //         $("#imp_act_edit_modal").modal();

    //     } else if (event.title == "Final Audit") {
    //         $("#final_edit_modal").modal();
    //     }





    //     // vendor time for annual edit modal
    //     $(".vendor1_time").html($("#event_start_time_edit_annual").val());

    //     //list view time for regular,annual,cluster
    //     var extrahr = parseInt($('#event_start_time_edit_annual').val().split(":")[0]) + 1;
    //     extrahr.toString();
    //     var a = $('#event_start_time_edit_annual').val().split(":");
    //     a[0] = extrahr.toString();
    //     a.toString();
    //     var b = a.toString();
    //     var plus_1 = b.replace(',', ':');
    //     $(".vendor2_time").html(plus_1);

    //     var extrahr_2 = parseInt($('#event_start_time_edit_annual').val().split(":")[0]) + 2;
    //     extrahr_2.toString();
    //     var c = $('#event_start_time_edit_annual').val().split(":");
    //     c[0] = extrahr_2.toString();
    //     c.toString();
    //     var d = c.toString();
    //     var plus_2 = d.replace(',', ':');
    //     $(".vendor3_time").html(plus_2);

    //     $(".td-input").click(function() {

    //         setInterval(function() {

    //             $(".vendor1_time").html($("#event_start_time_edit_annual").val());

    //             var extrahr = parseInt($('#event_start_time_edit_annual').val().split(":")[0]) + 1;
    //             extrahr.toString();
    //             var a = $('#event_start_time_edit_annual').val().split(":");
    //             a[0] = extrahr.toString();
    //             a.toString();
    //             var b = a.toString();
    //             var plus_1 = b.replace(',', ':');
    //             $(".vendor2_time").html(plus_1);

    //             var extrahr_2 = parseInt($('#event_start_time_edit_annual').val().split(":")[0]) + 2;
    //             extrahr_2.toString();
    //             var c = $('#event_start_time_edit_annual').val().split(":");
    //             c[0] = extrahr_2.toString();
    //             c.toString();
    //             var d = c.toString();
    //             var plus_2 = d.replace(',', ':');
    //             $(".vendor3_time").html(plus_2);

    //         }, 1000);
    //     });



    // }
</script>