<?php $this->load->view('header'); ?>
<?php $this->load->view('top'); ?>
<?php $this->load->view('vertical_navbar'); ?>
<?php $this->load->view('calendar_css') ?>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/standard-table.css">


<div style="width:103%">
    <div class="col">
        <br>
        <div class="card-yu pt-2 p-1">
            <ul class="nav nav-tabs nav-justified" style="width: 70%;">
                <li class="nav-item">
                    <a class="nav-link" id="annual-audit-tab" href="<?= base_url() ?>calendar/Calendar_controller/calendar_annual_event" aria-controls="active" aria-expanded="true">Planned Audit</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " id="regular-audit-tab" href="<?= base_url() ?>calendar/Calendar_controller/calendar_regular_event" aria-controls="active" aria-expanded="true">Full Site</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="desk-audit-tab" href="<?= base_url() ?>calendar/Calendar_controller/calendar_desk_event" aria-controls="active" aria-expanded="true">Desk Audit</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " id="cluster-audit-tab" href="<?= base_url() ?>calendar/Calendar_controller/calendar_cluster_event" aria-controls="active" aria-expanded="true">Cluster Audit</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " id="important-link-tab" href="<?= base_url() ?>calendar/Calendar_controller/rescheduled_calendar_event" aria-controls="link" aria-expanded="false">Rescheduled Audits</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link active" id="important-link-tab" href="<?= base_url() ?>calendar/Calendar_controller/final_calendar_event" aria-controls="link" aria-expanded="false">Final Audits</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " id="holiday-audit-tab" href="<?= base_url() ?>calendar/Calendar_controller/calendar_holiday_event" aria-controls="active" aria-expanded="true">Holiday</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="important-link-tab" href="<?= base_url() ?>calendar/Calendar_controller/calendar_important_act_event" aria-controls="link" aria-expanded="false">Important Act List</a>
                </li>
            </ul>

            <style>
                .filter_div_1 {
                    width: 102%;
                    position: absolute;
                    top: -12px;
                    left: -15px;
                    background-color: #f4f4f4;
                    z-index: 100;
                    box-shadow: 0px 4px 4px #c5c5c5;
                }
            </style>
            <div class="mt-1">
                <?php $this->load->view('dashboard_widget') ?>
            </div>
            <div class="">
                <div class="" style="position: relative;">
                    <div class="filter_div_1" id="filter_div_calendar_list" style="display: none;">
                        <?php $this->load->view('calendar_filter_view'); ?>
                    </div>

                    <div class="mt-1 mb-1">
                        <div class="row d-flex align-items-center">
                            <div class="col-3 pr-0 d-flex ">
                                <a href="<?= base_url() ?>calendar/Calendar_controller/calendar_view"><i class="la la-angle-left mr-1"></i>Back</a>
                            </div>

                            <div class="col-5">
                            </div>
                            <div class="col-3 d-flex justify-content-end" style="padding:0;margin-left:4rem">
                                <button class="btn-yu primary mr-1" data-toggle="tooltip" title="Filter" id="filter_button_1" style="width:35px;border-radius:50%;border:none">
                                    <i class="ft-filter text-white"></i>
                                </button>

                                <div class="">
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#"><i class="la la-list"></i></a>
                                        <a class="dropdown-item" href="#"> <i class="la la-th-large"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="calendar_list_view_div">
                        <table class="table display" id="calendar_list_grid_final" width="100%" border="0" cellpadding="0" cellspacing="0">
                            <thead>
                                <th class="no-sort checkbox adjust_checkbox_column thead-custom" style="padding-left:6px !important;">
                                    <label>
                                        <input type="checkbox" value="" id="select_all">
                                        <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                    </label>
                                </th>
                                <th class="thead-custom">#</th>
                                <th class="thead-custom">Audit Start Date</th>
                                <!-- <th class="thead-custom">Start Time</th> -->
                                <th class="thead-custom">Audit Month</th>
                                <th class="thead-custom">Audit Type</th>
                                <th class="thead-custom">Status</th>
                                <th class="thead-custom">Client</th>
                                <th class="thead-custom">Account</th>
                                <th class="thead-custom">Region</th>
                                <th class="thead-custom">Location</th>
                                <th class="thead-custom">Site</th>
                                <th class="thead-custom">Vendor</th>
                                <th class="thead-custom">Auditor</th>
                                <th class="thead-custom">Audit Service Type</th>
                                <th class="thead-custom" style="min-width: 100px !important;">Frequency</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <th class="checkbox adjust_checkbox_column">
                                        <label>
                                            <input type="checkbox" value="" name="check[]" class="checkbox">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>1</td>
                                    <td><b>03 Mar</b></td>
                                    <!-- <td>11:00 am</td> -->
                                    <td>Jan 2020</td>
                                    <td>O</td>
                                    <td>
                                        <div class="badge bg-scheduled" style=" width:100px;">Scheduled</div>
                                    </td>
                                    <td>JLL</td>
                                    <td>HDFC</td>
                                    <td>West</td>
                                    <td>Kandivali</td>
                                    <td>Charkop</td>
                                    <td>Croma</td>
                                    <td>Ami Sheth</td>
                                    <td>Online</td>
                                    <td>Monthly</td>
                                </tr>
                                <tr>
                                    <th class="checkbox adjust_checkbox_column">
                                        <label>
                                            <input type="checkbox" value="" name="check[]" class="checkbox">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>2</td>
                                    <td><b>03 Mar</b></td>
                                    <!-- <td>11:00 am</td> -->
                                    <td>Jun 2019</td>
                                    <td>R</td>
                                    <td>
                                        <div class="btn-group">
                                            <a id="calendar_status_5" href="#" class="status-label bg-auto-close dropdown-toggle" data-boundary="viewport" data-toggle="dropdown" aria-expanded="false">Auto-close<span class="caret"></span></a>
                                            <ul class="dropdown-menu dropdown-menu-left">
                                                <li class="auto_close_option" style="display: none;">
                                                    <a onclick="change_status(this,'calendar_status_5','Auto-close')"> <span class="status-mark position-left border-auto-close"></span>Auto-close</a>
                                                </li>
                                                <li class="reopen_option">
                                                    <a onclick="change_status(this,'calendar_status_5','Reopen')"> <span class="status-mark position-left border-reopen"></span>Reopen</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td>JLL</td>
                                    <td>HDFC</td>
                                    <td>West</td>
                                    <td>Kandivali</td>
                                    <td>Charkop</td>
                                    <td>Croma</td>
                                    <td>Ami Sheth</td>
                                    <td>Online</td>
                                    <td>Annually</td>
                                </tr>
                                <tr>
                                    <th class="checkbox adjust_checkbox_column">
                                        <label>
                                            <input type="checkbox" value="" name="check[]" class="checkbox">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>2</td>
                                    <td><b>03 Mar</b></td>
                                    <!-- <td>11:00 am</td> -->
                                    <td>Jun 2019</td>
                                    <td>R</td>
                                    <td>
                                        <div class="badge bg-audited" style="width:100px;">Audited</div>
                                    </td>
                                    <td>JLL</td>
                                    <td>HDFC</td>
                                    <td>West</td>
                                    <td>Kandivali</td>
                                    <td>Charkop</td>
                                    <td>Croma</td>
                                    <td>Ami Sheth</td>
                                    <td>Online</td>
                                    <td>Annually</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    <small>*Last date for vendor to upload document*</small><br>
                    <small>*After completion of Full Site with grace period final audit is scheduled*</small>


                    <table class="legend_table mr-3" style="width: 200px;">
                        <thead class="heading">
                            <th class="thead-custom">Sign</th>
                            <th class="discription thead-custom">Description</th>
                        </thead>
                        <tbody class="tbody">
                            <tr>
                                <td class="sign">O</td>
                                <td>Original</td>
                            </tr>
                            <tr>
                                <td class="sign">R</td>
                                <td>Remediation</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>
<?php $this->load->view('calendar_js') ?>