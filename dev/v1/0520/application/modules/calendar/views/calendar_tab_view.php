    <?php $this->load->view('header'); ?>
    <?php $this->load->view('top'); ?>
    <?php $this->load->view('vertical_navbar') ?>
    <?php $this->load->view('calendar_css') ?>
    <div style="width:103%">
        <div class="col">
            <br>
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="nav-item">
                                <a class="nav-link active" id="report-tab" data-toggle="tab" href="#calendar" aria-controls="report_list" aria-expanded="true">Calendar </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="region_wise-tab" data-toggle="tab" href="#event_tab" aria-controls="region_wise" aria-expanded="false">Event List</a>
                            </li>

                        </ul>
                        <div class="tab-content ">
                            <div role="tabpanel" class="tab-pane active" id="calendar" aria-labelledby="report-tab" aria-expanded="true">
                                <?php $this->load->view('calendar') ?>
                            </div>
                            <div class="tab-pane" id="event_tab" role="tabpanel" aria-labelledby="event_tab" aria-expanded="false">
                                <?php $this->load->view('calendar_list_view') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php $this->load->view('footer') ?>
    <?php $this->load->view('calendar_js') ?>


   
   