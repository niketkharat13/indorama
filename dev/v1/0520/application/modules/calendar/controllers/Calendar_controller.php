<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar_controller extends MY_Controller {


    public function __construct()
    {
        parent::__construct();

    }
    public function calendar_view(){

        $this->load->view('calendar');
    }
    public function calendar_event(){

        $this->load->view('calendar_list_view');
    }
    public function calendar_tab(){

        $this->load->view('calendar_tab_view');
    }

    public function calendar_annual_event(){

        $this->load->view('calendar_annual_event');
    }
    public function calendar_regular_event(){

        $this->load->view('calendar_regular_event');
    }
    public function calendar_cluster_event(){

        $this->load->view('calendar_cluster_event');
    }
    public function calendar_holiday_event(){

        $this->load->view('calendar_holiday_event');
    }
    public function calendar_important_act_event(){

        $this->load->view('calendar_important_act_event');
    }
    public function rescheduled_calendar_event(){

        $this->load->view('rescheduled_calendar_event');
    }
    public function final_calendar_event(){

        $this->load->view('calendar_final_event');
    }
    public function calendar_desk_event(){

        $this->load->view('calendar_desk_event');
    }
}
