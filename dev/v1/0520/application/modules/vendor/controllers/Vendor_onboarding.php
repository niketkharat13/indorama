<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vendor_onboarding extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function account()
    {

        $this->load->view('my_account');
    }
    public function vendor_main_list_view()
    {

        $this->load->view('vendor_main_list_view');
    }
    public function vendor_onboarding_dashboard()
    {
        $this->load->view('vendor_onboarding_dashboard');
    }

    public function add_vendor_landing_page()
    {
        $this->load->view('add_vendor_landing_page');
    }
    public function escalation()
    {
        $this->load->view('escalation');
    }
    public function vendor_list_view()
    {
        $this->load->view('vendor_user_list_view');
    }
    public function vendor_grid_view()
    {
        $this->load->view('vendor_grid_view');
    }
    public function my_team()
    {
        $this->load->view('team');
    }
    public function vendor_kyc()
    {
        $this->load->view('kyc');
    }
    public function my_license_tracker()
    {
        $this->load->view('license_tracker');
    }
    public function site_specific_kyc()
    {
        $this->load->view('new_page');
    }
    public function vendor_master()
    {
        $this->load->view('vendor_master');
    }
}
