<script>
    var count = 0;
    var shop_establishment_multiple_count = 0;
    var bocw_multiple_count = 0;
    var clra_count = 0;
    var state_multiple_ele_count = 0;

    function appendESIC() {
        console.log("Button Pressed");

        count += 1;
        var layout =
            '<div id="esi_div_' + count + '">' +
            '<div class="row align-items-center" style="margin-bottom: 1%;">' +
            '<div class="col-lg-3 col-md-6">' +
            '<a class="remove_element esi_ip_div" id=' + count + '><i class="ft-minus remove_icon"></i></a>' + '</div>' +
            '<div class="col-lg-5 col-md-5 col-sm-12 esi_ip_div search_dropdown_div">' +
            '<div class="row" >' +
            '<div class="col-lg-2 col-md-3 col-sm-3">' +
            '<label for="esic_sites_names_' + count + '" style="margin-top:8px">Sites</label>' +
            '</div>' +
            '<div class="col-lg-10 col-md-9 col-sm-7 xs-4">' +
            '<select name="kyc_esic_"' + count + '"[sites]" id="esic_sites_names_' + count + '" class="multiple-input">' +
            '<option value="">Charkop</option>' +
            '<option value="">Bandra</option>' +
            '<option value="">Andheri</option>' +
            '<option value="">Ahemdabad</option>' +
            '<option value="">Virar</option>' +
            '</select>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 esi_ip_div">' +
            '<div class="row">' +
            '<div class="col-md-12">' +
            '<i class="ft-info font-size-icon"></i>&nbsp;&nbsp;Goa, Maharashtra' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-lg-5 col-md-6 col-sm-12 esi_ip_div offset-lg-3">' +
            '<div class="row BottomSpace">' +
            '<div class="col-lg-2 col-md-3 col-sm-3">' +
            '<label for="esi_ip_' + count + '" style="margin-top:8px">ESI #</label>' +
            '</div>' +
            '<div class="col-lg-9 col-md-7 col-sm-7 xs-4">' +
            '<input type="text" id="esi_ip' + count + '" placeholder="Enter ESI # and upload" name="" value="" class="form-control  reset-cls focus-trigger">' +
            '<div>' +
            '<sub id="esic_upload_file_name_' + count + '" class="file_name" style="display: none"></sub>' +
            '<sub id="preview_mode_esic_' + count + '" class="preview_text" data-target="#preview_modal" data-toggle="modal">Preview</sub>' +
            '</div>' +
            '<sub>Only PDF and Max 2 MB - 1 File only</sub>' +
            '</div>' +
            '<div class="col-lg-1 col-md-2 col-sm-2 xs-1" style="padding: 2%;">' +
            '<i class="ft-paperclip info-1 font-medium-4 paperClip" onclick="open_file(\'esi_ip_file_uplaod_' + count + '\',\'esic_upload_file_name_' + count + '\',\'preview_mode_esic_' + count + '\')' + '"></i>' +
            '</div>' +
            '<input type="file" class="esi_ip_file_uplaod_' + count + '" style="display: none" accept="application/pdf">' +
            '</div>' +
            '</div>' +
            '<div class="col-lg-4 col-md-6 col-sm-12 esi_ip_div">' +
            '<div class="row BottomSpace">' +
            '<div class="col-lg-4 col-md-5 col-sm-3">' +
            '<label for="esi_Date">Date of Registration</label>' +
            '</div>' +
            '<div class="col-lg-7 col-md-7 col-sm-7">' +
            '<input type="text" id="esi_Date_' + count + '" placeholder="ESI Registration Date" name="" value="" class="form-control  reset-cls focus-trigger">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>'
        var new_append = document.createElement('div');
        new_append.classList.add('multiple_esi_licence_upload');
        new_append.innerHTML = layout;
        document.getElementById('esi_div').appendChild(new_append);
        setTimeout(function() {
            $('#esic_sites_names_' + count).selectivity({
                multiple: true,
                allowClear: true,
                placeholder: 'Select',
                // query: queryFunction
            });
            $('#esic_sites_names_' + count).selectivity('clear');
            $('.remove_element').click(function() {
                $(this).parent().parent().parent().parent().remove();
            })
            $('#esi_Date_' + count).dateDropper({
                dropWidth: 200,
                format: 'j F,Y'
            });
        }, 200);
    }

    // shop and establishment license amultiple
    function appendShop_Establishment() {
        shop_establishment_multiple_count += 1;
        var layout =
            '<div id="esi_div_' + shop_establishment_multiple_count + '" class="mt-1">' +
            '<div class="row align-items-center" style="margin-bottom: 1%;">' +
            '<div class="col-lg-3 col-md-6">' +
            '<a class="remove_element shop_ip_div" id=' + shop_establishment_multiple_count + '><i class="ft-minus remove_icon"></i></a>' + '</div>' +
            '<div class="col-lg-5 col-md-5 col-sm-12 shop_ip_div search_dropdown_div">' +
            '<div class="row" >' +
            '<div class="col-lg-2 col-md-3 col-sm-3">' +
            '<label for="s_e_sites_names_' + shop_establishment_multiple_count + '" style="margin-top:8px">Sites</label>' +
            '</div>' +
            '<div class="col-lg-10 col-md-9 col-sm-7 xs-4">' +
            '<select name="kyc_s_e_"' + shop_establishment_multiple_count + '"[sites]" id="s_e_sites_names_' + shop_establishment_multiple_count + '" class="multiple-input">' +
            '<option value="">Charkop</option>' +
            '<option value="">Bandra</option>' +
            '<option value="">Andheri</option>' +
            '<option value="">Ahemdabad</option>' +
            '<option value="">Virar</option>' +
            '</select>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 shop_ip_div">' +
            '<div class="row">' +
            '<div class="col-md-12">' +
            '<i class="ft-info font-size-icon"></i>&nbsp;&nbsp;Goa, Maharashtra' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-lg-3"></div>' +
            '<div class="col-lg-5 col-md-6 col-sm-12 shop_ip_div">' +
            '<div class="row BottomSpace">' +
            '<div class="col-lg-2 col-md-3 col-sm-3">' +
            '<label for="shop_no_ip' + shop_establishment_multiple_count + '" style="margin-top:8px">S&E #</label>' +
            '</div>' +
            '<div class="col-lg-9 col-md-7 col-sm-7 xs-4">' +
            '<input type="text" id="shop_no_ip' + shop_establishment_multiple_count + '" placeholder="Enter S&E # and upload" name="" value="" class="form-control  reset-cls focus-trigger">' +
            '<div>' +
            '<sub id="s_e_upload_file_name_' + shop_establishment_multiple_count + '" class="file_name" style="display: none"></sub>' +
            '<sub id="preview_mode_bocw_' + shop_establishment_multiple_count + '" class="preview_text" data-target="#preview_modal" data-toggle="modal">Preview</sub>' +
            '</div>' +
            '<sub>Only PDF and Max 2 MB - 1 File only</sub>' +
            '</div>' +
            '<div class="col-lg-1 col-md-2 col-sm-2 xs-1" style="padding: 2%;">' +
            '<i class="ft-paperclip info-1 font-medium-4 paperClip" onclick="open_file(\'s_e_ip_file_uplaod_' + shop_establishment_multiple_count + '\',\'s_e_upload_file_name_' + shop_establishment_multiple_count + '\',\'preview_mode_bocw_' + shop_establishment_multiple_count + '\')' + '"></i>' +
            '</div>' +
            '<input type="file" class="s_e_ip_file_uplaod_' + shop_establishment_multiple_count + '" style="display: none" accept="application/pdf">' +
            '</div>' +
            '</div>' +
            '<div class="col-lg-4 col-md-6 col-sm-12 shop_ip_div">' +
            '<div class="row BottomSpace">' +
            '<div class="col-lg-4 col-md-5 col-sm-3">' +
            '<label for="shop_Date_' + shop_establishment_multiple_count + '">Date of Registration</label>' +
            '</div>' +
            '<div class="col-lg-7 col-md-7 col-sm-7">' +
            '<input type="text" id="shop_Date_' + shop_establishment_multiple_count + '" placeholder="S&E Registration Date" name="" value="" class="form-control  reset-cls focus-trigger">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-lg-3"></div>' +
            '<div class="col-lg-5 col-md-6 col-sm-12 shop_ip_div">' +
            '<div class="row BottomSpace">' +
            '<div class="col-lg-2 col-md-3 col-sm-3">' +
            '<label for="shop_Date_exp_' + shop_establishment_multiple_count + '" style="margin-top:8px">Date of Expiry</label>' +
            '</div>' +
            '<div class="col-lg-10 col-md-7 col-sm-7">' +
            '<input type="text" id="shop_Date_exp_' + shop_establishment_multiple_count + '" placeholder="S&E Expiry Date" name="" value="" class="form-control reset-cls focus-trigger">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>'
        var new_append = document.createElement('div');
        new_append.classList.add('multiple_shop_and_establishment_licence_upload');
        new_append.innerHTML = layout;
        document.getElementById('shop_establishment_div').appendChild(new_append);
        setTimeout(function() {
            $('#s_e_sites_names_' + shop_establishment_multiple_count).selectivity({
                multiple: true,
                allowClear: true,
                placeholder: 'Select',
                // query: queryFunction
            });
            $('#s_e_sites_names_' + shop_establishment_multiple_count).selectivity('clear');
            $('.remove_element').click(function() {
                $(this).parent().parent().parent().parent().remove();
            })
            $('#shop_Date_' + shop_establishment_multiple_count).dateDropper({
                dropWidth: 200,
                format: 'j F,Y'
            });
            $('#shop_Date_exp_' + shop_establishment_multiple_count).dateDropper({
                dropWidth: 200,
                format: 'j F,Y'
            });
        }, 200);
    }

    // bocw license amultiple
    function appendBocw() {
        bocw_multiple_count += 1;
        var layout =
            '<div id="bocw_div_' + bocw_multiple_count + '" class="mt-1">' +
            '<div class="row align-items-center" style="margin-bottom: 1%;">' +
            '<div class="col-lg-3 col-md-6">' +
            '<a class="remove_element bocw_ip_div" id=' + bocw_multiple_count + '><i class="ft-minus remove_icon"></i></a>' +
            '</div>' +
            '<div class="col-lg-5 col-md-5 col-sm-12 bocw_ip_div search_dropdown_div">' +
            '<div class="row" >' +
            '<div class="col-lg-2 col-md-3 col-sm-3">' +
            '<label for="bocw_sites_names_' + bocw_multiple_count + '" style="margin-top:8px">Sites</label>' +
            '</div>' +
            '<div class="col-lg-10 col-md-9 col-sm-7 xs-4">' +
            '<select name="kyc_s_e_"' + bocw_multiple_count + '"[sites]" id="bocw_sites_names_' + bocw_multiple_count + '" class="multiple-input">' +
            '<option value="">Charkop</option>' +
            '<option value="">Bandra</option>' +
            '<option value="">Andheri</option>' +
            '<option value="">Ahemdabad</option>' +
            '<option value="">Virar</option>' +
            '</select>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 bocw_ip_div">' +
            '<div class="row">' +
            '<div class="col-md-12">' +
            '<i class="ft-info font-size-icon"></i>&nbsp;&nbsp;Goa, Maharashtra' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-lg-3"></div>' +
            '<div class="col-lg-5 col-md-6 col-sm-12 bocw_ip_div">' +
            '<div class="row BottomSpace">' +
            '<div class="col-lg-2 col-md-3 col-sm-3">' +
            '<label for="bocw_ip_' + bocw_multiple_count + '" style="margin-top:8px">Bocw #</label>' +
            '</div>' +
            '<div class="col-lg-9 col-md-7 col-sm-7 xs-4">' +
            '<input type="text" id="bocw_ip_' + bocw_multiple_count + '" placeholder="Enter Bocw # and upload" name="" value="" class="form-control  reset-cls focus-trigger">' +
            '<div>' +
            '<sub id="bocw_upload_file_name_' + bocw_multiple_count + '" class="file_name" style="display: none"></sub>' +
            '<sub id="preview_mode_bocw_' + bocw_multiple_count + '" class="preview_text" data-target="#preview_modal" data-toggle="modal">Preview</sub>' +
            '</div>' +
            '<sub >Only PDF and Max 2 MB - 1 File only</sub>' +
            '</div>' +
            '<div class="col-lg-1 col-md-2 col-sm-2 xs-1" style="padding: 2%;">' +
            '<i class="ft-paperclip info-1 font-medium-4 paperClip" onclick="open_file(\'bocw_ip_file_uplaod_' + bocw_multiple_count + '\',\'bocw_upload_file_name_' + bocw_multiple_count + '\',\'preview_mode_bocw_' + bocw_multiple_count + '\')' + '"></i>' +
            '</div>' +
            '<input type="file" class="bocw_ip_file_uplaod_' + bocw_multiple_count + '" style="display: none" accept="application/pdf">' +
            '</div>' +
            '</div>' +
            '<div class="col-lg-4 col-md-6 col-sm-12 bocw_ip_div">' +
            '<div class="row BottomSpace">' +
            '<div class="col-lg-4 col-md-5 col-sm-3">' +
            '<label for="bocw_Date_' + bocw_multiple_count + '">Date of Registration</label>' +
            '</div>' +
            '<div class="col-lg-7 col-md-7 col-sm-7">' +
            '<input type="text" id="bocw_Date_' + bocw_multiple_count + '" placeholder="Bocw Registration Date" name="" value="" class="form-control  reset-cls focus-trigger">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-lg-3"></div>' +
            '<div class="col-lg-5 col-md-6 col-sm-12 bocw_ip_div">' +
            '<div class="row BottomSpace">' +
            '<div class="col-lg-2 col-md-3 col-sm-3">' +
            '<label for="bocw_Date_exp_' + bocw_multiple_count + '" style="margin-top:8px">Date of Expiry</label>' +
            '</div>' +
            '<div class="col-lg-10 col-md-7 col-sm-7">' +
            '<input type="text" id="bocw_Date_exp_' + bocw_multiple_count + '" placeholder="Bocw Expiry Date" name="" value="" class="form-control reset-cls focus-trigger">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        var new_append = document.createElement('div');
        new_append.classList.add('multiple_bocw_licence_upload');
        new_append.innerHTML = layout;
        document.getElementById('bocw_div').appendChild(new_append);
        setTimeout(function() {
            $('#bocw_sites_names_' + bocw_multiple_count).selectivity({
                multiple: true,
                allowClear: true,
                placeholder: 'Select',
                // query: queryFunction
            });
            $('#bocw_sites_names_' + bocw_multiple_count).selectivity('clear');
            $('.remove_element').click(function() {
                $(this).parent().parent().parent().parent().remove();
            })
            $('#bocw_Date_' + bocw_multiple_count).dateDropper({
                dropWidth: 200,
                format: 'j F,Y'
            });
            $('#bocw_Date_exp_' + bocw_multiple_count).dateDropper({
                dropWidth: 200,
                format: 'j F,Y'
            });
        }, 200);
    }
    // clra multiple
    function appendClra() {
        clra_count += 1;
        var layout =
            '<div id="clra_div_' + clra_count + '" class="mt-1">' +
            '<div class="row align-items-center" style="margin-bottom: 1%;">' +
            '<div class="col-lg-3 col-md-6">' +
            '<a class="remove_element clra_ip_div" id=' + clra_count + '><i class="ft-minus remove_icon"></i></a>' + '</div>' +
            '<div class="col-lg-5 col-md-5 col-sm-12 clra_ip_div search_dropdown_div">' +
            '<div class="row" >' +
            '<div class="col-lg-2 col-md-3 col-sm-3">' +
            '<label for="clra_sites_names' + clra_count + '" style="margin-top:8px">Sites</label>' +
            '</div>' +
            '<div class="col-lg-10 col-md-9 col-sm-7 xs-4">' +
            '<select name="kyc_clra"' + clra_count + '"[sites]" id="clra_sites_names' + clra_count + '" class="multiple-input">' +
            '<option value="">Charkop</option>' +
            '<option value="">Bandra</option>' +
            '<option value="">Andheri</option>' +
            '<option value="">Ahemdabad</option>' +
            '<option value="">Virar</option>' +
            '</select>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 clra_ip_div">' +
            '<div class="row">' +
            '<div class="col-md-12">' +
            '<i class="ft-info font-size-icon"></i>&nbsp;&nbsp;Goa, Maharashtra' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-lg-3"></div>' +
            '<div class="col-lg-5 col-md-6 col-sm-12 clra_ip_div">' +
            '<div class="row BottomSpace">' +
            '<div class="col-lg-2 col-md-3 col-sm-3">' +
            '<label for="clra_ip' + clra_count + '" style="margin-top:8px">CLRA #</label>' +
            '</div>' +
            '<div class="col-lg-9 col-md-7 col-sm-7 xs-4">' +
            '<input type="text" id="clra_ip' + clra_count + '" placeholder="Enter CLRA # and upload" name="" value="" class="form-control  reset-cls focus-trigger">' +
            '<div>' +
            '<sub id="clra_ip_file_uplaod_name_' + clra_count + '" class="file_name" style="display: none"></sub>' +
            '<sub id="preview_mode_clra_' + clra_count + '" class="preview_text" data-target="#preview_modal" data-toggle="modal">Preview</sub>' +
            '</div>' +
            '<sub >Only PDF and Max 2 MB - 1 File only</sub>' +
            '</div>' +
            '<div class="col-lg-1 col-md-2 col-sm-2 xs-1" style="padding: 2%;">' +
            '<i class="ft-paperclip info-1 font-medium-4 paperClip" onclick="open_file(\'clra_ip_file_uplaod' + clra_count + '\',\'clra_ip_file_uplaod_name_' + clra_count + '\',\'preview_mode_clra_' + clra_count + '\')' + '"></i>' +
            '</div>' +
            '<input type="file" class="clra_ip_file_uplaod' + clra_count + '" style="display: none" accept="application/pdf">' +
            '</div>' +
            '</div>' +
            '<div class="col-lg-4 col-md-6 col-sm-12 clra_ip_div">' +
            '<div class="row BottomSpace">' +
            '<div class="col-lg-4 col-md-5 col-sm-3">' +
            '<label for="clra_Date_' + clra_count + '">Date of Registration</label>' +
            '</div>' +
            '<div class="col-lg-7 col-md-7 col-sm-7">' +
            '<input type="text" id="clra_Date_' + clra_count + '" placeholder="CLRA Registration Date" name="" value="" class="form-control  reset-cls focus-trigger">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-lg-3"></div>' +
            '<div class="col-lg-5 col-md-6 col-sm-12 clra_ip_div">' +
            '<div class="row BottomSpace">' +
            '<div class="col-lg-2 col-md-3 col-sm-3">' +
            '<label for="clra_Date_exp_' + clra_count + '" style="margin-top:8px">Date of Expiry</label>' +
            '</div>' +
            '<div class="col-lg-10 col-md-7 col-sm-7">' +
            '<input type="text" id="clra_Date_exp_' + clra_count + '" placeholder="CLRA Expiry Date" name="" value="" class="form-control reset-cls focus-trigger">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        var new_append = document.createElement('div');
        new_append.classList.add('multiple_clra_licence_upload');
        new_append.innerHTML = layout;
        document.getElementById('clra_div').appendChild(new_append);
        setTimeout(function() {
            $('#clra_sites_names' + clra_count).selectivity({
                multiple: true,
                allowClear: true,
                placeholder: 'Select',
                // query: queryFunction
            });
            $('#clra_sites_names' + clra_count).selectivity('clear');
            $('.remove_element').click(function() {
                $(this).parent().parent().parent().parent().remove();
            })
            $('#clra_Date_' + clra_count).dateDropper({
                dropWidth: 200,
                format: 'j F,Y'
            });
            $('#clra_Date_exp_' + clra_count).dateDropper({
                dropWidth: 200,
                format: 'j F,Y'
            });
        }, 200);
    }
    $('#esi_no').click(function() {
        $('.multiple_esi_licence_upload').remove();
    });
    $('#shop_no').click(function() {
        $('.multiple_shop_and_establishment_licence_upload').remove();
    });
    $('#bocw_no').click(function() {
        $('.multiple_bocw_licence_upload').remove();
    });
    $('#clra_no').click(function() {
        $('.multiple_clra_licence_upload').remove();
    });
    $('#covered_under_state').click(function() {
        $('.state_wise_multiple_upload_license').remove();
    })
    // pt multiple upload
    function appendStateWiseLicense() {
        state_multiple_ele_count += 1;
        var layout =
            '<div class="row " style="margin-bottom: 1%;">' +
            '<a class="state_multiple_remove_element clra_ip_div" id=' + state_multiple_ele_count + '><i class="ft-minus remove_icon"></i></a>' +
            '<div class="col-lg-6 col-md-6 col-sm-12">' +
            '<div class="row BottomSpace">' +
            '<div class="col-lg-3 col-md-3 col-sm-3">' +
            '<label for="gst_no" style="margin-top:8px">GST #</label>' +
            '</div>' +
            '<div class="col-lg-7 col-md-7 col-sm-7 xs-4">' +
            '<input type="text" id="gst_no_' + state_multiple_ele_count + '" placeholder="Enter GST # and upload" name="" value="" class="form-control  reset-cls focus-trigger">' +
            '<div>' +
            '<sub id="gst_ip_file_uplaod_name_' + state_multiple_ele_count + '" class="file_name" style="display: none"></sub>' +
            '<sub id="preview_mode_gst_' + state_multiple_ele_count + '" class="preview_text" data-target="#preview_modal" data-toggle="modal">Preview</sub>' +
            '</div>' +
            '<sub>Only PDF and Max 2 MB - 1 File only</sub>' +
            '<sub id="preview_mode_gst_' + state_multiple_ele_count + '" style="margin-top:10px;float:right;display:none" data-target="#preview_modal" data-toggle="modal">Preview</sub>' +
            '</div>' +
            '<div class="col-lg-2 col-md-2 col-sm-2 xs-1" style="padding: 2%">' +
            '<i class="ft-paperclip info-1 font-medium-4 paperClip" onclick="open_file(\'gst_file_uplaod_' + state_multiple_ele_count + '\',\'gst_ip_file_uplaod_name_' + state_multiple_ele_count + '\',\'preview_mode_gst_' + state_multiple_ele_count + '\')' + '"></i>' +
            '</div>' +
            '<input type="file" class="gst_file_uplaod_' + state_multiple_ele_count + '" style="display: none" accept="application/pdf">' +
            '</div>' +
            '</div>' +
            '<div class="col-lg-4 col-md-6 col-sm-12 state_name_gst">' +
            '<div class="row BottomSpace">' +
            '<input type="text" name="" value="GST for the State Name" class="form-control central" readonly>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="row " style="border-bottom: 1px solid #d5d5d5;">' +
            '<div class="col-lg-3 col-md-6 col-sm-12">' +
            '<div class="row BottomSpace">' +
            '<div class="col-lg-4 col-md-4 col-sm-3 xs-3">' +
            '<label>Professional Tax</label>' +
            '</div>' +
            '<div class="col-lg-4 col-md-4 col-sm-2 radio xs-2">' +
            '<input id="pt_yes_' + state_multiple_ele_count + '" name="pt_radio_' + state_multiple_ele_count + '" type="radio" onclick="show_ip(\'pt_ip_div_' + state_multiple_ele_count + '\')">' +
            '<label for="pt_yes_' + state_multiple_ele_count + '" class="radio-label">Yes</label>' +
            '</div>' +
            '<div class="col-lg-4 col-md-4 col-sm-2 xs-2 NoRadio radio">' +
            '<input id="pt_no_' + state_multiple_ele_count + '" name="pt_radio_' + state_multiple_ele_count + '" type="radio" onclick="hide_ip(\'pt_ip_div_' + state_multiple_ele_count + '\')">' +
            '<label for="pt_no_' + state_multiple_ele_count + '" class="radio-label">No</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-lg-5 col-md-6 col-sm-12 pt_ip_div_' + state_multiple_ele_count + '" style="display:none">' +
            '<div class="row BottomSpace">' +
            '<div class="col-lg-2 col-md-3 col-sm-3">' +
            '<label for="pt_ip" style="margin-top:8px">PT #</label>' +
            '</div>' +
            '<div class="col-lg-7 col-md-7 col-sm-7 xs-4">' +
            '<input type="text" id="pt_ip" placeholder="Enter Professional Tax # and upload" name="" value="" class="form-control  reset-cls focus-trigger">' +
            '<div>' +
            '<sub id="pt_file_upload_name_' + state_multiple_ele_count + '" class="file_name" style="display: none"></sub>' +
            '<sub id="preview_mode_pt_' + state_multiple_ele_count + '" class="preview_text" data-target="#preview_modal" data-toggle="modal">Preview</sub>' +
            '</div>' +
            '<sub >Only PDF and Max 2 MB - 1 File only</sub>' +
            '</div>' +
            '<div class="col-lg-2 col-md-2 col-sm-2 xs-1" style="padding: 2%">' +
            '<i class="ft-paperclip info-1 font-medium-4 paperClip" onclick="open_file(\'pt_ip_file_uplaod_' + state_multiple_ele_count + '\',\'pt_file_upload_name_' + state_multiple_ele_count + '\',\'preview_mode_pt_' + state_multiple_ele_count + '\')' + '"></i>' +
            '</div>' +
            '<input type="file" class="pt_ip_file_uplaod_' + state_multiple_ele_count + '" style="display: none" accept="application/pdf">' +
            '</div>' +
            '</div>' +
            '<div class="col-lg-4 col-md-6 col-sm-12 pt_ip_div_' + state_multiple_ele_count + '" style="display:none">' +
            '<div class="row BottomSpace">' +
            '<div class="col-lg-4 col-md-5 col-sm-3">' +
            '<label for="pt_Date">Date of Registration</label>' +
            '</div>' +
            '<div class="col-lg-7 col-md-7 col-sm-7">' +
            '<input type="text" id="pt_Date_' + state_multiple_ele_count + '" placeholder="PT Registration Date" name="" value="" class="form-control reset-cls focus-trigger">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        var new_append = document.createElement('div');
        new_append.classList.add('state_wise_multiple_upload_license');
        new_append.innerHTML = layout;
        document.getElementById('state_multiple').appendChild(new_append);
        setTimeout(function() {
            $('.state_multiple_remove_element').click(function() {
                $(this).parent().parent().remove();
            })
            $('#pt_Date_' + state_multiple_ele_count).dateDropper({
                dropWidth: 200,
                format: 'j F,Y'
            });
        }, 200);
    }
    // submit form confrim js 
    $(document).ready(function() {
        $(".btn-submit-form").on("click", function() {
            $.alert({
                title: 'User added successfully',
                type: 'green',
                content: '',
            });
        });
        toastr.options = {
            'closeButton': true,
            'debug': false,
            'newestOnTop': false,
            'progressBar': false,
            'positionClass': 'toast-top-right',
            'preventDuplicates': false,
            'showDuration': '1000',
            'hideDuration': '1000',
            'timeOut': '5000',
            'extendedTimeOut': '1000',
            'showEasing': 'swing',
            'hideEasing': 'linear',
            'showMethod': 'fadeIn',
            'hideMethod': 'fadeOut',
        }

        $('select[multiple].active.3col').multiselect({
            columns: 0,
            placeholder: 'Select Sites',
            search: true,
            searchOptions: {
                'default': 'Search Sites'
            },
            selectAll: true
        });
    });
    $('.back_dash').click(function() {
        window.location.href = "<?= base_url()?>dashboard/Dashboard_controller/dashboard_view";
    });
    $('.edit_user').click(function() {
        $('#edit_row').modal('show');
    });
    $('#edit_prefilled_data').click(function() {
        $('.edit_modal').modal('show');
    });
</script>

<script>
    // reset confirm js
    function vendor_reset_confirm() {
        $.confirm({
            title: 'Reset Form',
            type: 'orange',
            content: 'All data will be lost. Do you still want to reset ?',
            buttons: {
                text: 'Done!',
                btnClass: 'btn-yu',
                Yes: function() {
                    $.alert({
                        title: '',
                        content: 'Reset successfully',
                        type: 'green',
                        buttons: {
                            Okay: function() {
                                toastr.success("Reset successfully");
                            }
                        }
                    });
                },
                No: function() {
                    $.alert({
                        title: '',

                        content: 'Reset canceled',
                        type: 'red',
                    });
                }

            }
        });
    }
</script>

<script>
    function addVendor() {
        toastr.success("Successfully added new vendor");
    }

    function addNewVendorUser() {
        toastr.success("Successfully added new user");
    }
    // vendor my team table list view js--------------------------------------------------------
    $('#add_vendor').click(function() {
        $('#add_new_vendor_modal').modal('show');
    })
    // vendor my team table list view js--------------------------------------------------------
    var table = $('#vendor_team_list_view').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        "columnDefs": [{
            "targets": 0,
            "orderable": false,
        }],
        "bStateSave": true,
        'aaSorting': [
            [1, 'asc']
        ],
        dom: 'Bfr<"table-responsive"t>ip',
        buttons: [{
                className: 'filter_export_button',
                text: '<i class="la la-download download_and_export"></i>',
                titleAttr: 'Filter and Export',
            },
            {
                extend: 'colvis',
                text: '<i class="la la-eye-slash" title="Hide/Unhide Columns"></i>',
                titleAttr: 'Hide/Unhide Columns',
                className: 'excel_button'
            },
            {
                extend: 'pageLength',
                text: '<i class="la la-list-ol" title="Toggle Page Length"></i>',
                className: 'excel_button ',
                titleAttr: 'Toggle Page Length'
            },
        ],
    });
    new $.fn.dataTable.FixedColumns(table, {
        leftColumns: 1
    });
    // col resize 
    $(function() {
        $('#vendor_team_list_view').colResizable({
            liveDrag: true,
            gripInnerHtml: "<div class='grip'></div>",
            draggingClass: "dragging",
            resizeMode: 'overflow',
            sScrollXInner: "100%"
        });
    });

    // add user reset js 
    $(function() {
        $(".btn-reset-adduser").bind("click", function() {
            $("#division_dropdown")[0].selectedIndex = 0;
            $("#department_dropdown")[0].selectedIndex = 0;
            $("#report_dropdown")[0].selectedIndex = 0;
            $("#designation_dropdown")[0].selectedIndex = 0;
            $('#enter_name').val('');

        });
    });
    // <!-- vendor kyc form js ------------------- Start ------------>
    //    open  and close file upload on click and load its value in ip field start
    function show_ip(open_div) {
        $("." + open_div).show();
    }

    function hide_ip(close_div) {
        $("." + close_div).hide();
    }
    $("#central-select").hide();

    function change() {
        $("#radio-011").change(function() {
            $("#state-select").show();
            $("#central-select").hide();
        });
        $("#radio-022").change(function() {
            $('#state-select').hide();
            $("#central-select").show();
        });
    }
    // <!-- vendor kyc form js ends ------------------- ENDS------------------    -->
    // toastr for license tracker START
    function submit_data() {
        toastr.success('Successfully Submited');
    }
    // toastr for license tracker ENDS
    // vendor profile js    
    $(document).ready(function() {
        var readURL = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('.profile-pic').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(".file-upload").on('change', function() {
            readURL(this);
        });

        $(".upload-button").on('click', function() {
            $(".file-upload").click();
        });
    });

    function downloadBtn() {
        toastr.success("Template downloading started");
    }
</script>

<!-- License tracker -->
<script type="text/javascript">
    $(document).ready(function() {
        var table_list = $('#vendor_user_list_view').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            "columnDefs": [{
                "targets": 0,
                "orderable": false,
            }],
            "bStateSave": true,
            'aaSorting': [
                [1, 'asc']
            ],
            dom: 'Bfr<"table-responsive"t>ip',
            buttons: [{
                    extend: 'colvis',
                    text: '<i class="la la-eye-slash" title="Hide/Unhide Columns"></i>',
                    titleAttr: 'Hide/Unhide Columns',
                    className: 'excel_button'
                },
                {
                    extend: 'pageLength',
                    text: '<i class="la la-list-ol" title="Toggle Page Length"></i>',
                    className: 'excel_button ',
                    titleAttr: 'Toggle Page Length'
                },
            ],
        });
        new $.fn.dataTable.FixedColumns(table_list, {
            leftColumns: 1
        });
        $(function() {
            $('#vendor_user_list_view').colResizable({
                liveDrag: true,
                gripInnerHtml: "<div class='grip'></div>",
                draggingClass: "dragging",
                resizeMode: 'overflow',
                sScrollXInner: "100%"
            });
        });
    });

    $(document).ready(function() {
        var table_list = $('#license_view_table').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            "columnDefs": [{
                "targets": 0,
                "orderable": false,
            }],
            "bStateSave": true,
            'aaSorting': [
                [1, 'asc']
            ],
            dom: 'Bfr<"table-responsive"t>ip',
            buttons: [{
                    extend: 'colvis',
                    text: '<i class="la la-eye-slash" title="Hide/Unhide Columns"></i>',
                    titleAttr: 'Hide/Unhide Columns',
                    className: 'excel_button'
                },
                {
                    extend: 'pageLength',
                    text: '<i class="la la-list-ol" title="Toggle Page Length"></i>',
                    className: 'excel_button ',
                    titleAttr: 'Toggle Page Length'
                },
            ],
        });
        new $.fn.dataTable.FixedColumns(table_list, {
            leftColumns: 1
        });
        $(function() {
            $('#license_view_table').ColResizable({
                liveDrag: true,
                gripInnerHtml: "<div class='grip'></div>",
                draggingClass: "dragging",
                resizeMode: 'overflow',
                sScrollXInner: "100%"
            });
        });
    });

    function addLicence() {
        toastr.success("License has been added successfully");
    }
    $('#add_new_license').on('change', function() {
        var value = $("#add_new_license").selectivity('val');
        // alert(value);
        if (value == "msme") {
            $('.msme_div').show();
            $('.esi_div').hide();
            $('.pf_div').hide();
            $('.s_e_div').hide();
            $('.pt_div').hide();
            $('.clra_div').hide();
            $('.gst_div').hide();
            $('.bocw_div').hide();
        } else if (value == "pf") {
            $('.msme_div').hide();
            $('.esi_div').hide();
            $('.pf_div').show();
            $('.s_e_div').hide();
            $('.gst_div').hide();
            $('.pt_div').hide();
            $('.clra_div').hide();
            $('.bocw_div').hide();
        } else if (value == "gst") {
            $('.msme_div').hide();
            $('.esi_div').hide();
            $('.pf_div').hide();
            $('.s_e_div').hide();
            $('.pt_div').hide();
            $('.gst_div').show();
            $('.clra_div').hide();
            $('.bocw_div').hide();
        } else if (value == "s_e") {
            $('.msme_div').hide();
            $('.esi_div').hide();
            $('.pf_div').hide();
            $('.gst_div').hide();
            $('.s_e_div').show();
            $('.pt_div').hide();
            $('.clra_div').hide();
            $('.bocw_div').hide();
        } else if (value == "pt") {
            $('.msme_div').hide();
            $('.esi_div').hide();
            $('.pf_div').hide();
            $('.s_e_div').hide();
            $('.gst_div').hide();
            $('.pt_div').show();
            $('.clra_div').hide();
            $('.bocw_div').hide();
        } else if (value == "bocw") {
            $('.msme_div').hide();
            $('.esi_div').hide();
            $('.pf_div').hide();
            $('.s_e_div').hide();
            $('.pt_div').hide();
            $('.clra_div').hide();
            $('.gst_div').hide();
            $('.bocw_div').show();
        } else if (value == "clra") {
            $('.msme_div').hide();
            $('.esi_div').hide();
            $('.pf_div').hide();
            $('.s_e_div').hide();
            $('.pt_div').hide();
            $('.clra_div').show();
            $('.bocw_div').hide();
            $('.gst_div').hide();
        } else if (value == "esi") {
            $('.msme_div').hide();
            $('.esi_div').show();
            $('.pf_div').hide();
            $('.s_e_div').hide();
            $('.pt_div').hide();
            $('.clra_div').hide();
            $('.bocw_div').hide();
            $('.gst_div').hide();
        }
    });
</script>
<!-- KYC JS -->
<script>
    //    open  and close file upload on click and load its value in ip field start
    function open_file(uploader_class, input_id, preview_id) {
        $("." + uploader_class).click();
        $("." + uploader_class).change(function(e) {

            var fileName = e.target.files[0].name;
            $("#" + input_id).html(fileName);
            if ($("#" + input_id).html() != "") {
                $('#' + input_id).show();
                $('#' + preview_id).show();
                toastr.success('File upload successfully');
            }
        });
    }
    //    submit-form btn toastrr and reset value
    $(".btn_submit_vendor_form").bind("click", function() {
        $('.reset-cls').val('');
        toastr.success('Successfully Submited');
    })
    $("#central-select").hide();

    function check_covered_under(value, icon_class) {
        $('.' + icon_class).hide();
        if (value == 'central') {
            $('.small_state').hide();
            $(".state_name_gst").show();
            $('.central').show();
            $(".central_site").show();
            $('.' + icon_class).show();
        } else if (value == 'state') {
            $(".state_name_gst").show();
            $('.central').show();
            $('.small_state').show();
            $(".central_site").hide();
            $('.' + icon_class).hide();
        }
    }
    // <!-- vendor kyc form js ends
</script>
<!-- profile js -->
<script>
    $('#user_city').selectivity('val', 'location-1');
    $('#user_state').selectivity('val', 'state-1');
    $('#user_country').selectivity('val', 'region-2');

    function update_profile() {
        toastr.success("Your profile has been updated");
    }

    function updateVendor() {
        toastr.success("Vendor details has been updated");

    }
</script>

<!-- vendor user list view js  -->
<script>
    // limited no of characterss for email id
    var email = $('#email').text();
    if ($('#email').text().length > 27) {
        var e = $('#email').text().substr(0, 15);
        $('#email').text(e);
        $('#email').append('<i id="load" data-toggle="tooltip" class="la la-ellipsis-h" style="position: absolute;right: 29px;top: 50px;cursor:pointer"></i>');
    }
    $('#load').attr({
        "title": email
    });
</script>

<!-- add vendor landing page -->
<script>
    function redirect_Escalation() {
        window.location.href = '<?= base_url() ?>vendor/vendor_onboarding/escalation';
    }
</script>

<!-- vendor main list view -->
<script>
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        // $('.shawCalRanges').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#date_range_vendor_master').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        console.log("A new date selection was made: " + start.format('YYYY:MM:DD') + ' to ' + end.format('YYYY:MM:DD'));
    }
    $('#date_range_vendor_master').daterangepicker({
        autoApply: true,
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        alwaysShowCalendars: true,
        locale: {
            format: 'MMM D, YYYY'
            // format: 'YYYY:MM:D'
        }
    }, cb);

    function assign(id) {
        if (id == "1") {
            toastr.success("Auditor assigned successfully");
        } else if (id == "2") {
            toastr.success("KAO assigned successfully");
        }
    }
    $('#auditor_site_add_vendor_btn').click(function() {
        toastr.success("Vendor added successfully");
    })

    function add_vendor() {
        toastr.success("Vendor added successfully");
    }
</script>
<script>
    $(document).ready(function() {
        var table = $('#vendor_list_view').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            "bStateSave": true,
            "columnDefs": [{
                "targets": 0,
                "orderable": false,
            }],
            ordering: false,
            dom: 'Bfr<"table-responsive"t>ip',
            buttons: [{
                    className: 'filter_export_button',
                    text: '<i class="la la-download"></i>',
                    titleAttr: 'Filter and Export',
                },
                {
                    extend: 'colvis',
                    text: '<i class="la la-eye-slash" title="Hide/Unhide Columns"></i>',
                    titleAttr: 'Hide/Unhide Columns',
                    className: 'excel_button'
                },
                {
                    extend: 'pageLength',
                    text: '<i class="la la-list-ol" title="Toggle Page Length"></i>',
                    className: 'excel_button ',
                    titleAttr: 'Toggle Page Length'
                },
            ],

        });
        new $.fn.dataTable.FixedColumns(table, {
            leftColumns: 1
        });
        $(function() {
            $("#vendor_list_view").colResizable({
                liveDrag: true,
                gripInnerHtml: "<div class='grip'></div>",
                draggingClass: "dragging",
                resizeMode: 'overflow',
                sScrollXInner: "100%"
            });
        });
        $('#upload').click(function() {
            $('#upload_file').slideToggle(200);
        });
        $('#importfile').click(function() {
            $('#uploadfile2').modal('show');
        });

        $('#close').click(function() {
            $('#upload_file').modal('hide');
        });
    });
</script>
<!-- filter button -->
<script>
    setTimeout(function() {
        $('.filter_export_button').click(function() {
            $('.filter_div').show();
            $('#filter_div_save').show();
            $('#filter_div_apply').hide();
        });
        $('#filter_button_1').click(function() {
            $('.filter_div').show();
            $('#filter_div_apply').show();
            $('#filter_div_save').hide();
        });
        $("#filter_div_save").click(function() {
            $(".filter_div").hide();
        });
        $("#filter_div_apply").click(function() {
            $(".filter_div").hide();
        });
        $('#filter_div_close').click(function() {
            $('#filter_div').css('display', 'none');
        });
    }, 1000);

    $('#vendor_team_list_view .toggle-check').prop('checked', true);

    $("#importfile").click(function() {
        $("#uploadfile2").modal('show')
    });
    $('#prefilled_user').click(function() {
        $('#edit_user').modal('show');
    });
</script>
<!-- vendor mapping js-->
<script>
    function addVendor() {
        var vendor_name = $('#add_vendor_name').val();
        if (!vendor_name == "") {
            $('#map_vendor_dropdown').selectivity({
                items: ['Sanket P', 'Aniket M', 'Soham P', vendor_name],
            })
            $('#vendor_mapping_priciple_contract').selectivity({
                items: ['Sanket P', 'Aniket M', 'Soham P', vendor_name],
            })
            $('.vendor_mapping_btn_grp').show();
            $('.vendor_mapping_modal').show(1000);
            $('.add_vendor_modal').hide(500);
            $('.vendor_mapping_title').show();
            $('.add_vendor_title').hide();
            $('.add_vendor_btn_group').hide();
            $('.vendor_mapping_btn_grp').show();
            $('#map_vendor_dropdown').selectivity('val', vendor_name);
            toastr.success("Vendor added to vendor master");
        }
    }
    $('#auditor_site_add_master_vendor').click(function() {
        $('.add_vendor_modal').show(500);
        $('.vendor_mapping_modal').hide(1000);
        $('.vendor_mapping_title').hide();
        $('.add_vendor_title').show();
        $('.vendor_mapping_btn_grp').hide();
        $('.add_vendor_btn_group').show();
    })
    $('#add_vendor_contract_type').on("change", function() {
        var value = $('#add_vendor_contract_type').selectivity('val');
        if (value == "sub_contractor") {
            $('#principle_employee').show();
        } else {
            $('#principle_employee').hide();
        }
    });
    $('#vendor_mapping_site').on('change', function() {
        var value = $('#vendor_mapping_site').selectivity('val');
        if (value == "site_1") {
            $('#add_vendor_group').selectivity('val', 'group1');
        } else if (value == "site_2") {
            $('#add_vendor_group').selectivity('val', 'group3');
        } else if (value == "site_3") {
            $('#add_vendor_group').selectivity('val', 'group2');
        } else if (value == "site_4") {
            $('#add_vendor_group').selectivity('val', 'group1');
        }
    });

    function vendorMapping() {
        toastr.success("Vendor mapped to selected site");
    }

    $(".pdf_icon").click(function() {
        $("#pdf_review").modal("show");
        $('#comment_month').show();

    });
    $('#vendor_mapping_contract').on('change', function() {
        var value = $('#vendor_mapping_contract').selectivity('val');
        if (value == 'sub_contractor');
        $('#principle_contractor_dropdown').show();
    });
</script>
<!-- Start of preview js -->
<script type="text/javascript">
    $('#myModal').on('shown.bs.modal', function() {
        $('#myInput').trigger('focus')
    })

    function commentboxDisplay(comment_id, add_comment_btn_id, void_btn, monthDropdown, accept_btn) {
        $('#' + comment_id).toggle();
        $('#' + add_comment_btn_id).toggle();
        $('#' + void_btn).toggle();
        $('#' + monthDropdown).hide();
        $('#' + accept_btn).hide();
    }

    function submitComment(modal, add_comment_btn, void_btn, comment_id, accept_btn) {
        $('#' + comment_id).hide();
        $('#' + add_comment_btn).hide();
        $('#' + void_btn).show();
        $("#" + modal).modal('hide');
        $('#' + accept_btn).show();
        toastr.success("Comment Submitted Successfully");
        toastr.success("Document Rejected");
    }

    function documentAccepted() {
        toastr.success("Document Accepted");
    }
</script>
<!-- End of preview js -->

<!-- Profile Image JS -->
<script type="text/javascript">
    $(document).ready(function() {
        $("#buttonid").on("click", function() {
            $("#files").click();
        });

        var default_image = "<?= base_url() ?>app-assets/images/placeholder_avatar.png";

        if (window.File && window.FileList && window.FileReader) {
            $("#files").on("change", function(e) {
                var files = e.target.files,
                    filesLength = files.length;
                var f = files[0];
                var fileReader = new FileReader();
                fileReader.onload = function(e) {
                    var file = e.target;
                    $(".imageThumb").attr("src", e.target.result);
                    $(".remove_button").removeClass("hidden");
                    $("#buttonid").attr("title", "Change Image");

                    $(".remove_button").click(function() {
                        $(".imageThumb").attr("src", default_image);
                        $("#buttonid").attr("title", "Add Image");
                        $(".remove_button").classList.add("hidden");
                    });
                };
                fileReader.readAsDataURL(f);
            });
        } else {
            alert("Your browser doesn't support to File API");
        }
    });
</script>
<!-- START OF UPLOAD MODAL DOWNLOAD BUTTON JS-->
<script>
    // social btn js-----------------------------
    // add this rail gallery effect
    $(document).on('click', '#socialShare > .socialBox', function() {

        var self = $(this);
        var element = $('#socialGallery a');
        var c = 0;

        if (self.hasClass('animate')) {
            return;
        }

        if (!self.hasClass('open')) {

            self.addClass('open');

            TweenMax.staggerTo(element, 0.3, {
                    opacity: 1,
                    visibility: 'visible'
                },
                0.075);
            TweenMax.staggerTo(element, 0.3, {
                    top: -12,
                    ease: Cubic.easeOut
                },
                0.075);

            TweenMax.staggerTo(element, 0.2, {
                    top: 0,
                    delay: 0.1,
                    ease: Cubic.easeOut,
                    onComplete: function() {
                        c++;
                        if (c >= element.length) {
                            self.removeClass('animate');
                        }
                    }
                },
                0.075);

            self.addClass('animate');

        } else {

            TweenMax.staggerTo(element, 0.3, {
                    opacity: 0,
                    onComplete: function() {
                        c++;
                        if (c >= element.length) {
                            self.removeClass('open animate');
                            element.css('visibility', 'hidden');
                        };
                    }
                },
                0.075);
        }
    });
</script>
<!-- END OF UPLOAD MODAL DOWNLOAD BUTTON JS -->
<!-- START OF VENDOR JS -->
<script>
    $(".pdf_icon").click(function() {
        $("#pdf_preview").modal("show");
    });
    $(document).ready(function() {
        var table = $('#vendor_master').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            "columnDefs": [{
                "targets": 0,
                "orderable": false,
            }],
            "bStateSave": true,
            'aaSorting': [
                [1, 'asc']
            ],
            dom: 'Bfr<"table-responsive"t>ip',
            buttons: [{
                    className: 'filter_export_button',
                    text: '<i class="la la-download download_and_export"></i>',
                    titleAttr: 'Filter and Export',
                },
                {
                    extend: 'colvis',
                    text: '<i class="la la-eye-slash" title="Hide/Unhide Columns"></i>',
                    titleAttr: 'Hide/Unhide Columns',
                    className: 'excel_button'
                },
                {
                    extend: 'pageLength',
                    text: '<i class="la la-list-ol" title="Toggle Page Length"></i>',
                    className: 'excel_button ',
                    titleAttr: 'Toggle Page Length'
                },
            ],
        });
        new $.fn.dataTable.FixedColumns(table, {
            leftColumns: 1
        });
        // col resize
        $(function() {
            $('#vendor_master').colResizable({
                liveDrag: true,
                gripInnerHtml: "<div class='grip'></div>",
                draggingClass: "dragging",
                resizeMode: 'overflow',
                sScrollXInner: "100%"
            });
        });
    })

    function addNewVendor() {
        toastr.success("New Vendor added successfully");
    }
</script>
<!-- END OF MASTER JS -->

<!-- Status Change Function -->
<script type="text/javascript">
    function change_status(_this, status_id, status_name) {
        if (status_name == "Inactive") {
            className = "status-label bg-danger dropdown-toggle";
            $(_this).parent().parent().find('.inactive_option').css('display', 'none').siblings('li').css('display', 'block');
        } else if (status_name == "Active") {
            className = "status-label bg-green dropdown-toggle";
            $(_this).parent().parent().find('.active_option').css('display', 'none').siblings('li').css('display', 'block');
        } else if (status_name == "Complete") {
            className = "status-label bg-blue dropdown-toggle";
            $(_this).parent().parent().find('.complete_option').css('display', 'none').siblings('li').css('display', 'block');
        } else if (status_name == "Pending") {
            className = "status-label bg-pending dropdown-toggle";
            $(_this).parent().parent().find('.complete_option').css('display', 'none').siblings('li').css('display', 'block');
        }

        if (status_name == "Active") {
            $("#" + status_id).html(status_name + "&nbsp;&nbsp;<span class='caret'></span>");

        } else {
            $("#" + status_id).html(status_name + "<span class='caret'></span>");
        }
        $("#" + status_id).removeClass();
        $("#" + status_id).addClass(className);
    }
    // Date Range Filter
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#date_range_license_tracker').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        console.log("A new date selection was made: " + start.format('YYYY:MM:DD') + ' to ' + end.format('YYYY:MM:DD'));
    }

    $('#date_range_license_tracker').daterangepicker({
        autoApply: true,
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        alwaysShowCalendars: true,
        locale: {
            format: 'MMM D, YYYY'
            // format: 'YYYY:MM:D'
        }
    }, cb);

    cb(start, end);
</script>
<!--END Status Change Function -->