    <link rel="stylesheet" href="<?= base_url() ?>app-assets/css/media.css">
    <link rel="stylesheet" href="<?= base_url() ?>app-assets/css/star_mobile_widget.css">

    <style>
        .common_grid_style {
            display: -moz-inline-grid;
            display: inline-grid;
        }

        .font-size-icon {
            color: #2f8cbe;
            font-size: 16px;
        }

        /* my account page style---------- */
        .nav-vertical .nav-left.nav-tabs li.nav-item a.nav-link {
            min-width: 9rem;
            border-right: 2px solid #DDDDDD;
        }

        .nav-vertical .nav-left.nav-tabs.nav-border-left li.nav-item a.nav-link.active {
            border-left: 3px solid #008000;
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
            color: #555555;
        }

        .la-angle-left {
            cursor: pointer;
        }

        .star4 {
            color: orange
        }

        .nav-vertical .nav-left.nav-tabs.nav-border-left li.nav-item a.nav-link {
            color: #00517cf5;
        }

        .tabs-styles {
            border-bottom: 1px solid black;
        }

        .margin-left-adjust {
            margin-left: -20px;
        }

        .common_size_div {
            width: 200px;
            height: 100px;
        }

        .profile_cls {
            clip-path: polygon(0 49%, 82% 49%, 100% 100%, 0% 100%);
            background: #4285f4;
            color: white;
            /* z-index: 3; */


        }

        .team_cls {
            clip-path: polygon(18% 49%, 82% 49%, 100% 100%, 0 100%);
            background: #ea4335;
            margin-left: -72px;
            color: white;
            z-index: 3;

        }

        .kyc_cls {
            clip-path: polygon(18% 49%, 82% 49%, 100% 100%, 0 100%);
            background: #fbbc05;
            margin-left: -72px;
            color: white;
            z-index: 2;



        }

        .license_cls {
            clip-path: polygon(18% 49%, 82% 49%, 100% 100%, 0 100%);
            background: #34a853;
            margin-left: -72px;
            color: white;
            z-index: 1;
        }

        .p_size_common {
            margin-top: 63px;
            /* margin-left: 62px; */
            height: 45px;
            cursor: pointer;
            text-align: center;
        }

        .active_tab {
            z-index: 5 !important;
            font-size: 16px;
            transition: 0.2s;
        }

        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 0px solid #ccc;
            border-top: none;
        }

        .info-12 {
            color: #00517cf5;
            font-size: 1.6rem !important;



        }

        .icon-bg-style {
            background: whitesmoke;
            width: 35px;
            border-radius: 50%;
            cursor: pointer;

        }

        .drodown_style_vendor {
            margin-left: -14px;
        }

        /* vendor_kyc_styles------------------------------------------------------------------------------- */

        /* paper icon styles and postion */
        .info-1 {
            color: #00517cf5;
            cursor: pointer;
        }

        .icon-bg-style {
            background: whitesmoke;
            width: 35px;
            border-radius: 50%;
            cursor: pointer;
        }

        .form-control-position {
            position: absolute;
            top: 1px;
            right: 0;
            z-index: 2;
            display: block;
            width: 2.5rem;
            height: 2.5rem;
            line-height: 3.2rem;
            right: 1rem;
        }

        .ip_sub_cls {
            margin-left: 10px;
        }


        /* vendor license tracker css starts ------------------------------------------------------------------ */

        .page_title {
            font-size: 18px;

        }

        .sub-title {
            font-size: 16px;
        }

        .font-size-14 {
            font-size: 14px;
        }

        .btn-min-width {
            min-width: 6.5rem;
        }

        .margin-adjust-1 {
            margin-bottom: 20px;
            /* margin-top: 10px; */
            margin-left: -25px;
        }

        .auto-save-div {
            position: absolute;

            right: 23px;
            top: 10px;
        }

        /* <!-- saving and saved notification --> */
        .saving-cls,
        .saved-cls {
            color: #008000;
            font-size: 16px;
        }

        .margin-adjust {
            margin-right: 5px;
        }

        .tab-form p {
            font-size: 14px;

        }

        .tab-form h2 {
            font-size: 18px;
        }

        .nav.nav-tabs.nav-top-border .nav-item a {
            color: #003b7e;
        }

        .nav.nav-tabs.nav-top-border .nav-item a.nav-link.active {
            border-top: 3px solid #003b7e;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            color: #555555;
        }

        .custom-control-input:checked~.custom-control-label::before {
            color: #FFFFFF;
            border-color: #003b7e;
            background-color: #003b7e;
        }

        .nav.nav-tabs.nav-top-border .nav-item a:hover {
            color: #003b7e;
        }


        /* vendor license tracker css ENDS ------------------------------------------------------------------ */


        /* vendor Profile css  Start--------------------------------------------------------------------- */

        .col-sm-12 {
            margin-top: 1rem !important;
        }

        #employee_img {
            object-fit: cover;
            border-radius: 50%;
            height: 90px;
            width: 90px;
            border: 3px solid #00517cf5;
        }

        .head {
            /* clip-path: polygon(0 8%, 80% 8%, 100% 100%, 0% 100%);
                background: #00517cf5; */
            color: #fff;
            width: 500px;

        }

        .profile-pic {
            max-width: 200px;
            max-height: 200px;
            display: block;
        }

        .file-upload {
            display: none;
        }

        .circle {
            border-radius: 1000px !important;
            overflow: hidden;
            width: 128px;
            height: 128px;
            border: 8px solid rgba(255, 255, 255, 0.7);
            position: absolute;
            top: 72px;
        }

        img {
            height: auto;
        }

        .p-image {
            position: absolute;
            top: 167px;
            right: 30px;
            color: #666666;
            transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
        }

        .p-image:hover {
            transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
        }

        .upload-button {
            font-size: 1.2em;
        }

        .upload-button:hover {
            transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
            color: #999;
        }

        .media_btn {
            width: 60px;
            height: 60px !important;
            position: absolute;
            /* top: 76px; */
            border-radius: 50%;
            z-index: 1;
            left: 30rem;
        }

        @media only screen and (max-width: 575px) {
            .media_btn {
                position: fixed;
                top: 85%;
                left: 85%;
            }
        }

        @media (min-width: 576px) and (max-width: 767px) {
            .media_btn {
                position: fixed;
                top: 86%;
                left: 88%;
            }
        }

        @media (min-width: 768px) and (max-width: 1199px) {
            .media_btn {
                position: fixed;
                top: 86%;
                left: 89%;
            }
        }

        /* vendor Profile css  Ends--------------------------------------------------------------------- */


        /* vendor TEam TAB css  Start--------------------------------------------------------------------- */
        .dataTables_wrapper .dataTables_filter input {
            margin-bottom: 6px;
        }

        .table-options-left {
            position: fixed;
            left: 36px;
        }

        table.dataTable thead th,
        table.dataTable thead td {
            border-bottom: 1px solid #e3ebf3;
        }

        button.dt-button {
            text-align: left !important;
        }

        .table {
            color: black;
            width: 100%;
        }

        .table thead th {
            vertical-align: bottom;
            border-bottom: 3px solid #003B7E;
            border-top: 1px solid #E3EBF3 !important;
        }

        .table th,
        .table td {
            border-top: none;
        }

        .page-item.active .page-link {
            z-index: 1;
            color: #FFFFFF;
            background-color: #003B7E;
            border-color: #003B7E;
        }

        .btn-secondary {
            border-color: #003b7e !important;
            background-color: #003B7E !important;
            color: #FFFFFF;
        }

        .btn-secondary:hover {
            border-color: #003b7e !important;
            background-color: white !important;
            color: #003b7e !important;
            transition-delay: 10s;
            -webkit-transition-delay: 10s;
            /* for Safari & Chrome */
        }



        .table-loader {
            visibility: visible;
            display: table-caption;
            content: " ";
            width: 100%;
            height: 600px;
            background-image:
                linear-gradient(rgba(235, 235, 235, 1) 1px, transparent 0),
                linear-gradient(90deg, rgba(235, 235, 235, 1) 1px, transparent 0),
                linear-gradient(90deg, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0.5) 15%, rgba(255, 255, 255, 0) 30%),
                linear-gradient(rgba(240, 240, 242, 1) 35px, transparent 0);

            background-repeat: repeat;

            background-size:
                1px 35px,
                calc(100% * 0.1666666666) 1px,
                30% 100%,
                2px 70px;

            background-position:
                0 0,
                0 0,
                0 0,
                0 0;

            animation: shine 0.5s infinite;
        }

        table.dataTable.no-footer {
            border-bottom: 1px solid #e3ebf3;
        }

        @keyframes shine {
            to {
                background-position:
                    0 0,
                    0 0,
                    40% 0,
                    0 0;
            }
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
            height: 35px;
            color: #fff !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current,
        .dataTables_wrapper .dataTables_paginate .paginate_button.current {
            background: none !important;
            color: #003b7e !important;
            outline: none;
            border: 1px solid #003b7e !important;
            border-radius: 7px;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current,
        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {

            color: #fff !Important;
        }

        .dataTables_wrapper .dataTables_filter input {
            border: none;
            border-bottom: 1px solid black;
            padding: 5px;
            outline: none;
        }

        .table-heading-elements-yu {
            position: absolute;

        }

        .table-card-header-yu {
            position: relative;
        }

        .table-yu {
            margin-top: 2.5rem !important;
        }

        .table-yu-margin-top {
            margin-top: -14.5rem !important;
        }

        .hide_button:hover {
            background-image: linear-gradient(to right, #003B7E 0%, #003B7E 100%) !important;
            border-color: #fff !important;
            color: #fff !important;
        }

        .page_length_button:hover {
            background-image: linear-gradient(to right, #003B7E 0%, #003B7E 100%) !important;
            border-color: #fff !important;
            color: #fff !important;
        }

        .excel_button:hover {
            background-image: linear-gradient(to right, #003B7E 0%, #003B7E 100%) !important;
            border-color: #fff !important;
            color: #fff !important;
        }

        .hide_button {
            background-image: linear-gradient(to bottom, #fff 0%, #ffffff 100%) !important;
            border-color: #000;
        }

        .page_length_button {
            background-image: linear-gradient(to bottom, #fff 0%, #ffffff 100%) !important;
            border-color: #000;
        }

        .excel_button {
            background-image: linear-gradient(to bottom, #fff 0%, #ffffff 100%) !important;
            border-color: #000;
        }

        .card-title {
            margin-bottom: 0rem;
        }

        .position-right {
            position: absolute;
            top: 0;
            right: 0;
        }

        .filter_button {
            border-radius: 0px !important;
        }


        .filter_div {
            width: 100%;
            position: absolute;
            background-color: #f4f4f4;
            z-index: 100;
            box-shadow: 0px 4px 4px #c5c5c5;
        }

        .card-height {
            height: 175px;
            background-color: #f4f4f4;
            margin-bottom: 0px;
        }

        .checkbox label:after,
        .radio label:after {
            content: '';
            display: table;
            clear: both;
        }


        .checkbox .cr,
        .radio .cr {
            position: relative;
            display: inline-block;
            border: 1px solid #a9a9a9;
            border-radius: .25em;
            width: 1.3em;
            height: 1.3em;
            margin-right: .5em;
        }

        .radio .cr {
            border-radius: 50%;
        }

        .remove_icon {
            border-radius: 50%;
            color: white;
            background-color: red;
        }

        .checkbox .cr .cr-icon,
        .radio .cr .cr-icon {
            position: absolute;
            font-size: .8em;
            line-height: 0;
            top: 50%;
            left: 20%;
        }

        .radio .cr .cr-icon {
            margin-left: 0.04em;
        }

        .checkbox label input[type="checkbox"],
        .radio label input[type="radio"] {
            display: none;
        }

        .checkbox label input[type="checkbox"]+.cr>.cr-icon,
        .radio label input[type="radio"]+.cr>.cr-icon {
            transform: scale(3) rotateZ(-20deg);
            opacity: 0;
            transition: all .3s ease-in;
        }

        .icon {
            border-radius: 50%;
            color: white;
            background-color: green;
        }

        .checkbox label input[type="checkbox"]:checked+.cr>.cr-icon,
        .radio label input[type="radio"]:checked+.cr>.cr-icon {
            transform: scale(1) rotateZ(0deg);
            opacity: 1;
        }

        .checkbox label input[type="checkbox"]:disabled+.cr,
        .radio label input[type="radio"]:disabled+.cr {
            opacity: .5;
        }

        .dataTables_wrapper.no-footer div.dataTables_scrollHead table.dataTable,
        .dataTables_wrapper.no-footer div.dataTables_scrollBody>table {
            margin-top: 0px !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current,
        .dataTables_wrapper .dataTables_paginate .paginate_button.current {
            color: #000 !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current,
        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #000 !important;
        }

        .option_style {
            overflow-y: hidden;
            height: 125px;
        }

        .option_style:hover {
            overflow-y: auto;
        }

        ::-webkit-scrollbar {
            height: 8px;
            width: 8px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            border-radius: 8px;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #c5c5c5;
            border-radius: 10px;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #003B7E;
        }

        .link {
            color: #008FA1 !important;
        }

        .confirm-style {
            padding: 0.5rem 0.75rem !important;
            font-size: 0.875rem !important;
            line-height: 1 !important;
            border-radius: 0.21rem !important;
            background-color: #003b7e !important;
            color: #fff !important;
        }

        .jconfirm.jconfirm-white .jconfirm-box .jconfirm-buttons button.btn-default:hover,
        .jconfirm.jconfirm-light .jconfirm-box .jconfirm-buttons button.btn-default {
            padding: 0.5rem 0.75rem !important;
            font-size: 0.875rem !important;
            line-height: 1 !important;
            border-radius: 0.21rem !important;
            background-color: #003b7e !important;
            color: #fff !important;
        }

        .jconfirm .jconfirm-box {
            color: #003b7e;

        }

        div.dt-button-collection button.dt-button,
        div.dt-button-collection div.dt-button,
        div.dt-button-collection a.dt-button {
            border: none !important;
            background: none !important;
        }

        .checkbox label:after,
        .radio label:after {
            content: '';
            display: table;
            clear: both;
        }

        th input {
            padding-left: 5px;
            padding-right: 5px
        }

        .checkbox .cr,
        .radio .cr {
            position: relative;
            display: inline-block;
            border: 1px solid #a9a9a9;
            border-radius: .25em;
            width: 1.3em;
            height: 1.3em;
            margin-right: .5em;
        }

        .radio .cr {
            border-radius: 50%;
        }

        .checkbox .cr .cr-icon,
        .radio .cr .cr-icon {
            position: absolute;
            font-size: .8em;
            line-height: 0;
            top: 50%;
            left: 20%;
            cursor: pointer;
        }

        .radio .cr .cr-icon {
            margin-left: 0.04em;
        }

        .checkbox label input[type="checkbox"],
        .radio label input[type="radio"] {
            display: none;
        }

        .checkbox label input[type="checkbox"]+.cr>.cr-icon,
        .radio label input[type="radio"]+.cr>.cr-icon {
            transform: scale(3) rotateZ(-20deg);
            opacity: 0;
            transition: all .3s ease-in;
        }

        .checkbox label input[type="checkbox"]:checked+.cr>.cr-icon,
        .radio label input[type="radio"]:checked+.cr>.cr-icon {
            transform: scale(1) rotateZ(0deg);
            opacity: 1;
        }

        .checkbox label input[type="checkbox"]:disabled+.cr,
        .radio label input[type="radio"]:disabled+.cr {
            opacity: .5;
        }

        button.dt-button:active:not(.disabled):hover:not(.disabled),
        button.dt-button.active:not(.disabled):hover:not(.disabled),
        div.dt-button:active:not(.disabled):hover:not(.disabled),
        div.dt-button.active:not(.disabled):hover:not(.disabled),
        a.dt-button:active:not(.disabled):hover:not(.disabled),
        a.dt-button.active:not(.disabled):hover:not(.disabled) {

            background: #0073b0 !important;
            color: #fff !important;

        }

        button.dt-button:active:not(.disabled):hover,
        button.dt-button.active:not(.disabled):hover,
        div.dt-button:active:not(.disabled):hover,
        div.dt-button.active:not(.disabled):hover,
        a.dt-button:active:not(.disabled):hover,
        a.dt-button.active:not(.disabled):hover {
            background: #0073b0;
            color: #000 !important;

        }

        div.dt-button-collection button.dt-button.active:not(.disabled) {
            margin-bottom: 0px;
        }

        .dt-button:hover {
            background-color: red !important;
            color: #fff !important;
        }

        .dt-button.button-page-length:hover {
            color: black;
        }

        div.dt-button-collection button.dt-button,
        div.dt-button-collection div.dt-button,
        div.dt-button-collection a.dt-button:hover {
            color: #000 !important;
        }

        .dt-button:focus {
            background-color: red !important;
            color: #fff !important;

        }

        div.dt-button-collection button.dt-button.active:not(.disabled) {
            background: #6f6f6f !important;
            color: #fff !important;
        }

        button.dt-button:hover {
            background: #6f6f6f;
        }

        div.dt-button-collection {
            padding: 0px !important;
        }

        /*div.dt-button-collection button.dt-button.active:not(.disabled){
                background:none !important;
                color: #000 !important;
            }*/
        div.dt-button-collection {
            width: 120px;

        }

        div.dt-button-background {
            background: none;
        }

        .btn-default {
            height: 35px !important;
            padding: 0;
            width: 100px !important;
            border-radius: 25px !important;
        }

        .table td {
            padding: 0.25rem !important;
        }

        .toggle-small {
            cursor: pointer;
            text-indent: -9999px;
            width: 27px;
            height: 15px;
            background: grey;
            border-radius: 100px;
            position: relative;
        }

        .toggle-small:after {
            content: '';
            position: absolute;
            top: 3px;
            left: 2px;
            width: 9px;
            height: 9px;
            background: #fff;
            border-radius: 90px;
            transition: 0.3s;
        }

        .table th {
            padding-top: 0.4rem !important;
            padding-bottom: 0.4rem !important;
            padding-right: 0.4rem !important;
            padding-left: 0.2rem !important;
        }

        .table th,
        .table td {
            border-bottom: 0.2px solid #eff7ff !important;
        }

        label {
            margin-bottom: 0;
        }

        /* width */

        div.dt-button-collection button.dt-button,
        div.dt-button-collection div.dt-button,
        div.dt-button-collection a.dt-button {
            border: none !important;
            background: none !important;
        }

        .checkbox label:after,
        .radio label:after {
            content: '';
            display: table;
            clear: both;
        }

        th input {
            padding-left: 5px;
            padding-right: 5px
        }

        .checkbox .cr,
        .radio .cr {
            position: relative;
            display: inline-block;
            border: 1px solid #a9a9a9;
            border-radius: .25em;
            width: 1.3em;
            height: 1.3em;
            margin-right: .5em;
        }

        .radio .cr {
            border-radius: 50%;
        }

        .checkbox .cr .cr-icon,
        .radio .cr .cr-icon {
            position: absolute;
            font-size: .8em;
            line-height: 0;
            top: 50%;
            left: 20%;
            cursor: pointer;
        }

        .radio .cr .cr-icon {
            margin-left: 0.04em;
        }

        .checkbox label input[type="checkbox"],
        .radio label input[type="radio"] {
            display: none;
        }

        .checkbox label input[type="checkbox"]+.cr>.cr-icon,
        .radio label input[type="radio"]+.cr>.cr-icon {
            transform: scale(3) rotateZ(-20deg);
            opacity: 0;
            transition: all .3s ease-in;
        }

        .checkbox label input[type="checkbox"]:checked+.cr>.cr-icon,
        .radio label input[type="radio"]:checked+.cr>.cr-icon {
            transform: scale(1) rotateZ(0deg);
            opacity: 1;
        }

        .checkbox label input[type="checkbox"]:disabled+.cr,
        .radio label input[type="radio"]:disabled+.cr {
            opacity: .5;
        }

        button.dt-button:active:not(.disabled):hover:not(.disabled),
        button.dt-button.active:not(.disabled):hover:not(.disabled),
        div.dt-button:active:not(.disabled):hover:not(.disabled),
        div.dt-button.active:not(.disabled):hover:not(.disabled),
        a.dt-button:active:not(.disabled):hover:not(.disabled),
        a.dt-button.active:not(.disabled):hover:not(.disabled) {

            background: #0073b0 !important;
            color: #fff !important;

        }

        button.dt-button:active:not(.disabled):hover,
        button.dt-button.active:not(.disabled):hover,
        div.dt-button:active:not(.disabled):hover,
        div.dt-button.active:not(.disabled):hover,
        a.dt-button:active:not(.disabled):hover,
        a.dt-button.active:not(.disabled):hover {
            background: #0073b0;
            color: #000 !important;

        }

        div.dt-button-collection button.dt-button.active:not(.disabled) {
            margin-bottom: 0px;
        }

        .dt-button:hover {
            background-color: red !important;
            color: #fff !important;
        }

        .dt-button.button-page-length:hover {
            color: black;
        }

        div.dt-button-collection button.dt-button,
        div.dt-button-collection div.dt-button,
        div.dt-button-collection a.dt-button:hover {
            color: #000 !important;
        }

        .dt-button:focus {
            background-color: red !important;
            color: #fff !important;

        }

        div.dt-button-collection button.dt-button.active:not(.disabled) {
            background: #6f6f6f !important;
            color: #fff !important;
        }

        button.dt-button:hover {
            background: #6f6f6f;
        }

        div.dt-button-collection {
            padding: 0px !important;
        }

        /*div.dt-button-collection button.dt-button.active:not(.disabled){
                background:none !important;
                color: #000 !important;
            }*/
        div.dt-button-collection {
            width: 120px;

        }

        div.dt-button-background {
            background: none;
        }

        .btn-default {
            height: 35px !important;
            padding: 0;
            width: 100px !important;
            border-radius: 25px !important;
        }

        .table td {
            padding: 0.25rem !important;
        }

        .toggle-small {
            cursor: pointer;
            text-indent: -9999px;
            width: 27px;
            height: 15px;
            background: grey;
            border-radius: 100px;
            position: relative;
        }

        .toggle-small:after {
            content: '';
            position: absolute;
            top: 3px;
            left: 2px;
            width: 9px;
            height: 9px;
            background: #fff;
            border-radius: 90px;
            transition: 0.3s;
        }

        .table th {
            padding-top: 0.4rem !important;
            padding-bottom: 0.4rem !important;
            padding-right: 0.4rem !important;
            padding-left: 0.2rem !important;
        }

        .table th,
        .table td {
            border-bottom: 0.2px solid #eff7ff !important;
        }

        label {
            margin-bottom: 0;
        }

        .tb th {
            padding-top: 0.4rem !important;
            padding-bottom: 0.4rem !important;
            padding-right: 0.4rem !important;
            padding-left: 0.2rem !important;
        }

        .tb td {
            padding: 0.25rem !important;
        }

        .icon-size {
            font-size: 20px;
        }

        .table-responsive {
            height: 287px;
            overflow-y: scroll;
        }

        /* edit and delete btn styles for inline row */
        .height-color {
            font-size: 19px;
            color: #00517cf5;
            cursor: pointer;
        }

        .height-color-2 {
            font-size: 19px;
            color: #ff9aa2;
            cursor: pointer;
        }

        .edit_delete_icon {
            width: 50px;
            /* display: flex;
                justify-content: space-between; */
        }

        .table th,
        .table td {
            border-top: none !important;
        }

        /* width */

        /* vendor TEam TAB css  Ends--------------------------------------------------------------------- */
    </style>

    <style type="text/css">
        .profile-picture-upload {
            display: inline;
        }

        .imagePreview {
            vertical-align: middle;
            width: 100px;
            height: 100px;
            border-radius: 50%;
            margin-right: 8px;
            -webkit-box-shadow: 0px 3px 10px 2px rgba(0, 0, 0, 0.35);
            -moz-box-shadow: 0px 3px 10px 2px rgba(0, 0, 0, 0.35);
            box-shadow: 0px 3px 10px 2px rgba(0, 0, 0, 0.35);
        }

        .hidden {
            position: absolute;
            width: 0px;
            height: 0px;
            left: -999999px;
        }

        .action-button {
            border: 1px solid black;
            background: none;
            padding: 8px;
            cursor: pointer;
            outline: 0;
            border-radius: 6px;
        }

        .action-button.mode-upload {
            color: #004085;
            border-color: #b8daff;
            background-color: #cce5ff;
        }

        .action-button.mode-remove {
            color: #721c24;
            border: 1px solid #f5c6cb;
            background-color: #f8d7da;
        }
    </style>


    <style>
        /* Icon Style */
        .ft-plus,
        .ft-minus {
            cursor: pointer
        }

        .btn-default {
            height: 35px !important;
            padding: 0;
            width: 100px !important;
            border-radius: 25px !important;
        }

        /* Cell Editing */
        .cellEditing input[type=text] {
            width: 100%;
            border: 0;
            background-color: rgb(255, 253, 210);
        }

        /* Date Range CSS */
        .daterangepicker {
            width: 650px;
        }

        #myUL {
            padding-left: 0.5rem
        }

        .display {
            width: 100%;
            overflow-x: scroll
        }

        /* Cell Editing CSS */
        .cellEditing input[type=date] {
            width: 100%;
            border: 0;
            background-color: rgb(255, 253, 210);
        }

        .cellEditing select {
            width: 100%;
            border: 0;
            background-color: rgb(255, 253, 210);
        }

        .cellEditing input[type=number] {
            width: 100%;
            border: 0;
            background-color: rgb(255, 253, 210);
        }

        .font600 {
            font-weight: 600
        }

        /* Toggle Div */
        .toggle-div {
            position: absolute;
            z-index: 1;
            width: 260px;
            left: 162px !important;
            top: 329px !important;
            box-shadow: -6px 20px 11px rgba(0, 0, 0, 0.1);
        }

        .filter-hover:hover {
            background: #eee;
        }

        .parent .fixed_child {
            position: absolute;
        }

        /* DropZone css */
        .dropzone .dz-message {
            /* dropzone size adjustment */
            font-size: 20px;
            position: absolute;
            top: 60%;
            left: 0;
            width: 100%;
            height: 60px;
            margin-top: -30px;
            color: #0a5881;
            text-align: center;
        }

        /* Expand tr CSS */
        .child-tr {
            display: none;
        }

        .dropzone .dz-message:before {
            /* dropzone size adjustment */
            content: '\e94b';
            font-family: 'feather';
            font-size: 20px;
            position: absolute;
            top: 30px;
            width: 80px;
            height: 80px;
            display: inline-block;
            left: 50%;
            margin-left: -40px;
            line-height: 1;
            z-index: 2;
            color: #0a5881;
            text-indent: 0;
            font-weight: normal;
        }

        .custom-checkbox .custom-control-input:checked~.custom-control-label::after {
            background-color: #00517cf5 !important;
            height: 1.1rem !important;
        }

        .bgbtn {
            background-color: 00517cf5 !important;
        }

        .jcontent {
            justify-content: center;
        }

        .radio_box-style label {
            width: 100px;
            float: left;
            border: 1px solid #eee;
            margin-right: 20px;
            padding: 24px 5px 17px;
            text-align: center;
            margin-top: 10px;
            cursor: pointer;
            border-radius: 2px;
            font-size: var(--fs13px);
            position: relative;
            overflow: hidden;
        }

        .radio_box-style input[type='radio']:checked+label {
            border: 1px solid #2e71d9;
            color: #2e71d9;
        }

        .radio_box-style label:before {
            content: "";
            width: 13px;
            height: 13px;
            border: 1px solid #ccc;
            float: left;
            position: absolute;
            top: 7px;
            left: 7px;
            border-radius: 50%;
        }

        .radio_box-style label:after {
            content: "";
            width: 7px;
            height: 7px;
            float: left;
            position: absolute;
            top: 10px;
            left: 10px;
            border-radius: 50%;
        }

        .radio_box-style input[type='radio']:checked+label:after {
            background: #2e71d9;
        }

        .radio_box-style {
            margin-left: 9px;
        }

        .hl {
            margin-left: 9px;
        }

        .d6 {
            margin-left: 15px;
        }

        .bt1 {

            color: #FFF;

            font-size: var(--fs13px);
            font-weight: normal;
            text-align: center;
            cursor: pointer;
            padding: 7px 15px;
            outline: none;
            border-radius: 2px;
        }

        .sb {
            border: 1px solid;
        }

        .c1 {
            width: 200px;

        }

        .icon {
            border-radius: 50%;
            color: white;
            background-color: green;
        }

        .c2 {
            width: 400px;

        }

        #child {
            position: sticky;
            bottom: 70px;
            left: 100px;
        }

        .bg {
            background-color: #f4f5fa
        }

        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: text-top;
            border-top: 1px solid #626E82;
        }

        .tc {
            color: red;
        }



        .bg {
            background-color: #FFF !important;
            padding-bottom: 0px;
            padding-top: 10px;
            position: sticky;
            bottom: 0px;
            /*left: 100px;*/

        }


        /* Dropzone CSS */
        .dropzone {
            min-height: 95px;
            border: 2px dashed rgb(10, 88, 129);
            background: white;
            padding: 20px 20px;
        }

        .dropzone,
        .dropzone * {
            box-sizing: border-box;
        }

        /* Datatable CSS */
        table.dataTable thead th,
        table.dataTable thead td {
            min-width: 60px;
            white-space: nowrap !important;
        }

        .table thead th {
            border-right: 1px dotted #c5d5ec;
            padding-left: 20px !important;
            padding-right: 5px !important;
        }

        .JColResizer>tbody>tr>td,
        .JColResizer>tbody>tr>th {
            overflow: hidden;
            padding-left: 5px !important;
            padding-right: 5px !important;
        }

        .option-table {
            cursor: move;
        }

        .adjust_checkbox_column {
            min-width: 20px !important;
            text-align: center;
            vertical-align: middle !important;
        }

        table.dataTable thead .sorting_asc {
            background: url("http://cdn.datatables.net/1.10.0/images/sort_asc.png") no-repeat center left;
        }

        table.dataTable thead .sorting_desc {
            background: url("http://cdn.datatables.net/1.10.0/images/sort_desc.png") no-repeat center left;
        }

        table.dataTable thead .sorting {
            background: url("http://cdn.datatables.net/1.10.0/images/sort_both.png") no-repeat center left;
        }

        .toggle-div-row {
            left: 160px;
            top: 177px;
        }

        /*Freezing 1st Column and Row in Table - SOHAM*/
        #audit_history_list_view table {
            position: relative;
            border-collapse: collapse;
        }

        td,
        th {
            padding: 0.25em;
        }

        thead th {
            position: -webkit-sticky;
            /* for Safari */
            position: sticky;
            top: 0;
            background: #fff;
            color: #fff;
        }

        thead th:first-child {
            left: 0;
            z-index: 1;
        }

        .thead-custom {

            background-color: #0a5881 !important;
        }

        tbody th {
            position: -webkit-sticky;
            /* for Safari */
            position: sticky;
            left: 0;
            top: 0;
            z-index: 11;
            background: #d5e1e7;
            border-right: 1px solid #CCC;
        }

        .table-responsive {
            height: 382px;
            overflow-y: scroll
        }

        .li {
            list-style-type: none;
        }

        .btn-option-dropdown {
            width: 15px;
            height: 15px;
            background: #eee;
            border-radius: 2px;
            border: 1px solid #bbb;
            color: #bbb;
            font-size: 9px;
            line-height: 9px;
            padding: 0px;
            margin: 1px 1px 0 5px;
            float: right;

        }

        .p-option {
            font-weight: 100;
            font-size: 14px;
        }

        .filter {
            position: absolute;
            width: 200px;
            background: white;
            right: -162px;
            display: none;
            top: 35px;
            z-index: 1060;
            opacity: 1;
            box-shadow: 0px 0px 8px 0px #c5c5c5;
            padding-left: 10px;
            padding-right: 10px;
            cursor: pointer;
        }

        /* confrim js styles  ------------------------------------------------------------*/
        .confirm-style {
            padding: 0.5rem 0.75rem !important;
            font-size: 0.875rem !important;
            line-height: 1 !important;
            border-radius: 0.21rem !important;
            background-color: #00517cf5 !important;
            color: #fff !important;
        }

        .jconfirm.jconfirm-white .jconfirm-box .jconfirm-buttons button.btn-default:hover,
        .jconfirm.jconfirm-light .jconfirm-box .jconfirm-buttons button.btn-default {
            padding: 0.5rem 0.75rem !important;
            font-size: 0.875rem !important;
            line-height: 1 !important;
            border-radius: 0.21rem !important;
            background-color: #00517cf5 !important;
            color: #fff !important;
        }

        /* daterange th style  */
        .daterangepicker th {
            width: auto;
            color: #000;
        }
    </style>

    <style type="text/css">
        /*MEDIA QUERY FOR SITES TABLE*/
        @media (max-width: 599px) {
            .table-responsive {
                overflow-x: hidden !important;
                overflow-y: hidden !important;
            }
        }

        @media (min-width: 600px) and (max-width: 767px) {
            .table-responsive {
                overflow-x: hidden !important;
                overflow-y: hidden !important;
            }
        }

        @media (min-width: 768px) and (max-width: 1199px) {
            .table-responsive {
                overflow-x: hidden !important;
                overflow-y: hidden !important;
            }
        }

        /*MEDIA QUERY FOR SITES FILTERS*/
        @media (max-width: 599px) {
            #basic {
                display: none;
            }
        }

        @media (min-width: 600px) and (max-width: 767px) {
            #basic {
                display: none;
            }
        }

        @media (min-width: 768px) and (max-width: 991px) {
            .site_widget_count {
                font-size: 1.74rem;
                margin-bottom: 0;
            }

            .site_widget_count_title {
                font-size: 12px;
                white-space: nowrap;
            }

            .site_widget_count_icon {
                font-size: 19px !important;
                margin-top: 5px;
            }
        }

        @media (min-width: 992px) and (max-width: 1199px) {
            .site_widget_count {
                font-size: 2rem;
                margin-bottom: 0;
            }

            .site_widget_count_title {
                font-size: 14px;
                white-space: nowrap;
            }

            .site_widget_count_icon {
                font-size: 22px !important;
                margin-top: 5px;
            }
        }

        @media (min-width: 1200px) {
            .site_widget_count {
                font-size: 2.74rem;
                margin-bottom: 0;
            }

            .site_widget_count_icon {
                font-size: 25px !important;
                margin-top: 14px;
            }
        }
    </style>
    <!-- License Track -->
    <style>
        .pdf_icon {
            font-size: 20px !important;
            color: #00517cf5;
            cursor: pointer;
        }

        .preview {
            width: 100%;
            border: none;
        }

        .document_name_pdf {
            font-size: 16px;

        }

        .filter_div_license {
            width: 102%;
            position: absolute;
            top: -12px;
            left: -15px;
            background-color: #f4f4f4;
            z-index: 100;
            box-shadow: 0px 4px 4px #c5c5c5;
        }

        .send_email_to_karma {
            color: #4285f4 !important;
        }

        .label_mt_1 {
            margin-top: 10px !important;
        }

        .label_margin_mt {
            margin-top: 21px;
        }
    </style>
    <!--onboarding css -->
    <style>
        .progress {

            width: 84px;
            height: 83px;
            line-height: 150px;
            background: none;
            margin: 0 auto;
            margin-bottom: 0px;
            box-shadow: none;
            position: relative;

        }

        .progress:after {
            content: "";
            width: 100%;
            height: 100%;
            border-radius: 50%;
            border: 12px solid #fff;
            position: absolute;
            top: 0;
            left: 0;
        }

        .progress>span {
            width: 50%;
            height: 100%;
            overflow: hidden;
            position: absolute;
            top: 0;
            z-index: 1;
        }

        .progress .progress-left {
            left: 0;
        }

        .progress .progress-bar {
            width: 100%;
            height: 100%;
            background: none;
            border-width: 12px;
            border-style: solid;
            position: absolute;
            top: 0;
        }

        .progress .progress-left .progress-bar {
            left: 100%;
            border-top-right-radius: 80px;
            border-bottom-right-radius: 80px;
            border-left: 0;
            -webkit-transform-origin: center left;
            transform-origin: center left;
        }

        .progress .progress-right {
            right: 0;
        }

        .progress .progress-right .progress-bar {
            left: -100%;
            border-top-left-radius: 80px;
            border-bottom-left-radius: 80px;
            border-right: 0;
            -webkit-transform-origin: center right;
            transform-origin: center right;
            animation: loading-1 1.8s linear forwards;
        }

        .progress .progress-value {

            width: 91%;
            height: 90%;
            border-radius: 50%;
            background:

                #44484b;

            font-size: 15px;

            color:

                #fff;
            line-height: 86px;
            text-align: center;
            position: absolute;
            top: 2%;
            left: 5%;

        }

        .progress.blue .progress-bar {
            border-color: #049dff;
        }

        .progress.blue .progress-left .progress-bar {
            animation: loading-2 1.5s linear forwards 1.8s;
        }

        .progress.yellow .progress-bar {
            border-color: #fdba04;
        }

        .progress.yellow .progress-left .progress-bar {
            animation: loading-3 1s linear forwards 1.8s;
        }

        .progress.pink .progress-bar {
            border-color: #ed687c;
        }

        .progress.pink .progress-left .progress-bar {
            animation: loading-4 0.4s linear forwards 1.8s;
        }

        .progress.green .progress-bar {
            border-color: #1abc9c;
        }

        .progress.green .progress-left .progress-bar {
            animation: loading-5 1.2s linear forwards 1.8s;
        }

        @keyframes loading-1 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(180deg);
                transform: rotate(180deg);
            }
        }

        @keyframes loading-2 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(144deg);
                transform: rotate(144deg);
            }
        }

        @keyframes loading-3 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(90deg);
                transform: rotate(90deg);
            }
        }

        @keyframes loading-4 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(36deg);
                transform: rotate(36deg);
            }
        }

        @keyframes loading-5 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(126deg);
                transform: rotate(126deg);
            }
        }

        @media only screen and (max-width: 990px) {
            .progress {
                margin-bottom: 20px;
            }
        }
    </style>
    <!-- KYC form -->
    <style type="text/css">
        .padding-right {
            padding-right: unset;
        }

        .padding-left {
            padding-left: unset;
        }

        .width-set {
            width: 100% !important;
        }

        @media(max-width: 767px) {
            .padding-right {
                padding-right: 15px !important;
            }

            .padding-left {
                padding-left: 15px !important;
            }

            .width-set {
                width: -moz-available !important;
            }

            .text_Space {
                padding-top: 5px !important;
                padding-bottom: 0px !important;
                padding-left: 10px !important;
                padding-right: 10px !important;
            }
        }

        @media(max-width:575px) {
            .xs-1 {
                -moz-box-flex: 0;
                flex: 0 0 14%;
                max-width: 14%;
            }

            .xs-2 {
                -moz-box-flex: 0;
                flex: 0 0 30%;
                max-width: 30%;
            }

            .xs-3 {
                -moz-box-flex: 0;
                flex: 0 0 35%;
                max-width: 35%;
            }

            .xs-4 {
                -moz-box-flex: 0;
                flex: 0 0 85%;
                max-width: 85%;
            }
        }

        @media(max-width:1450px) {
            .NoRadio {
                margin-left: -12% !important
            }
        }

        @media(max-width:991px) {
            .NoRadio {
                margin-left: 0% !important
            }
        }

        @media(max-width:575px) {
            .NoRadio {
                margin-left: 0% !important
            }
        }


        .paperClip {
            vertical-align: -moz-middle-with-baseline;
        }

        .BottomSpace {
            margin-bottom: 3% !important;
        }
    </style>
    <!-- profile css -->
    <style>
        .My_btn {
            width: 60px;
            height: 60px !important;
            border-radius: 50%;

        }

        .states_label {
            display: flex;
            align-items: center;
            justify-content: flex-end;
        }

        .figure-caption {
            margin-top: 6%;
        }

        .col-lg-8 {
            padding: 1%
        }

        .text_Space {
            padding: 10px;
        }

        .labelmargin-top {
            margin-top: 8px;
        }

        /* .col-sm-12 {
            margin-top: 0rem !important;
        } */
        .remove_element {
            margin-top: 7px
        }

        .state_multiple_remove_element {
            margin-top: 21px
        }

        .default_filter_element {
            display: none;
        }
    </style>
    <!-- vendor user list view -->
    <style>
        .img {
            object-fit: cover;
            border-radius: 50%;
            height: 75px;
            width: 75px;
        }

        .col-padding-top-bottom-yu {
            padding-top: 5px;
            padding-bottom: 5px
        }

        .icon-div {
            position: absolute;
            top: 6px;
            right: 16px;
            display: none;
        }

        .action-profile {
            display: none;
        }


        .box:hover .action-profile {
            display: inline;
        }
    </style>
    <!-- onboard css -->
    <style>
        #circle1:hover {
            background-color: #f26229;
        }

        .checkbox .cr .cr-icon,
        .radio .cr .cr-icon {
            font-size: .6em !important;
        }

        #circle2:hover {
            background-color: #f26229;
        }

        #circle3:hover {
            background-color: #f26229;
        }

        #circle4:hover {
            background-color: #f26229;
        }

        #circle5:hover {
            background-color: #f26229;
        }

        .bg-color {
            background-color: #f26229 !important;
        }

        .center {
            display: flex;
            align-items: center;
            list-style: circle
        }

        .dot {
            height: 12px;
            width: 12px;
            background-color: #7c7c7c;
            border-radius: 50%;
            display: inline-block;
            position: absolute;
            margin-top: 6%;
        }

        .line {
            display: inline-block;
            height: 10;
            border: 0;
            border-top: 3px solid #eeeeee;
            /* YOUR COLOR HERE */
            margin: 1em 0;
            padding: 0;
            width: 116%;
            margin-left: -60.7%;
            position: absolute;
        }

        @media(max-width:534px) {
            .timeline-img {
                width: 37px !important;
            }
        }

        .checkbox .cr,
        .radio .cr {
            width: 1em !important;
            height: 1em !important;
            margin-right: .2em !important;
        }

        @media(max-width:604px) {
            .timeline-text {
                font-size: 14px !important;
                margin-bottom: 10% !important
            }
        }

        .timeline-text {
            margin-bottom: 2%;
            text-align: center;
            font-size: 18px
        }

        .timeline-img {
            width: 70px;
        }

        .padding {
            padding-right: unset !important;
            padding-left: unset !important
        }

        @media(max-width:940px) {
            .dot {
                margin-top: 8% !important
            }
        }

        @media(max-width:780px) {
            .dot {
                margin-top: 10% !important
            }
        }

        @media (min-width: 992px) and (max-width: 1199px) {
            .dot {
                margin-top: 8% !important
            }
        }

        @media (min-width: 1200px) and (max-width: 1299px) {
            .dot {
                margin-top: 8% !important
            }
        }

        @media (min-width: 1300px) and (max-width: 1499px) {
            .dot {
                margin-top: 6% !important
            }
        }

        @media (min-width: 1500px) and (max-width: 1699px) {
            .dot {
                margin-top: 5.3% !important
            }
        }

        @media (min-width: 1700px) and (max-width: 1799px) {
            .dot {
                margin-top: 4.3% !important
            }
        }

        @media (min-width: 1800px) {
            .dot {
                margin-top: 4.3% !important
            }
        }

        .wecolcome-header {
            text-align: center;
            font-size: 312%;
            color: #4e4e51;


        }

        .wecolcome-slogan {
            text-align: center;
            font-size: 132%;
            color: #1c2a75;


        }

        .wecolcome-body {
            font-size: 138%;
            color: #1c2a75;

        }

        .wecolcome-sub-content-header {
            color: #f26229;
            text-decoration: underline;
        }

        .welcome-background {
            background: linear-gradient(#fff, #ffa27e);

        }

        .progress {

            width: 84px;
            height: 83px;
            line-height: 150px;
            background: none;
            margin: 0 auto;
            margin-bottom: 0px;
            box-shadow: none;
            position: relative;

        }

        .progress:after {
            content: "";
            width: 100%;
            height: 100%;
            border-radius: 50%;
            border: 12px solid #fff;
            position: absolute;
            top: 0;
            left: 0;
        }

        .progress>span {
            width: 50%;
            height: 100%;
            overflow: hidden;
            position: absolute;
            top: 0;
            z-index: 1;
        }

        .progress .progress-left {
            left: 0;
        }

        .progress .progress-bar {
            width: 100%;
            height: 100%;
            background: none;
            border-width: 12px;
            border-style: solid;
            position: absolute;
            top: 0;
        }

        .progress .progress-left .progress-bar {
            left: 100%;
            border-top-right-radius: 80px;
            border-bottom-right-radius: 80px;
            border-left: 0;
            -webkit-transform-origin: center left;
            transform-origin: center left;
        }

        .progress .progress-right {
            right: 0;
        }

        .progress .progress-right .progress-bar {
            left: -100%;
            border-top-left-radius: 80px;
            border-bottom-left-radius: 80px;
            border-right: 0;
            -webkit-transform-origin: center right;
            transform-origin: center right;
            animation: loading-1 1.8s linear forwards;
        }

        .progress .progress-value {

            width: 91%;
            height: 90%;
            border-radius: 50%;
            background:

                #44484b;

            font-size: 15px;

            color:

                #fff;
            line-height: 86px;
            text-align: center;
            position: absolute;
            top: 2%;
            left: 5%;

        }

        .progress.blue .progress-bar {
            border-color: #049dff;
        }

        .progress.blue .progress-left .progress-bar {
            animation: loading-2 1.5s linear forwards 1.8s;
        }

        .progress.yellow .progress-bar {
            border-color: #fdba04;
        }

        .progress.yellow .progress-left .progress-bar {
            animation: loading-3 1s linear forwards 1.8s;
        }

        .progress.pink .progress-bar {
            border-color: #ed687c;
        }

        .progress.pink .progress-left .progress-bar {
            animation: loading-4 0.4s linear forwards 1.8s;
        }

        .progress.green .progress-bar {
            border-color: #1abc9c;
        }

        .progress.green .progress-left .progress-bar {
            animation: loading-5 1.2s linear forwards 1.8s;
        }

        @keyframes loading-1 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(180deg);
                transform: rotate(180deg);
            }
        }

        @keyframes loading-2 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(144deg);
                transform: rotate(144deg);
            }
        }

        @keyframes loading-3 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(90deg);
                transform: rotate(90deg);
            }
        }

        @keyframes loading-4 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(36deg);
                transform: rotate(36deg);
            }
        }

        @keyframes loading-5 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(126deg);
                transform: rotate(126deg);
            }
        }

        @media only screen and (max-width: 990px) {
            .progress {
                margin-bottom: 20px;
            }
        }

        .size-color {
            font-size: 100px;
            color: #192133;
        }
    </style>
    <!-- vendor widget -->
    <style>
        .line-arrange {
            margin-left: -1%
        }

        .min-max-3 {
            max-width: 1213px;
            min-width: 1231px;
            margin-left: 27px;
            margin-bottom: 15px;

        }

        .minimal select {

            /* styling */
            background-color: white;
            border: thin solid #0a5881;
            border-radius: 4px;
            display: inline-block;

            line-height: 1.5em;
            padding: 0.5em 3.5em 0.5em 1em;

            /* reset */

            margin: 0;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-appearance: none;
            -moz-appearance: none;
        }

        select.minimal {
            background-image:
                linear-gradient(45deg, transparent 50%, #0a5881 50%),
                linear-gradient(135deg, #0a5881 50%, transparent 50%),
                linear-gradient(to right, #ccc, #ccc);
            background-position:
                calc(100% - 20px) calc(1em + 2px),
                calc(100% - 15px) calc(1em + 2px),
                calc(100% - 2.5em) 0.5em;
            background-size:
                5px 5px,
                5px 5px,
                1px 1.5em;
            background-repeat: no-repeat;
        }

        select.minimal:focus {
            background-image:
                linear-gradient(45deg, #0a5881 50%, transparent 50%),
                linear-gradient(135deg, transparent 50%, #0a5881 50%),
                linear-gradient(to right, #ccc, #ccc);
            background-position:
                calc(100% - 15px) 1em,
                calc(100% - 20px) 1em,
                calc(100% - 2.5em) 0.5em;
            background-size:
                5px 5px,
                5px 5px,
                1px 1.5em;
            background-repeat: no-repeat;
            border-color: #0a5881;
            outline: 0;
        }


        select:-moz-focusring {
            color: transparent;
            text-shadow: 0 0 0 #000;
        }


        /* widget styles */
        .count_in_widget {
            font-size: 35px;
            font-weight: 600;
        }

        .star {
            color: green
        }

        .star3 {
            color: red;
        }

        .star2 {
            color: orange;
        }

        .dropdown_height {
            height: 30px;
        }

        .margin-left-30 {
            margin-left: -30px;
        }
    </style>
    <!-- add vendor landing page -->

    <style>
        /* vendor_kyc_styles------------------------------------------------------------------------------- */

        /* paper icon styles and postion */
        .info-1 {
            color: #00517cf5;
        }

        .preview_text {
            margin-top: 10px;
            float: right;
            display: none;
            cursor: pointer;
        }

        .icon-bg-style {
            background: whitesmoke;
            width: 35px;
            border-radius: 50%;
            cursor: pointer;
        }

        .form-control-position {
            position: absolute;
            top: 1px;
            right: 0;
            z-index: 2;
            display: block;
            width: 2.5rem;
            height: 2.5rem;
            line-height: 3.2rem;
            right: 1rem;
        }

        .ip_sub_cls {
            margin-left: 10px;
        }
    </style>
    <style type="text/css">
        @media(max-width:575px) {
            .xs-1 {
                -moz-box-flex: 0;
                flex: 0 0 14%;
                max-width: 14%;
            }

            .xs-2 {
                -moz-box-flex: 0;
                flex: 0 0 30%;
                max-width: 30%;
            }

            .xs-3 {
                -moz-box-flex: 0;
                flex: 0 0 35%;
                max-width: 35%;
            }

            .xs-4 {
                -moz-box-flex: 0;
                flex: 0 0 85%;
                max-width: 85%;
            }
        }

        .label_mt {
            margin-top: 8px;
        }

        .radio {
            margin: 0.5rem;
        }

        @media(max-width:1450px) {
            .NoRadio {
                margin-left: -12% !important
            }
        }

        @media(max-width:991px) {
            .NoRadio {
                margin-left: 0% !important
            }
        }

        @media(max-width:575px) {
            .NoRadio {
                margin-left: 0% !important
            }
        }


        .paperClip {
            vertical-align: -moz-middle-with-baseline;
        }

        .BottomSpace {
            margin-bottom: 3% !important;

        }

        .la-key {
            color: #00517cf5;
        }
    </style>
    <!-- vendor team page css -->
    <style>
        .table-test {
            width: 98% !important;
        }

        .mt-9 {
            margin-top: 9px
        }

        .filter_div_team {
            width: 102%;
            position: absolute;
            top: -12px;
            left: -15px;
            background-color: #f4f4f4;
            z-index: 100;
            box-shadow: 0px 4px 4px #c5c5c5;
        }
    </style>
    <!-- vendor main list view css -->
    <style>
        .table-test {
            width: 98% !important;
        }

        .filter_div_team {
            width: 102%;
            position: absolute;
            top: -12px;
            left: -15px;
            background-color: #f4f4f4;
            z-index: 100;
            box-shadow: 0px 4px 4px #c5c5c5;
        }
    </style>
    <!-- timeline css -->
    <style>
        .bg-color {
            color: #f26229 !important
        }

        @media (max-width: 600px) {
            .timeline-horizontal {
                padding-left: 0%
            }

            .timeline-option-1 {
                min-width: 75px !important;
            }

            .timeline-option-2 {
                min-width: 75px !important;
            }

            .timeline-option-3 {
                min-width: 75px !important;
            }

            .timeline-option-4 {
                min-width: 75px !important;
            }

            .timeline-option-5 {
                min-width: 124px;
            }

            .timeline-horizontal:before {
                left: 34px;
                width: 75%
            }
        }

        @media (min-width: 600px) and (max-width: 767px) {
            .timeline-horizontal {
                padding-left: 0%
            }

            .timeline-option-1 {
                min-width: 135px !important;
            }

            .timeline-option-2 {
                min-width: 135px !important;
            }

            .timeline-option-3 {
                min-width: 135px !important;
            }

            .timeline-option-4 {
                min-width: 135px !important;
            }

            .timeline-option-5 {
                min-width: 124px;
            }

            .timeline-horizontal:before {
                left: 34px;
                width: 68%
            }
        }

        @media (min-width: 768px) and (max-width: 991px) {
            .timeline-horizontal {
                padding-left: 0%
            }

            .timeline-option-1 {
                min-width: 135px !important;
            }

            .timeline-option-2 {
                min-width: 135px !important;
            }

            .timeline-option-3 {
                min-width: 135px !important;
            }

            .timeline-option-4 {
                min-width: 135px !important;
            }

            .timeline-option-5 {
                min-width: 124px;
            }

            .timeline-horizontal:before {
                left: 35px;
                width: 79%
            }
        }

        @media (min-width: 992px) and (max-width: 1199px) {
            .timeline-horizontal {
                padding-left: 2%
            }

            .timeline-option-1 {
                min-width: 200px !important;
            }

            .timeline-option-2 {
                min-width: 200px !important;
            }

            .timeline-option-3 {
                min-width: 200px !important;
            }

            .timeline-option-4 {
                min-width: 200px !important;
            }

            .timeline-option-5 {
                min-width: 124px;
            }

            .timeline-horizontal:before {
                left: 50px;
                width: 83%
            }
        }

        @media (min-width: 1200px) and (max-width: 1499px) {
            .timeline-horizontal {
                padding-left: 4%
            }

            .timeline-option-1 {
                min-width: 180px !important;
            }

            .timeline-option-2 {
                min-width: 180px !important;
            }

            .timeline-option-3 {
                min-width: 180px !important;
            }

            .timeline-option-4 {
                min-width: 180px !important;
            }

            .timeline-option-5 {
                min-width: 180px;
            }

            .timeline-horizontal:before {
                left: 86px;
                width: 64%;
                top: 50px;
            }
        }

        @media (min-width: 1500px) and (max-width: 1799px) {
            .timeline-horizontal {
                padding-left: 2%
            }

            .timeline-option-1 {
                min-width: 250px !important;
            }

            .timeline-option-2 {
                min-width: 250px !important;
            }

            .timeline-option-3 {
                min-width: 250px !important;
            }

            .timeline-option-4 {
                min-width: 250px !important;
            }

            .timeline-option-5 {
                min-width: 250px;
            }
        }

        @media (min-width: 1800px) {
            .timeline-horizontal {
                padding-left: 2%
            }

            .timeline-option-1 {
                min-width: 200px !important;
            }

            .timeline-option-2 {
                min-width: 200px !important;
            }

            .timeline-option-3 {
                min-width: 200px !important;
            }

            .timeline-option-4 {
                min-width: 200px !important;
            }

            .timeline-option-5 {
                min-width: 124px;
            }
        }
    </style>
    <!-- escalation css -->
    <style>
        /* vendor_kyc_styles------------------------------------------------------------------------------- */
        /* paper icon styles and postion */
        .info-1 {
            color: #00517cf5;
        }

        #preview_mode_1,
        #preview_mode_2 {
            cursor: pointer
        }

        .icon-bg-style {
            background: whitesmoke;
            width: 35px;
            border-radius: 50%;
            cursor: pointer;
        }

        .form-control-position {
            position: absolute;
            top: 1px;
            right: 0;
            z-index: 2;
            display: block;
            width: 2.5rem;
            height: 2.5rem;
            line-height: 3.2rem;
            right: 1rem;
        }

        .ip_sub_cls {
            margin-left: 10px;
        }
    </style>
    <style type="text/css">
        @media(max-width:575px) {
            .xs-1 {
                -moz-box-flex: 0;
                flex: 0 0 14%;
                max-width: 14%;
            }

            .xs-2 {
                -moz-box-flex: 0;
                flex: 0 0 30%;
                max-width: 30%;
            }

            .xs-3 {
                -moz-box-flex: 0;
                flex: 0 0 35%;
                max-width: 35%;
            }

            .xs-4 {
                -moz-box-flex: 0;
                flex: 0 0 85%;
                max-width: 85%;
            }
        }

        @media(max-width:1450px) {
            .NoRadio {
                margin-left: -12% !important
            }
        }

        @media(max-width:991px) {
            .NoRadio {
                margin-left: 0% !important
            }
        }

        @media(max-width:575px) {
            .NoRadio {
                margin-left: 0% !important
            }
        }


        .paperClip {
            vertical-align: -moz-middle-with-baseline;
        }

        .BottomSpace {
            margin-bottom: 3% !important;

        }
    </style>
    <!-- Start of Preview CSS -->
    <style type="text/css">
        .preview {
            width: 100%;
            border: none;
        }

        .comment_box {
            width: 300px !important;
        }

        .default_access {
            width: 250px !important;
        }
    </style>
    <!-- End of Preview CSS -->


    <!-- Profile Photo CSS -->
    <style type="text/css">
        .imageThumb {
            vertical-align: middle;
            width: 100px;
            height: 100px;
            border-radius: 50%;
            margin-right: 8px;
            -webkit-box-shadow: 0px 3px 10px 2px rgba(0, 0, 0, 0.35);
            -moz-box-shadow: 0px 3px 10px 2px rgba(0, 0, 0, 0.35);
            box-shadow: 0px 3px 10px 2px rgba(0, 0, 0, 0.35);
        }

        .pip {
            margin: 10px 10px 0 0;
        }

        .remove_button {
            background-color: #fff;
            width: 30px;
            height: 30px;
            border-radius: 50%;
            position: absolute;
            top: 110px;
            left: 50px;
            padding-top: 2px;
        }

        .remove_button:hover {
            background: white;
            color: black;
            cursor: pointer;
        }

        .hidden {
            display: none;
        }

        .add_button {
            background-color: #fff;
            width: 30px;
            height: 30px;
            border-radius: 50%;
            position: absolute;
            top: 76px;
            left: 100px;
            padding-top: 2px;
        }
        .common_btn{
            width:35px;border-radius:50%;border:none
        }
        #buttonid {
            padding-top: 3px;
            padding-left: 3px;
            font-size: 24px;
            color: #003b7e;
        }

        #remove_button_icon {
            padding-top: 3px;
            padding-left: 3px;
            font-size: 24px;
            color: #c5c5c5;
        }

        @media (max-width: 600px) {
            .user_email {
                font-size: 12px;
            }

            .btn-width {
                margin-top: 10px
            }
        }

        .site-icon {
            font-size: 20px;
            color: orange;
        }

        .table_buttons {
            width: 35px;
            border-radius: 50%;
        }

        #socialShare {
            /* width: 100%; */
            margin-top: -4px;
            text-align: center;
            margin-right: 12px;
        }
    </style>
    <!-- download button css -->
    <style>
        #socialShare {
            /* width: 100%; */
            margin-top: -4px;
            text-align: center;
            /* margin-right: 18px; */
        }

        #socialShare a,
        #socialShare>.socialBox {
            position: relative;
            float: none;
            display: inline-block;
            color: #fff;
            font-size: 20px;
            padding: 5px;
            background-color: transparent;
            width: 35px;
            height: 35px;
            /* line-height: 40px; */
            text-align: center;
            border-radius: 50%;
        }

        #socialShare a {
            background-color: rgba(0, 0, 0, 0.2);
        }

        #socialShare>*>span {
            background-color: #00517cf5;
            box-shadow: 0 0 0 #00517cf5;
            display: block;
            color: #fff;
            font-size: 16px;
            padding: 8px;
            width: 35px;
            height: 35px;
            line-height: 18px;
            text-align: center;
            border-radius: 50%;
            -webkit-transform: scale(1);
            -moz-transform: scale(1);
            transform: scale(1);
            -webkit-transition: all .35s ease-in-out;
            -moz-transition: all .35s ease-in-out;
            transition: all .35s ease-in-out;

            /* margin-top: -4px; */
        }

        #socialGallery {
            /* left: -8%; */
            margin: 0 auto 0;
            position: absolute;
            top: 0;
            transform: translate(-50%, 0);
            visibility: hidden;
            width: 400px;
            left: -45px;
        }

        #socialGallery a {
            visibility: hidden;
            opacity: 0;
            margin: 5px 2px;
            background-color: #00517cf5;
            position: relative;
            top: 10px;
        }

        #socialGallery a>span {
            position: relative;
            top: 4px;
            left: 4px;
            border-radius: 50%;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
        }

        .socialToolBox {
            cursor: default;
        }

        .pointer {
            cursor: pointer
        }

        @media only screen and (max-width: 480px) {
            #socialGallery {
                width: 120px;
            }
        }

        .font-size-46 {
            font-size: 46px;
        }
    </style>
    <!-- Status Change CSS -->
    <style type="text/css">
        .dropdown-menu>li {
            position: relative;
            margin-bottom: 1px;
        }

        .dropdown-menu>li>a {
            padding: 8px 10px;
            outline: 0;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .dropdown-menu>li>a {
            display: block;
            padding: 3px 20px;
            clear: both;
            font-weight: normal;
            line-height: 1.5384616;
            color: #333333;
            white-space: nowrap;
        }

        .btn-group,
        .btn-group-vertical {
            position: relative;
            display: inline-block;
            vertical-align: middle;
        }

        .bg-blue {
            background-color: #03A9F4;
            border-color: #03A9F4;
            color: #fff;
        }

        .bg-green {
            background-color: #a1d36e;
            border-color: #a1d36e;
            color: #fff;
        }

        .bg-orange {
            background-color: #FF9800;
            border-color: #FF9800;
            color: #fff;
        }

        .bg-danger {
            background-color: #F44336;
            border-color: #F44336;
            color: #fff;
        }

        .bg-success {
            background-color: #4CAF50 !important;
            border-color: #4CAF50;
            color: #fff;
        }

        .status-label {
            font-weight: 500;
            padding: 2px 5px 1px 5px;
            line-height: 1.5384616;
            border: 1px solid transparent;
            /* text-transform: uppercase; */
            font-size: 12px;
            letter-spacing: 0.1px;
            border-radius: 0.25rem;
            width: 90px;
            display: flex;
            justify-content: flex-end;
            align-items: center;
            padding: 2px;
        }

        .status-label:hover {
            color: #fff;
        }

        /*Dropdown Option*/
        .border-danger {
            border-color: #F44336;
        }

        .border-success {
            border-color: #4CAF50;
        }

        .border-warning {
            border-color: #FF5722;
        }

        .border-blue {
            border-color: #03A9F4;
        }

        .position-left {
            margin-right: 7px;
        }

        .status-mark {
            width: 8px;
            height: 8px;
            display: inline-block;
            border-radius: 50%;
            border: 2px solid;
        }
        .bg-pending {
            background-color: #568dd5;
            border-color: #568dd5;
            color: white;
        }

        .border-pending {
            border: 1px solid #568dd5;
        }
    </style>
    <!-- END Status Change CSS -->