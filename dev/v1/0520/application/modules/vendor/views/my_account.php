<?php $this->load->view('header'); ?>
<?php $this->load->view('top'); ?>
<?php $this->load->view('vertical_navbar'); ?>
<?php $this->load->view('vendor/vendor_css'); ?>

<style type="text/css">
    #circle1 {
        background-color: green
    }
</style>
<div style="width:103%">
    <div class="col">
       
        <br>
        <div class="card-yu p-2">
            <div>
                <div class="card-header pt-0">
                    <h4 class="card-title"><i class="la la-angle-left mr-1 back-button"></i>My Account</h4>
                </div>
                <div class="card-content">
                    <div id="profile" class="tabcontent" style="display: block">
                        <div class="ml-2">
                            <div class="row">
                                <div class="col-lg-2 col-md-3 col-sm-3 col-4 profile-picture-upload">
                                    <div class="field" align="left">
                                        <input id="files" type="file" hidden />
                                        <a class="add_button" type="button" value="Add Avtar">
                                            <i id="buttonid" class="ft-camera" title="Add Image"></i>
                                        </a>
                                    </div>
                                    <span class="pip">
                                        <img class="imageThumb" src="<?= base_url() ?>app-assets/images/placeholder_avatar.png" title="" />
                                        <br />
                                        <span class="remove_button hidden">
                                            <i id="remove_button_icon" class="ft-trash-2"></i>
                                        </span>
                                    </span>
                                </div>
                                <div class="col-lg-8 col-md-6 col-sm-6 col-8">
                                    <h3 class="">Purushothaman TR.</h3>
                                    <h5 class="user_email">purushothaman.tr@yashussunlimited.com</h5>
                                </div>

                                <div class="col-lg-2 col-md-2 col-sm-3 col-12" style="text-align: right;">
                                    <button class="btn-yu primary My_btn" id="edit" data-toggle="modal" title="Add Employee" data-target=".datamodal"><i class="la la-edit"></i></button>
                                </div>
                            </div>
                            <hr>
                            <br>
                            <div class=" pb-1" style="border-radius:10px">
                                <div class="profile" style="margin-left: -1rem;">
                                    <div class="row text_Space">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <label style="font-size:16px" id="a" class="mb-0">First Name</label>
                                                </div>
                                                <div class="col-lg-7">
                                                    <h4 style="font-size:15px;margin-top:4px" id="first_name">Purushothaman</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="row">
                                                <div class="col-lg-4"><label style="font-size:16px" class="mb-0">Email Address</label></div>
                                                <div class="col-lg-7">
                                                    <h4 style="font-size:15px;margin-top:4px" id="user_email">purushothaman.tr@yashussunlimited.com</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row text_Space">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <label style="font-size:16px" id="a" class="mb-0">Last Name</label>
                                                </div>
                                                <div class="col-lg-7">
                                                    <h4 style="font-size:15px;margin-top:4px" id="last_name">TR</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <label style="font-size:16px" class="mb-0">Username</label>
                                                </div>
                                                <div class="col-lg-7">
                                                    <h4 style="font-size:15px;margin-top:4px" id="username">karven1</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row text_Space">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <label style="font-size:16px" class="mb-0">Contact</label>
                                                </div>
                                                <div class="col-lg-7">
                                                    <h4 style="font-size:15px;margin-top:4px" id="contact"> 9821836917</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- edit modal--------------------------------------------------------- -->
<div class="modal fade text-left datamodal" id="edit_profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="" id="edit_profile_form">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel16">Edit Profile</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-4 col-md-3">
                                        <label for="name_field_first" style="margin-top:20px">First Name</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                        <input type="text" class="form-control " name="" id="name_field_first" value="Purushothaman">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-4 col-md-3">
                                        <label for="name_field_last" style="margin-top:20px">Last Name</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                        <input type="text" class="form-control" name="" id="name_field_last" value="TR">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-4 col-md-3">
                                        <label for="email_field" style="margin-top:20px">Email ID</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                        <input type="text" class="form-control" name="" id="email_field" value="purushothaman.tr@yashussunlimited.com">
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-4 col-md-3">
                                        <label for="username_ip" style="margin-top:20px">Username</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                        <input type="text" class="form-control " name="" id="username_ip" value="karven1">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-4 col-md-3">
                                        <label for="contact_field" style="margin-top:20px">Contact</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                        <input type="text" class="form-control " name="" id="contact_field" value="9821836917">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-yu btn-width primary text-white save_btn_edit_profile" data-dismiss="modal" onclick="update_profile()">Save</button>
                    <button type="button" class="btn-yu btn-width primary text-white mr-2" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>
<!-- vendor module js -->
<?php $this->load->view('vendor/vendor_js'); ?>


<script>
    $(".back-button").click(function(){
        window.location.href = "<?= base_url()?>dashboard/Dashboard_controller/landing_page";
    });
</script>