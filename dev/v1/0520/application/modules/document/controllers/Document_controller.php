<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document_controller extends MY_Controller {


    public function __construct()
    {
        parent::__construct();

    }

    // Login screen for user

    public function document_view(){

        $this->load->view('my_document');
    }

    public function request_list(){

        $this->load->view('request_list');
    }

    public function recordtoreport(){

        $this->load->view('recordtoreport');
    }
    
    public function document_unclassified(){

        $this->load->view('unclassified_view');
    }

    public function fpa(){

        $this->load->view('fpa');
    }
    public function know_your_format()
    {
        $this->load->view('know_your_format');
    }
    public function document_history(){
        $this->load->view('document_history');
    }

    public function document_preview(){
        $this->load->view('document_preview');
    }
}
