   <script type="text/javascript">
       function change_status(_this, status_id, status_name) {
           if (status_name == "Approve") {
               $.confirm({
                   title: 'Approve!',
                   type: 'orange',
                   content: 'Do you want to ?',
                   buttons: {
                       Approve: function() {
                           $.alert({
                               title: '',
                               content: 'Approved',
                               type: 'green',
                           });
                       },
                       cancel: function() {
                           $.alert({
                               title: '',
                               content: 'Canceled',
                               type: 'red',
                           });
                       },
                   }
               });

               className = "status-label bg-green dropdown-toggle";
               $(_this).parent().parent().find('.Approve _option').css('display', 'none').siblings('li').css('display', 'block');
           } else if (status_name == "Query") {
               $.confirm({
                   title: 'Query',
                   content: '' +
                       '<form action="" class="formName">' +
                       '<div class="form-group">' +
                       '<label>Remark</label>' +
                       '<input type="text" placeholder="Your remark" class="name form-control" required />' +
                       '</div>' +
                       '</form>',
                   buttons: {
                       formSubmit: {
                           text: 'Submit',
                           action: function() {
                               var name = this.$content.find('.name').val();
                               if (!name) {
                                   $.alert('provide a valid remark');
                                   return false;
                               }
                               $.alert({
                                   title: '',
                                   content: 'Status updated',
                                   type: 'green',
                               });
                           }
                       },
                       cancel: function() {
                           //close
                           $.alert({
                               title: '',
                               content: 'Canceled',
                               type: 'red',
                           });
                       },
                   },
               });
               className = "status-label bg-orange dropdown-toggle";
               $(_this).parent().parent().find('.query_option').css('display', 'none').siblings('li').css('display', 'block');
           } else if (status_name == "Reject") {
               $.confirm({
                   title: 'Reject',
                   content: '' +
                       '<form action="" class="formName">' +
                       '<div class="form-group">' +
                       '<label>Remark</label>' +
                       '<input type="text" placeholder="Your remark" class="reject form-control" required />' +
                       '</div>' +
                       '</form>',
                   buttons: {
                       formSubmit: {
                           text: 'Submit',
                           action: function() {
                               var name = this.$content.find('.reject').val();
                               if (!name) {
                                   $.alert('provide a valid remark');
                                   return false;
                               }
                               $.alert({
                                   title: '',
                                   content: 'Status updated',
                                   type: 'green',
                               });
                           }
                       },
                       cancel: function() {
                           //close
                           $.alert({
                               title: '',
                               content: 'Canceled',
                               type: 'red',
                           });
                       },
                   },
               });
               className = "status-label bg-danger dropdown-toggle";
               $(_this).parent().parent().find('.reject_option').css('display', 'none').siblings('li').css('display', 'block');
           } else if (status_name == "Pending") {
               className = "status-label bg-pending dropdown-toggle";
               $(_this).parent().parent().find('.complete_option').css('display', 'none').siblings('li').css('display', 'block');
           }

           if (status_name == "Active") {
               $("#" + status_id).html(status_name + "&nbsp;&nbsp;<span class='caret'></span>");

           } else {
               $("#" + status_id).html(status_name + "<span class='caret'></span>");
           }
           $("#" + status_id).removeClass();
           $("#" + status_id).addClass(className);
       }

       function unable_upload_field(upload_field_class) {
           $(".upload_file_" + upload_field_class).click();

           $(".upload_file_" + upload_field_class).change(function(e) {
               var fileName = e.target.files[0].name;

               $("#file_name_display_" + upload_field_class).text(fileName);
           });
       }

       $(document).ready(function() {
           // DataTable Assignment to My Document View Table-------------------------------------------------------------------------------
           var table = $('#document_grid_view_table').DataTable({
               orderCellsTop: true,
               fixedHeader: true,
               "columnDefs": [{
                   "targets": 0,
                   "orderable": false,
               }],
               "bStateSave": true,
               'aaSorting': [
                   [1, 'asc']
               ],
               dom: 'Bf<"table-responsive"t>ip',
               buttons: [
                   <?php
                    if (isset($_GET['upload_confirmation'])) {
                        if ($_GET['upload_confirmation'] == "yes") {
                    ?> {
                               className: 'delete_button document_delete_icon',
                               text: '<i class="la la-trash"></i>',
                               titleAttr: 'Delete All',

                           },
                   <?php
                        }
                    }
                    ?> {
                       className: 'export_button filter_export_button',
                       text: '<i class="la la-download"></i>',
                       titleAttr: 'Download',

                   },
                   {
                       extend: 'colvis',
                       text: '<i class="la la-eye-slash" title="Hide/Unhide Columns"></i>',
                       titleAttr: 'Hide/Unhide Columns',
                       className: 'excel_button'
                   },
                   {
                       extend: 'pageLength',
                       text: '<i class="la la-list-ol" title="Toggle Page Length"></i>',
                       className: 'excel_button ',
                       titleAttr: 'Toggle Page Length'
                   },
               ],

           });
           new $.fn.dataTable.FixedColumns(table, {
               leftColumns: 1
           });
           $(function() {
               $('#document_grid_view_table').colResizable({
                   liveDrag: true,
                   gripInnerHtml: "<div class='grip'></div>",
                   draggingClass: "dragging",
                   resizeMode: 'overflow',
                   sScrollXInner: "100%"
               });
           });
       });

       // Select All Checkbox-------------------------------------------------------------------------------
       $("#select_all").change(function() { //"select all" change 
           var status = this.checked; // "select all" checked status
           $('.checkbox').each(function() { //iterate all listed checkbox items
               this.checked = status; //change ".checkbox" checked status
           });
       });

       $('.checkbox').change(function() { //".checkbox" change 
           //uncheck "select all", if one of the listed checkbox item is unchecked
           if (this.checked == false) { //if this item is unchecked
               $("#select_all")[0].checked = false; //change "select all" checked status to false
           }

           //check "select all" if all checkbox items are checked
           if ($('.checkbox:checked').length == $('.checkbox').length) {
               $("#select_all")[0].checked = true; //change "select all" checked status to true
           }
       });

       // Toggle Filter Div-------------------------------------------------------------------------------
       setTimeout(() => {
           $('#filter_div_save').click(function() {
               $('#filter_div').css('display', 'none');
           });

           $('#filter_div_close').click(function() {
               $('#filter_div').css('display', 'none');
           });

           $('.filter_export_button').click(function() {
               $('.filter_div').show();
               $('#filter_div_save').show();
               $('#filter_div_apply').hide();
           });

           $('#filter_button').click(function() {
               $('.filter_div').show();
               $('#filter_div_apply').show();
               $('#filter_div_save').hide();
           });

           $("#filter_div_apply").click(function() {
               $(".filter_div").hide()
           });
       }, 2000);

       // Toggle Upload Module-------------------------------------------------------------------------------
       $('#upload_file').modal('hide');
       $('#upload').click(function() {
           $('#upload_file').slideToggle(200);
       });
       $('#close').click(function() {
           $('#upload_file').hide();
       });

       // Date Range Filter Widget-------------------------------------------------------------------------------
       var start = moment().subtract(29, 'days');
       var end = moment();

       function cb(start, end) {
           $('#date_range').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
           console.log("A new date selection was made: " + start.format('YYYY:MM:DD') + ' to ' + end.format('YYYY:MM:DD'));
       }

       $('#date_range').daterangepicker({
           autoApply: true,
           startDate: start,
           endDate: end,
           ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
           },
           alwaysShowCalendars: true,
           locale: {
               format: 'MMM D, YYYY'
               // format: 'YYYY:MM:D'
           }
       }, cb);
       cb(start, end);

       // Delete confrim js ----------------------------------------------------------------------------------------
       $(document).ready(function() {
           $(".void_button").on("click", function() {

               $.confirm({
                   title: 'Delete!',
                   type: 'orange',
                   content: 'You are about to delete the selected Records... <br><b>Do you want to Delete selected records?</b>',
                   buttons: {
                       text: 'Done!',
                       btnClass: 'btn-yu',
                       delete: function() {
                           $.alert({
                               title: '',

                               content: 'Selected records are Deleted. <br><b>Record Deleted!</b>',
                               type: 'green',
                           });
                       },
                       cancel: function() {
                           $.alert({
                               title: '',

                               content: 'Record Deletion Failed!',
                               type: 'red',
                           });
                       }

                   }
               });
           });
       });
   </script>

   <!-- Unclassified Document Table Datatable Initialization -->
   <script>
       var table = $('#unclassified_document_view_table').DataTable({
           orderCellsTop: true,
           fixedHeader: true,
           "columnDefs": [{
               "targets": 0,
               "orderable": false,
           }],
           "bStateSave": true,
           'aaSorting': [
               [1, 'asc']
           ],
           dom: 'Bf<"table-responsive"t>ip',
           buttons: [{
                   className: 'export_button filter_export_button',
                   text: '<i class="la la-download"></i>',
                   titleAttr: 'Download',

               },
               {
                   extend: 'colvis',
                   text: '<i class="la la-eye-slash" title="Hide/Unhide Columns"></i>',
                   titleAttr: 'Hide/Unhide Columns',
                   className: 'excel_button'
               },
               {
                   extend: 'pageLength',
                   text: '<i class="la la-list-ol" title="Toggle Page Length"></i>',
                   className: 'excel_button ',
                   titleAttr: 'Toggle Page Length'
               },
           ],

       });
       new $.fn.dataTable.FixedColumns(table, {
           leftColumns: 1
       });
       $(function() {
           $('#unclassified_document_view_table').colResizable({
               liveDrag: true,
               gripInnerHtml: "<div class='grip'></div>",
               draggingClass: "dragging",
               resizeMode: 'overflow',
               sScrollXInner: "100%"
           });
       });
   </script>

   <!-- Licence Tracker Document Table Datatable Initialization -->
   <script>
       $('.approve_btn').click(function() {
           toastr.success("Document Approved");
       });
       $('.reject_btn').click(function() {
           toastr.error("Document Rejected");
       });
       $('.back_dash').click(function() {
           window.history.back();
       })
       var table = $('#licence_tracker_document_view_table').DataTable({
           orderCellsTop: true,
           fixedHeader: true,
           "columnDefs": [{
               "targets": 0,
               "orderable": false,
           }],
           "bStateSave": true,
           'aaSorting': [
               [1, 'asc']
           ],
           dom: 'Bf<"table-responsive"t>ip',
           buttons: [{
                   className: 'export_button filter_export_button',
                   text: '<i class="la la-download"></i>',
                   titleAttr: 'Download',

               },
               {
                   extend: 'colvis',
                   text: '<i class="la la-eye-slash" title="Hide/Unhide Columns"></i>',
                   titleAttr: 'Hide/Unhide Columns',
                   className: 'excel_button'
               },
               {
                   extend: 'pageLength',
                   text: '<i class="la la-list-ol" title="Toggle Page Length"></i>',
                   className: 'excel_button ',
                   titleAttr: 'Toggle Page Length'
               },
           ],

       });
       new $.fn.dataTable.FixedColumns(table, {
           leftColumns: 1
       });
       $(function() {
           $('#licence_tracker_document_view_table').colResizable({
               liveDrag: true,
               gripInnerHtml: "<div class='grip'></div>",
               draggingClass: "dragging",
               resizeMode: 'overflow',
               sScrollXInner: "100%"
           });
       });
   </script>

   <!-- My Document -->
   <script type="text/javascript">
       // PDF Preview function ----------------------------------------------------------------------------------------
       $("#je_yes_download").click(function() {
           $("#pdf_preview").modal("show");
       });

       // Display Checkbox and Delete option when the user uploads the document from the Plus button at the Bottom

       $(".document_delete_icon").hide();
       $(".document_upload_button").hide();

       function submit_document_edits() {
           toastr.success('Document Uploaded Successfully');
           setTimeout(function() {
               window.location.href = '<?= base_url() ?>document/Document_controller/request_list';
           }, 2000);
       }
   </script>