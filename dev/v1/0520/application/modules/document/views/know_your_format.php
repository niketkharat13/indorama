<?php $this->load->view('header'); ?>
<?php $this->load->view('top'); ?>
<?php $this->load->view('vertical_navbar'); ?>
<link rel="stylesheet" href="<?= base_url() ?>app-assets/css/table.css">
<?php $this->load->view('document_css'); ?>
<style type="text/css">
    .count_fount {
        font-size: 30px;
    }
</style>
<div style="width:103%">
    <br>
    <div class="col">
        <div class="card-yu">
            <div class="main_list_div p-1">
                <div class="filter_div" id="filter_div" style="display: none;">
                    <?php $this->load->view('document_filter_view'); ?>
                </div>
                <div class="mb-1">
                    <div class="p-1 row d-flex align-items-center justify-content-between">
                        <h4 class="ml-1">Know Your Format</h4>
                        <div class="button_div col-lg-10 col-md-10 col-sm-12 d-flex justify-content-end">
                            <button class="btn-yu primary mr-1 table_buttons" id="add_new_format_btn" title="Add New Format">
                                <i class="ft-plus text-white" style="font-size: 20px;"></i>
                            </button>
                            <button class="btn-yu primary mr-1 table_buttons" id="filter_button" title="Filter">
                                <i class="ft-filter text-white"></i>
                            </button>
                            <!-- <button id="upload_button" class="btn-yu primary table_buttons" title="Bulk Upload" data-toggle="modal" data-target="#upload_file">
                                <i class="ft-upload text-white"></i>
                            </button> -->
                        </div>
                    </div>
                </div>

                <table class="table display" id="know_your_format_list_view" width="100%" border="0" cellpadding="0" cellspacing="0">
                    <thead>
                        <th class="thead-custom" style="min-width: 30px;">#</th>
                        <th class="thead-custom">Action</th>
                        <th class="thead-custom">Appropriate Government</th>
                        <th class="thead-custom">State</th>
                        <th class="thead-custom">Act</th>
                        <th class="thead-custom" style="min-width: 270px !important">Obligation</th>
                        <th class="thead-custom">Attachments</th>
                        <th class="thead-custom">Last updated on</th>
                        <th class="thead-custom">Last updated by</th>
                    </thead>
                    <tbody>
                        <tr>
                            <th>
                                1
                            </th>
                            <td>
                                <a href="<?=base_url()?>document/document_controller/document_history" data-toggle="tooltip" title="" data-original-title="Document History"><i class="la la-clock-o"></i></a>
                                &nbsp;&nbsp;&nbsp;
                                <a data-toggle="modal" title="Edit" class="edit_format_btn"><i class="ft-edit-3 height-color"></i></a>&nbsp;
                            </td>
                            <td>State</td>
                            <td>
                                Maharashtra
                            </td>
                            <td>CLRA</td>
                            <td>Contract Labour Licence or Application along with security deposit details</td>
                            <td>
                                <a href="<?= base_url() ?>app-assets/Test.pdf" download>
                                    <i class="ft-download height-color download_attached_document"></i>
                                </a>
                            </td>
                            <td>22 Apr 2020</td>
                            <td>Aniket Y</td>
                        </tr>
                        <tr>
                            <th>
                                2
                            </th>
                            <td>
                                <a href="<?= base_url() ?>document/document_controller/document_history" data-toggle="tooltip" title="" data-original-title="Document History"><i class="la la-clock-o"></i></a>
                                &nbsp;&nbsp;&nbsp;
                                <a data-toggle="modal" title="Edit" class="edit_format_btn"><i class="ft-edit-3 height-color "></i></a>&nbsp;
                            </td>
                            <td>State</td>
                            <td>
                                Maharashtra
                            </td>
                            <td>ESI</td>
                            <td>ESI registration is mandatory if employees strength exceeds 20 and employees are drawing gross salary of less than 21000 per month</td>
                            <td>
                                <a href="<?= base_url() ?>app-assets/Test.pdf" download>
                                    <i class="ft-download height-color download_attached_document"></i>
                                </a>
                            </td>
                            <td>22 Apr 2020</td>
                            <td>Niket K</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- START OF ADD DOCUMENT FORMAT MODAL -->
<div class="modal fade text-left datamodal" id="add_new_format" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel167">Add New Format</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="row mt-1">
                            <div class="col-3 d-flex align-items-center">
                                <label class="" for="new_format_appropriate_govt">Appropriate Government</label>
                            </div>
                            <div class="col-9 search_dropdown_div">
                                <select class="selectivity_dropdown" id="new_format_appropriate_govt">
                                    <option value="state">State</option>
                                    <option value="common">Common</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="row mt-1">
                            <div class="col-3 d-flex align-items-center">
                                <label class="" for="new_format_act">Act</label>
                            </div>
                            <div class="col-9 search_dropdown_div">
                                <select class="selectivity_dropdown" id="new_format_act">
                                    <option value="act_1">Bonus</option>
                                    <option value="act_2">ESI</option>
                                    <option value="act_3">PF</option>
                                    <option value="act_4">MBenefit</option>
                                    <option value="act_5">MWages</option>
                                    <option value="act_6">HRA</option>
                                    <option value="act_7">PWages</option>
                                    <option value="act_8">PTax</option>
                                    <option value="act_9">LWF</option>
                                    <option value="act_10">S&E</option>
                                    <option value="act_11">Other</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="row mt-1">
                            <div class="col-3 d-flex align-items-center">
                                <label class="" for="act_obligation">Obligations</label>
                            </div>
                            <div class="col-9 search_dropdown_div">
                                <select class="selectivity_dropdown" id="act_obligation">

                                </select>
                                <small>*Based on Act*</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="row mt-1">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <label for="udhyog_ip" style="margin-top:8px">Document</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7 xs-4">
                                <input type="text" id="udhyog_ip" placeholder="Enter Document # and upload" name="" value="" class="form-control reset-cls focus-trigger">
                                <div>
                                    <sub id="document_file_name" style="display: none"></sub>
                                    <sub id="preview_mode_document" class="preview_text" data-target="#preview_modal" data-toggle="modal">Preview</sub>
                                </div>
                                <sub>Only PDF and Max 2 MB - 1 File only</sub>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 xs-1" style="padding: 2%;">
                                <i class="ft-paperclip info-1 font-medium-4 paperClip" onclick="open_file('new_file_upload','document_file_name','preview_mode_document')"></i>
                            </div>
                            <input type="file" class="new_file_upload" style="display: none" accept="application/pdf">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-yu btn-width primary text-white" onclick="vendor_reset_confirm()">Reset</button>
                <button type="button" class="btn-yu btn-width primary text-white" onclick="vendor_draft_toastr()">Save As Draft</button>
                <button type="button" class="btn-yu btn-width primary text-white" data-dismiss="modal" onclick="addNewVendor()">Submit </button>
            </div>
        </div>
    </div>
</div>
<!-- END OF ADD DOCUMENT FORMAT MODAL -->
<!-- START OF EDIT DOCUMENT FORMAT MODAL -->
<div class="modal fade text-left datamodal" id="edit_format_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel167">Edit Format</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="row mt-1">
                            <div class="col-3 d-flex align-items-center">
                                <label class="" for="edit_format_appropriate_govt">Appropriate Government</label>
                            </div>
                            <div class="col-9 search_dropdown_div">
                                <select class="selectivity_dropdown" id="edit_format_appropriate_govt">
                                    <option value="state">State</option>
                                    <option value="common">Common</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="row mt-1">
                            <div class="col-3 d-flex align-items-center">
                                <label class="" for="edit_format_act">Act</label>
                            </div>
                            <div class="col-9 search_dropdown_div">
                                <select class="selectivity_dropdown" id="edit_format_act">
                                    <option value="act_1">Bonus</option>
                                    <option value="act_2">ESI</option>
                                    <option value="act_3">PF</option>
                                    <option value="act_4">MBenefit</option>
                                    <option value="act_5">MWages</option>
                                    <option value="act_6">HRA</option>
                                    <option value="act_7">PWages</option>
                                    <option value="act_8">PTax</option>
                                    <option value="act_9">LWF</option>
                                    <option value="act_10">S&E</option>
                                    <option value="act_11">Other</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="row mt-1">
                            <div class="col-3 d-flex align-items-center">
                                <label class="" for="edit_act_obligation">Obligations</label>
                            </div>
                            <div class="col-9 search_dropdown_div">
                                <select class="selectivity_dropdown" id="edit_act_obligation">

                                </select>
                                <small>*Based on Act*</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="row mt-1">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <label for="udhyog_ip" style="margin-top:8px">Document</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7 xs-4">
                                <input type="text" id="udhyog_ip" placeholder="Enter Document # and upload" name="" value="" class="form-control reset-cls focus-trigger">
                                <div>
                                    <sub id="edit_document_file_name">Demo Document.pdf</sub>
                                    <sub id="edit_preview_mode_document" class="edit_preview_text" data-target="#preview_modal" data-toggle="modal">Preview</sub>
                                </div>
                                <sub>Only PDF and Max 2 MB - 1 File only</sub>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 xs-1" style="padding: 2%;">
                                <i class="ft-paperclip info-1 font-medium-4 paperClip" onclick="open_file('edit_file_upload','edit_document_file_name','edit_preview_mode_document')"></i>
                            </div>
                            <input type="file" class="edit_file_upload" style="display: none" accept="application/pdf">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-yu btn-width primary text-white" onclick="vendor_reset_confirm()">Reset</button>
                <button type="button" class="btn-yu btn-width primary text-white" onclick="vendor_draft_toastr()">Save As Draft</button>
                <button type="button" class="btn-yu btn-width primary text-white" data-dismiss="modal" onclick="updateFormat()">Submit </button>
            </div>
        </div>
    </div>
</div>
<!-- END OF EDIT DOCUMENT FORMAT MODAL -->
<!-- preview modal  -->
<?php $this->load->view('preview'); ?>
<!-- preview modal  -->
<?php $this->load->view('footer'); ?>
<?php $this->load->view('document_js'); ?>