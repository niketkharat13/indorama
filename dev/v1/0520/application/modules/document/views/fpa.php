    <?php $this->load->view('header'); ?>
    <?php $this->load->view('top'); ?>
    <?php $this->load->view('vertical_navbar'); ?>
    <link rel="stylesheet" href="<?= base_url() ?>app-assets/css/table.css">
    <?php $this->load->view('document_css'); ?>
    <div style="width:103%">
        <div class="col">
            <br>
            <div class="card-yu pt-2 p-1">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a class="nav-link" id="document_repository" href="<?= base_url() ?>document/Document_controller/request_list" aria-controls="active" aria-expanded="true">Request List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="unclassified_document" href="<?= base_url() ?>document/Document_controller/recordtoreport" aria-controls="link" aria-expanded="false">R2R Task</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" id="licence_tracker_document" href="<?= base_url() ?>document/Document_controller/fpa" aria-controls="link" aria-expanded="false">FP&A Task</a>
                    </li>
                </ul>
                <div class="margin-top-50">
                    <div class="">
                        <div class="filter_div" id="filter_div" style="display:none; left: 1px;top: 1px;">
                            <?php $this->load->view('document_filter_view'); ?>
                        </div>
                        <div class="mt-1 mb-1">
                            <div class="row d-flex align-items-center">
                                <div class="col d-flex justify-content-end" style="padding:0;">
                                    <button class="btn-yu primary mr-1" id="filter_button" style="width:35px;border-radius:50%;border:none">
                                        <i class="ft-filter text-white"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <table class="table" id="licence_tracker_document_view_table">
                            <thead>
                                <tr>
                                    <th class="no-sort checkbox adjust_checkbox_column thead-custom" style="padding-left:6px !important;z-index:3;">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" id="select_all">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <th class="thead-custom">Workflow Status</th>
                                    <th class="thead-custom">Sub Process Type</th>
                                    <th class="thead-custom">User Name</th>
                                    <th class="thead-custom">Start Date</th>
                                    <th class="thead-custom">End Date</th>
                                    <th class="thead-custom">TAT</th>
                                    <th class="thead-custom">Ageing</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th class="no-sort checkbox adjust_checkbox_column " style="padding-left:6px !important;z-index:2;">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" id="select_all">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>
                                        <div class="badge" style=" background-color: #f5ad5f; width:120px;">Submitted to TL</div>
                                    </td>
                                    <td>MIS reports with operational activities</td>
                                    <td>Daniel</td>
                                    <td>09/05/2020</td>
                                    <td>09/05/2020</td>
                                    <td>&nbsp;</td>
                                    <td>16.00</td>
                                </tr>
                                <tr>
                                    <th class="no-sort checkbox adjust_checkbox_column " style="padding-left:6px !important;z-index:2;">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" id="select_all">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>
                                        <div class="badge" style=" background-color: #935b1a; width:120px;">TL Review</div>
                                    </td>
                                    <td>MIS reporting based on actuals</td>
                                    <td>John</td>
                                    <td>09/05/2020</td>
                                    <td>10/05/2020</td>
                                    <td>&nbsp;</td>
                                    <td>16.00</td>
                                </tr>
                                <tr>
                                    <th class="no-sort checkbox adjust_checkbox_column " style="padding-left:6px !important;z-index:2;">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" id="select_all">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>
                                        <div class="badge" style=" background-color: #a1d36e; width:120px;">Sent to RO User</div>
                                    </td>
                                    <td>MIS reports for senior management</td>
                                    <td>Apple</td>
                                    <td>09/05/2020</td>
                                    <td>09/05/2020</td>
                                    <td>&nbsp;</td>
                                    <td>16.00</td>
                                </tr>
                                <tr>
                                    <th class="no-sort checkbox adjust_checkbox_column " style="padding-left:6px !important;z-index:2;">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" id="select_all">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>
                                        <div class="badge" style=" background-color: #f5ad5f; width:120px;">Submitted to TL</div>
                                    </td>
                                    <td>MIS reporting based on actuals</td>
                                    <td>Seed</td>
                                    <td>10/05/2020</td>
                                    <td>10/05/2020</td>
                                    <td>&nbsp;</td>
                                    <td>15.00</td>
                                </tr>
                                <tr>
                                    <th class="no-sort checkbox adjust_checkbox_column " style="padding-left:6px !important;z-index:2;">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" id="select_all">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>
                                        <div class="badge" style=" background-color: #935b1a; width:120px;">TL Review</div>
                                    </td>
                                    <td>MIS reports for senior management</td>
                                    <td>Daniel</td>
                                    <td>10/05/2020</td>
                                    <td>11/05/2020</td>
                                    <td>&nbsp;</td>
                                    <td>15.00</td>
                                </tr>
                                <tr>
                                    <th class="no-sort checkbox adjust_checkbox_column " style="padding-left:6px !important;z-index:2;">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" id="select_all">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>
                                        <div class="badge" style=" background-color: #568dd5; width:120px;">Yet to Start</div>
                                    </td>
                                    <td>MIS reports with operational activities</td>
                                    <td>Apple</td>
                                    <td>10/05/2020</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>15.00</td>
                                </tr>
                                <tr>
                                    <th class="no-sort checkbox adjust_checkbox_column " style="padding-left:6px !important;z-index:2;">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" id="select_all">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>
                                        <div class="badge" style=" background-color: #f5ad5f; width:120px;">Submitted to TL</div>
                                    </td>
                                    <td>MIS reports for senior management</td>
                                    <td>Daniel</td>
                                    <td>10/05/2020</td>
                                    <td>10/05/2020</td>
                                    <td>&nbsp;</td>
                                    <td>15.00</td>
                                </tr>
                                <tr>
                                    <th class="no-sort checkbox adjust_checkbox_column " style="padding-left:6px !important;z-index:2;">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" id="select_all">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>
                                        <div class="badge" style=" background-color: #3cccc6; width:120px;">RO User Approved</div>
                                    </td>
                                    <td>MIS reports for senior management</td>
                                    <td>Andrea</td>
                                    <td>10/05/2020</td>
                                    <td>12/05/2020</td>
                                    <td>&nbsp;</td>
                                    <td>15.00</td>
                                </tr>
                                <tr>
                                    <th class="no-sort checkbox adjust_checkbox_column " style="padding-left:6px !important;z-index:2;">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" id="select_all">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>
                                        <div class="badge" style=" background-color: #f5ad5f; width:120px;">Submitted to TL</div>
                                    </td>
                                    <td>MIS reports with operational activities</td>
                                    <td>Daniel</td>
                                    <td>12/05/2020</td>
                                    <td>12/05/2020</td>
                                    <td>&nbsp;</td>
                                    <td>13.00</td>
                                </tr>
                                <tr>
                                    <th class="no-sort checkbox adjust_checkbox_column " style="padding-left:6px !important;z-index:2;">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" id="select_all">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>
                                        <div class="badge" style=" background-color: #935b1a; width:120px;">TL Review</div>
                                    </td>
                                    <td>MIS reporting based on actuals</td>
                                    <td>John</td>
                                    <td>12/05/2020</td>
                                    <td>13/05/2020</td>
                                    <td>&nbsp;</td>
                                    <td>13.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="upload_file" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <?php $this->load->view('multiple_file_upload_view'); ?>
            </div>
        </div>
    </div>
    <!-- preview modal  -->
    <?php $this->load->view('preview'); ?>
    <!-- preview modal  -->
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('document_js'); ?>