    <?php $this->load->view('header'); ?>
    <?php $this->load->view('top'); ?>
    <?php $this->load->view('vertical_navbar'); ?>
    <link rel="stylesheet" href="<?= base_url() ?>app-assets/css/table.css">
    <?php $this->load->view('document_css'); ?>

    <style type="text/css">
        .count_fount {
            font-size: 30px;
        }

        .size {
            font-size: 20px;
            margin-left: 5px;
            cursor: pointer;

        }

        .color-1 {
            color: #4285f4;
        }

        .color-2 {
            color: orange;
        }

        .color-3 {
            color: #8BC34A;
            margin-left: 5px;
            cursor: pointer;


        }
    </style>

    <div style="width:103%">
        <div class="col">
            <br>
            <div class="card-yu pt-2 p-1">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item ">
                        <a class="nav-link active" id="document_repository" href="<?= base_url() ?>document/Document_controller/request_list" aria-controls="active" aria-expanded="true">Request List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="unclassified_document" href="<?= base_url() ?>document/Document_controller/recordtoreport" aria-controls="link" aria-expanded="false">R2R Task</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="licence_tracker_document" href="<?= base_url() ?>document/Document_controller/fpa" aria-controls="link" aria-expanded="false">FP&A Task</a>
                    </li>

                </ul>
                <div class="margin-top-50">
                    <div class="">
                        <div class="filter_div" id="filter_div" style="display:none; left: 1px;top: 1px;">
                            <?php $this->load->view('document_filter_view'); ?>
                        </div>
                        <div class="mt-1 mb-1">
                            <div class="row d-flex align-items-center">
                                <div class="col d-flex justify-content-end" style="padding:0;">
                                    <button class="btn-yu primary mr-1" id="filter_button" style="width:35px;border-radius:50%;border:none">
                                        <i class="ft-filter text-white"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <table class="table " id="document_grid_view_table">
                            <thead>
                                <th class="no-sort checkbox adjust_checkbox_column thead-custom" style="padding-left:6px !important;z-index:3;">
                                    <label class="document_checkbox_icon">
                                        <input type="checkbox" value="" id="select_all">
                                        <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                    </label>
                                </th>
                                <th class="thead-custom" style="min-width: 80px;">Action</th>
                                <th class="thead-custom">Status</th>
                                <th class="thead-custom">Request #</th>
                                <th class="thead-custom">Company Code</th>
                                <th class="thead-custom" style="min-width: 90px;">Company Name</th>
                                <th class="thead-custom">Activity Type</th>
                                <th class="thead-custom" style="min-width: 130px;">Sub Activity Type</th>
                                <th class="thead-custom">Recurring</th>
                                <th class="thead-custom">Region</th>
                                <th class="thead-custom">Country</th>
                                <th class="thead-custom">Segment</th>
                                <th class="thead-custom" style="min-width: 150px;">Product</th>
                                <th class="thead-custom">Cost Center</th>
                                <th class="thead-custom">Amount</th>
                                <th class="thead-custom">JE String</th>
                                <th class="thead-custom">Request Date</th>
                                <th class="thead-custom">Last Action Date</th>
                                <th class="thead-custom">Ageing in Days</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <th class="checkbox adjust_checkbox_column" style="z-index:3">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" name="check[]" class="checkbox">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>
                                        <div>
                                            <a href="<?= base_url() ?>document/Document_controller/document_preview">
                                                <i class="ft-eye size color-1" data-toggle="tooltip" title="View"></i>
                                            </a>
                                            <!-- <i class="ft-download size color-2" data-toggle="tooltip" title="Download" id=je_yes_download></i> -->
                                            <i class="la la-comment color-3" style="font-size:24px" data-toggle="tooltip" title="Comment"></i>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="badge" style=" background-color: #568dd5; width:82px;">Open</div>
                                    </td>
                                    <td>100</td>
                                    <td class="dropdown_act_editable">0004</td>
                                    <td class="dropdown_title_editable">Company 1</td>
                                    <td> Reclassification </td>
                                    <td> Sub Activity 1 </td>
                                    <td>Yes</td>
                                    <td>Region 1</td>
                                    <td>India</td>
                                    <td>Segment 1</td>
                                    <td>Product 1</td>
                                    <td>Cost Center 1</td>
                                    <td class="dropdown_state_editable">$ 1300</td>
                                    <td class="text_editable">Yes</td>
                                    <td>01 Apr 2020</td>
                                    <td>15 Apr 2020</td>
                                    <td>39</td>
                                </tr>
                                <tr>
                                    <th class="checkbox adjust_checkbox_column" style="z-index:3">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" name="check[]" class="checkbox">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>
                                        <div>
                                            <a href="<?= base_url() ?>document/Document_controller/document_preview">
                                                <i class="ft-eye size color-1" data-toggle="tooltip" title="View"></i>
                                            </a>
                                            <!-- <i class="ft-download size color-2" data-toggle="tooltip" title="Download" id=je_yes_download></i> -->
                                            <i class="la la-comment color-3" style="font-size:24px" data-toggle="tooltip" title="Comment"></i>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="badge" style=" background-color: #FF9800; width:82px;">Query</div>
                                    </td>
                                    <td>101</td>
                                    <td class="dropdown_act_editable">0005</td>
                                    <td class="dropdown_title_editable">Company 2</td>
                                    <td> Rectification </td>
                                    <td> Sub Activity 2 </td>
                                    <td>No</td>
                                    <td>Region 2</td>
                                    <td>China</td>
                                    <td>Segment 2</td>
                                    <td>Product 2</td>
                                    <td>Cost Center 2</td>
                                    <td class="dropdown_state_editable">$ 10000</td>
                                    <td class="text_editable">No</td>
                                    <td>01 Apr 2020</td>
                                    <td>15 Apr 2020</td>
                                    <td>39</td>
                                </tr>
                                <tr>
                                    <th class="checkbox adjust_checkbox_column" style="z-index:3">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" name="check[]" class="checkbox">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>
                                        <div>
                                            <a href="<?= base_url() ?>document/Document_controller/document_preview">
                                                <i class="ft-eye size color-1" data-toggle="tooltip" title="View"></i>
                                            </a>
                                            <!-- <i class="ft-download size color-2" data-toggle="tooltip" title="Download" id=je_yes_download></i> -->
                                            <i class="la la-comment color-3" style="font-size:24px" data-toggle="tooltip" title="Comment"></i>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="badge" style=" background-color: #568dd5; width:82px;">Open</div>
                                    </td>
                                    <td>102</td>
                                    <td class="dropdown_act_editable">0006</td>
                                    <td class="dropdown_title_editable">Company 3</td>
                                    <td> Provison </td>
                                    <td> Sub Activity 4 </td>
                                    <td>No</td>
                                    <td>Region 3</td>
                                    <td>Thailand</td>
                                    <td>Segment 3</td>
                                    <td>Product 3</td>
                                    <td>Cost Center 3</td>
                                    <td class="dropdown_state_editable">$ 10000</td>
                                    <td class="text_editable">No</td>
                                    <td>01 Apr 2020</td>
                                    <td>15 Apr 2020</td>
                                    <td>39</td>
                                </tr>
                                <tr>
                                    <th class="checkbox adjust_checkbox_column" style="z-index:3">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" name="check[]" class="checkbox">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>
                                        <div>
                                            <a href="<?= base_url() ?>document/Document_controller/document_preview">
                                                <i class="ft-eye size color-1" data-toggle="tooltip" title="View"></i>
                                            </a>
                                            <!-- <i class="ft-download size color-2" data-toggle="tooltip" title="Download" id=je_yes_download></i> -->
                                            <i class="la la-comment color-3" style="font-size:24px" data-toggle="tooltip" title="Comment"></i>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="badge" style=" background-color: #568dd5; width:82px;">Open</div>
                                    </td>
                                    <td>103</td>
                                    <td class="dropdown_act_editable">0007</td>
                                    <td class="dropdown_title_editable">Company 4</td>
                                    <td> Inter-Company </td>
                                    <td> Sub Activity 4 </td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>UK</td>
                                    <td>Segment 4</td>
                                    <td>Product 4</td>
                                    <td>Cost Center 4</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>01 Apr 2020</td>
                                    <td>15 Apr 2020</td>
                                    <td>39</td>
                                </tr>
                                <tr>
                                    <th class="checkbox adjust_checkbox_column" style="z-index:3">
                                        <label class="document_checkbox_icon">
                                            <input type="checkbox" value="" name="check[]" class="checkbox">
                                            <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                        </label>
                                    </th>
                                    <td>
                                        <div>

                                            <a href="<?= base_url() ?>document/Document_controller/document_preview">
                                                <i class="ft-eye size color-1" data-toggle="tooltip" title="View"></i>
                                            </a>
                                            <!-- <i class="ft-download size color-2" data-toggle="tooltip" title="Download" id=je_yes_download></i> -->
                                            <i class="la la-comment color-3" style="font-size:24px" data-toggle="tooltip" title="Comment"></i>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="badge" style=" background-color: #568dd5; width:82px;">Open</div>
                                    </td>
                                    <td>104</td>
                                    <td class="dropdown_act_editable">0008</td>
                                    <td class="dropdown_title_editable">Company 5</td>
                                    <td> Others </td>
                                    <td> Sub Activity 5 </td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>USA</td>
                                    <td>Segment 5</td>
                                    <td>Product 5</td>
                                    <td>Cost Center 5</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>01 Apr 2020</td>
                                    <td>15 Apr 2020</td>
                                    <td>39</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <button class="btn-yu primary mr-1 document_upload_button" style="float: right; width: 100px; display:none;" onclick="submit_document_edits()">Submit</button>

                    </div>

                </div>
            </div>
        </div>

    </div>


    <div id="upload_file" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <?php $this->load->view('multiple_file_upload_view'); ?>
            </div>
        </div>
    </div>

    <!-- preview modal  -->
    <?php $this->load->view('preview'); ?>
    <!-- preview modal  -->
    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('document_js'); ?>