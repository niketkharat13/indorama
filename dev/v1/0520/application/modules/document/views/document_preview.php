    <?php $this->load->view('header'); ?>
    <?php $this->load->view('top'); ?>
    <?php $this->load->view('vertical_navbar'); ?>
    <link rel="stylesheet" href="<?= base_url() ?>app-assets/css/table.css">
    <?php $this->load->view('document_css'); ?>

    <style>
        .My_btn {
            width: 50px;
            height: 50px !important;
            border-radius: 50%;
            padding: 11px !important;
            font-size: 25px;
        }

        .text_Space {
            padding: 10px;
        }

        .text_Space label {
            font-weight: 600;
        }

        .attactment_div .card-body {
            border: 1px solid darkgrey;
            border-radius: 0.25rem;
            background-color: #f4f5f7;
            cursor: pointer;
        }
    </style>

    <div style="width:103%">
        <div class="col">
            <br>
            <div class="card-yu p-2">
                <br>
                <div>
                    <div class="card-content">
                        <div id="profile" class="tabcontent" style="display: block">
                            <div class="ml-1">
                                <div class="row">
                                    <div class="col-lg-10">
                                        <h4 class="card-title"><i class="la la-angle-left mr-1 back_dash cursor-pointer"></i>Document Preview</h4>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-3 col-12" style="text-align: right;">
                                        <a href="http://13.234.152.155/nimble/home-overview.html" class="cursor-pointer" target='_blank'>
                                            <button class="btn-yu primary My_btn cursor-pointer" id="add" data-toggle="tooltip" title="Add" data-target=".datamodal"><i class="ft-plus"></i></button>
                                        </a>
                                    </div>
                                </div>
                                <hr>
                                <br>
                                <div style="border-radius:10px">
                                    <div style="margin-left:1.5rem;">
                                        <div class="row text_Space">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-size:16px" class="mb-0">Request #</label>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <h4 style="font-size:15px;margin-top:4px" id="request_number">100</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-4"><label style="font-size:16px" class="mb-0">Company Code</label></div>
                                                    <div class="col-lg-7">
                                                        <h4 style="font-size:15px;margin-top:4px" id="company_code">0005</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row text_Space">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-size:16px" id="a" class="mb-0">Activity Type</label>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <h4 style="font-size:15px;margin-top:4px" id="activity_code">Rectification</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-size:16px" class="mb-0">Sub Activity</label>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <h4 style="font-size:15px;margin-top:4px" id="sub_activity_code">Sub Activity 5</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row text_Space">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-size:16px" class="mb-0">Recurring</label>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <h4 style="font-size:15px;margin-top:4px" id="recurring">Yes</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-size:16px" class="mb-0">Region</label>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <h4 style="font-size:15px;margin-top:4px" id="region">Region 1</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row text_Space">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-size:16px" class="mb-0">Country</label>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <h4 style="font-size:15px;margin-top:4px" id="country">India</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-size:16px" class="mb-0">Segment</label>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <h4 style="font-size:15px;margin-top:4px" id="segment">Segment 1</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row text_Space">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-size:16px" class="mb-0">Product</label>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <h4 style="font-size:15px;margin-top:4px" id="product">Product 1</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-size:16px" class="mb-0">Cost Center</label>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <h4 style="font-size:15px;margin-top:4px" id="cost_center">Cost Center 1</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row text_Space">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-size:16px" class="mb-0">Amount</label>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <h4 style="font-size:15px;margin-top:4px" id="amount">1.300</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-size:16px" class="mb-0">JE String</label>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <h4 style="font-size:15px;margin-top:4px" id="je_string">Yes</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row text_Space">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-size:16px" class="mb-0">Request Date</label>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <h4 style="font-size:15px;margin-top:4px" id="request_date">25 Dec 2109</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-size:16px" class="mb-0">Last Action Date</label>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <h4 style="font-size:15px;margin-top:4px" id="last_action_date">15 Apr 2020</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row text_Space">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label style="font-size:16px" class="mb-0">Ageing in Days</label>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <h4 style="font-size:15px;margin-top:4px" id="ageing_days">39</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h4>Attachments</h4>
                    <div class="row attactment_div">
                        <div class="col-xl-2 col-md-6 col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="align-self-center">
                                                <i class="la la-file-pdf-o danger info font-large-1 float-left"></i>
                                            </div>
                                            <div class="media-body text-right">
                                                <span>Attachment 1</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="align-self-center">
                                                <i class="la la-file-excel-o success  font-large-1 float-left"></i>
                                            </div>
                                            <div class="media-body text-right">
                                                <span>Attachment 2</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="align-self-center">
                                                <i class="la la-file-image-o info font-large-1 float-left"></i>
                                            </div>
                                            <div class="media-body text-right">
                                                <span>Attachment 3</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-2 col-md-6 col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="align-self-center">
                                                <i class="la la-file-word-o warning font-large-1 float-left"></i>
                                            </div>
                                            <div class="media-body text-right">
                                                <span>Attachment 4</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="align-self-center">
                                                <i class="la la-file-excel-o success font-large-1 float-left"></i>
                                            </div>
                                            <div class="media-body text-right">
                                                <span>Attachment 5</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-6 col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="align-self-center">
                                                <i class="la la-file-pdf-o danger font-large-1 float-left"></i>
                                            </div>
                                            <div class="media-body text-right">
                                                <span>Attachment 5</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('document_js'); ?>