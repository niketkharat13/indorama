	<style type="text/css">
		.tdWidth {
			min-width: 180px !important;
		}

		.default_filter_element_div .table-responsive {
			height: auto;
		}

		.filter_table label {
			margin-bottom: 4px;
		}

		.default_filter_element_div table {
			height: 200px !important;
		}
	</style>

	<div class="row">
		<div class="default_filter_element_div col-lg-12" style="white-space: nowrap;overflow-x: auto !important;overflow-y: auto !important;">
			<table class="table table-responsive filter_table" style="padding: 1%;margin-bottom: 0%;">
				<tr>
					<form id="document_filter_form">
						<td class="tdWidth document_filter_element">
							<h4>By Status</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Approved
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Query
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Download
								</div>
							</div>
						</td>
						<td class="tdWidth document_filter_element">
							<h4>By Company Code</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										0004
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										0005
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										0006
								</div>
							</div>
						</td>

						<td class="tdWidth document_filter_element">
							<h4>By Company Name</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Company Name 1
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Company Name 2
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Company Name 3
								</div>
							</div>
						</td>
						<td class="tdWidth document_filter_element">

							<h4>By Activity Type</h4>
							<div class="option_style" id="document_filter_element_audit_type">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Reclassification
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Rectification
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Provison
									</label>
								</div>


							</div>
						</td>
						<td class="tdWidth document_filter_element">

							<h4>By Sub-Activity Type</h4>
							<div class="option_style" id="document_filter_element_audit_type">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Sub-Activity 1
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Sub-Activity 2
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Sub-Activity 3
									</label>
								</div>
							</div>
						</td>
						<td class="tdWidth document_filter_element">
							<h4>By Recurring</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Yes
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										No
									</label>
								</div>
							</div>
						</td>

						<td class="tdWidth document_filter_element">
							<h4>By Region</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Region 1
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Region 2
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Region 3
									</label>
								</div>

							</div>
						</td>
						<td class="tdWidth document_filter_element">
							<h4>By Country</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										India
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										China
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Thailand
									</label>
								</div>

							</div>
						</td>
						<td class="tdWidth document_filter_element">
							<h4>By Segment</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Segment 1
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Segment 2
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Segment 3
									</label>
								</div>

							</div>
						</td>
						<td class="tdWidth document_filter_element">
							<h4>By Product</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Product 1
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Product 2
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Product 3
									</label>
								</div>

							</div>
						</td>
						<td class="tdWidth document_filter_element">
							<h4>By Cost Center</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Cost Center 1
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Cost Center 2
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Cost Center 3
									</label>
								</div>

							</div>
						</td>
						<td class="tdWidth document_filter_element">
							<div>
								<h4>By Amount</h4>
								<div class="option_style">
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											1.300
										</label>
									</div>
									<div class="checkbox">
										<label class="d-flex">
											<input type="checkbox" value="">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
											100.000
										</label>
									</div>
								</div>
							</div>
						</td>

						<td class="tdWidth document_filter_element">
							<h4>By JE String</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Yes
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										No
									</label>
								</div>
							</div>
						</td>
						<td class="tdWidth recordtoreport_filter">
							<h4>By Sub Process Type</h4>
							<div class="option_style">
								<div class="d-flex">
									<div class="checkbox">
										<label class="">
											<input type="checkbox" value="" id="1">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										</label>
										<label for="1">Intercompany transactions & reconciliations</label>
									</div>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Bank reconciliations
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Fixed asset accounting
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Month end closing and Provisions
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Payroll accounting
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Statutory audit
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Statutory reporting
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										TB Review and GL scrutiny
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Physical verification
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Physical verification
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Physical verification
									</label>
								</div>
							</div>
						</td>
						<td class="tdWidth fpa_filter">
							<h4>By Sub Process Type</h4>
							<div class="option_style">
								<div class="d-flex">
									<div class="checkbox">
										<label class="">
											<input type="checkbox" value="" id="1">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										</label>
										<label for="1">MIS reporting based on actuals </label>
									</div>
								</div>
								<div class="d-flex">
									<div class="checkbox">
										<label class="">
											<input type="checkbox" value="" id="2">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										</label>
										<label for="2">MIS reports for senior management</label>
									</div>
								</div>
								<div class="d-flex">
									<div class="checkbox">
										<label class="">
											<input type="checkbox" value="" id="3">
											<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										</label>
										<label for="3">MIS reports with operational activities </label>
									</div>
								</div>
							</div>
						</td>
						<td class="tdWidth common_filter">
							<h4>By User Name</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Apple
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Seed
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Daniel
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Apple
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										John
									</label>
								</div>
							</div>
						</td>
						<td class="tdWidth common_filter">
							<h4>By Workflow Status</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Yet to Start
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										TL Review
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Submitted to TL
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Sent to RO User
									</label>
								</div>
							</div>
						</td>
						<!-- <td class="tdWidth recordtoreport_filter">
							<h4>By TAT</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Yet to Start
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										TL Review
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Submitted to TL
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Sent to RO User
									</label>
								</div>
							</div>
						</td> -->
						<td class="tdWidth common_filter">
							<h4>By Ageing</h4>
							<div class="option_style">
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										16.0
									</label>
								</div>
							</div>
						</td>
						<td class="tdWidth">
							<h4>Date Range</h4>
							<div class="form" style="float: right; width: 250px; margin-right: 15px;">
								<div class="form-group">
									<div class="input-group">
										<input type="text" class="form-control shawCalRanges" id="document_updated_on_date_range" name="date_range">
										<div class="input-group-append">
											<span class="input-group-text">
												<span class="fa fa-filter"></span>
											</span>
										</div>
									</div>
								</div>
							</div>
						</td>
					</form>
				</tr>
			</table>
		</div>
	</div>
	<hr>
	<div style="padding: 10px;">
		<button class="btn btn-sm primary filter_div_export_cls_1" style="width:135px;" id="filter_div_save">Apply and Export</button>
		<button class="btn btn-sm primary filter_div_apply_cls_1" style="width:135px" id="filter_div_apply">Apply</button>
		<button class="btn btn-sm btn-dark filter_div_close_cls_1" style="width:100px" id="filter_div_close">Close</button>
	</div>


	<script type="text/javascript">
		$(document).ready(function() {
			display_filter_option_onurl();
		});

		function display_filter_option_onurl() {
			var value = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
			/* if (value == "document_view" || value == "document_view?upload_confirmation=yes" || value == "document_unclassified") {
				$(".document_filter_element").show();
				$(".default_filter_element").hide();
				$(".vendor_license_filter_element").hide();
				$('.know_your_format_filter').hide();
				$(".document_history_filter").hide()

			} else if (value == "licence_tracker_document" || value == "my_license_tracker" || value == "my_license") {
				$(".default_filter_element").hide();
				$(".document_filter_element").hide();
				$(".vendor_license_filter_element").show();
				$('.know_your_format_filter').hide();
				$(".document_history_filter").hide();
			} else if (value == "know_your_format") {
				$(".default_filter_element").hide();
				$(".document_filter_element").hide();
				$(".vendor_license_filter_element").hide();
				$('.know_your_format_filter').show();
				$(".document_history_filter").hide();
			} else if (value == "document_history") {
				$(".default_filter_element").hide();
				$(".document_filter_element").hide();
				$(".vendor_license_filter_element").hide();
				$('.know_your_format_filter').hide();
				$(".document_history_filter").show();
			}
			 else */
			if (value == "recordtoreport") {
				$('.recordtoreport_filter').show();
				$('.common_filter').show();
				$('.fpa_filter').hide();
				$('.document_filter_element').hide();
			} else if (value == "fpa") {
				$('.recordtoreport_filter').hide();
				$('.common_filter').show();
				$('.fpa_filter').show();
				$('.document_filter_element').hide();
			} else if (value == "request_list") {
				$('.recordtoreport_filter').hide();
				$('.common_filter').hide();
				$('.fpa_filter').hide();
				$('.document_filter_element').show();
			}
		}
		setTimeout(function() {
			// Date Range Filter
			var start = moment().subtract(29, 'days');
			var end = moment();

			function cb(start, end) {
				$('#document_updated_on_date_range').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
				console.log("A new date selection was made: " + start.format('YYYY:MM:DD') + ' to ' + end.format('YYYY:MM:DD'));
			}

			$('#document_updated_on_date_range').daterangepicker({
				autoApply: true,
				startDate: start,
				endDate: end,
				ranges: {
					'Today': [moment(), moment()],
					'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					'This Month': [moment().startOf('month'), moment().endOf('month')],
					'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				},
				alwaysShowCalendars: true,
				locale: {
					format: 'MMM D, YYYY'
					// format: 'YYYY:MM:D'
				}
			}, cb);

			cb(start, end);
		}, 2000);
	</script>