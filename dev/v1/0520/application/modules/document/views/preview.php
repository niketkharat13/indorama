<style type="text/css">
    .preview {
        width: 100%;
        border: none;
    }

    .comment_box {
        width: 300px !important;
    }

    .default_access {
        width: 250px !important;
    }
</style>
<div class="modal fade text-left" id="pdf_preview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document" style="height:310% !important">
        <div class="modal-content" style="height: 30%">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel16">Preview / Document.pdf</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe src="<?= base_url() ?>app-assets/Test.pdf" class="preview" height="100%"></iframe>
            </div>
            <div class="modal-footer">
                <div class="search_dropdown_div col-2">
                    <div id="comment_month_div">
                        <select name="traditional[multiple][]" id="comment_month" class="multiple-input default_access" multiple>
                            <option value='jan'>January</option>
                            <option value='feb'>February</option>
                            <option value='mar'>March</option>
                            <option value='apr'>April</option>
                            <option value='may'>May</option>
                            <option value='jun'>June</option>
                            <option value='jul'>July</option>
                            <option value='aug'>August</option>
                            <option value='sept'>September</option>
                            <option value='oct'>October</option>
                            <option value='nov'>November</option>
                            <option value='dec'>December</option>
                        </select>
                    </div>
                </div>
                <textarea class="form-control comment_box" rows=2 style="display:none" id="comment_box"></textarea>
                <button type="button" class="btn btn-danger-yu round btn-min-width mr-1 btn-standard-yu" onclick="commentboxDisplay('comment_box','add_comment_btn','void_btn_id','comment_month_div','accept_btn')" id="void_btn_id">Void</button>
                <button type="button" class="btn btn-danger-yu round btn-min-width mr-1 btn-standard-yu" style="display:none" id="add_comment_btn" onclick="submitComment('preview_modal','add_comment_btn','void_btn_id','comment_box','accept_btn')" data-dismiss="modal">Add Comment</button>
                <button type="button" class="btn-lg round btn-min-width mr-1 btn-standard btn-success-yu" data-dismiss="modal" onclick="documentAccepted()" id="accept_btn">Accept</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="preview_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document" style="height:310% !important">
        <div class="modal-content" style="height: 30%">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel16">Preview / Document.pdf</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe src="<?= base_url() ?>app-assets/Test.pdf" class="preview" height="100%"></iframe>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<!-- Start of Preview JS -->
<script type="text/javascript">
    $('#myModal').on('shown.bs.modal', function() {
        $('#myInput').trigger('focus')
    })

    function commentboxDisplay(comment_id, add_comment_btn_id, void_btn, monthDropdown, accept_btn) {
        $('#' + comment_id).toggle();
        $('#' + add_comment_btn_id).toggle();
        $('#' + void_btn).toggle();
        $('#' + monthDropdown).hide();
        $('#' + accept_btn).hide();
    }

    function submitComment(modal, add_comment_btn, void_btn, comment_id, accept_btn) {
        $('#' + comment_id).hide();
        $('#' + add_comment_btn).hide();
        $('#' + void_btn).show();
        $("#" + modal).modal('hide');
        $('#' + accept_btn).show();
        toastr.success("Comment Submitted Successfully");
        toastr.success("Document Rejected");
    }

    function documentAccepted() {
        toastr.success("Document Accepted");
    }
</script>
<!-- End of Preview JS -->