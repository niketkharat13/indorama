<?php $this->load->view('header'); ?>
<?php $this->load->view('top'); ?>
<?php $this->load->view('vertical_navbar'); ?>
<link rel="stylesheet" href="<?= base_url() ?>app-assets/css/table.css">
<?php $this->load->view('document_css'); ?>
<style type="text/css">
    .count_fount {
        font-size: 30px;
    }
</style>
<div style="width:103%">
    <br>
    <div class="col">
        <div class="card-yu">
            <div class="main_list_div p-1">
                <div class="filter_div" id="filter_div" style="display: none;">
                    <?php $this->load->view('document_filter_view'); ?>
                </div>
                <div class="mb-1">
                    <div class="p-1 row d-flex align-items-center justify-content-between">
                        <h4 class="ml-1">Document History</h4>
                        <div class="button_div col-lg-10 col-md-10 col-sm-12 d-flex justify-content-end">
                            <button class="btn-yu primary mr-1 table_buttons" id="filter_button" title="Filter">
                                <i class="ft-filter text-white"></i>
                            </button>
                            <!-- <button id="upload_button" class="btn-yu primary table_buttons" title="Bulk Upload" data-toggle="modal" data-target="#upload_file">
                                <i class="ft-upload text-white"></i>
                            </button> -->

                        </div>
                    </div>
                </div>

                <table class="table display" id="document_history" width="100%" border="0" cellpadding="0" cellspacing="0">
                    <thead>
                        <th class="thead-custom">Version</th>
                        <th class="thead-custom">Updated on</th>
                        <th class="thead-custom">Updated By</th>
                    </thead>
                    <tbody>
                        <tr>
                            <th>
                                Version 1
                            </th>
                            <td>
                               23 Mar 2020
                            </td>
                            <td>Niket K</td>
                        </tr>
                        <tr>
                            <th>
                                Version 2
                            </th>
                            <td>
                                22 Apr 2020
                            </td>
                            <td>Aniket Y</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- preview modal  -->
<?php $this->load->view('preview'); ?>
<!-- preview modal  -->
<?php $this->load->view('footer'); ?>
<?php $this->load->view('document_js'); ?>