
<style type="text/css">
    .dropdown-menu>li {
        position: relative;
        margin-bottom: 1px;
    }

    .dropdown-menu>li>a {
        padding: 8px 10px;
        outline: 0;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .dropdown-menu>li>a {
        display: block;
        padding: 3px 20px;
        clear: both;
        font-weight: normal;
        line-height: 1.5384616;
        color: #333333;
        white-space: nowrap;
    }

    .btn-group,
    .btn-group-vertical {
        position: relative;
        display: inline-block;
        vertical-align: middle;
    }

    .bg-blue {
        background-color: #03A9F4;
        border-color: #03A9F4;
        color: #fff;
    }

    .bg-green {
        background-color: #8BC34A;
        border-color: #8BC34A;
        color: #fff;
    }

    .bg-orange {
        background-color: #FF9800;
        border-color: #FF9800;
        color: #fff;
    }

    .bg-danger {
        background-color: #F44336;
        border-color: #F44336;
        color: #fff;
    }

    .bg-success {
        background-color: #4CAF50 !important;
        border-color: #4CAF50;
        color: #fff;
    }

    .status-label {
        font-weight: 500;
        padding: 2px 5px 1px 5px;
        line-height: 1.5384616;
        border: 1px solid transparent;
        /*text-transform: uppercase;*/
        font-size: 12px;
        letter-spacing: 0.1px;
        border-radius: 0.25rem;
        width: 90px;
        display: flex;
        justify-content: space-around;
        align-items: center;
        padding: 2px;
    }

    .status-label:hover {
        color: #fff;
    }

    /*Dropdown Option*/
    .border-danger {
        border-color: #F44336;
    }

    .border-success {
        border-color: #4CAF50;
    }

    .border-warning {
        border-color: #FF5722;
    }

    .border-blue {
        border-color: #03A9F4;
    }

    .position-left {
        margin-right: 7px;
    }

    .status-mark {
        width: 8px;
        height: 8px;
        display: inline-block;
        border-radius: 50%;
        border: 2px solid;
    }
</style>
<!-- END Status Change CSS -->
<style>
    @media (min-width: 316px) and (max-width: 1024px) {

        .nav.nav-tabs.nav-justified {
            width: 82% !important;
            z-index: 10;
        }
    }

    @media (min-width: 316px) and (max-width: 1024px) {
        .nav.nav-tabs.nav-justified {
            width: 100% !important;

        }

        .nav.nav-tabs.nav-justified .nav-item a.nav-link {
            font-size: 12px;
        }

        .margin-top-50 {
            margin-top: -5px !important;
        }
    }

    @media only screen and (max-width: 425px) {
        .nav.nav-tabs.nav-justified {
            display: flex;
            flex-direction: column;
        }
    }



    .nav-vertical .nav-left.nav-tabs li.nav-item a.nav-link {
        min-width: 9rem;
        border-right: 2px solid #DDDDDD;
    }

    .nav-vertical .nav-left.nav-tabs.nav-border-left li.nav-item a.nav-link.active {
        border-left: 3px solid #008000;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        color: #555555;
    }

    .nav-vertical .nav-left.nav-tabs.nav-border-left li.nav-item a.nav-link {
        color: #00517cf5;
    }

    .tabs-styles {
        border-bottom: 1px solid black;
    }

    .table thead th {
        border-top: none !important;
    }

    .dataTables_wrapper .dataTables_filter input {
        margin-bottom: 6px;
    }

    .table-options-left {
        position: fixed;
        left: 36px;
    }

    table.dataTable thead th,
    table.dataTable thead td {
        border-bottom: 1px solid #e3ebf3;
    }

    button.dt-button {
        text-align: left !important;
    }

    .table {
        color: black;
        width: 100%;
    }

    .table thead th {
        vertical-align: bottom;
        border-bottom: 3px solid #003B7E;
        border-top: 1px solid #E3EBF3 !important;
    }

    .table th,
    .table td {
        border-top: none;
    }

    .page-item.active .page-link {
        z-index: 1;
        color: #FFFFFF;
        background-color: #003B7E;
        border-color: #003B7E;
    }

    .btn-secondary {
        border-color: #003b7e !important;
        background-color: #003B7E !important;
        color: #FFFFFF;
    }

    .btn-secondary:hover {
        border-color: #003b7e !important;
        background-color: white !important;
        color: #003b7e !important;
        transition-delay: 10s;
        -webkit-transition-delay: 10s;
        /* for Safari & Chrome */
    }



    .download_button {
        background: none !important;
    }

    .download_button:hover {
        background-image: linear-gradient(to right, #00517cf5 0%, #00517cf5 100%) !important;
        border-color: #fff !important;
        color: white;
    }

    .table-loader {
        visibility: visible;
        display: table-caption;
        content: " ";
        width: 100%;
        height: 600px;
        background-image:
            linear-gradient(rgba(235, 235, 235, 1) 1px, transparent 0),
            linear-gradient(90deg, rgba(235, 235, 235, 1) 1px, transparent 0),
            linear-gradient(90deg, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0.5) 15%, rgba(255, 255, 255, 0) 30%),
            linear-gradient(rgba(240, 240, 242, 1) 35px, transparent 0);

        background-repeat: repeat;

        background-size:
            1px 35px,
            calc(100% * 0.1666666666) 1px,
            30% 100%,
            2px 70px;

        background-position:
            0 0,
            0 0,
            0 0,
            0 0;

        animation: shine 0.5s infinite;
    }

    table.dataTable.no-footer {
        border-bottom: 1px solid #e3ebf3;
    }

    @keyframes shine {
        to {
            background-position:
                0 0,
                0 0,
                40% 0,
                0 0;
        }
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button {
        height: 35px;
        color: #fff !important;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current,
    .dataTables_wrapper .dataTables_paginate .paginate_button.current {
        background: none !important;
        color: #003b7e !important;
        outline: none;
        border: 1px solid #003b7e !important;
        border-radius: 7px;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current,
    .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {

        color: #fff !Important;
    }

    .dataTables_wrapper .dataTables_filter input {
        border: none;
        border-bottom: 1px solid black;
        padding: 5px;
        outline: none;
    }

    .table-heading-elements-yu {
        position: absolute;

    }

    .table-card-header-yu {
        position: relative;
    }

    .table-yu {
        margin-top: 2.5rem !important;
    }

    .table-yu-margin-top {
        margin-top: -14.5rem !important;
    }

    .hide_button:hover {
        background-image: linear-gradient(to right, #003B7E 0%, #003B7E 100%) !important;
        border-color: #fff !important;
        color: #fff !important;
    }

    .page_length_button:hover {
        background-image: linear-gradient(to right, #003B7E 0%, #003B7E 100%) !important;
        border-color: #fff !important;
        color: #fff !important;
    }

    .excel_button:hover {
        background-image: linear-gradient(to right, #003B7E 0%, #003B7E 100%) !important;
        border-color: #fff !important;
        color: #fff !important;
    }

    .hide_button {
        background-image: linear-gradient(to bottom, #fff 0%, #ffffff 100%) !important;
        border-color: #000;
    }

    .page_length_button {
        background-image: linear-gradient(to bottom, #fff 0%, #ffffff 100%) !important;
        border-color: #000;
    }

    .excel_button {
        background-image: linear-gradient(to bottom, #fff 0%, #ffffff 100%) !important;
        border-color: #000;
    }

    .card-title {
        margin-bottom: 0rem;
    }

    .position-right {
        position: absolute;
        top: 0;
        right: 0;
    }

    .filter_button {
        border-radius: 0px !important;
    }


    .filter_div {
        width: 100%;
        position: absolute;
        background-color: #f4f4f4;
        z-index: 100;
        box-shadow: 0px 4px 4px #c5c5c5;
    }

    .card-height {
        height: 175px;
        background-color: #f4f4f4;
        margin-bottom: 0px;
    }

    .checkbox label:after,
    .radio label:after {
        content: '';
        display: table;
        clear: both;
    }


    .checkbox .cr,
    .radio .cr {
        position: relative;
        display: inline-block;
        border: 1px solid #a9a9a9;
        border-radius: .25em;
        width: 1.3em;
        height: 1.3em;
        margin-right: .5em;
    }

    .radio .cr {
        border-radius: 50%;
    }

    .checkbox .cr .cr-icon,
    .radio .cr .cr-icon {
        position: absolute;
        font-size: .8em;
        line-height: 0;
        top: 50%;
        left: 20%;
    }

    .radio .cr .cr-icon {
        margin-left: 0.04em;
    }

    .checkbox label input[type="checkbox"],
    .radio label input[type="radio"] {
        display: none;
    }

    .checkbox label input[type="checkbox"]+.cr>.cr-icon,
    .radio label input[type="radio"]+.cr>.cr-icon {
        transform: scale(3) rotateZ(-20deg);
        opacity: 0;
        transition: all .3s ease-in;
    }

    .checkbox label input[type="checkbox"]:checked+.cr>.cr-icon,
    .radio label input[type="radio"]:checked+.cr>.cr-icon {
        transform: scale(1) rotateZ(0deg);
        opacity: 1;
    }

    .checkbox label input[type="checkbox"]:disabled+.cr,
    .radio label input[type="radio"]:disabled+.cr {
        opacity: .5;
    }

    .dataTables_wrapper.no-footer div.dataTables_scrollHead table.dataTable,
    .dataTables_wrapper.no-footer div.dataTables_scrollBody>table {
        margin-top: 0px !important;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current,
    .dataTables_wrapper .dataTables_paginate .paginate_button.current {
        color: #000 !important;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current,
    .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        color: #000 !important;
    }

    .option_style {
        overflow-y: hidden;
        height: 125px;
    }

    .option_style:hover {
        overflow-y: auto;
    }

    ::-webkit-scrollbar {
        height: 8px;
        width: 8px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        border-radius: 8px;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: #c5c5c5;
        border-radius: 10px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
        background: #003B7E;
    }

    .link {
        color: #008FA1 !important;
    }

    .confirm-style {
        padding: 0.5rem 0.75rem !important;
        font-size: 0.875rem !important;
        line-height: 1 !important;
        border-radius: 0.21rem !important;
        background-color: #003b7e !important;
        color: #fff !important;
    }

    .jconfirm.jconfirm-white .jconfirm-box .jconfirm-buttons button.btn-default:hover,
    .jconfirm.jconfirm-light .jconfirm-box .jconfirm-buttons button.btn-default {
        padding: 0.5rem 0.75rem !important;
        font-size: 0.875rem !important;
        line-height: 1 !important;
        border-radius: 0.21rem !important;
        background-color: #003b7e !important;
        color: #fff !important;
    }

    .jconfirm .jconfirm-box {
        color: #003b7e;

    }

    div.dt-button-collection button.dt-button,
    div.dt-button-collection div.dt-button,
    div.dt-button-collection a.dt-button {
        border: none !important;
        background: none !important;
    }

    .checkbox label:after,
    .radio label:after {
        content: '';
        display: table;
        clear: both;
    }

    th input {
        padding-left: 5px;
        padding-right: 5px
    }

    .checkbox .cr,
    .radio .cr {
        position: relative;
        display: inline-block;
        border: 1px solid #a9a9a9;
        border-radius: .25em;
        width: 1.3em;
        height: 1.3em;
        margin-right: .5em;
    }

    .radio .cr {
        border-radius: 50%;
    }

    .checkbox .cr .cr-icon,
    .radio .cr .cr-icon {
        position: absolute;
        font-size: .8em;
        line-height: 0;
        top: 50%;
        left: 20%;
        cursor: pointer;
    }

    .radio .cr .cr-icon {
        margin-left: 0.04em;
    }

    .checkbox label input[type="checkbox"],
    .radio label input[type="radio"] {
        display: none;
    }

    .checkbox label input[type="checkbox"]+.cr>.cr-icon,
    .radio label input[type="radio"]+.cr>.cr-icon {
        transform: scale(3) rotateZ(-20deg);
        opacity: 0;
        transition: all .3s ease-in;
    }

    .checkbox label input[type="checkbox"]:checked+.cr>.cr-icon,
    .radio label input[type="radio"]:checked+.cr>.cr-icon {
        transform: scale(1) rotateZ(0deg);
        opacity: 1;
    }

    .checkbox label input[type="checkbox"]:disabled+.cr,
    .radio label input[type="radio"]:disabled+.cr {
        opacity: .5;
    }

    button.dt-button:active:not(.disabled):hover:not(.disabled),
    button.dt-button.active:not(.disabled):hover:not(.disabled),
    div.dt-button:active:not(.disabled):hover:not(.disabled),
    div.dt-button.active:not(.disabled):hover:not(.disabled),
    a.dt-button:active:not(.disabled):hover:not(.disabled),
    a.dt-button.active:not(.disabled):hover:not(.disabled) {

        background: #0073b0 !important;
        color: #fff !important;

    }

    button.dt-button:active:not(.disabled):hover,
    button.dt-button.active:not(.disabled):hover,
    div.dt-button:active:not(.disabled):hover,
    div.dt-button.active:not(.disabled):hover,
    a.dt-button:active:not(.disabled):hover,
    a.dt-button.active:not(.disabled):hover {
        background: #0073b0;
        color: #000 !important;

    }

    div.dt-button-collection button.dt-button.active:not(.disabled) {
        margin-bottom: 0px;
    }

    .dt-button:hover {
        color: #fff !important;
    }

    .dt-button.button-page-length:hover {
        color: black;
    }

    div.dt-button-collection button.dt-button,
    div.dt-button-collection div.dt-button,
    div.dt-button-collection a.dt-button:hover {
        color: #000 !important;
    }

    .dt-button:focus {
        background-color: red !important;
        color: #fff !important;

    }

    div.dt-button-collection button.dt-button.active:not(.disabled) {
        background: #6f6f6f !important;
        color: #fff !important;
    }

    button.dt-button:hover {
        background: #6f6f6f;
    }

    div.dt-button-collection {
        padding: 0px !important;
    }

    div.dt-button-collection {
        width: 120px;

    }

    div.dt-button-background {
        background: none;
    }

    .btn-default {
        height: 35px !important;
        padding: 0;
        width: 100px !important;
        border-radius: 25px !important;
    }

    .table td {
        padding: 0.25rem !important;
    }

    .toggle-small {
        cursor: pointer;
        text-indent: -9999px;
        width: 27px;
        height: 15px;
        background: grey;
        border-radius: 100px;
        position: relative;
    }

    .toggle-small:after {
        content: '';
        position: absolute;
        top: 3px;
        left: 2px;
        width: 9px;
        height: 9px;
        background: #fff;
        border-radius: 90px;
        transition: 0.3s;
    }

    .table th {
        padding-top: 0.4rem !important;
        padding-bottom: 0.4rem !important;
        padding-right: 0.4rem !important;
        padding-left: 0.2rem !important;
    }

    .table th,
    .table td {
        border-bottom: 0.2px solid #eff7ff !important;
    }

    label {
        margin-bottom: 0;
    }

    .btn-default {
        height: 35px !important;
        padding: 0;
        width: 100px !important;
        border-radius: 25px !important;
    }

    .table td {
        padding: 0.25rem !important;
    }

    .toggle-small {
        cursor: pointer;
        text-indent: -9999px;
        width: 27px;
        height: 15px;
        background: grey;
        border-radius: 100px;
        position: relative;
    }

    .toggle-small:after {
        content: '';
        position: absolute;
        top: 3px;
        left: 2px;
        width: 9px;
        height: 9px;
        background: #fff;
        border-radius: 90px;
        transition: 0.3s;
    }

    .table th {
        padding-top: 0.4rem !important;
        padding-bottom: 0.4rem !important;
        padding-right: 0.4rem !important;
        padding-left: 0.2rem !important;
    }

    .table th,
    .table td {
        border-bottom: 0.2px solid #eff7ff !important;
    }

    label {
        margin-bottom: 0;
    }

    #circle5 {
        background-color: #f26229 !important;
    }

    .tb th {
        padding-top: 0.4rem !important;
        padding-bottom: 0.4rem !important;
        padding-right: 0.4rem !important;
        padding-left: 0.2rem !important;
    }

    .tb td {
        padding: 0.25rem !important;
    }

    .tb {
        width: 100% !important;
    }

    .thead-custom {

        background-color: #0a5881 !important;
    }

    .table-responsive {
        height: 287px;
        overflow-y: scroll;
    }

    .date_field_style {
        padding: 5px;
        height: 30px;
    }

    .dropdown_field_style {
        padding: 5px;
        height: 30px;
    }

    .date_field_label {
        margin-top: 6px;
    }

    .ft-paperclip {
        color: #0a5881;
        font-size: 16px;
        cursor: pointer;
    }

    div.dt-button-collection button.dt-button,
    div.dt-button-collection div.dt-button,
    div.dt-button-collection a.dt-button {
        border: none !important;
        background: none !important;
    }

    .checkbox label:after,
    .radio label:after {
        content: '';
        display: table;
        clear: both;
    }

    th input {
        padding-left: 5px;
        padding-right: 5px
    }

    .checkbox .cr,
    .radio .cr {
        position: relative;
        display: inline-block;
        border: 1px solid #a9a9a9;
        border-radius: .25em;
        width: 1.3em;
        height: 1.3em;
        margin-right: .5em;
    }

    .radio .cr {
        border-radius: 50%;
    }

    .checkbox .cr .cr-icon,
    .radio .cr .cr-icon {
        position: absolute;
        font-size: .8em;
        line-height: 0;
        top: 50%;
        left: 20%;
        cursor: pointer;
    }

    .radio .cr .cr-icon {
        margin-left: 0.04em;
    }

    .checkbox label input[type="checkbox"],
    .radio label input[type="radio"] {
        display: none;
    }

    .checkbox label input[type="checkbox"]+.cr>.cr-icon,
    .radio label input[type="radio"]+.cr>.cr-icon {
        transform: scale(3) rotateZ(-20deg);
        opacity: 0;
        transition: all .3s ease-in;
    }

    .checkbox label input[type="checkbox"]:checked+.cr>.cr-icon,
    .radio label input[type="radio"]:checked+.cr>.cr-icon {
        transform: scale(1) rotateZ(0deg);
        opacity: 1;
    }

    .checkbox label input[type="checkbox"]:disabled+.cr,
    .radio label input[type="radio"]:disabled+.cr {
        opacity: .5;
    }

    button.dt-button:active:not(.disabled):hover:not(.disabled),
    button.dt-button.active:not(.disabled):hover:not(.disabled),
    div.dt-button:active:not(.disabled):hover:not(.disabled),
    div.dt-button.active:not(.disabled):hover:not(.disabled),
    a.dt-button:active:not(.disabled):hover:not(.disabled),
    a.dt-button.active:not(.disabled):hover:not(.disabled) {

        background: #0073b0 !important;
        color: #fff !important;

    }

    button.dt-button:active:not(.disabled):hover,
    button.dt-button.active:not(.disabled):hover,
    div.dt-button:active:not(.disabled):hover,
    div.dt-button.active:not(.disabled):hover,
    a.dt-button:active:not(.disabled):hover,
    a.dt-button.active:not(.disabled):hover {
        background: #0073b0;
        color: #000 !important;

    }

    div.dt-button-collection button.dt-button.active:not(.disabled) {
        margin-bottom: 0px;
    }

    .dt-button:hover {
        color: #fff !important;
    }

    .dt-button.button-page-length:hover {
        color: black;
    }

    div.dt-button-collection button.dt-button,
    div.dt-button-collection div.dt-button,
    div.dt-button-collection a.dt-button:hover {
        color: #000 !important;
    }

    .dt-button:focus {
        background-color: red !important;
        color: #fff !important;

    }

    div.dt-button-collection button.dt-button.active:not(.disabled) {
        background: #6f6f6f !important;
        color: #fff !important;
    }

    button.dt-button:hover {
        background: #6f6f6f;
    }

    div.dt-button-collection {
        padding: 0px !important;
    }

    div.dt-button-collection {
        width: 120px;

    }

    div.dt-button-background {
        background: none;
    }
</style>

<style>
    .pdf_icon {
        font-size: 20px !important;
        color: #00517cf5;
        cursor: pointer;
    }

    .preview {
        width: 100%;
        border: none;
    }

    .document_name_pdf {
        font-size: 16px;

    }

    .void_button {
        background: none !important;
    }

    .legend_table,
    th,
    td {
        border: 1px solid #c5c5c5;
    }



    .heading {
        background-color: #003B7E;
        color: white
    }

    .sign {
        background-color: #d5e1e7;
    }

    .discription {
        width: 152px;
    }

    html body a:hover {
        color: #00517cf5;
    }

    .nav.nav-tabs.nav-justified {
        width: 60%;
        z-index: 10;
    }

    .nav-tabs .nav-link.active,
    .nav-tabs .nav-item.show .nav-link {
        color: #00517cf5;
    }

    .margin-top-50 {
        margin-top: -50px;
    }

    /* #year_selection_footer{
        width:  185px !important;
    } */

    .comm_height {
        height: 30px;
    }
</style>

<!-- My Document CSS -->
<style>
    #outerContainer #sidebarContainer #mainContainer #toolbarContainer #toolbarViewer {
        /* to hide toolbar in pdf view */
        display: none !important;
    }

    #outerContainer #mainContainer div.toolbar {
        display: none !important;
        /* hide PDF viewer toolbar */
    }

    #outerContainer #mainContainer #viewerContainer {
        top: 0 !important;
        /* move doc up into empty bar space */
    }

    iframe viewer-pdf-toolbar {
        display: none !important;
    }

    .height-color {
        font-size: 19px;
        color: #00517cf5;
        cursor: pointer;
    }

    .table_buttons {
        width: 35px;
        border-radius: 50%;
    }

    .preview_text {
        margin-top: 10px;
        float: right;
        display: none;
        cursor: pointer;
    }

    .edit_preview_text {
        margin-top: 10px;
        float: right;
        cursor: pointer;
    }
</style>
<!-- Status Change CSS -->
<style type="text/css">
    .dropdown-menu>li {
        position: relative;
        margin-bottom: 1px;
    }

    .dropdown-menu>li>a {
        padding: 8px 10px;
        outline: 0;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .dropdown-menu>li>a {
        display: block;
        padding: 3px 20px;
        clear: both;
        font-weight: normal;
        line-height: 1.5384616;
        color: #333333;
        white-space: nowrap;
    }

    .btn-group,
    .btn-group-vertical {
        position: relative;
        display: inline-block;
        vertical-align: middle;
    }

    .bg-blue {
        background-color: #03A9F4;
        border-color: #03A9F4;
        color: #fff;
    }

    .bg-green {
        background-color: #8BC34A;
        border-color: #8BC34A;
        color: #fff;
    }

    .bg-orange {
        background-color: #FF9800;
        border-color: #FF9800;
        color: #fff;
    }

    .bg-danger {
        background-color: #F44336;
        border-color: #F44336;
        color: #fff;
    }

    .bg-success {
        background-color: #4CAF50 !important;
        border-color: #4CAF50;
        color: #fff;
    }

    .status-label {
        font-weight: 500;
        padding: 2px 5px 1px 5px;
        line-height: 1.5384616;
        border: 1px solid transparent;
        /*text-transform: uppercase;*/
        font-size: 12px;
        letter-spacing: 0.1px;
        border-radius: 0.25rem;
        width: 100px;
        display: flex;
        justify-content: space-around;
        align-items: center;
        padding: 2px;
    }

    .status-label:hover {
        color: #fff;
    }

    /*Dropdown Option*/
    .border-danger {
        border-color: #F44336;
    }

    .border-success {
        border-color: #4CAF50;
    }

    .border-warning {
        border-color: #FF5722;
    }

    .border-blue {
        border-color: #03A9F4;
    }

    .position-left {
        margin-right: 7px;
    }

    .status-mark {
        width: 8px;
        height: 8px;
        display: inline-block;
        border-radius: 50%;
        border: 2px solid;
    }
</style>
<!-- END Status Change CSS -->