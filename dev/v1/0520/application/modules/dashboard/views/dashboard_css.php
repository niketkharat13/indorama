	<link rel="stylesheet" href="<?= base_url() ?>app-assets/css/star_mobile_widget.css">

	<!-- styles for dashboard ----------->
	<style>
		.c3 text {
			font-family: 'Roboto', sans-serif !important;
			font-size: 12px;
			color:#6B6F82; 
		}

		.card-header .card-title{
			color:#6B6F82; 
		}

		.dash_icon {
			color: #003B7E;
			font-size: 22px;
		}

		.dashboard_main_div_1 {
			margin-left: -15px;
		}

		.dashboard_main_div_2 {
			margin-left: -25px;
		}

		.risk_widget_cls {
			margin-top: -10px;
			padding-left: 16px;
			padding-right: 30px;
			margin-bottom: 1rem;
		}

		.margin_bottom_comm {
			margin-bottom: 15px;
		}
	</style>

	<style type="text/css">
		.option-card {
			display: flex;
			justify-content: space-between;
			align-items: center;
			margin-top: 8px;
			margin-left: 20px;
			margin-right: 20px;
			border-bottom: 1px solid #0000003b;
		}

		.min-max {

			max-width: 606px;
			min-width: 599px;
			margin-left: 20px;
			height: 350px;
			margin-bottom: 15px;
		}

		.min-max-2 {
			max-width: 645px;
			min-width: 599px;
			margin-left: 15px;
			height: 350px;
			margin-bottom: 15px;

		}

		.min-max-3 {
			max-width: 1232px;
			min-width: 1231px;
			margin-left: 19px;
			margin-bottom: 15px;
		}

		.min-max-4 {
			min-width: 1300px;
			max-width: 1301px;
			margin-left: 14px;
			margin-bottom: 15px;
		}

		.min-widget-2 {
			width: 320px;
		}

		.min-widget {
			width: 300px;
		}



		.vertical-compact-menu.menu-open .content,
		.vertical-compact-menu.menu-open .footer {
			margin-left: 70px;
		}

		.margin-left-10 {
			margin-left: 20px;
		}

		.nav.nav-tabs.nav-underline .nav-item a.nav-link {
			color: #0A5781;
		}

		label {
			/*keep this style there*/
			margin-bottom: 10px;
		}

		.fix {
			width: 645px !important;
			height: 370px !important;
		}


		.btn-close {
			border: none;
			background: none;
		}

		.c3-chart-arc text {
			fill: #000;
			font-size: 14px;
			text-anchor: middle !important;
			transform: translate(-20, -30) !important;
			display: none;

		}

		.table td {
			border: none !important;
		}

		.nav.nav-tabs.nav-justified {
			width: 35%;
		}

		.center-yu {
			display: flex;
			align-items: center;
			/* height: 334px; */
			justify-content: center;
			margin-top: 3rem;
		}

		.c3-legend-item-data3 {
			margin-left: -10px;
		}

		.vertical-compact-menu.menu-open .content,
		.vertical-compact-menu.menu-open .footer {
			margin-left: 65px;
		}

		.pos {

			font-size: 18px;
			text-align: center;
			margin-left: 10px;

		}

		.pos-1 {

			font-size: 18px;
			text-align: center;
			margin-left: 10px;




		}

		.pos-2 {

			font-size: 18px;
			text-align: center;
			margin-left: 10px;
			margin-left: 10px;

		}

		.custom-view {
			display: flex;
			justify-content: space-between;
			align-items: center;
			width: 99%;
			margin: auto;
			margin-top: 30px;

		}

		.toggle-div {
			position: absolute;
			right: 0px;
			z-index: 10000;
			width: 260px;
			top: 45px;
			box-shadow: -6px 20px 11px rgba(0, 0, 0, 0.1);
		}

		.sidenav {
			height: 100%;
			width: 0;
			position: absolute;
			z-index: 1;
			top: 45px;
			right: 85px;
			overflow-x: hidden;
			transition: 0.5s;
		}

		.two-button {
			text-align: right;
			line-height: auto;
		}

		.la-clock-o:before {
			color: #ff5c73fc;
			font-size: 25px;
		}

		.la-check-circle-o:before {
			color: green;
			font-size: 25px;
		}

		.la-exclamation-triangle::before {
			font-size: 25px;
		}

		.bot-icon-style {
			font-size: 58px !important;
			color: GREEN;
			animation: blinker 1s cubic-bezier(.5, 0, 1, 1) infinite alternate;
		}

		@keyframes blinker {
			from {
				opacity: 1;
			}

			to {
				opacity: 0;
			}
		}

		.toggle-check {
			height: 0;
			width: 0;
			visibility: hidden;
		}

		.toggle {
			cursor: pointer;
			text-indent: -9999px;
			width: 74px;
			height: 35px;
			background: grey;
			display: block;
			border-radius: 100px;
			position: relative;
			margin-right: 100px !Important;
		}

		.toggle:after {
			content: '';
			position: absolute;
			top: 5px;
			left: 5px;
			width: 25px;
			height: 25px;
			background: #fff;
			border-radius: 90px;
			transition: 0.3s;
		}

		input:checked+.toggle {
			background: #bada55;
		}

		input:checked+.toggle:after {
			left: calc(100% - 5px);
			transform: translateX(-100%);
		}

		.input-box-standard {
			outline: none;
			border: none;
			border-bottom: 1px solid #ab127f;
			width: 200px;
			padding: 3px;
		}


		.customize-div {
			width: 100%;
			position: absolute;
			background-color: #f4f4f4;
			z-index: 100;
			box-shadow: 0px 4px 4px #c5c5c5;
			top: 7rem;
		}

		.option-style {
			font-size: 1rem;
			font-weight: 400;
			border-radius: 0.25rem;
			width: 30%;
			height: 28px;
			border: none;
			border-bottom: 3px solid #00517cf5;
			margin-top: -3px;
			padding-left: 3px;
			padding-right: 5px;
			outline: none;
		}

		#custom-button {
			background-color: #0073b0;
			width: 37px;
			border-radius: 10px !important;
		}

		.custom-button-size {
			font-size: 30px;
		}

		.status-mark {
			width: 8px;
			height: 8px;
			display: inline-block;
			border-radius: 50%;
			border: 2px solid;
		}

		.title-div {
			flex-grow: 1;
		}

		.yr-filter {
			font-size: 1rem;
			font-weight: 400;
			border-radius: 0.25rem;
			width: 150px;
			height: 28px;
			/* border: none; */
			border-bottom: 3px solid #00517cf5;
			border-color: #00517cf5;
			/* margin-top: -3px; */
			padding-left: 3px;
			padding-right: 5px;
			outline: none;
			background: none;
		}

		/* <!-- styles for dashboard ends ---------- --> */



		/* <!-- audit statistics css start--------- --> */
		/* table 1 and table 2 styles for task list */
		.table-1,
		.table-2 {
			width: 100%;
			text-align: center;
		}

		.nav.nav-tabs.nav-underline .nav-item a.newcls:before {
			position: absolute;
			bottom: -1px;
			left: 0;
			width: 100%;
			height: 4px;
			background: #bdb5b5fc;
			content: '';

		}

		.nav-bg {
			width: 100% !important;
		}

		.table th,
		.table td {
			border: none;
		}

		.stats {
			justify-content: space-between;
			align-items: center;
		}

		.newcls {
			color: #00517cf5 !important;

		}

		.newcls:hover {
			color: #00517cf5 !important;
		}

		.statistics-p-style-audit {
			margin-top: 10px;
			font-size: 18px;
			margin-left: 15px;
		}

		/* <!-- table 1 and table 2 styles for task list  ends --> */


		/* <!-- audit statistics css endss--------- --> */


		/* <!--  statistics css start--------- --> */

		/* table 1 and table 2 styles for task list */

		.table-1,
		.table-2 {
			width: 100%;
			text-align: center;
		}


		.nav.nav-tabs.nav-underline .nav-item a.newcls:before {
			position: absolute;
			bottom: -1px;
			left: 0;
			width: 100%;
			height: 4px;
			background: #bdb5b5fc;
			content: '';

		}


		.nav-bg {
			width: 100% !important;
		}

		.stats {
			justify-content: space-between;
			align-items: center;
		}

		.newcls {
			color: #00517cf5 !important;

		}

		.newcls:hover {
			color: #00517cf5 !important;
		}

		.statistics-p-style {
			margin-top: 10px;
			font-size: 18px;
			margin-left: 15px;
			flex-grow: 1
		}



		.table th,
		.table td {
			border: none;
		}

		.table-1 th,
		.table-1 td,
		.table-2 th,
		.table-2 td {
			border-bottom: 0.2px solid #eff7ff !important;

		}

		.yr-filter-1 {
			font-size: 1rem;
			font-weight: 400;
			border-radius: 0.25rem;
			width: 90px;
			height: 28px;
			/* border: none; */
			border-bottom: 4px solid #00517cf5;
			border-color: #00517cf5;
			/* margin-top: -3px; */
			padding-left: 3px;
			padding-right: 5px;
			outline: none;
			background: none;
		}

		/* table 1 and table 2 styles for task list ends*/

		/* <!--  statistics css ends--------- --> */


		/* <!--  statistics css ends--------- --> */


		/* tabs css-- start---------------------------------------------- */
		/* tabs styles-------  */
		.stats {
			justify-content: space-between;
			align-items: center;
		}

		.table-task-list {
			color: #6B6F82 !important;
		}

		/* tabs css-- ends---------------------------------------------- */


		/* widget_1 css starts---------------------- */

		.size-star {
			font-size: 76px;
			margin-bottom: -56px;
			margin-top: -14px;
			color: #f15561;
		}

		.option-style-2 {
			font-size: 1rem;
			font-weight: 400;
			border-radius: 0.25rem;
			width: 30%;
			height: 28px;
			border-bottom: 4px solid #00517cf5;
			border-color: #00517cf5;
			margin-top: 3px;
			margin-right: 23px;
			padding-left: 3px;
			padding-right: 5px;
			outline: none;
			background: none;
		}

		/* widget_1 css ends---------------------- */
	</style>
	<style type="text/css">
		.line-arrange {
			margin-left: -1%
		}

		.min-max-3 {
			max-width: 1213px;
			min-width: 1231px;
			margin-left: 27px;
			margin-bottom: 15px;

		}



		.minimal select {

			/* styling */
			background-color: white;
			border: thin solid #0a5881;
			border-radius: 4px;
			display: inline-block;

			line-height: 1.5em;
			padding: 0.5em 3.5em 0.5em 1em;

			/* reset */

			margin: 0;
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
			-webkit-appearance: none;
			-moz-appearance: none;
		}

		select.minimal {
			background-image:
				linear-gradient(45deg, transparent 50%, #0a5881 50%),
				linear-gradient(135deg, #0a5881 50%, transparent 50%),
				linear-gradient(to right, #ccc, #ccc);
			background-position:
				calc(100% - 20px) calc(1em + 2px),
				calc(100% - 15px) calc(1em + 2px),
				calc(100% - 2.5em) 0.5em;
			background-size:
				5px 5px,
				5px 5px,
				1px 1.5em;
			background-repeat: no-repeat;
		}

		select.minimal:focus {
			background-image:
				linear-gradient(45deg, #0a5881 50%, transparent 50%),
				linear-gradient(135deg, transparent 50%, #0a5881 50%),
				linear-gradient(to right, #ccc, #ccc);
			background-position:
				calc(100% - 15px) 1em,
				calc(100% - 20px) 1em,
				calc(100% - 2.5em) 0.5em;
			background-size:
				5px 5px,
				5px 5px,
				1px 1.5em;
			background-repeat: no-repeat;
			border-color: #0a5881;
			outline: 0;
		}


		select:-moz-focusring {
			color: transparent;
			text-shadow: 0 0 0 #000;
		}


		/* widget styles */
		.count_in_widget {
			font-size: 35px;
			font-weight: 600;
		}

		.star {
			color: green
		}

		.star3 {
			color: red;
		}

		.star2 {
			color: orange;
		}

		.dropdown_height {
			height: 30px;
		}

		.margin-left-30 {
			margin-left: -30px;
		}
	</style>
	<style>
		.pending_cls_a {
			background: #fa6f57;
			padding: 2px;
			border-radius: 3px;
			color: white;
		}

		.pending_cls_a:hover {
			color: white;
		}
	</style>
	<style type="text/css">
		.nav.nav-tabs.nav-justified {
			width: unset !important;
		}
	</style>
	<style type="text/css">
		.selectdiv {
			position: relative;
			/*Don't really need this just for demo styling*/

			float: left;
			min-width: 104%;
			margin-bottom: 9%;
		}

		/*To remove button from IE11, thank you Matt */
		select {

			/* styling */
			background-color: white;
			border: thin solid #0a5881;
			border-radius: 4px;
			display: inline-block;

			line-height: 1.5em;
			padding: 0.5em 3.5em 0.5em 1em;

			/* reset */

			margin: 0;
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
			-webkit-appearance: none;
			-moz-appearance: none;
		}

		select.minimal {
			background-image:
				linear-gradient(45deg, transparent 50%, #0a5881 50%),
				linear-gradient(135deg, #0a5881 50%, transparent 50%),
				linear-gradient(to right, #ccc, #ccc);
			background-position:
				calc(100% - 20px) calc(1em + 2px),
				calc(100% - 15px) calc(1em + 2px),
				calc(100% - 2.5em) 0.5em;
			background-size:
				5px 5px,
				5px 5px,
				1px 1.5em;
			background-repeat: no-repeat;
		}

		select.minimal:focus {
			background-image:
				linear-gradient(45deg, #0a5881 50%, transparent 50%),
				linear-gradient(135deg, transparent 50%, #0a5881 50%),
				linear-gradient(to right, #ccc, #ccc);
			background-position:
				calc(100% - 15px) 1em,
				calc(100% - 20px) 1em,
				calc(100% - 2.5em) 0.5em;
			background-size:
				5px 5px,
				5px 5px,
				1px 1.5em;
			background-repeat: no-repeat;
			border-color: #0a5881;
			outline: 0;
		}


		select:-moz-focusring {
			color: transparent;
			text-shadow: 0 0 0 #000;
		}

		.mr-9 {
			margin-right: -9.5rem !important;
		}
	</style>
	<style>
		.margin-adjust-widget {
			margin-bottom: 1px !important;
			height: 97px;
		}

		.info_impact_score_color {
			color: #ff0000;
		}

		.min-widget {
			width: auto !important;
		}

		.min-max-3 {
			min-width: auto;
		}

		@media (max-width: 575px) {
			.ImpactBottom {
				margin-bottom: 2%;
			}
		}

		.col5 {

			height: unset;
		}

		@media (max-width: 767.98px) {

			.col7,
			.col5 {
				height: unset !important;
			}
		}

		.padding-adjust {
			padding-right: unset !important;
		}

		.money-arrange {
			font-size: 265% !important;
			color: red;
		}

		.file-arrange {
			font-size: 265% !important;
			color: #f90;
		}

		.check-arrange {
			font-size: 265% !important;
			color: #ffd558;
		}
	</style>