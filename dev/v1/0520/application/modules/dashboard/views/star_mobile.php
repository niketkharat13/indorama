    <section class="mobile_widget mt-1 mb-1">
        <!-- vendor -->
        <div class="vendor_container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-yu p-1 mb-1">
                        <div class="row">
                            <h4 class="ml-1">Vendor Performance</h4>
                            <div class="col-sm-12 text-center">
                                <h2>Total :&nbsp;<span>34</span></h2>
                                <h4 style="color:#c5c5c5;font-size:1rem">Last Updated on : 10:54:12 05 (Mar 2020)</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6" style="padding-right:5px">
                    <div class="card-yu p-1 mb-1">
                        <p class="text-center">
                            <span class="widget_1_span" style="color:#464855;">(Past 12 Months)</span>
                        </p>
                        <div class="col">
                            <div>
                                <h5 class="text-muted widget_name text-bold-500 mb-0 count_in_widget text-center">
                                    <span id="excellent_count_total">10</span>&nbsp;</h5>
                            </div>
                            <div class="d-flex justify-content-center">
                                <h2 class="text-bold-600 star">
                                    ★★★★★</h2>
                            </div>
                            <small class="d-flex justify-content-center small_style_common">Excellent</small>
                        </div>
                        <div class="col mt-1">
                            <div>
                                <h5 class=" text-muted widget_name text-bold-500 mb-0 count_in_widget text-center">
                                    <span id="good_count_total">17</span>&nbsp;</h5>
                            </div>
                            <div class="d-flex justify-content-center widget2">
                                <h2 class="text-bold-600 star2">
                                    ★★★★</h2>
                            </div>
                            <small class="d-flex justify-content-center small_style_common">Good</small>
                        </div>
                        <div class="col mt-1">
                            <div>
                                <h5 class=" text-muted widget_name text-bold-500 mb-0 count_in_widget text-center">
                                    <span id="poor_count_total">7</span>&nbsp;
                                </h5>
                            </div>
                            <div class="widget2 d-flex justify-content-center">
                                <h2 class="text-bold-600 star3" data-toggle="tooltip" title="60% Below">
                                    ★★★</h2>
                            </div>
                            <small class="d-flex justify-content-center small_style_common">Poor</small>
                        </div>
                    </div>
                </div>
                <div class="col-6" style="padding-left:5px">
                    <div class="card-yu p-1 mb-1">
                        <div class="minimal text-center">
                            <form action="" id="cluster_master_widget_dropdown">
                                <select name="country" class="minimal" id="cluster_master_widget_dropdown">
                                    <option value="month_1">Jan 2020</option>
                                    <option value="month_2">Dec 2019</option>
                                    <option value="month_3">Nov 2019</option>
                                    <option value="month_4">Oct 2019</option>
                                    <option value="month_5">Sep 2019</option>
                                    <option value="month_6">Aug 2019</option>
                                    <option value="month_7">Jul 2019</option>
                                    <option value="month_8">Jun 2019</option>
                                    <option value="month_9">May 2019</option>
                                    <option value="month_10">Apr 2019</option>
                                    <option value="month_11">Mar 2019</option>
                                    <option value="month_12">Feb 2019</option>
                                </select>
                            </form>
                        </div>
                        <div class="col">
                            <div>
                                <h5 class="text-muted widget_name text-bold-500 mb-0 count_in_widget text-center">
                                    <span id="excellent_count_month">7</span>&nbsp;</h5>
                            </div>
                            <div class="d-flex justify-content-center">
                                <h2 class="text-bold-600 star">
                                    ★★★★★</h2>
                            </div>
                            <small class="d-flex justify-content-center small_style_common">Excellent</small>
                        </div>
                        <div class="col mt-1">
                            <div>
                                <h5 class=" text-muted widget_name text-bold-500 mb-0 count_in_widget text-center">
                                    <span id="good_count_month">4</span>&nbsp;</h5>
                            </div>
                            <div class="d-flex justify-content-center widget2">
                                <h2 class="text-bold-600 star2">
                                    ★★★★</h2>
                            </div>
                            <small class="d-flex justify-content-center small_style_common">Good</small>
                        </div>
                        <div class="col mt-1">
                            <div>
                                <h5 class=" text-muted widget_name text-bold-500 mb-0 count_in_widget text-center">
                                    <span id="poor_count_month">3</span>&nbsp;
                                </h5>
                            </div>
                            <div class="widget2 d-flex justify-content-center">
                                <h2 class="text-bold-600 star3" data-toggle="tooltip" title="60% Below">
                                    ★★★</h2>
                            </div>
                            <small class="d-flex justify-content-center small_style_common">Poor</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- site -->
        <div class="site_container" style="display: none;">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-yu p-1 mb-1">
                        <div class="row">
                            <h4 class="ml-1">Site Performance</h4>
                            <div class="col-sm-12 text-center">
                                <h2>Total :&nbsp;<span>34</span></h2>
                                <h4 style="color:#c5c5c5;font-size:1rem">Last Updated on : 10:54:12 05 (Mar 2020)</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6" style="padding-right:5px">
                    <div class="card-yu p-1 mb-1">
                        <p class="text-center">
                            <span class="widget_1_span" style="color:#464855;">(Past 12 Months)</span>
                        </p>
                        <div class="col">
                            <div>
                                <h5 class="text-muted widget_name text-bold-500 mb-0 count_in_widget text-center">
                                    <span id="excellent_count_total">10</span>&nbsp;</h5>
                            </div>
                            <div class="d-flex justify-content-center">
                                <h2 class="text-bold-600 star">
                                    ★★★★★</h2>
                            </div>
                            <small class="d-flex justify-content-center small_style_common">Excellent</small>
                        </div>
                        <div class="col mt-1">
                            <div>
                                <h5 class=" text-muted widget_name text-bold-500 mb-0 count_in_widget text-center">
                                    <span id="good_count_total">17</span>&nbsp;</h5>
                            </div>
                            <div class="d-flex justify-content-center widget2">
                                <h2 class="text-bold-600 star2">
                                    ★★★★</h2>
                            </div>
                            <small class="d-flex justify-content-center small_style_common">Good</small>
                        </div>
                        <div class="col mt-1">
                            <div>
                                <h5 class=" text-muted widget_name text-bold-500 mb-0 count_in_widget text-center">
                                    <span id="poor_count_total">7</span>&nbsp;
                                </h5>
                            </div>
                            <div class="widget2 d-flex justify-content-center">
                                <h2 class="text-bold-600 star3" data-toggle="tooltip" title="60% Below">
                                    ★★★</h2>
                            </div>
                            <small class="d-flex justify-content-center small_style_common">Poor</small>
                        </div>
                    </div>
                </div>
                <div class="col-6" style="padding-left:5px">
                    <div class="card-yu p-1 mb-1">
                        <div class="minimal text-center">
                            <form action="" id="cluster_master_widget_dropdown">
                                <select name="country" class="minimal" id="cluster_master_widget_dropdown">
                                    <option value="month_1">Jan 2020</option>
                                    <option value="month_2">Dec 2019</option>
                                    <option value="month_3">Nov 2019</option>
                                    <option value="month_4">Oct 2019</option>
                                    <option value="month_5">Sep 2019</option>
                                    <option value="month_6">Aug 2019</option>
                                    <option value="month_7">Jul 2019</option>
                                    <option value="month_8">Jun 2019</option>
                                    <option value="month_9">May 2019</option>
                                    <option value="month_10">Apr 2019</option>
                                    <option value="month_11">Mar 2019</option>
                                    <option value="month_12">Feb 2019</option>
                                </select>
                            </form>
                        </div>
                        <div class="col">
                            <div>
                                <h5 class="text-muted widget_name text-bold-500 mb-0 count_in_widget text-center">
                                    <span id="excellent_count_month">7</span>&nbsp;</h5>
                            </div>
                            <div class="d-flex justify-content-center">
                                <h2 class="text-bold-600 star">
                                    ★★★★★</h2>
                            </div>
                            <small class="d-flex justify-content-center small_style_common">Excellent</small>
                        </div>
                        <div class="col mt-1">
                            <div>
                                <h5 class=" text-muted widget_name text-bold-500 mb-0 count_in_widget text-center">
                                    <span id="good_count_month">4</span>&nbsp;</h5>
                            </div>
                            <div class="d-flex justify-content-center widget2">
                                <h2 class="text-bold-600 star2">
                                    ★★★★</h2>
                            </div>
                            <small class="d-flex justify-content-center small_style_common">Good</small>
                        </div>
                        <div class="col mt-1">
                            <div>
                                <h5 class=" text-muted widget_name text-bold-500 mb-0 count_in_widget text-center">
                                    <span id="poor_count_month">3</span>&nbsp;
                                </h5>
                            </div>
                            <div class="widget2 d-flex justify-content-center">
                                <h2 class="text-bold-600 star3" data-toggle="tooltip" title="60% Below">
                                    ★★★</h2>
                            </div>
                            <small class="d-flex justify-content-center small_style_common">Poor</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>