	

	<div>
		<div class="row">
			<div class="col-md-12">
				<div class="card-yu margin_bottom_comm" style="height:350px;">
					<div class="card-header pb-0">
						<h4 class="card-title">Audit Score</h4>
					</div>
					<div class="card-body">
						<div class="row ">
							<div class="col">
								<ul class="nav nav-tabs nav-underline no-hover-bg nav-justified nav-bg" style="width:unset !important">
									<li class="nav-item" onclick="show_dropdown('client_dropdown','account_dropdown')">
										<a class="nav-link active newcls" id="client_audit_score" data-toggle="tab" href="#client_tab" aria-controls="client_tab" aria-expanded="true">Client</a>
									</li>
									<li class="nav-item" onclick="show_dropdown('account_dropdown','client_dropdown')">
										<a class="nav-link newcls" id="account_audit_score" data-toggle="tab" href="#account_tab" aria-controls="account_tab" aria-expanded="false">Accounts</a>
									</li>
								</ul>
								<div class="tab-content px-1 pt-1">
									<div role="tabpanel" class="tab-pane active" id="client_tab" aria-labelledby="client_tab" aria-expanded="true">
										<div class=" table-responsive">
											<form class="audit_score_client_table">
												<table class=" table-1">
													<tbody>
														<tr>
															<th colspan="4" style="width: 190px" class="text-left">Name</th>
															<th>Full Score</th>
															<th>Critical Score</th>
															<th>Weighted Score</th>
														</tr>
														<tr>
															<td colspan="4" style="width: 190px" class="text-left">HDFC</td>
															<td>20%</td>
															<td>80%</td>
															<td>100%</td>

														</tr>
														<tr>
															<td colspan="4" style="width: 190px" class="text-left">SBI</td>
															<td>60%</td>
															<td>40%</td>
															<td>100%</td>
														</tr>
														<tr>
															<td colspan="4" style="width: 190px" class="text-left">RBL</td>
															<td>50%</td>
															<td>40%</td>
															<td>90%</td>
														</tr>
														<tr>
															<td colspan="4" style="width: 190px" class="text-left">JLL</td>
															<td>30%</td>
															<td>70%</td>
															<td>100%</td>
														</tr>
														<tr>
															<td colspan="4" style="width: 190px" class="text-left">Accenture</td>
															<td>30%</td>
															<td>40%</td>
															<td>70%</td>
														</tr>
													</tbody>
												</table>
											</form>
										</div>
									</div>
									<div class="tab-pane" id="account_tab" role="tabpanel" aria-labelledby="account_tab" aria-expanded="false">
										<div class="table-responsive">
											<form action="" class="audit_score_account_table">
												<table class=" table-2">
													<tbody>
														<tr>

															<th colspan="4" style="width: 190px" class="text-left">Name</th>
															<th>Full Score</th>
															<th>Critical Score</th>
															<th>Weighted Score</th>

														</tr>
														<tr>
															<td colspan="4" style=" width: 190px" class="text-left">JP Morgan Services India Pvt Ltd</td>
															<td>20%</td>
															<td>80%</td>
															<td>100%</td>
														</tr>

														<tr>
															<td colspan="4" style="width: 190px" class="text-left">JLL</td>
															<td>20%</td>
															<td>80%</td>
															<td>100%</td>

														</tr>
														<tr>
															<td colspan="4" style=" width: 190px" class="text-left">Accenture</td>
															<td>20%</td>
															<td>80%</td>
															<td>100%</td>
														</tr>

														<tr>
															<td colspan="4" style="width: 190px" class="text-left">Lehman Brothers</td>
															<td>20%</td>
															<td>80%</td>
															<td>100%</td>

														</tr>

													</tbody>
												</table>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<small class="ml-1">*Only visible to Vendor*</small>
				</div>
			</div>
		</div>
		
	</div>

	