<!-- User wise status  -->
<div class="row">
    <div class="col-12">
        <div class="card" style="height: 350px;">
            <div class="card-header pb-0">
                <h4 class="card-title">User wise_Request Status</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <div id="user_wise" style="margin-top: -1rem"></div>
                </div>
            </div>
        </div>
    </div>
</div>
