<style type="text/css">
    .tdWidth {
        min-width: 180px !important;
    }

    .default_filter_element_div .table-responsive {
        height: auto;
    }

    .filter_table label {
        margin-bottom: 4px;
    }

    .default_filter_element_div table {
        height: 200px !important;
    }
</style>

<div class="row">


    <div class="default_filter_element_div col-lg-12" style="white-space: nowrap;overflow-x: auto !important;overflow-y: auto !important;">
        <table class="table table-responsive filter_table" style="padding: 1%;margin-bottom: 0%;">
            <tr>
                <!-- default filter -->
                <td class="tdWidth default_filter_element">
                    <h4>Filter 1</h4>
                    <div class="option_style">
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option one
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option two
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option three
                            </label>
                        </div>
                    </div>
                </td>
                <td class="tdWidth default_filter_element">
                    <h4>Filter 2</h4>
                    <div class="option_style">
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option one
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option two
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option three
                            </label>
                        </div>
                    </div>
                </td>
                <td class="tdWidth default_filter_element">
                    <h4>Filter 3</h4>
                    <div class="option_style">
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option one
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option two
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option three
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option four
                            </label>
                        </div>
                    </div>
                </td>
                <td class="tdWidth default_filter_element">
                    <h4>Filter 4</h4>
                    <div class="option_style" class="option_style">
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option one
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option two
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option three
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option four
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option five
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option six
                            </label>
                        </div>
                    </div>
                </td>
                <td class="tdWidth default_filter_element">
                    <h4>Filter 5</h4>
                    <div class="option_style">
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option one
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option two
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option three
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option four
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option five
                            </label>
                        </div>
                    </div>
                </td>
                <td class="tdWidth default_filter_element">
                    <h4>Filter 5</h4>
                    <div class="form" style="float: right; width: 250px; margin-right: 15px;">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control shawCalRanges" id="date_range" name="date_range">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <span class="fa fa-filter"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                <!-- dashboard filter ------------- -->
                <td class=" dashboard_filter_element">
                    <h4>Filter By</h4>
                    <div class="option_style" id="dashboard_filter_element_filter">
                        <form id="dashboard_filter_radio">
                            <div class="checkbox">
                                <input id="filter_by_radio_1" name="filter_by_radio" value="monthly" type="radio" onclick="filter()">
                                <label for="filter_by_radio_1" class="radio-label">Monthly</label>
                            </div>

                            <div class="checkbox">
                                <input id="filter_by_radio_2" name="filter_by_radio" value="quarterly" type="radio" onclick="filter()">
                                <label for="filter_by_radio_2" class="radio-label">Quarterly</label>
                            </div>
                            <div class="checkbox">
                                <input id="filter_by_radio_3" name="filter_by_radio" value="biannual" type="radio" onclick="filter()">
                                <label for="filter_by_radio_3" class="radio-label">Bi-Annual</label>

                            </div>
                            <div class="checkbox">

                                <input id="filter_by_radio_4" name="filter_by_radio" value="annual" type="radio" onclick="filter()">
                                <label for="filter_by_radio_4" class="radio-label">Annually</label>

                            </div>
                        </form>
                    </div>
                </td>
                <td class="tdWidth  dashboard_filter_element_month_div" style="display: none">
                    <h4>By Months</h4>
                    <div class="option_style">
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="" name="dashboard_filter_month" onclick="onlyOne(this,'1')" id="dashboard_filter_element_month_1">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                January
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="" name="dashboard_filter_month" onclick="onlyOne(this,'2')" id="dashboard_filter_element_month_2">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                February
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="" name="dashboard_filter_month" onclick="onlyOne(this,'3')" id="dashboard_filter_element_month_3">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                March
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="" name="dashboard_filter_month" id="dashboard_filter_element_month_4" onclick="onlyOne(this,'4')">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                April
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="" name="dashboard_filter_month" onclick="onlyOne(this,'5')" id="dashboard_filter_element_month_5">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                May
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="" name="dashboard_filter_month" onclick="onlyOne(this,'6')" id="dashboard_filter_element_month_6">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                June
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="" name="dashboard_filter_month" onclick="onlyOne(this,'7')" id="dashboard_filter_element_month_7">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                July
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="" name="dashboard_filter_month" id="dashboard_filter_element_month_8" onclick="onlyOne(this,'8')">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                August
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="" name="dashboard_filter_month" id="dashboard_filter_element_month_9" onclick="onlyOne(this,'9')">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                September
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="" name="dashboard_filter_month" id="dashboard_filter_element_month_10" onclick="onlyOne(this,'10')">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                October
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="" name="dashboard_filter_month" id="dashboard_filter_element_month_11" onclick="onlyOne(this,'11')">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                November
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="" name="dashboard_filter_month" id="dashboard_filter_element_month_12" onclick="onlyOne(this,'12')">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                December
                            </label>
                        </div>
                    </div>
                </td>
                <td class=" dashboard_filter_element">
                    <h4>Filter 1</h4>
                    <div class="option_style">
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option one
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option two
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option three
                            </label>
                        </div>
                    </div>
                </td>
                <td class=" dashboard_filter_element">
                    <h4>Filter 2</h4>
                    <div class="option_style">
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option one
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option two
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option three
                            </label>
                        </div>
                    </div>
                </td>
                <td class="tdWidth dashboard_filter_element">
                    <h4>Filter 3</h4>
                    <div class="option_style">
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option one
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option two
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option three
                            </label>
                        </div>
                    </div>
                </td>
                <td class="tdWidth dashboard_filter_element">
                    <h4>Filter 4</h4>
                    <div class="option_style">
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option one
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option two
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option three
                            </label>
                        </div>
                    </div>
                </td>
                <td class="dashboard_filter_element">
                    <h4>Filter 5</h4>
                    <div class="option_style">
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option one
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option two
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option three
                            </label>
                        </div>
                    </div>
                </td>
                <td class="  dashboard_filter_element">
                    <h4>Filter 6</h4>
                    <div class="option_style">
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option one
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option two
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="d-flex">
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
                                Option three
                            </label>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<hr>
<div style="padding: 10px;">
    <button class="btn btn-sm primary filter_div_export_cls_1" style="width:135px;" id="filter_div_save">Apply and Export</button>
    <button class="btn btn-sm primary filter_div_apply_cls_1" style="width:135px" id="filter_div_apply">Apply</button>

    <button class="btn btn-sm btn-dark filter_div_close_cls_1" style="width:100px" id="filter_div_close">Close</button>

    <!-- <button class="btn btn-sm btn-dark" style="width:100px" id="listview_filter_div_close"  style="display: none;">Close</button> -->

</div>



<script type="text/javascript">
    $(document).ready(function() {
        var value = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        if (value == "dashboard_view") {
            $(".dashboard_filter_element").show();
            $(".default_filter_element").hide();



        } else {
            $(".dashboard_filter_element").hide();
            $(".default_filter_element").show();
        }
    });

    function onlyOne(checkbox, month_id) {
        var checkboxes = document.getElementsByName('dashboard_filter_month')
        checkboxes.forEach((item) => {
            if (item !== checkbox) item.checked = false
        });
        if (month_id == "1") {
            $(".widget_1_span").html("(Jan)");

        } else if (month_id == "2") {
            $(".widget_1_span").html("(Feb)");
        } else if (month_id == "3") {
            $(".widget_1_span").html("(Mar)");
        } else if (month_id == "4") {
            $(".widget_1_span").html("(Apr)");
        } else if (month_id == "5") {
            $(".widget_1_span").html("(May)");
        } else if (month_id == "6") {
            $(".widget_1_span").html("(Jun)");
        } else if (month_id == "7") {
            $(".widget_1_span").html("(Jul)");
        } else if (month_id == "8") {
            $(".widget_1_span").html("(Aug)");
        } else if (month_id == "9") {
            $(".widget_1_span").html("(Sep)");
        } else if (month_id == "10") {
            $(".widget_1_span").html("(Oct)");
        } else if (month_id == "11") {
            $(".widget_1_span").html("(Nov)");
        } else if (month_id == "12") {
            $(".widget_1_span").html("(Dec)");
        } else if (month_id == "13") {
            // $(".widget_1_span").html("(Dec)");
            $(".vendor_container").show();
            $(".site_container").hide();
        } else if (month_id == "14") {
            // $(".widget_1_span").html("(Dec)");
            $(".vendor_container").hide();
            $(".site_container").show();
        }
    }
</script>