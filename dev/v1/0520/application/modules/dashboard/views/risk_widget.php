  
  <div class="row">
      <div class="col-xl-4 col-lg-4 col-sm-4 ImpactBottom padding-adjust">
          <div class="card-yu pull-up pull-up-1 risk_widget min-widget margin-adjust-widget ">
              <div class="card-content">
                  <div class="card-body">
                      <div class="media d-flex">
                          <div class="media-body text-left">

                              <h2 id="no_of_vendor_non_compliant_monetary_checklist" class="info_impact_score_color">40</h2>
                              <h6 style="font-size: 12px;"># of Vendors Non Compliant - Monetary Checklist</h6>
                          </div>
                          <div>
                              <i class="la la-money money-arrange"></i>
                              <i class="icon-basket-loaded info font-large-2 float-right"></i>
                          </div>
                      </div>
                      <div class="progress progress-sm  mb-0 box-shadow-2">
                          <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 100%; background: #ff0000;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-sm-4 ImpactBottom padding-adjust">
          <div class="card-yu pull-up pull-up-1 risk_widget min-widget margin-adjust-widget ">
              <div class="card-content">
                  <div class="card-body">
                      <div class="media d-flex">
                          <div class="media-body text-left">
                              <h2 id="no_of_vendor_non_compliant_license_checklist" class="" style="color:#ff9900;">20</h2>
                          </div>
                          <div>
                              <i class="la la-files-o file-arrange"></i>
                              <i class="icon-pie-chart warning font-large-2 float-right"></i>
                          </div>
                      </div>
                      <div>
                          <h6 style="font-size:11.7px;"># of Vendors Non Compliant - License & Registration Checklist</h6>
                      </div>
                      <div class="progress progress-sm  mb-0 box-shadow-2">
                          <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 100%; background:#ff9900;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-sm-4 ImpactBottom padding-adjust">
          <div class="card-yu pull-up pull-up-1 risk_widget min-widget margin-adjust-widget ">
              <div class="card-content">
                  <div class="card-body">
                      <div class="media d-flex">
                          <div class="media-body text-left">
                              <h2 id="no_of_vendor_non_compliant_procedures_checklist" style="color:#ffd558;">10</h3>
                                  <h6 style="font-size: 12px;"># of Vendors Non Compliant - Procedures Checklist</h6>
                          </div>
                          <div>
                              <i class="la la-check-square check-arrange"></i>
                              <i class="icon-user-follow success font-large-2 float-right"></i>
                          </div>
                      </div>
                      <div class="progress progress-sm  mb-0 box-shadow-2">
                          <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 100%; background:#ffd558;" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>