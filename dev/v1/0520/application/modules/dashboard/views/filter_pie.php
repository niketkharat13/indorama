    
    <div class="card margin_bottom_comm" style="height: 350px;">
        <div class="card-header">
            <h4 class="card-title">Vendor Audit Final Status(Current Month)</h4>
        </div>
        <div class="card-content collapse show">
            <div class="card-body">
                <div id="dashboard-donut-chart" class="text-center" style="top:-1rem;"></div>
            </div>
        </div>
    </div>
