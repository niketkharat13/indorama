
    <style type="text/css">
        hr.dash1 {
            border: 3px solid #4285f4;
            border-radius: 5px;
            width: 80%;
            margin-left: -15px;
        }
        hr.dash2 {
            border: 3px solid #ea4335;
            border-radius: 5px;
            width: 80%;
            margin-left: -19px;
        }
        hr.dash3 {
            border: 3px solid #fbbc05;
            border-radius: 5px;
            width: 80%;
            margin-left: -28px;
        }
        hr.dash4 {
            border: 3px solid #34a853;
            border-radius: 5px;
            width: 80%;
            margin-left: -6px;
        }
        hr.dash5 {
            border: 3px solid #4285f4;
            border-radius: 5px;
            width: 80%;
            margin-left: -15px;
        }
    </style>
    <section id="basic" class="">
        <br>
        <!-- vendor container -->
        <div class="container vendor_container">
            <div class="row">
                <div class="col-lg-6 col-md-8 col-sm-6">
                    <h4 style="color:#c5c5c5;font-size:1rem">Last Updated on : 10:54:12 - 26 May 2020</h4>
                </div>
                <div class="performance_div col-lg-6 col-md-4 col-sm-4 text-right d-flex justify-content-end align-items-center" style="margin-top: -10px">
                    <h5 class="mr-2" style="margin-top:10px; font-weight: 700;">
                        Workflow Status
                    </h5>
                    <form action="" id="widget_1_month_year_filter">
                        <div class="minimal">
                            <select name="country" class="minimal" id="site_widget">
                                <option value="month_1">Jan 2020</option>
                                <option value="month_2">Dec 2019</option>
                                <option value="month_3">Nov 2019</option>
                                <option value="month_4">Oct 2019</option>
                                <option value="month_5">Sep 2019</option>
                                <option value="month_6">Aug 2019</option>
                                <option value="month_7">Jul 2019</option>
                                <option value="month_8">Jun 2019</option>
                                <option value="month_9">May 2019</option>
                                <option value="month_10">Apr 2019</option>
                                <option value="month_11">Mar 2019</option>
                                <option value="month_12">Feb 2019</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row mt-1">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="row">
                        <div class="col">
                            <div class="align-self-center widget2 d-flex">

                                <span class="count_in_widget">
                                    34</span>
                            </div>
                            <div>
                                <h5 class=" text-muted widget_name text-bold-500 mb-0" class="d-flex align-items-center">
                                    <span style="color:#00517cf5;font-weight:550" class="text-bold-500">Total</span>&nbsp;
                            </div>
                        </div>
                        <div class="col">
                            <div>
                                <h5 class=" text-muted widget_name text-bold-500 mb-0 count_in_widget" class="d-flex align-items-center">
                                    <span class="" style="margin-left:2.1rem" data-toggle="tooltip" title="">10</span>&nbsp; </h5>
                            </div>
                            <div class="align-self-center widget2 d-flex">
                                <hr class="dash1">
                            </div>
                            <small class="d-flex justify-content-center" style="margin-left: -58px; font-size: 1rem; margin-top: -10px;">RO User Approved</small>
                        </div>
                        <div class="col">
                            <div>
                                <h5 class=" text-muted widget_name text-bold-500 mb-0 count_in_widget" class="d-flex align-items-center">
                                    <span class="" style="margin-left:2rem;" data-toggle="tooltip" title="">17</span>&nbsp;</h5>
                            </div>
                            <div class="align-self-center widget2 d-flex">
                                <hr class="dash2">
                            </div>
                            <small class="d-flex justify-content-center" style="margin-left: -66px; font-size: 1rem; margin-top: -10px;">Sent to RO User</small>
                        </div>
                        <div class="col">
                            <div>
                                <h5 class=" text-muted widget_name text-bold-500 mb-0 count_in_widget" class="d-flex align-items-center">
                                    <span class="" style="margin-left: 2.2rem" data-toggle="tooltip" title="">7</span>&nbsp; </h5>
                            </div>
                            <div class="align-self-center widget2 d-flex">
                                <hr class="dash3">
                            </div>
                            <small class="d-flex justify-content-center" style="margin-left: -80px; font-size: 1rem; margin-top: -10px;">Submitted to TL</small>
                        </div>
                        <div class="col tablet_view margin-left-30">
                            <div>
                                <h5 class=" text-muted widget_name text-bold-500 mb-0 count_in_widget" class="d-flex align-items-center">
                                    <span class="" style="margin-left:3.75rem" data-toggle="tooltip" title="">7</span>&nbsp; </h5>
                            </div>
                            <div class="align-self-center widget2 d-flex">
                                <hr class="dash4">
                            </div>
                            <small class="d-flex justify-content-center" style="margin-left: -44px; font-size: 1rem; margin-top: -10px;">TL Review</small>
                        </div>
                        <div class="col">
                            <div>
                                <h5 class=" text-muted widget_name text-bold-500 mb-0 count_in_widget" class="d-flex align-items-center">
                                    <span class="" style="margin-left:3.1rem; " data-toggle="tooltip" title="">4</span>&nbsp;</h5>
                            </div>
                            <div class="align-self-center widget2 d-flex">
                                <hr class="dash5">
                            </div>
                            <small class="d-flex justify-content-center" style="margin-left: -58px; font-size: 1rem; margin-top: -10px;">Yet to Start</small>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <br>
    </section>