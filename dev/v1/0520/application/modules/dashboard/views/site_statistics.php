    
    <div class="row  small-two-widget  ">
        <div class="col-md-12 col-lg-12">
            <div class="card margin_bottom_comm">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex justify-content-around">
                            <div class="text-left">
                                <h6 class="text-muted "><b>Total Sites</b></h6>
                                <h3 id="total_no_sites">100</h3>
                            </div>
                            <div class="text-center">
                                <h6 class="text-arrange" style="color:#34a853"><b>Compliant</b></h6>
                                <h3 id="total_no_compliant">80</h3>
                            </div>
                            <div class="text-center">

                                <h6 class="text-arrange" style="color:#ff0000"><b>Non-Compliant</b>
                                </h6>
                                <h3 id="total_no_non_compliant">20</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>