

	<div class="row">
		<div class="col-md-12">
			<div class="card-yu margin_bottom_comm" style="height: 350px;">
				<div class="card-header pb-0">
					<h4 class="card-title">Audit Schedule</h4>
				</div>
				<div class="card-body">
					<div class="row ">
						<div class="col-md-12">
							<ul class="nav nav-tabs nav-underline no-hover-bg nav-justified nav-bg">
								<li class="nav-item">
									<a class="nav-link active newcls" id="active-tab3223" data-toggle="tab" href="#active3223" aria-controls="active3223" aria-expanded="true">Online</a>
								</li>
								<li class="nav-item">
									<a class="nav-link newcls" id="link-tab3224" data-toggle="tab" href="#link3224" aria-controls="link3224" aria-expanded="false">Physical</a>
								</li>
							</ul>

							<div class="tab-content px-1 pt-1">
								<div role="tabpanel" class="tab-pane active" id="active3223" aria-labelledby="active-tab3223" aria-expanded="true">
									<div class=" table-responsive">
										<form class="audit_schedule_online_table">

											<table class=" table-1">
												<tbody>
													<tr style="border:2px">
														<th colspan="3" style="width: 105px;"></th>
														<th>Scheduled</th>
														<th>On going</th>
														<th>Completed</th>
													</tr>
													<tr>
														<td colspan="3" style="text-align: left">Regular:
														</td>
														<td>6</td>
														<td>3</td>
														<td>2</td>
													</tr>
													<tr>
														<td colspan="3" style="text-align: left">Final:</td>
														<td>4</td>
														<td>2</td>
														<td>1</td>
													</tr>
													<tr>
														<td colspan="3" style="text-align: left">
															Remediation:
														</td>
														<td>1</td>
														<td>8</td>
														<td>1</td>
													</tr>
												</tbody>
											</table>
										</form>
									</div>
								</div>
								<div class="tab-pane" id="link3224" role="tabpanel" aria-labelledby="link-tab3224" aria-expanded="false">
									<div class="table-responsive">
										<form class="audit_schedule_online_table">

											<table class=" table-2  ">
												<tbody>
													<tr style="border:2px">
														<th colspan="3" style="width: 105px;"></th>
														<th>Scheduled</th>
														<th>On going</th>
														<th>Completed</th>
													</tr>
													<tr>
														<td colspan="3" style="text-align: left">Regular:
														</td>
														<td>4</td>
														<td>8</td>
														<td>1</td>
													</tr>
													<tr>
														<td colspan="3" style="text-align: left">Final:</td>
														<td>1</td>
														<td>6</td>
														<td>1</td>
	
													</tr>
													<tr>
														<td colspan="3" style="text-align: left">
															Remediation:
														</td>
														<td>6</td>
														<td>7</td>
														<td>2</td>
													</tr>
												</tbody>
											</table>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	