    <script>
        // <!-- dashboard js --------------------------- -->

        $(document).ready(function() {
            $('#table-view-list').DataTable({
                "dom": '"t"'
            });
        });
        $('.nav-menu-main').click(function() {
            if ($('.nav-menu-main').hasClass('is-active')) {
                $('.min-max').addClass('margin-left-10');
                $('.min-max').addClass('min-max-2');
                $('.pull-up-1').addClass('min-widget-2');
                $('.min-max-3').addClass('min-max-4');
                $('.margin-left-12').addClass('margin-left-25');
            } else {
                $('.min-max').removeClass('margin-left-10');
                $('.min-max').removeClass('min-max-2');
                $('.min-max-3').removeClass('min-max-4');
                $('.margin-left-12').removeClass('margin-left-25');
                $('.pull-up-1').removeClass('min-widget-2');

            }
        });

        // keep there---------------only this script---
        $('#dashboard_hide_show_toggle_div input[type=checkbox]').attr('checked', true);

        // dashboard stats js for dropdown after tabs selection starts
        function show_dropdown(dropdown_show, dropdown_hide) {

            $("." + dropdown_show).show();
            $("." + dropdown_hide).hide();

            if (dropdown_show != "client_dropdown")
                $(".first-filter-dropdown").addClass("mr-9");

            else
                $(".first-filter-dropdown").removeClass("mr-9");
        }
        // dashboard stats js for dropdown after tabs selection ends


        // <!-- js to change value of widget-1    -->
        function dropdown_client(filter_dropdown_class) {
            var selectedClient = $("." + filter_dropdown_class).children("option:selected").val();
            if (selectedClient == "month_1") {
                $('#client-site').text(3);
                $("#client-c").text(1);
                $("#client-nc").text(2);
            } else if (selectedClient == "month_2") {
                $('#client-site').text(4);
                $("#client-c").text(1);
                $("#client-nc").text(3);
            } else if (selectedClient == "month_3") {
                $('#client-site').text(2);
                $("#client-c").text(2);
                $("#client-nc").text(0);
            } else if (selectedClient == "month_4") {
                $('#client-site').text(5);
                $("#client-c").text(4);
                $("#client-nc").text(1);
            }
        }
        // <!-- js to change value of widget-1   ends -->
        // Dashboard JS
        $('#filter_button').click(function() {
            $('.filter_div').show();
            $('#filter_div_apply').show();
            $('#filter_div_save').hide();
        });
        $('#filter_div_apply').click(function() {

            $('#filter_div').css('display', 'none');
        });
        $('#filter_div_close').click(function() {
            $('#filter_div').css('display', 'none');
        });

        function widget_1(filter_by_id) {
            if ($('input[name=filter_by_radio_2]:checked', '#dashboard_filter_radio_2').val() == "vendor") {

            } else if ($('input[name=filter_by_radio_2]:checked', '#dashboard_filter_radio_2').val() == "site") {
                $(".vendor_container").hide();
                $(".site_container").show();
            }
        }
    </script>




    <script>
        $(window).on("load", function() {

            // Callback that creates and populates a data table, instantiates the category data chart, passes in the data and draws it.
            var categoryData = c3.generate({
                bindto: '#user_wise',
                size: {
                    height: 300
                },
                color: {
                    pattern: ['#3cccc6', '#ff6d01', '#a1d36e', '#fbbc04', '#fa6f57', '#568dd5']
                },

                // Create the data table.
                data: {
                    x: 'x',
                    columns: [
                        ['x', 'Andrea', 'Apple', 'Cristina', 'Daniel', 'John', 'Maria', 'Natalia', 'Seed'],
                        ['Yet to Start', 0, 16, 0, 16, 32, 0, 0, 8],
                        ['TL Review', 0, 8, 0, 32, 8, 0, 0, 32],
                        ['Submitted to TL', 0, 24, 0, 16, 8, 0, 0, 40],
                        ['Sent to RO User', 0, 8, 0, 32, 16, 0, 0, 24],
                        ['RO User Approved', 16, 0, 24, 0, 0, 24, 16, 0],
                        ['Completed', 0, 32, 0, 8, 24, 0, 0, 16]
                    ],
                    groups: [
                        ['Completed', 'RO User Approved', 'Sent to RO User', 'Submitted to TL', 'TL Review', 'Yet to Start', ]
                    ],
                    type: 'bar'
                },
                // bar: {
                //     width: {
                //         ratio: 0.3
                //     }
                // },

                axis: {
                    x: {
                        type: 'category' // this needed to load string x value
                    }
                },
                grid: {
                    y: {
                        show: true
                    }
                },
            });


            // Resize chart on sidebar width change
            $(".menu-toggle").on('click', function() {
                categoryData.resize();
            });

            setTimeout(function() {
                categoryData.resize();

            })



            // workflow graph--------------------------
            var workflowdata = c3.generate({
                bindto: '#workflow',
                size: {
                    height: 300
                },
                color: {
                    pattern: ['#ff6d01', '#a1d36e', '#fbbc04', '#fa6f57', '#568dd5']
                },
                // padding: {
                //     left: 15
                // },

                // Create the data table.
                data: {
                    x: 'x',
                    columns: [
                        ['x', 'Completed', 'RO User Approved', 'Sent to RO User', 'Submitted to TL', 'TL Review', 'Yet to Start'],

                        ['Grand Total', 80, 80, 80, 88, 80, 72],
                        ['13/05/2020', 16, 40, 20, 20, 16, 16],
                        ['12/05/2020', 24, 0, 20, 24, 24, 20],
                        ['10/05/2020', 20, 40, 24, 24, 20, 20],
                        ['09/05/2020', 20, 0, 16, 20, 20, 16]
                    ],
                    groups: [
                        ['Grand Total', '13/05/2020', '12/05/2020', '10/05/2020', '09/05/2020']
                    ],
                    type: 'bar'
                },
                // bar: {
                //     width: {
                //         ratio: 0.3
                //     }
                // },

                axis: {
                    x: {
                        type: 'category', // this needed to load string x value,
                        height: 40
                    }
                },
                grid: {
                    y: {
                        show: true
                    }
                },
            });


            // Resize chart on sidebar width change
            $(".menu-toggle").on('click', function() {
                workflowdata.resize();
            });

            setTimeout(function() {
                workflowdata.resize();
            }, 100)


            // volume graph---------------------------------
            var volumedata = c3.generate({
                bindto: '#volume',
                size: {
                    height: 350
                },
                color: {
                    pattern: ['#a1d36e', '#fbbc04', '#fa6f57', '#568dd5']
                },
                

                // Create the data table.
                data: {
                    x: 'x',
                    columns: [
                        ['x', 'Bank reconciliationsCoA updates', 'Fixed asset accounting', 'Intercompany transactions and reconciliations', 'Internal audit', 'MIS reporting based on actuals', 'MIS reports for senior management', "MIS reports with operational activities", 'Month end closing and Provisions', 'Payroll accounting', 'Physical verification', 'Statutory audit', 'Statutory reporting', 'TB Review and GL scrutiny'],

                        ['13/05/2020', 8, 8, 6, 2, 6, 4, 8, 4, 2, 8, 2, 8, 8, 4],
                        ['12/05/2020', 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8],
                        ['10/05/2020', 8, 12, 16, 12, 16, 8, 12, 8, 12, 8, 16, 8, 8, 4],
                        ['09/05/2020', 8, 4, 8, 8, 8, 4, 4, 4, 8, 8, 4, 8, 8, 8]
                    ],
                    groups: [
                        ['13/05/2020', '12/05/2020', '10/05/2020', '09/05/2020']
                    ],
                    type: 'bar'
                },
                // bar: {
                //     width: {
                //         ratio: 0.3
                //     }
                // },

                axis: {
                    x: {
                        type: 'category',
                        tick: {
                            rotate: -45,
                            // multiline: false,
                            // outer: false
                        },
                        height: 100
                    }
                },
                grid: {
                    y: {
                        show: true
                    }
                },
            });


            // Resize chart on sidebar width change
            $(".menu-toggle").on('click', function() {
                volumedata.resize();
            });

            setTimeout(function() {
                volumedata.resize();
            }, 100)
        });
    </script>