    
    <div class="row  small-two-widget  ">
        <div class="col-md-12 col-lg-12">
            <div class="card margin_bottom_comm">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex justify-content-around">
                            <div class="text-left">
                                <h6 class="text-muted "><b>Total # of Licence</b></h6>
                                <h3 id="no_of_licence_total">100</h3>
                            </div>
                            <div class="text-center">
                                <h6 class="text-arrange" style="color:#34a853"><b># of Valid licence</b></h6>
                                <h3 id="no_of_licence_valid">90</h3>
                            </div>
                            <div class="text-center">
                                <h6 class="text-arrange" style="color:#ff0000"><b># of Expired licence</b></h6>
                                <h3 id="no_of_licence_valid">10</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>