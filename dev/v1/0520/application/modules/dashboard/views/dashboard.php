    <?php $this->load->view('header'); ?>
    <?php $this->load->view('top'); ?>
    <?php $this->load->view('vertical_navbar'); ?>
    <?php $this->load->view('dashboard/dashboard_css'); ?>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= base_url() ?>app-assets/css/media.css">

    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">

    <div style="width:102%;">
        <div class="col ">
            <div class="custom-view  ">
                <div class="title-div">
                    <h3>
                        Welcome
                    </h3>
                    <p>
                        Purushothaman TR
                    </p>
                </div>

                <style type="text/css">
                    .filter_div {
                        width: 98% !important;
                    }
                </style>
                <!-- filter view---- -->
                <div class="filter_div" id="filter_div" style="display:none; left: 14px;top: 1px;">
                    <?php $this->load->view('dashboard_filter'); ?>
                </div>


                <button class="mr-1 option-button" id="filter_button">
                    <i class="ft-filter dash_icon"></i>
                </button>

                <!-- hide show widget starts -->
                <div class="option-button-div" id="dashboard_hide_show_toggle_div">

                    <button class="option-button op-button" data-toggle='tooltip' title="Customize View"><i class="ft-more-horizontal dash_icon"></i>
                    </button>
                    <div class="card toggle-div" style="display: none; height: 375px;overflow-y: scroll;">
                        <div style="padding: 10px; border-bottom: 1px solid #0000003b;">
                            <p style="text-align:left; margin-bottom: 0px; margin-left: 10px">
                                Customize your home screen with widgets. Toggle to enable or disable.
                                <br>
                                Click save to confirm changes.
                            </p>
                        </div>

                        <table id="sortable" style="overflow-y: auto; height: 50px; width:100%;">
                            <tbody class="option-table">

                                <tr>
                                    <td colspan="4">

                                        <div class="option-card">
                                            <p>User wise_Request Status</p>

                                            <div>
                                                <input type="checkbox" id="switch-small-12" class="toggle-check"><label title="Hide" class="toggle-small" for="switch-small-12"></label>
                                            </div>
                                        </div>

                                    </td>
                                </tr>


                                <tr>

                                    <td colspan="4">
                                        <div class="option-card">
                                            <p>Workflow Analysis - Status Wise</p>

                                            <div>
                                                <input type="checkbox" id="switch-small-2" class="toggle-check"><label title="Hide" class="toggle-small" for="switch-small-2"></label>
                                            </div>
                                        </div>

                                    </td>
                                </tr>


                                <tr>
                                    <td colspan="4">
                                        <div class="option-card">
                                            <p>Volume Analysis</p>

                                            <div>
                                                <input type="checkbox" id="switch-small-0" class="toggle-check"><label title="Hide" class="toggle-small" for="switch-small-0"></label>

                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">

                                        <div class="option-card">
                                            <p>Request List</p>
                                            <div>
                                                <input type="checkbox" id="switch-small-1" class="toggle-check"><label title="Hide" class="toggle-small" for="switch-small-1"></label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- hide show widget ends -->
            </div>

            <!-- dashboard start  -->
            <section id="drag-area">
                <div class="row" id="card-drag-area">
                    <!-- widget 1 -->


                    <!-- widget 3 -->
                    <div class="col-lg-6 col-md-12 col-sm-12 filter_pie">
                        <?php $this->load->view('user_wise_status'); ?>
                    </div>
                    <!-- widget 3 ends-->

                    <!-- widget 4 -->
                    <div class="col-lg-6 col-md-12 col-sm-12 widget_4">
                        <?php $this->load->view('workflow_status'); ?>
                    </div>
                    <!-- widget 4 ends-->

                    <!-- widget 5 -->
                    <div class="col1 col-lg-12 col-md-12 col-lg-6 widget_5">
                        <?php $this->load->view('volume'); ?>
                    </div>
                    <!-- widget 5 ends-->

                    <!-- widget 9  -->
                    <div class="col-lg-12 col-md-12 col-lg-12 col-sm-12 col2 widget_9">
                        <?php $this->load->view('dashboard/table-view-list'); ?>
                    </div>
                    <!-- widget 9 ends  -->

                </div>
            </section>
        </div>
    </div>

    <?php $this->load->view('footer'); ?>
    <?php $this->load->view('dashboard/dashboard_js'); ?>