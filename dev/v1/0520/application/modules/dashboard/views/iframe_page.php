    <?php $this->load->view('header'); ?>
    <?php $this->load->view('top'); ?>
    <?php $this->load->view('vertical_navbar');
    ?>
    <style>
        .iframe__style{
            height: 100vh;
            width: 100%;
        }
    </style>
    <div style="width:103%">
        <div class="col">
            <br>
            <div class="pt-2 p-1">
                <div class="row">
                    <div id="iframe_div" class="col-lg-12 col-md-12 col-sm-12"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var iframe_url = localStorage.getItem('iframe');
        var iframe_title = localStorage.getItem('iframe_title');
        let layout = `<iframe class='iframe__style' src='${iframe_url}' title='${iframe_title}'></iframe>`;
        setTimeout(() => {
            $('#iframe_div').append(layout);
        }, 100);
    </script>
    <?php $this->load->view('footer'); ?>