    <?php $this->load->view('header'); ?>
    <?php $this->load->view('top'); ?>
    <?php $this->load->view('dashboard_css'); ?>
    <?php $this->load->view('vertical_navbar'); ?>
    <style>
        .tile {
            height: 120px;
            display: flex;
            flex-direction: column;
            justify-content: space-evenly;
            align-items: center;
        }

        .circle_icon {
            display: inline-block;
            height: 70px;
            width: 70px;
            vertical-align: top;
            border-radius: 50%;
            cursor: pointer;
            text-align: center;
            /* line-height: 10px; */
            background: white;
            /* box-shadow: inset 0px 0px 20px 0px; */
            box-shadow: 1px 3px 8px -3px #7DE3BD;
        }

        .landing_icons_custom {
            margin-top: 27%;
            width: 30px;
        }

        .clickable_tiles {
            cursor: pointer;
        }

        .clickable_tiles:hover {
            box-shadow: 1px 3px 8px -3px #7DE3BD;
        }

        .margin-left-1 {
            margin-left: -1rem;
        }

        #raise_request_modal .modal-dialog {
            top: 14rem;
        }

        .clickable_tiles_iframe {
            cursor: pointer;
        }

        .clickable_tiles_iframe:hover {
            box-shadow: 1px 3px 8px -3px #7DE3BD;
        }

        .clickable_tiles_iframe_modal {
            cursor: pointer;
        }

        .clickable_tiles_iframe_modal:hover {
            box-shadow: 1px 3px 8px -3px #7DE3BD;
        }

        .font-size-18 {
            font-size: 18px;
        }
    </style>
    <script>
        localStorage.clear();
    </script>
    <div style="width:103%">
        <div class="col">
            <br>
            <div class="pt-2 p-1">
                <div class="row">
                    <div class="col widget_1">
                        <div class="site-1 card-yu mb-2">
                            <?php $this->load->view('dashboard/widget_1'); ?>
                        </div>
                    </div>
                </div>

                <div class="row attactment_div justify-content-center">
                    <div class="col">
                        <div class="card clickable_tiles">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="tile">
                                        <span class="circle_icon">
                                            <img src="<?= base_url() ?>app-assets/images/landing_page/dashboard.png" class="landing_icons_custom">
                                        </span>
                                        <a class="font-size-18" href="<?= base_url() ?>dashboard/Dashboard_controller/dashboard_view">Dashboard</a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card clickable_tiles">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="tile">
                                        <span class="circle_icon">
                                            <img src="<?= base_url() ?>app-assets/images/landing_page/request_list.png" class="landing_icons_custom">
                                        </span>
                                        <a class="font-size-18" href="<?= base_url() ?>document/Document_controller/request_list">Request List</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card clickable_tiles_iframe">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="tile">
                                        <span class="circle_icon">
                                            <img src="<?= base_url() ?>app-assets/images/landing_page/my_inbox.png" class="landing_icons_custom">
                                        </span>
                                        <a class="font-size-18 nav_links" data-iframe="http://13.234.152.155/Forms/account/login?returnUrl=%2fForms%2fHome%2fTasks#?poolGroup=tasks&pool=tasks_owned&viewFrame=%7B%22showHeader%22:true,%22fullScreen%22:false,%22showViewerOverlay%22:false,%22draftOpen%22:false,%22fullScreenMemory%22:false,%22showingTeamManagement%22:false,%22showingTeamOverview%22:false%7D" data-iframe-title="My Inbox">My Inbox</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card clickable_tiles_iframe_modal" data-toggle="modal" data-target="#raise_request_modal">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="tile">
                                        <span class="circle_icon">
                                            <img src="<?= base_url() ?>app-assets/images/landing_page/raise_request.png" class="landing_icons_custom">
                                        </span>
                                        <a class="font-size-18" class="req_popup">Raise a Request</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card clickable_tiles">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="tile">
                                        <span class="circle_icon">
                                            <img src="<?= base_url() ?>app-assets/images/landing_page/calendar.png" class="landing_icons_custom">
                                        </span>
                                        <a class="font-size-18" href="<?= base_url() ?>calendar/Calendar_controller/calendar_view">Schedule</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('footer'); ?>
    <script>
        $(".clickable_tiles").click(function() {
            window.location = $(this).find("a").attr("href");
        });
        $('.clickable_tiles_iframe').click(function() {
            $(this).find('.nav_links').click();
        });
        setTimeout(() => {
            $('.circle_icon_modal').click(function() {
                $(this).next().click();
            });
            $('.nav_links_modal').click(function() {
                let iframe_value = $(this).attr('data-iframe');
                let iframe_title = $(this).attr('data-iframe-title');
                localStorage.setItem('iframe', iframe_value);
                localStorage.setItem('iframe_title', iframe_title);
                window.location.href = "<?= base_url() ?>dashboard/dashboard_controller/dynamic_content";
            });
        }, 200);
    </script>
    
    <!--Raise request Modal -->
    <div class="modal fade" id="raise_request_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Raise a Request</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center d-flex justify-content-center">
                        <div class="tile" style="margin-right:4rem">
                            <span class="circle_icon circle_icon_modal">
                                <img src="<?= base_url() ?>app-assets/images/landing_page/fpa.png" class="landing_icons_custom">
                            </span>
                            <a class="nav_links" data-iframe="http://13.234.152.155/Forms/peJol" data-iframe-title="FP&A Task">FP & A Task </a> </div>
                        <div class="tile">
                            <span class="circle_icon circle_icon_modal">
                                <img src="<?= base_url() ?>app-assets/images/landing_page/r2r.png" class="landing_icons_custom">
                            </span>
                            <a class="nav_links" data-iframe="http://13.234.152.155/Forms/peJon" data-iframe-title="R2R Task">R2R Task</a>
                        </div>
                        <!-- <button class="nav_links btn btn-primary-yu primary mr-1" data-iframe="http://13.234.152.155/Forms/peJol" data-iframe-title="FP&A Task">FP & A Task</button>
                        <button class="nav_links btn btn-primary-yu primary" data-iframe="http://13.234.152.155/Forms/peJon" data-iframe-title="R2R Task">R2R Task</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>