	<style>
		.border-open {
			border: 1px solid #568dd5 !important
		}
	</style>
	<div class="card height-adjust">
		<div class="card-header pb-0">
			<h4 class="card-title">Ageing in Days Report</h4>
			<a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
				</ul>
			</div>
		</div>
		<br>
		<div class="card-content collapse show">
			<div class="card-body pt-0">
				<div class="table-responsive" style="height: 286px; overflow-y: inherit;">
					<form id="table_list_view_dashboard`"></form>
					<table class="table table-task-list table-hover" id="table-view-list">
						<thead>
							<tr>
								<th scope="col">Request #</th>
								<th scope="col">Company Code</th>
								<th scope="col">Company Name</th>
								<th scope="cal">Activity Type</th>
								<th scope="col">Sub Activity Type</th>
								<th scope="col">Ageing in Days</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row"><a style="color:#6B6F82;" href="./reports/reports_controller/report_table_view">
										<span class="status-mark  border-open position-left"></span>
										100</a></th>
								<td style="white-space: nowrap;">0004</td>
								<td>Company 1</td>
								<td>Reclassification</td>
								<td>Sub Activity 1</td>
								<td>39</td>
							</tr>
							<tr>
								<th scope="row"><a style="color:#6B6F82;" href="./reports/reports_controller/report_table_view">
										<span class="status-mark  border-open position-left"></span>
										101</a></th>
								<td style="white-space: nowrap;">0005</td>
								<td>Company 2</td>
								<td>Rectification</td>
								<td>Sub Activity 2</td>
								<td>39</td>
							</tr>
							<tr>
								<th scope="row"><a style="color:#6B6F82;" href="./reports/reports_controller/report_table_view">
										<span class="status-mark  border-open position-left"></span>
										102</a></th>
								<td style="white-space: nowrap;">0006</td>
								<td>Company 3</td>
								<td>Rectification</td>
								<td>Sub Activity 3</td>
								<td>39</td>
							</tr>
							<tr>
								<th scope="row"><a style="color:#6B6F82;" href="./reports/reports_controller/report_table_view">
										<span class="status-mark  border-open position-left"></span>
										103</a></th>
								<td style="white-space: nowrap;">0007</td>
								<td>Company 4</td>
								<td>Provison</td>
								<td>Sub Activity 4</td>
								<td>39</td>
							</tr>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>