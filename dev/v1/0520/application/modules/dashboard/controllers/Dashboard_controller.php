<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_controller extends MY_Controller {


    
    public function dashboard_view(){
        $this->load->view('dashboard');
    }

    public function landing_page(){
        $this->load->view('landing_page');
    }
    public function dynamic_content()
    {
        $this->load->view('iframe_page');
    }
   
}
