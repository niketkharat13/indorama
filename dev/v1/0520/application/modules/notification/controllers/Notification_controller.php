<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_controller extends MY_Controller {


    public function __construct()
    {
        parent::__construct();

    }
    public function notification(){
        $this->load->view('notification_view');
    }
}