<style>
    .notification_dropdown {
        width: 90%;
    }

    .notification_dropdown .selectivity-input {
        border: none;
    }

    .notification_dropdown .selectivity-input .selectivity-single-select {
        background: none;
    }

    .height-fixed {
        height: 600px;
    }

    .notification_dropdown .selectivity-input {
        box-shadow: none;
    }

    .back_icon {
        font-size: 18px;
        margin-right: 15px;
        cursor: pointer;

    }

    .notification_dropdown .selectivity-single-selected-item-remove {
        display: none;
    }

    .notification_dropdown .selectivity-single-result-container {
        position: absolute;
        top: 9px;
        right: 15px;
        left: 0px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }

    .notification_dropdown .selectivity-single-selected-item {
        font-size: 15px;
    }

    .notification_dropdown .selectivity-input .selectivity-caret {
        top: 14px;
    }

    .table thead th {
        vertical-align: bottom;
        border-bottom: 0 !important;
        border-top: 0 !important;
        font-weight: 100;
    }

    .size {
        font-size: 30px;
    }

    .common_p {
        font-size: 14px;
    }

    .active_link {
        text-decoration: underline !important;
        color: darkblue !important;
    }

    .vertical-compact-menu.menu-open .content,
    .vertical-compact-menu.menu-open .footer {
        margin-left: 0;
    }

    @media (min-width: 347px) and (max-width: 1440px) {
        .common_p {
            font-size: 14px;
        }
        .notification_type
        {
            font-size: 12px;
        }
    }

    @media (min-width: 289px) and (max-width: 1024px) {
        .common_tab {
            padding: 0;

        }

        .common_p {
            font-size: 12px;
        }
        .notification_type
        {
            font-size: 11px;
        }
    }
</style>