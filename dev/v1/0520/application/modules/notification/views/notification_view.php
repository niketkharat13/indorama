  <?php $this->load->view('header'); ?>
  <?php $this->load->view('top'); ?>
  <?php $this->load->view('notification_css'); ?>
  <style>
    /* keep here */
    .notification_dropdown .selectivity-input {
      border: none !important;
    }
  </style>

  <div class="mt-3">
    <div class="row">
      <div class="col-lg-2 col-md-3 col-sm-3 col-12">
        <div class="side-menu-notification">
          <div class="d-flex align-items-center">
            <span class="ft-arrow-left back_icon"></span>
            <div class="notification_dropdown">
              <select class="selectivity_dropdown" id="notification_dropdown">
                <option value="notification_1">Personal Preferences</option>
                <option value="notification_2">Project</option>
                <option value="notification_3">Portal Config</option>
              </select>
            </div>
          </div>
          <hr>
          <div class="d-flex flex-column ml-1">
            <a class="mb-1 active_link">Personal Email</a>
            <a class="mb-1">Organization Notification</a>
            <a class="mb-1">Activity Reminder</a>
          </div>
        </div>
      </div>
      <div class="col-lg-10 col-md-9 col-sm-9 col-12">
        <section id="drag-area">
          <div class="row">
            <!-- 1 widget -->
            <div class="col-lg-6 col-md-12 col-sm-12">
              <div class="card-yu">
                <div class="card-header pb-0 d-flex">
                  <div class="mr-1">
                    <ul class="list-inline mb-0">
                      <li>
                        <i class="mbri-alert site-icon size"></i>
                      </li>
                    </ul>
                  </div>
                  <div>
                    <h4 class="card-title" id="heading-icon">Notify me for all my activities</h4>
                  </div>
                  <div class="col-5">
                    <div class="d-flex justify-content-end">
                      <input type="checkbox" id="toggle_20" class="toggle-check active-toggle"><label data-toggle="tooltip" title="Turn off all the Notification " class="toggle-small" for="toggle_20"></label>
                    </div>
                  </div>
                </div>
                <br>
              </div>
            </div>
          </div>

          <div class="row" id="card-drag-area">
            <!-- 2 widget -->
            <div class="col-lg-6 col-md-12 col-sm-12 widget_1">
              <div class="card-yu">
                <div class="card-header pb-0 d-flex">
                  <div class="mr-1">
                    <ul class="list-inline mb-0">
                      <li>
                        <i class="mbri-map-pin site-icon size"></i>
                      </li>
                    </ul>
                  </div>
                  <div class="col-9">
                    <h4 class="card-title" id="heading-icon">Site Created</h4>
                    <p>Manage email notifications for site</p>
                  </div>
                  <div class="col-2">
                    <div class="d-flex justify-content-end">
                      <input type="checkbox" id="toggle_23" class="toggle-check active-toggle">
                      <label data-toggle="tooltip" title="Turn off all the Notification for this module" class="toggle-small" for="toggle_23"></label>
                    </div>
                  </div>
                </div>
                <hr class="mb-0 mt-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-6 col-md-4 col-sm-6 col-6">
                      <p class="common_p">NOTIFY ME FOR THE TASKS</p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center common_tab">
                      <p class="notification_type">Whatsapp</p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center common_tab">
                      <p class="notification_type">SMS</p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center common_tab">
                      <p class="notification_type">Email</p>
                    </div>

                  </div>
                  <div class="row mt-1">
                    <div class="col-lg-6 col-md-4 col-sm-6 col-6 common_p">Site Created</div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_1" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_1"></label>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_2" class="toggle-check active-toggle"><label title="Hide" class="toggle-small" for="toggle_2"></label>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_24" class="toggle-check active-toggle"><label title="Hide" class="toggle-small" for="toggle_24"></label>
                      </div>
                    </div>
                  </div>
                  <div class="row mt-1">
                    <div class="col-lg-6 col-md-4 col-sm-6 col-6 common_p">Site Status Updated</div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_3" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_3"></label>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_4" class="toggle-check active-toggle"><label title="Hide" class="toggle-small" for="toggle_4"></label>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_25" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_25"></label>
                      </div>
                    </div>
                  </div>
                  <div class="row mt-1">
                    <div class="col-lg-6 col-md-4 col-sm-6 col-6 common_p">Site Reports</div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_5" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_5"></label>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_6" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_6"></label>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_26" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_26"></label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- 3 widget -->
            <div class="col-lg-6 col-md-12 col-sm-12 widget_1">
              <div class="card-yu">
                <div class="card-header pb-0 d-flex">
                  <div class="mr-1">
                    <ul class="list-inline mb-0">
                      <li>
                        <i class="mbri-users site-icon size"></i>
                      </li>
                    </ul>
                  </div>
                  <div class="col-9">
                    <h4 class="card-title" id="heading-icon">Admin Module</h4>
                    <p>Manage email notifications for Admin</p>
                  </div>
                  <div class="col-2">
                    <div class="d-flex justify-content-end">
                      <input type="checkbox" id="toggle_22" class="toggle-check active-toggle">
                      <label data-toggle="tooltip" title="Turn off all the Notification for this module" class="toggle-small" for="toggle_22"></label>
                    </div>
                  </div>
                </div>
                <hr class="mb-0 mt-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-6 col-md-4 col-sm-6 col-6">
                      <p class="common_p">NOTIFY ME FOR THE TASKS</p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center common_tab">
                      <p class="notification_type">Whatsapp</p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center common_tab">
                      <p class="notification_type">SMS</p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center common_tab">
                      <p class="notification_type">Email</p>
                    </div>

                  </div>
                  <div class="row mt-1">
                    <div class="col-lg-6 col-md-4 col-sm-6 col-6 common_p">Masters Created</div>
                    <div class="col-lg- col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_7" class="toggle-check active-toggle"><label title="Hide" class="toggle-small" for="toggle_7"></label>
                      </div>
                    </div>
                    <div class="col-lg- col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_8" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_8"></label>
                      </div>
                    </div>
                    <div class="col-lg- col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_27" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_27"></label>
                      </div>
                    </div>
                  </div>
                  <div class="row mt-1">
                    <div class="col-lg-6 col-md-4 col-sm-6 col-6 common_p">Masters Edited/Updated</div>
                    <div class="col-lg- col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_9" class="toggle-check active-toggle"><label title="Hide" class="toggle-small" for="toggle_9"></label>
                      </div>
                    </div>
                    <div class="col-lg- col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_10" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_10"></label>
                      </div>
                    </div>
                    <div class="col-lg- col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_28" class="toggle-check active-toggle"><label title="Hide" class="toggle-small" for="toggle_28"></label>
                      </div>
                    </div>
                  </div>
                  <div class="row mt-1">
                    <div class="col-lg-6 col-md-4 col-sm-6 col-6 common_p">Masters Exported</div>
                    <div class="col-lg- col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_12" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_12"></label>
                      </div>
                    </div>
                    <div class="col-lg- col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_13" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_13"></label>
                      </div>
                    </div>
                    <div class="col-lg- col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_29" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_29"></label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- 4 widget -->
            <div class="col-lg-6 col-md-12 col-sm-12 widget_1">
              <div class="card-yu">
                <div class="card-header pb-0 d-flex">
                  <div class="mr-1">
                    <ul class="list-inline mb-0">
                      <li>
                        <i class="mbri-edit site-icon size"></i>
                      </li>
                    </ul>
                  </div>
                  <div class="col-9">
                    <h4 class="card-title" id="heading-icon">Audit Module</h4>
                    <p>Manage email notifications for audit</p>
                  </div>
                  <div class="col-2">
                    <div class="d-flex justify-content-end">
                      <input type="checkbox" id="toggle_21" class="toggle-check active-toggle">
                      <label data-toggle="tooltip" title="Turn off all the Notification for this module" class="toggle-small" for="toggle_21"></label>
                    </div>
                  </div>
                </div>
                <hr class="mb-0 mt-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-6 col-md-4 col-sm-6 col-6">
                      <p class="common_p">NOTIFY ME FOR THE TASKS</p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center common_tab">
                      <p class="notification_type">Whatsapp</p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center common_tab">
                      <p class="notification_type">SMS</p>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center common_tab">
                      <p class="notification_type">Email</p>
                    </div>

                  </div>
                  <div class="row mt-1">
                    <div class="col-lg-6 col-md-4 col-sm-6 col-6 common_p">Audit Scheduled</div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_14" class="toggle-check active-toggle"><label title="Hide" class="toggle-small" for="toggle_14"></label>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_15" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_15"></label>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_30" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_30"></label>
                      </div>
                    </div>
                  </div>
                  <div class="row mt-1">
                    <div class="col-lg-6 col-md-4 col-sm-6 col-6 common_p">Audit Status Updates</div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_16" class="toggle-check active-toggle"><label title="Hide" class="toggle-small" for="toggle_16"></label>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_17" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_17"></label>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_31" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_31"></label>
                      </div>
                    </div>
                  </div>
                  <div class="row mt-1">
                    <div class="col-lg-6 col-md-4 col-sm-6 col-6 common_p">Audit Reports</div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_18" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_18"></label>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_19" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_19"></label>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 text-center">
                      <div>
                        <input type="checkbox" id="toggle_32" class="toggle-check"><label title="Hide" class="toggle-small" for="toggle_32"></label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>

  <?php $this->load->view('footer'); ?>
  <?php $this->load->view('notification_js'); ?>