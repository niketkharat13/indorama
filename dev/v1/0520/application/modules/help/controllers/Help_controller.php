<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Help_controller extends MY_Controller {


    public function __construct()
    {
        parent::__construct();

    }
    public function help(){
        $this->load->view('help');
    }
}