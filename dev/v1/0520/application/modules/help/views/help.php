<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Help Center</title>

  <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/editors/summernote.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/editors/codemirror.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/editors/theme/monokai.css">
  <!-- END: Vendor CSS-->

  <!-- BEGIN: Theme CSS-->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/components.css">

    <!-- Toastr JS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/plugins/toastr/toastr.min.css">
    <!-- End Toastr JS -->


    <!-- END: Theme CSS-->

    <style type="text/css">
      .todo-item{
        padding: 3px !important;
      }

      .primary_font_color
      {
        color: #003b7e;
      }

      .secondary_font_color
      {
        color: #6b6f82;
      }

      .yu-badge-primary
      {
        background-color: #003b7e;
      }

      .yu-border-bottom
      {
        border-bottom: 1px solid #c5c5c5; 
      }

      #version_table tr
      {
        border-bottom: 1px solid #c5c5c5; 
      }

      #version_table th
      {
        font-size: 16px;
      }

      .card_mb_0{
        margin-bottom: 0px;
      }

      .round_button{
        border-radius: 50%; 
        height: 35px; 
        width: 35px;
      }

      .primary_background{
        background-color: #003b7e !important;
      }

      .ft-chevron-left{
        cursor: pointer;
        color: #003b7e;
        font-weight: 700;
        vertical-align: middle;
      }
    </style>
    <style type="text/css">
    .note-popover .popover-content .dropdown-menu, .panel-heading.note-toolbar .dropdown-menu {
        min-width: 160px;
    }
  </style>
</head>

<body>
  
  <div id="content">
      <!-- Summernote Click to edit start -->
        <section id="summernote-edit-save">
            <div class="row">
                <div class="col-12">
                    <div class="card card_mb_0">
                        <div class="card-header">
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                            <div class="card card_mb_0">
                            	<div class="card todo-details rounded-0 card_mb_0">
  			                        <div class="sidebar-toggle d-block d-lg-none">
                                  <i class="ft-menu font-large-1"></i>
                                </div>
                                <div style="height:35px; padding: 15px 12px 0px 20px;">
        			                    <span><h4 style="float: left;"><i class="ft-chevron-left" data-toggle="tooltip" title="Back" onclick="window.history.back();"></i>&nbsp;Help</h4></span>
                                  <span>
                                    <div class="form-group" style="float: right;">
                                      <button id="edit" class="btn btn-primary round_button primary_background" type="button" data-toggle="tooltip" title="Edit"><i class="la la-pencil" style="margin-left: -3px;"></i></button>
                                      <button id="save" class="btn btn-success round_button" type="button" data-toggle="tooltip" title="Save"><i class="la la-save" style="margin-left: -3px;"  onclick="save()"></i></button>
                                    </div>
                                  </span>
                                </div>
  			                        <div class="card-content">
  			                            <div class="card-body">
  			                                <form id="form-todo-list">
  			                                    <div id="todo-list" class="todo-list media-list media-bordered">
  			                                        <div class="todo-item media">
  			                                            <div class="media-body">
  			                                                <div class="todo-title">
                                                          <table style="width: 100%;" id="version_table">
                                                            <tr>
                                                              <th>Version</th>
                                                              <th>Date</th>
                                                              <th>Comment</th>
                                                            </tr>
                                                            <tr class="primary_font_color">
                                                              <td>
                                                                <b>Ver 3 :</b> Help Document <span class="mr-2 badge yu-badge-primary badge-primary ">Current Version</span>
                                                              </td>
                                                              <td>
                                                                <span class="todo-desc"><b>Last Edited on :</b> 24 Mar 2020 at 10:00 am</span>
                                                              </td>
                                                              <td>
                                                                <b>Updated by :</b> Soham Patil
                                                              </td>
                                                            </tr>
                                                            
                                                            <tr class="secondary_font_color">
                                                              <td>
                                                                <b>Ver 2 :</b> Help Document 
                                                              </td>
                                                              <td>
                                                                <span class="todo-desc"><b>Last Edited on :</b> 23 Mar 2020 at 11:30 am</span>
                                                              </td>
                                                              <td>
                                                                <b>Updated by :</b> Soham Patil
                                                              </td>
                                                            </tr>

                                                            <tr class="secondary_font_color">
                                                              <td>
                                                                <b>Ver 1 :</b> Help Document <!-- <span class="mr-2 badge badge-success">Published</span> -->
                                                              </td>
                                                              <td>
                                                                <span class="todo-desc"><b>Last Edited on :</b> 23 Mar 2020 at 11:00 am</span>
                                                              </td>
                                                              <td>
                                                                <b>Updated by :</b> Soham Patil
                                                              </td>
                                                            </tr>
                                                          </table>
  			                                                </div>  			                                                
  			                                            </div>
  			                                        </div>
  			                                    </div>
  			                                </form>
  			                            </div>
  			                        </div>
                              </div>
                            </div>                            
                        <!-- </div>
                        <div class="card-header collapse show"> -->
                          <div class="card">
                            <hr>
                            <div class="card-body">
                                <form class="form-horizontal" action="#">
                                    <div class="form-group">
                                        <div class="summernote-edit">
                                            <h1>
                                                <img alt="YU Logo" class="right" src="<?= base_url() ?>app-assets/images/yu_logo.png" height="100" style="float:right;" /> Apollo 11</h1>
                                            <p><strong>Apollo 11</strong> was the spaceflight that landed the first humans, Americans <a href="http://en.wikipedia.org/wiki/Neil_Armstrong">Neil Armstrong</a> and <a href="http://en.wikipedia.org/wiki/Buzz_Aldrin">Buzz Aldrin</a>, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.</p>
                                            <p>Armstrong spent about <s>three and a half</s> two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&nbsp;kg) of lunar material for return to Earth. A third member of the mission, <a href="http://en.wikipedia.org/wiki/Michael_Collins_(astronaut)">Michael Collins</a>, piloted the <a href="http://en.wikipedia.org/wiki/Apollo_Command/Service_Module">command</a> spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.</p>
                                            <h2>Broadcasting and <em>quotes</em></h2>
                                            <p>Broadcast on live TV to a world-wide audience, Armstrong stepped onto the lunar surface and described the event as:</p>
                                            <blockquote>
                                                <p>One small step for [a] man, one giant leap for mankind.</p>
                                            </blockquote>

                                            <p>Apollo 11 effectively ended the <a href="http://en.wikipedia.org/wiki/Space_Race">Space Race</a> and fulfilled a national goal proposed in 1961 by the late U.S. President <a href="http://en.wikipedia.org/wiki/John_F._Kennedy">John F. Kennedy</a> in a speech before the United States Congress:</p>

                                            <blockquote>
                                                <p>[...] before this decade is out, of landing a man on the Moon and returning him safely to the Earth.</p>
                                            </blockquote>

                                            <h2>Technical details</h2>

                                            <p>Launched by a <strong>Saturn V</strong> rocket from <a href="http://en.wikipedia.org/wiki/Kennedy_Space_Center">Kennedy Space Center</a> in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of <a href="http://en.wikipedia.org/wiki/NASA">NASA</a>&#39;s Apollo program. The Apollo spacecraft had three parts:</p>

                                            <ol>
                                                <li><strong>Command Module</strong> with a cabin for the three astronauts which was the only part which landed back on Earth</li>
                                                <li><strong>Service Module</strong> which supported the Command Module with propulsion, electrical power, oxygen and water</li>
                                                <li><strong>Lunar Module</strong> for landing on the Moon.</li>
                                            </ol>

                                            <p>After being sent to the Moon by the Saturn V&#39;s upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the <a href="http://en.wikipedia.org/wiki/Mare_Tranquillitatis">Sea of Tranquility</a>. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the <a href="http://en.wikipedia.org/wiki/Pacific_Ocean">Pacific Ocean</a> on July 24.</p>

                                            <hr />
                                            <p><small>Source: <a href="http://en.wikipedia.org/wiki/Apollo_11">Wikipedia.org</a></small></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!-- Summernote Click to edit end -->
  </div>
</body>
  <!-- BEGIN: Vendor JS-->
    <script type="text/javascript" src="<?= base_url() ?>app-assets/vendors/js/vendors.min.js"></script>
  <!-- BEGIN Vendor JS-->
  <!-- BEGIN: Page Vendor JS-->
    <script src="<?= base_url() ?>app-assets/vendors/js/editors/summernote/summernote.js"></script>
  <!-- END: Page Vendor JS-->
  <!-- BEGIN: Page JS-->
    <script src="<?= base_url() ?>app-assets/js/scripts/editors/editor-summernote.js"></script>
  <!-- END: Page JS-->
  <!-- BEGIN: Theme JS-->
    <script type="text/javascript" src="<?= base_url() ?>app-assets/js/core/app-menu.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>app-assets/js/core/app.js"></script>

    <script type="text/javascript" src="<?= base_url() ?>app-assets/js/plugins/toastr_js/toastr.min.js"></script>
  <!-- END: Theme JS-->
  <script type="text/javascript">
    function save()
    {
      toastr.success('Data Saved Sucessfully!');
      setTimeout(function(){
        window.history.back();
      },3000);
    }
  </script>
</html>