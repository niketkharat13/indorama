<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Session_management extends MY_Controller {
	
	 function __construct() 
    {
        parent::__construct();
	}

	public function set_session()
    {
        $key = $_POST["key"];
        $val = $_POST["val"];
		$_SESSION[$key] = $val;
        echo 1;
    }

    public function get_session()
    {
        $key = $_POST["key"];
        echo (isset($_SESSION[$key]))? $_SESSION[$key]:"";
    }

    public function remove_session()
    {
        $key = $_POST["key"];
        if($key != "user_id" || $key != "user_name" || $key != "user_role" || $key != "user_departments"):
            unset($_SESSION[$key]);
        endif;
		echo 1;
    }
    
    public function set_session_key($key,$val)
    {
        $_SESSION[$key] = $val;
    }
    
    public function remove_session_key($key)
    {
        if($key != "user_id" || $key != "user_name" || $key != "user_role" || $key != "user_departments"):
            unset($_SESSION[$key]);
        endif;
    }
        
	public function set_column_settings($entity_id = 0)
	{
		if($entity_id):
			$this->set_session_key("column_settings_".$entity_id,$_POST);
		endif;
		echo 1;
	}
}
