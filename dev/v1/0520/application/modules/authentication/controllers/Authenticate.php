<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authenticate extends MY_Controller {


    public function __construct()
    {
        parent::__construct();

    }

    // Login screen for user

    public function login(){
        $this->load->view('login');
    }
    
    public function profile_view(){

        $this->load->view('profile_view');
    }
    public function reset_password(){
        $this->load->view('reset_password');
    }
    public function change_password(){
        $this->load->view('change_password');
    }
    //Uncomment this function to View the MFA
    // public function mfa(){
    //     $this->load->view('mfa');
    // }
    public function signup(){
        $this->load->view('sign_up');
    }
    public function change_theme(){
        $this->load->view('theme_selecter');
    }
    public function logout()
    {
        session_destroy();
        echo true;
    }
}
