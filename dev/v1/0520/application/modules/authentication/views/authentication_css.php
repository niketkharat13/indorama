<!-- login page and sign up page -->
<!-- <script>
    var images = ['karma1_login_bg.jpg', 'karma2_login_bg.jpg', 'karma3_login_bg.jpg', 'karma4_login_bg.jpg', 'karma5_login_bg.jpg', 'karma6_login_bg.jpg', 'karma7_login_bg.jpg', 'karma8_login_bg.jpg'];
    var i = 0;
    setInterval(
        function() {
            {
                $('.parent-div').css('background-image', "url(" + 'http://localhost/nf/wechecked/dev/v5_3/0220/app-assets/images/' + images[i] + ")");
                $('.parent-div').fadeIn('slow');
                $('.parent-div').css('tra','')
                i += 1;
                if (i == images.length) {
                    i = 0; // completing loop
                }
            }
        }, 5000);
</script> -->
<style>
    .parent-div {
        width: 100%;
        height: 100vh;
        background-size: 100% 100% !important;
        background-repeat: no-repeat;
        animation: changebackround 10s infinite alternate;
        transition: 1s ease-in;
    }

    @keyframes changebackround {
        0% {
            background-image: url(<?= base_url() ?>app-assets/images/login_background.jpg);

        }
        25% {
            background-image: url(<?= base_url() ?>app-assets/images/login_background.jpg);

        }

        50% {
            background-image: url(<?= base_url() ?>app-assets/images/login_background.jpg);

        }

        75% {
            background-image: url(<?= base_url() ?>app-assets/images/login_background.jpg);

        }

        100% {
            background-image: url(<?= base_url() ?>app-assets/images/login_background.jpg);
        }
    }

    .btn-width-110 {
        width: 110px
    }

    .form {
        margin-bottom: 0px !important
    }

    .button {
        margin-top: 15px
    }

    .mt_0-5 {
        margin-top: 0.5rem !important
    }

    .parent-div:before {
        content: " ";
        background-color: black;
        opacity: 0.5;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    @keyframes shrink {
        0% {
            background-size: 100% 100%;
        }

        100% {
            background-size: 110% 110%;
        }
    }

    .m-link {
        color: #fff;
    }

    .text {
        margin-top: -9px;
        font-size: 14px;
        margin-left: 22px;
    }

    .digit-group input {
        width: 30px;
        height: 30px;
        background-color: #c5c5c5;
        border: none;
        line-height: 50px;
        text-align: center;
        font-size: x-large;
        font-weight: 200;
        color: white;
        /* margin: 0 2px; */
        background: none;
        outline: none;
        border-bottom: 2px solid #fff;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
        /* margin-bottom: 0px; */
        padding-bottom: 0px;
    }

    .digit-group .splitter {
        padding: 0 5px;
        color: white;
        font-size: 24px;
    }

    .prompt {
        margin-bottom: 20px;
        font-size: 20px;
        color: #fff;
    }

    .card-margin {
        display: flex;
        justify-content: flex-end;
        margin-right: 100px
    }

    @media only screen and (max-width:694px) {
        .card-margin {
            margin-right: 0PX;
        }

        .parent-div {
            display: flex;
            justify-content: center;
        }
    }

    .sign-up-div {
        display: flex;
        justify-content: center;
    }

    .form-group {
        margin-bottom: 0rem;
    }

    .help-block {
        font-size: 14px;
        margin-bottom: -25px;
        /* margin-top: -10px; */
        color: #fff !important;


    }

    .form-group.issue .help-block,
    .form-group.issue .help-inline {
        color: #fff !important;
    }
</style>
<!-- change password -->
<style>
    input#pwd {
        color: #000;
        float: left;
    }

    input#signup_password {
        color: #000;
        float: left;
    }

    .ft-eye {
        display: flex;
        align-items: center;
        cursor: pointer;
    }

    .disabled_input {
        cursor: not-allowed;
    }

    .compare-msg {
        color: red;
    }

    .ft-eye-off {
        display: flex;
        align-items: center;
        cursor: pointer;
    }

    #signup_pwd_strength_wrap {
        border: 1px solid #D5CEC8;
        left: -112%;
        display: none;
        float: left;
        padding: 10px;
        position: absolute;
        width: 320px;
        background: white;
    }

    #signup_pwd_strength_wrap:before,
    #signup_pwd_strength_wrap:after {
        content: ' ';
        height: 0;
        position: absolute;
        width: 0;
        border: 10px solid transparent;
        /* arrow size */
    }

    #signup_pwd_strength_wrap:before {
        border-bottom: 7px solid rgba(0, 0, 0, 0);
        border-right: 7px solid #f8f9fa;
        border-top: 7px solid rgba(0, 0, 0, 0);
        display: inline-block;
        right: -18px;
        position: absolute;
        top: 10px;
        transform: rotateY(180deg);
    }

    #pwd_strength_wrap {
        border: 1px solid #D5CEC8;
        left: 103%;
        display: none;
        float: left;
        padding: 10px;
        position: absolute;
        width: 320px;
    }

    #pwd_strength_wrap:before,
    #pwd_strength_wrap:after {
        content: ' ';
        height: 0;
        position: absolute;
        width: 0;
        border: 10px solid transparent;
        /* arrow size */
    }

    #pwd_strength_wrap:before {
        border-bottom: 7px solid rgba(0, 0, 0, 0);
        border-right: 7px solid rgba(0, 0, 0, 0.1);
        border-top: 7px solid rgba(0, 0, 0, 0);
        content: "";
        display: inline-block;
        left: -18px;
        position: absolute;
        top: 10px;
    }

    input#reset_password {
        color: #000;
        float: left;
    }

    #reset_pwd_strength_wrap {
        border: 1px solid #D5CEC8;
        left: 103%;
        display: none;
        float: left;
        padding: 10px;
        position: absolute;
        width: 320px;
    }

    #pwd_strength_wrap:after {
        border-bottom: 6px solid rgba(0, 0, 0, 0);
        border-right: 6px solid #fff;
        border-top: 6px solid rgba(0, 0, 0, 0);
        content: "";
        display: inline-block;
        left: -16px;
        position: absolute;
        top: 11px;
    }

    #pswd_info ul {
        list-style-type: none;
        margin: 5px 0 0;
        padding: 0;
    }

    #pswd_info ul li {
        background: url(icon_pwd_strength.png) no-repeat left 2px;
        padding: 0 0 0 20px;
    }

    #pswd_info ul li.valid {
        background-position: left -42px;
        color: green;
    }

    #passwordStrength {
        display: block;
        height: 5px;
        margin-bottom: 10px;
        transition: all 0.4s ease;
    }

    .custom-input-div {
        font-size: 1rem;
        background-color: #FFFFFF;
        background-clip: padding-box;
        border: 1px solid #BABFC7;
        border-radius: 0.25rem;
        display: flex
    }

    .cust-dark {
        padding-top: 15px !important
    }

    .strength0 {
        background: none;
        /* too short */
        width: 0px;
    }

    .strength1 {
        background: none repeat scroll 0 0 #FF4545;
        /* weak */
        width: 25px;
    }

    .strength2 {
        background: none repeat scroll 0 0 #FFC824;
        /* good */
        width: 75px;
    }

    .strength3 {
        background: none repeat scroll 0 0 #6699CC;
        /* strong */
        width: 100px;
    }

    .strength4 {
        background: none repeat scroll 0 0 #008000;
        /* best */
        width: 150px;
    }

    .margin {
        margin-top: 1.5rem;
    }

    @media only screen and (max-width:824px) {
        .dynamic_margin {
            margin-top: 210px;
        }

        #pwd_strength_wrap {
            left: 0px;
            top: 50px;
            width: 270px;
        }

        #reset_pwd_strength_wrap {
            left: 0px;
            top: 50px;
            width: 290px;
        }

        #signup_pwd_strength_wrap {
            left: 0px;
            top: 267px;
            width: 295px;
        }
    }
</style>
<!-- mfa css -->
<style>
    .icon {
        position: absolute;
        top: 13px;
        left: 46px;
        font-size: 27px !important;
    }

    .box {
        height: 120px;
        border-radius: 5px;
        border: 2px solid #F2F2F2;
        cursor: pointer;
    }

    .active {
        background: #F8FFFD;
        border: 2px solid #10BC83;
    }

    .setup {
        position: absolute;
        bottom: 0;
        color: #0291FF;
        display: none;
        left: 40%;
    }

    .box:hover .setup {
        display: inline;
    }

    .trusted-title {
        font-size: 15px;
        margin-top: 7px;
        display: inline;
    }
</style>
<!-- profile view css -->
<style>
    .col-sm-12 {
        margin-top: 1rem !important;
    }

    #employee_img {
        object-fit: cover;
        border-radius: 50%;
        height: 75px;
        width: 75px;
    }

    .head {
        clip-path: polygon(0 8%, 80% 8%, 100% 100%, 0% 100%);
        background: #00517cf5;
        color: #fff;
        width: 500px;

    }

    .profile-pic {
        max-width: 200px;
        max-height: 200px;
        display: block;
    }

    .file-upload {
        display: none;
    }

    .circle {
        border-radius: 1000px !important;
        overflow: hidden;
        width: 128px;
        height: 128px;
        border: 8px solid rgba(255, 255, 255, 0.7);
        position: absolute;
        top: 72px;
    }

    img {
        max-width: 100%;
        height: auto;
    }

    .p-image {
        position: absolute;
        top: 167px;
        right: 30px;
        color: #666666;
        transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
    }

    .p-image:hover {
        transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
    }

    .upload-button {
        font-size: 1.2em;
    }

    .upload-button:hover {
        transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
        color: #999;
    }

    .media_btn {
        width: 60px;
        height: 60px !important;
        position: absolute;
        top: 76px;
        border-radius: 50%;
        z-index: 1;
        left: 295px
    }

    @media only screen and (max-width: 575px) {
        .media_btn {
            position: fixed;
            top: 78%;
            left: 79%;
        }
    }

    @media (min-width: 576px) and (max-width: 767px) {
        .media_btn {
            position: fixed;
            top: 12%;
            left: 88%;
        }
    }

    @media (min-width: 768px) and (max-width: 1199px) {
        .media_btn {
            position: fixed;
            top: 12%;
            left: 89%;
        }
    }
</style>