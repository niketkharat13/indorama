<?php $this->load->view('header'); ?>
<?php $this->load->view('top'); ?>
<?php $this->load->view('vertical_navbar'); ?>
<?php $this->load->view('authentication_css') ?>
</style>
<div class="">
    <div class="">
        <div class="card p-2" style="margin-top:2.5rem">
            <h4 style="font-style:16px">MFA Modes</h4>
            <div class="row mt-2">
                <div class="col-md-3 col-sm-12 mt-1">
                    <div class="box active text-center" id="box-1">
                        <div class="d-flex justify-content-center align-items-center">
                            <i class="la la-google icon" style="color:#ffb900"></i>
                            <p style="font-size:15px" class="mb-0 mt-1">Google Authenticator</p>
                        </div>
                        <p style="font-size:12px;margin-top:10px;color:#c5c5c5">Google 2-Step Verification</p>
                        <p class="setup ">Setup Now</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 mt-1">
                    <div class="box text-center" id="box-2">
                        <div class="d-flex justify-content-center align-items-center">
                            <i class="ft-message-square icon" style="color:#00a4ee"></i>
                            <p style="font-size:15px" class="mb-0 mt-1">Mobile-based OTP</p>
                        </div>
                        <p style="font-size:12px;margin-top:10px;color:#c5c5c5">Sending OTP to your Registered Mobile</p>
                        <p class="setup">Setup Now</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 mt-1">
                    <div class="box text-center" id="box-3">
                        <div class="d-flex justify-content-center align-items-center">
                            <i class="ft-clock icon" style="color:#7fba00"></i>
                            <p style="font-size:15px" class="mb-0 mt-1">Time-based OTP</p>
                        </div>
                        <p style="font-size:12px;margin-top:10px;color:#c5c5c5">Scan the QR code with your authenticator app to receive verification codes</p>
                        <p class="setup">Setup Now</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 mt-1">
                    <div class="box text-center">
                        <div class="">

                            <p style="font-size:15px" class="mb-0 mt-1">Backup Verification Code</p>
                            <p class="mb-0" style="font-size:12px" id="msg-1">Use this code for Ofline Verification</p>
                        </div>
                        <button class="btn-yu primary mt-1" style="width:200px" id="genrate">Generate</button>
                        <p class="mt-1 mb-0" style="font-size:16px" id="code"></p>
                        <p style="font-size:12px" id="msg">These codes can be used to access your account if you have trouble signing in.</p>
                    </div>
                </div>
            </div>
            <h4 style="font-style:16px" class="mt-2">Trusted Devices</h4>
            <p style="font-size:15px">Assign trusted browsers and sign in to your account without verification for the next 180 days.</p>
            <hr>
            <div class="row pl-1">
                <div class=" col-md-1 col-sm-12 pr-0">
                    <i style="font-size:30px" class="la la-desktop"></i>
                </div>
                <div class="col-md-5 col-sm-12 pl-0 d-flex align-items-center">
                    <h4 class="mb-0 trusted-title">Personal Computer</h4>
                    <i style="font-size:22px;color:#00a4ee" class="la la-windows ml-5"></i>
                    <i style="font-size:22px;color:#ffb900" class="la la-chrome ml-2"></i>
                </div>
                <div class="col-md-4 col-sm-12">
                    <h4 class="mb-0 trusted-title" style="color:#c5c5c5">Mumbai, Maharashtra, India</h4>
                </div>
            </div>
            <div class="row pl-1 mt-1 ">
                <div class=" col-md-1 col-sm-5 pr-0">
                    <i style="font-size:30px" class="la la-mobile-phone"></i>
                </div>
                <div class="col-md-5 col-sm-5 pl-0 d-flex align-items-center">
                    <h4 class="mb-0 trusted-title">Iphone 11 Pro</h4>
                    <i style="font-size:22px;margin-left:94px;;color:#00a4ee" class="la la-windows"></i>
                    <i style="font-size:22px;color:#ffb900" class="la la-chrome ml-2"></i>
                </div>
                <div class="col-md-4 col-sm-12">
                    <h4 class="mb-0 trusted-title" style="color:#c5c5c5">Mumbai, Maharashtra, India</h4>
                </div>
            </div>
            <hr>
            <div class="security mt-2">
                <h4 style="font-style:16px">Security Question</h4>
                <div class="row mt-1">
                    <div class="col-md-4 col-sm-12 mt-1">
                        <select name="" id="" class="form-control">
                            <option value="">What is Your Pet Name ?</option>
                            <option value="">What is Your Mother Middle Name ?</option>
                            <option value="">What is Your Primary School Name ?</option>
                            <option value=""> “What was your first car’s make and model ?</option>
                        </select>
                    </div>
                    <div class="col-md-1 col-sm-12 mt-1">
                        <label for="" class="mb-0">Answer</label>
                    </div>
                    <div class="col-md-4 col-sm-12 mt-1">
                        <input type="text" name="" id="" class="form-control">
                    </div>
                    <div class="col-md-3 col-sm-12 mt-1">
                        <button class="btn-yu primary" style="width:100px">Cancel</button>
                        <button class="btn-yu primary ml-1" style="width:100px">Next</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('authentication_js') ?>
<?php $this->load->view('footer') ?>