<html>

<head>
    <link rel="icon" href="<?= base_url() ?>app-assets/images/ico/fav.ico" type="image/png" sizes="16x16">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/bootstrap.css">


    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/ui-standard.css">
    <title>
        Indorama
    </title>
    <script src="<?= base_url() ?>app-assets/js/jquery.js"></script>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport" />
    <!--Fonts and icons-->
    <script type="text/javascript" src="<?= base_url() ?>app-assets/fonts/font-awesome/css/font-awesome.min.css"></script>
    <link rel="stylesheet" href="<?= base_url() ?>app-assets/fonts/icomon/iconmonstr-iconic-font.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/fonts/feather/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/ui-standard.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/plugins/confirm_js/jquery-confirm.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/fonts/mobiriseicons/24px/mobirise/style.css">
</head>
<?php $this->load->view('authentication_css'); ?>
<style>
    .sign_up_logo {
        margin-top: 28px;
        width: 232px;
        margin-left: 18px;
    }
</style>

<body>
    <div>
        <div class="parent-div">
            <div class="">
                <div class="">
                    <div class="card-margin">
                        <div class="">
                            <div class="card card-login-dark " style="margin-top:160px !important;">
                                <div class=" card-header-primary text-center card-header-darkk card-body-dark">
                                    <img id="yu-logo" class="sign_up_logo" src="<?= base_url() ?>app-assets/images/yu_logo.png" />
                                </div>
                                <!-- <input type="hidden" id="landing_parameter" value="<?= $landing_parameter ?>"> -->
                                <div class="card-body mt-5" style="padding: 1rem !important;">
                                    <div id="box">
                                        <form method="post" id="login_form" class="form" action="<?= base_url() ?>authentication/authenticate/role_handler">
                                            <div class="prompt">
                                                <h2>
                                                    <p class="login-para-dark" access_control>Login</p>
                                                </h2>
                                            </div>
                                            <div id="div1">
                                                <div class="row-yu">
                                                    <i class="ft-user cust cust-dark"></i>
                                                    <input type="text" id="login_username" name="login_username" class="form-control username-style" access_control placeholder="Enter Username" />
                                                </div>
                                                <div>
                                                    <p class="text-left text" id="validation_text" style="color:white"></p>
                                                </div>
                                            </div>
                                            <div class="row-yu">
                                                <i class="ft-lock cust-dark"></i>
                                                <div class="password-style d-flex bg-white" style="width:240px;border-radius:0.25rem">
                                                    <input type="password" id="password" name="password" class="form-control" access_control placeholder="Enter Password" style="border:none" />
                                                    <i class="ft-eye-off ml-0 mr-1" id="login_show_password" onclick="showpassword('login_show_password','password')"></i>
                                                </div>

                                            </div>
                                            <div class="input-group-prepend login-div" access_control>
                                                <input type="button" value="Log in" id="login_link" class="btn-yu btn-width-110 login-button-dark button">
                                            </div>

                                            <div class="input-group mt_0-5 forgot-div">
                                                <p class="forgot-password-dark" style="cursor:pointer" id="forget_credential" onclick="togglePage('box','forget_pwd')">Forgot Credential ? </p>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="otp" style="display:none">
                                        <form method="post" id="otp_form" class="digit-group" data-group-name="digits" data-autosubmit="false" autocomplete="off" style="margin-left: -3px" class="form">
                                            <div class="prompt">
                                                <p style="font-size:14px;" class="otp-para-dark dark-text text-center" style="font-size: 14px;">OTP Sent</p>
                                                <div style="margin-left: 2.83rem;">
                                                    <p style="font-size:14px;">We have just send you the verification code</p>
                                                    <p style="font-size:14px;">The OTP will expire in <b><span id="timer"></span>
                                                        </b></p>
                                                </div>
                                            </div>
                                            <input type="text" style="margin-left:11px" id="digit-1" name="digit-1" data-next="digit-2" />
                                            <input type="text" id="digit-2" name="digit-2" data-next="digit-3" data-previous="digit-1" />
                                            <input type="text" id="digit-3" name="digit-3" data-next="digit-4" data-previous="digit-2" />
                                            <span class="splitter">&ndash;</span>
                                            <input type="text" id="digit-4" name="digit-4" data-next="digit-5" data-previous="digit-3" />
                                            <input type="text" id="digit-5" name="digit-5" data-next="digit-6" data-previous="digit-4" />
                                            <input type="text" id="digit-6" name="digit-6" data-previous="digit-5" style="margin-left:-2px ">
                                            <div class="text-center" style="color:#fff; margin-top: 10px">
                                                <p style="margin-top:0;cursor:pointer " id="resend">Resend OTP?</p>
                                                <button class="btn login-button-dark" id="cancel_btn" style="margin-top:-10px;" onclick="togglePage('otp','box')"> Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="forget_pwd" style="display:none">
                                        <form action="get" id="forgot_password_form" class="form">
                                            <div class="prompt">
                                                <p style="font-size:20px;" class="otp-para-dark dark-text text-center">Forgot Password</p>
                                                <div>
                                                    <p style="font-size:14px;margin-left:22px" id="forgot_username_link">Enter Your Username</p>
                                                </div>
                                            </div>
                                            <div class="row-yu">
                                                <i class="fa fa-user cust cust-dark"></i><input type="text" id="recovery_email_id" name="recovery_email_id" class="form-control username-style" access_control placeholder="Enter Username" />
                                            </div>
                                            <div class="input-group-prepend login-div" access_control>
                                                <input type="button" value="Submit" id="forget-submit-btn" class="btn-yu btn-width-110 login-button-dark button" onclick="togglePage('forget_pwd','password_recovery')">
                                            </div>

                                            <div class="input-group mt_0-5 forgot-div">
                                                <p class="forgot-password-dark" style="cursor:pointer" id="fuser" onclick="togglePage('forget_pwd','forgot_username')">Forgot Username ? </p>
                                            </div>
                                            <div class="input-group mt_0-5 forgot-div">
                                                <i class="ft-arrow-left" style="font-size:15px;margin-top:4px;color:#fff;margin-right:5px"></i>
                                                <p class="forgot-password-dark" style="cursor:pointer" id="back_to_login_forget_password" onclick="togglePage('forget_pwd','box')">Back to Login</p>
                                            </div>
                                        </form>
                                    </div>

                                    <div id="password_recovery" style="display:none">
                                        <form action="post" id="password_recovery_form" class="form">
                                            <p style="font-size:20px" class="text-white text-center">Password Recovery</p>
                                            <p style="font-size:14px" class="text-white mb-0 text-center">We have sent instructions on your registerd Email ID</p>
                                            <div class="input-group-prepend mt-1 login-div" access_control>
                                                <a href="<?= base_url() ?>authentication/authenticate/reset_password" style="font-size:14px;text-decoration:underline" class="text-white">Open in Email</a>
                                            </div>
                                            <div class="input-group-prepend login-div" access_control>
                                                <input type="button" value="Back to Login" id="back_to_login_password_recovery" class="btn-yu btn-width login-button-dark button" onclick="togglePage('password_recovery','box')">
                                            </div>
                                        </form>
                                    </div>
                                    <div id="forgot_username" style="display:none">
                                        <form action="post" id="forget_username_form" class="form">
                                            <p style="font-size:20px" class="text-white text-center">Forgot Username</p>
                                            <p style="font-size:14px;margin-left:26px" class="text-white mb-0">Send Recovery Options Via</p>
                                            <div class="d-flex" style="margin-left:26px">
                                                <div class="radio ml-0">
                                                    <input id="email-option" name="radio" type="radio">
                                                    <label for="email-option" class="radio-label text-white" onclick="appropiateInput('email-box','mobile-box','mail_msg','phone_msg')">Email ID</label>
                                                </div>
                                                <div class="radio ml-0">
                                                    <input id="mobile-option" name="radio" type="radio">
                                                    <label for="mobile-option" class="radio-label text-white" onclick="appropiateInput('mobile-box','email-box','phone_msg','mail_msg')">Mobile #</label>
                                                </div>
                                            </div>
                                            <div id="mobile-box" style="display:none">
                                                <p style="font-size:14px;margin-left:26px" class="text-white">Enter Your Registered Mobile #</p>
                                                <div class="row-yu">
                                                    <i class="ft-phone cust-dark"></i>
                                                    <input type="text" id="recovery-contact" name="contact" class="form-control password-style" access_control placeholder="Enter Phone #" />
                                                </div>
                                            </div>
                                            <div id="email-box" style="display:none">
                                                <p style="font-size:14px;margin-left:26px" class="text-white">Enter Your Registered Email ID</p>
                                                <div class="row-yu">
                                                    <i class="ft-mail cust-dark"></i>
                                                    <input type="text" id="recovery-mail" name="contact" class="form-control password-style" access_control placeholder="Enter Email ID" />
                                                </div>
                                            </div>
                                            <div class="input-group-prepend login-div" access_control>
                                                <input type="button" value="Submit" id="username-submit" class="btn-yu btn-width login-button-dark button" onclick="togglePage('forgot_username','username_recovery')">
                                            </div>
                                            <div class="input-group mt_0-5 forgot-div">
                                                <i class="ft-arrow-left" style="font-size:15px;margin-top:4px;color:#fff;margin-right:5px"></i>
                                                <p class="forgot-password-dark" style="cursor:pointer;" id="back_to_login_forget_username" onclick="togglePage('forgot_username','box')">Back to Login</p>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="username_recovery" style="display:none">
                                        <form action="post" id="account_recovery_form" class="form">
                                            <p style="font-size:20px" class="text-white">Account Recovery</p>
                                            <p id="mail_msg" style="font-size:14px;display:none" class="text-white">We have Send Mail to Your Registerd Email ID</p>
                                            <p id="phone_msg" style="font-size:14px;display:none" class="text-white">We have Send SMS to Your Registerd Mobile No.</p>
                                            <div class="input-group-prepend login-div" access_control>
                                                <input type="button" value="Back to Login" id="back_to_login_username_recovery" class="btn-yu btn-width login-button-dark" onclick="togglePage('username_recovery','box')">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?= base_url() ?>app-assets/js/jquery.min.js"></script>
    <script src="<?= base_url() ?>app-assets/js/plugins/popper_js/popper.min.js"></script>
    <script src="<?= base_url() ?>app-assets/js/custom-ui-standard.js" language="javascript"></script>
    <script src="<?= base_url() ?>app-assets/js/plugins/confirm_js/jquery-confirm.min.js"></script>
    <?php $this->load->view('authentication_js'); ?>
</body>
<!-- <script>
    //OTP
    $("#digit-6").on("keyup", function(e) {
        if (e.key != "Backspace") {
            var otp = $("#digit-1").val() + $("#digit-2").val() + $("#digit-3").val() + $("#digit-4").val() + $("#digit-5").val() + $("#digit-6").val();
            if (otp == '123456') {

                var user_role = "";

                $.ajax({
                    url: "<?= base_url() ?>authentication/Session_management/get_session",
                    type: "POST",
                    dataType: 'HTML',
                    data: "key=role",
                    success: function(data) {
                        user_role = data;


                        var login_state = window.location.search;
                        if (login_state == "?initial_login=true") {
                            if (user_role == "karven1") {
                                window.location.href = "<?= base_url() ?>dashboard/Dashboard_controller/dashboard_view";
                            } else if (user_role == "karcli") {
                                window.location.href = "<?= base_url() ?>dashboard/Dashboard_controller/dashboard_view";
                            } else if (user_role == "karaud") {
                                window.location.href = "<?= base_url() ?>dashboard/Dashboard_controller/dashboard_view";
                            } else if (user_role == "karadm") {
                                window.location.href = "<?= base_url() ?>dashboard/Dashboard_controller/dashboard_view";
                            }
                            $.dialog({
                                title: 'LOGIN',
                                content: 'successfully',
                                type: 'green',
                            });
                        } else if (login_state == "?initial_login=enter") {
                            if (user_role == "karven1") {
                                window.location.href = "<?= base_url() ?>dashboard/Dashboard_controller/dashboard_view";
                            } else if (user_role == "karcli") {
                                window.location.href = "<?= base_url() ?>dashboard/Dashboard_controller/dashboard_view";
                            } else if (user_role == "karaud") {
                                window.location.href = "<?= base_url() ?>dashboard/Dashboard_controller/dashboard_view";
                            } else if (user_role == "karadm") {
                                window.location.href = "<?= base_url() ?>dashboard/Dashboard_controller/dashboard_view";
                            }
                            $.dialog({
                                title: 'LOGIN',
                                content: 'successfully',
                                type: 'green',
                            });
                        }
                    }
                });
            } else {
                $.dialog({
                    title: '',
                    content: 'WRONG OTP!!',
                    type: 'red',
                });
            }
        }

    });
</script> -->


</html>