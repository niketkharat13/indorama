<?php $this->load->view('header'); ?>
<?php $this->load->view('authentication_css') ?>
<div class="">
    <div class="col">
        <div class="card p-4" style="margin-top:2.5rem">
            <div class="container">
                <h4 style="font-size:18px">Reset Password</h4>
                <div class="row mt-2">
                    <div class="col-md-3 col-sm-12">
                        <h4 style="font-size:15px;margin-top:11px">New Password</h4>
                    </div>
                    <div class="col-md-3 col-sm-12 pl-0 custom-input-div password-field ">
                        <input class="form-control" id="reset_password" style="border:none" type="password" placeholder="New Password">
                        <i class="ft-eye-off" id="reset_show_new_password_btn"></i>
                        <div id="reset_pwd_strength_wrap">
                            <div id="passwordDescription">Password not entered</div>
                            <div id="passwordStrength" class="strength0"></div>
                            <div id="pswd_info">
                                <strong>Strong Password Tips:</strong>
                                <ul>
                                    <li class="invalid" id="length">At least 6 characters</li>
                                    <li class="invalid" id="pnum">At least one number</li>
                                    <li class="invalid" id="capital">At least one lowercase &amp; one uppercase letter</li>
                                    <li class="invalid" id="spchar">At least one special character</li>
                                </ul>
                            </div><!-- END pwd_strength_wrap -->
                        </div>
                    </div>
                </div>
                <div class="row margin" id="dynamic">
                    <div class="col-md-3 col-sm-12">
                        <h4 style="font-size:15px;margin-top:11px">Confirm Password</h4>
                    </div>
                    <div class="col-md-3 col-sm-12 pl-0 custom-input-div">
                        <input disabled class="form-control disabled_input" id="cnfm_reset_password" style="border:none" type="password" placeholder="Confirm Password"><i class="ft-eye-off" id="reset_show_confirm_password_btn"></i>
                    </div>

                </div>
                <div class="row" id="">
                    <div class="col-md-3 col-sm-12"></div>
                    <div class="col-md-3 col-sm-12 pl-0">
                      <p id="reset_compare_msg" class="compare-msg"></p>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-3"></div>
                    <div class="col-4">
                        <button class="btn-yu primary disabled_input" style="width:150px;margin-left:44px" id="reset_password_btn" onclick="resetPassword()" disabled>Reset Password</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer') ?>
<?php $this->load->view('authentication_js') ?>
<script>
    $('.footer').hide();
</script>
    