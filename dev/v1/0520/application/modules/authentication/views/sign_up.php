<html>

<head>
    <link rel="icon" href="<?= base_url() ?>app-assets/images/ico/fav.png" type="image/png" sizes="16x16">
    <link rel="stylesheet" href="<?= base_url() ?>app-assets/css/bootstrap.min.css">
    <link rel="icon" type="image/png" href="./assets/img/favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/ui-standard.css">
    <title>
        Indorama
    </title>
    <script src="<?= base_url() ?>app-assets/js/jquery.min.js"></script>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport" />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="<?= base_url() ?>app-assets/fonts/icomon/iconmonstr-iconic-font.min.css">
    <script type="text/javascript" src="<?= base_url() ?>app-assets/fonts/font-awesome/css/font-awesome.min.css"></script>
    <link rel="stylesheet" href="<?= base_url() ?>app-assets/fonts/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/fonts/feather/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/ui-standard.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/plugins/confirm_js/jquery-confirm.min.css">

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/fonts/mobiriseicons/24px/mobirise/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/plugins/forms/validation/form-validation.css">
    <!-- Toastr JS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/plugins/toastr/toastr.min.css">
    <!-- End Toastr JS -->


</head>
<?php $this->load->view('authentication_css'); ?>
<style>
    .sign_up_logo {
        margin-top: 28px;
        width: 232px;
        margin-left: 18px;
    }
</style>

<body>
    <div class="parent-div">
        <div class="">
            <div class="">
                <div class="card-margin">
                    <div class="">
                        <div class="card card-login-dark " style="margin-top:160px !important;">
                            <div class=" card-header-primary text-center card-header-darkk card-body-dark">
                                <img id="yu-logo" class="sign_up_logo" alt="Yashuss Unlimited" src="<?= base_url() ?>app-assets/images/yu_logo.png"/>
                            </div>
                            <div class="card-body mt-5">
                                <div id="sign-up-box">
                                    <div class="prompt">
                                        <h2>
                                            <p class="login-para-dark">Sign Up</p>
                                        </h2>
                                    </div>

                                    <form class="form-horizontal form-simple" action="<?= base_url() ?>authentication/Authenticate/login" novalidate id="sign_up_form">
                                        <fieldset class=" ">
                                            <div class="d-flex"> <i class="ft-user cust cust-dark"></i>

                                                <input type="text" style="background-color: rgb(232, 240, 254);" class="form-control username-style" id="user-name" placeholder="Enter Username" onfocusOut="check()">

                                            </div>
                                        </fieldset>
                                        <fieldset class="">
                                            <div class=" d-flex" style="margin-bottom:10px;  ">
                                                <i class="ft-mail cust cust-dark " style="margin-right:10px"></i>
                                                <div class="controls">
                                                    <input type="email" name="email" class="form-control" required id="signup_mail_id" data-validation-required-message="This field is required" style="margin-right:31px; background-color: rgb(232, 240, 254);" placeholder="Enter Email ID">
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="">
                                            <div class="form-group">
                                                <div class="d-flex">
                                                    <i class="ft-lock cust cust-dark"></i>
                                                    <input type="password" name="password" class="form-control password-style pass-1" required data-validation-required-message="This field is required" style="margin-left: 10px; background-color: rgb(232, 240, 254);" placeholder="Password" id="signup_password">
                                                    <div id="signup_pwd_strength_wrap">
                                                        <div id="passwordDescription">Password not entered</div>
                                                        <div id="passwordStrength" class="strength0"></div>
                                                        <div id="pswd_info">
                                                            <strong>Strong Password Tips:</strong>
                                                            <ul>
                                                                <li class="invalid" id="length">At least 6 characters</li>
                                                                <li class="invalid" id="pnum">At least one number</li>
                                                                <li class="invalid" id="capital">At least one lowercase &amp; one uppercase letter</li>
                                                                <li class="invalid" id="spchar">At least one special character</li>
                                                            </ul>
                                                        </div><!-- END pwd_strength_wrap -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" id="dynamic">
                                                <div class="d-flex">
                                                    <i class="ft-lock cust cust-dark"></i>
                                                    <input type="password" name="password2" data-validation-match-match="password" class="form-control password-style pass-2 disabled_input" required style="margin-left: 10px;background-color: rgb(232, 240, 254);" id="signup_confirm_password" placeholder="Repeat Password Again" disabled>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <p id="signup_compare_msg" class="compare-msg text-white text-center"></p>
                                        <div class="input-group-prepend mt-4  sign-up-div btn-block" style="margin-bottom:-25px;">
                                            <input type="button" value="Sign Up" id="sign_up_btn" class="btn login-button-dark disabled_input" onclick="sign()" disabled>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?= base_url() ?>app-assets/js/custom-ui-standard.js" language="javascript"></script>
    <script src="<?= base_url() ?>app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
    <script src="<?= base_url() ?>app-assets/js/scripts/forms/form-login-register.js"></script>
    <script src="<?= base_url() ?>app-assets/js/scripts/forms/validation/form-validation.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>app-assets/js/plugins/toastr_js/toastr.min.js"></script>
    <?php $this->load->view('authentication_js'); ?>
</body>

</html>