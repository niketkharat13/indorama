<script>
    $('#login_link').click(function() {
        if ($('#login_username').val() == "purushothaman.tr" && $('#password').val() == "We1c0me@123") {
            window.location.href = '<?= base_url() ?>dashboard/dashboard_controller/landing_page';
        } else if ($('#login_username').val() == "" && $('#password').val() == "") {
            $('#validation_text').text('Please Enter Fields');
        } else if ($('#login_username').val() !== "purushothaman.tr" || $('#password').val() !== "We1c0me@123") {
            $('#validation_text').text('Please Enter Valid Credential');
        }
    });
    $("input#signup_password").blur(function() {
        $('#dynamic').removeClass("dynamic_margin");
        $("#signup_pwd_strength_wrap").fadeOut(400);
    });
    $('#signup_password').click(function() {
        $('#dynamic').addClass("dynamic_margin");
    });
    $("input#signup_password").on("focus keyup", function() {
        var score = 0;
        var a = $(this).val();
        var desc = new Array();

        // strength desc
        desc[0] = "Too short";
        desc[1] = "Weak";
        desc[2] = "Good";
        desc[3] = "Strong";
        desc[4] = "Best";

        $("#signup_pwd_strength_wrap").fadeIn(400);

        // password length
        if (a.length >= 6) {
            $("#length").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#length").removeClass("valid").addClass("invalid");
        }

        // at least 1 digit in password
        if (a.match(/\d/)) {
            $("#pnum").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#pnum").removeClass("valid").addClass("invalid");
        }

        // at least 1 capital & lower letter in password
        if (a.match(/[A-Z]/) && a.match(/[a-z]/)) {
            $("#capital").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#capital").removeClass("valid").addClass("invalid");
        }

        // at least 1 special character in password {
        if (a.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) {
            $("#spchar").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#spchar").removeClass("valid").addClass("invalid");
        }
        if (score > 1) {
            $('#signup_confirm_password').prop('disabled', false);
            $('#signup_confirm_password').removeClass('disabled_input');
        } else if (score < 2) {
            $('#signup_confirm_password').prop('disabled', true);
            $('#signup_confirm_password').addClass('disabled_input');
            $('#signup_confirm_password').val('');
        }
        if (a.length > 0) {
            //show strength text
            $("#passwordDescription").text(desc[score]);
            // show indicator
            $("#passwordStrength").removeClass().addClass("strength" + score);
        } else {
            $("#passwordDescription").text("Password not entered");
            $("#passwordStrength").removeClass().addClass("strength" + score);
        }
    });

    $('#signup_confirm_password').keyup(function() {
        var password = $('#signup_password').val();
        var c_password = $('#signup_confirm_password').val();
        if (password != c_password) {
            $('#signup_compare_msg').html("Password Doesn't Match");
            $('#sign_up_btn').prop('disabled', true);
            $('#sign_up_btn').addClass('disabled_input')
        } else {
            $('#signup_compare_msg').html("");
            $('#sign_up_btn').prop('disabled', false);
            $('#sign_up_btn').removeClass('disabled_input')
        }
    });
    // Enter Key Button Press Code
    $(document).keypress(function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if ($("#box").is(":visible")) {
                $('#login_link').click();
            } else if ($("#forgot_username").is(":visible")) {
                $('#username-submit').click();
            } else if ($("#forget_pwd").is(":visible")) {
                $('#forget-submit-btn').click();
            } else if ($("#password_recovery").is(":visible")) {
                $('#back_to_login_password_recovery').click();
            } else if ($("#username_recovery").is(":visible")) {
                $('#back_to_login_username_recovery').click();
            } else if ($("#username_recovery").is(":visible")) {
                $('#back_to_login_username_recovery').click();
            } else if ($("#sign-up-box").is(":visible")) {
                if ($('#signup_password').val() == $('#signup_confirm_password').val()) {
                    $('#sign_up_btn').click();
                }
            }
            $("#digit-1").focus();
        }
    });
    //Send Recovery Options Via Box Hide Show Function
    function appropiateInput(show, hide, show1, hide1) {
        $('#' + show).show();
        $('#' + hide).hide();
        $('#' + show1).show();
        $('#' + hide1).hide();
    }

    // page toggle function----It will hide show appropiate component
    function togglePage(hide, show) {
        $('#' + hide).hide(1000);
        $('#' + show).show(500);
    }

    // Resend OTP Code
    $("#resend").click(function() {
        otptimer();
    });
</script>
<script>
    // email mapping to user name function
    // function check() {

    //     var user_name = $("#user-name").val();
    //     if (user_name == "purushothaman.tr" || user_name == "karven2" || user_name == "karven3" || user_name == "karven4" || user_name == "karven5") {
    //         if (user_name == "karven1") {
    //             document.getElementById("signup_mail_id").value = "karven1@gmail.com";
    //         } else if (user_name == "karven2") {
    //             document.getElementById("signup_mail_id").value = "";
    //         } else if (user_name == "karven3") {
    //             document.getElementById("signup_mail_id").value = "";
    //         } else if (user_name == "karven4") {
    //             document.getElementById("signup_mail_id").value = "";
    //         } else if (user_name == "karven5") {
    //             document.getElementById("signup_mail_id").value = "";
    //         } else {
    //             document.getElementById("signup_mail_id").value = "";
    //             toastr.warning('Please check and Re-enter!');
    //             toastr.error('Username does not exist in our system ');
    //         }
    //     } else if (user_name == "karaud") {
    //         document.getElementById("signup_mail_id").value = "karaud@gmail.com";

    //     } else if (user_name == "karadm") {
    //         document.getElementById("signup_mail_id").value = "karadm@gmail.com";
    //     } else if (user_name == "karcli") {
    //         document.getElementById("signup_mail_id").value = "karcli@gmail.com";
    //     }
    // }
    // Sign up function
    function sign() {
        var password = $(".pass-1").val();
        var confirmPassword = $(".pass-2").val();
        if (password == confirmPassword) {
            toastr.success("Signup Successfully");
            setInterval(function() {
                window.location.href = "<?= base_url() ?>authentication/Authenticate/login?initial_login=true";
            }, 1000)
            // window.location.href = "<?= base_url() ?>authentication/Authenticate/login";
        } else {
            toastr.error("Signup Failed");
        }
    }
</script>
<!-- change password js -->
<script>
    function changePassword() {
        toastr.success("Password has been updated");
        setInterval(function() {
            window.location.href = "<?= base_url() ?>authentication/Authenticate/login?initial_login=true"
        }, 3000);
    }
    $("input#change_pwd").blur(function() {
        $('#dynamic').removeClass("dynamic_margin");
        $("#pwd_strength_wrap").fadeOut(400);
    });
    $('#change_pwd').click(function() {
        $('#dynamic').addClass("dynamic_margin");
    });
    $("input#change_pwd").on("focus keyup", function() {
        var score = 0;
        var a = $(this).val();
        var desc = new Array();

        // strength desc
        desc[0] = "Too short";
        desc[1] = "Weak";
        desc[2] = "Good";
        desc[3] = "Strong";
        desc[4] = "Best";

        $("#pwd_strength_wrap").fadeIn(400);

        // password length
        if (a.length >= 6) {
            $("#length").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#length").removeClass("valid").addClass("invalid");
        }

        // at least 1 digit in password
        if (a.match(/\d/)) {
            $("#pnum").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#pnum").removeClass("valid").addClass("invalid");
        }

        // at least 1 capital & lower letter in password
        if (a.match(/[A-Z]/) && a.match(/[a-z]/)) {
            $("#capital").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#capital").removeClass("valid").addClass("invalid");
        }

        // at least 1 special character in password {
        if (a.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) {
            $("#spchar").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#spchar").removeClass("valid").addClass("invalid");
        }
        if (score > 1) {
            $('#cnfm_pwd').prop('disabled', false);
            $('#cnfm_pwd').removeClass('disabled_input');
        } else if (score < 2) {
            $('#cnfm_pwd').prop('disabled', true);
            $('#cnfm_pwd').addClass('disabled_input');
            $('#cnfm_pwd').val('');
        }
        if (a.length > 0) {
            //show strength text
            $("#passwordDescription").text(desc[score]);
            // show indicator
            $("#passwordStrength").removeClass().addClass("strength" + score);
        } else {
            $("#passwordDescription").text("Password not entered");
            $("#passwordStrength").removeClass().addClass("strength" + score);
        }
    });
    $('#show_confirm_password_btn').click(function() {
        if ($('#cnfm_pwd').get(0).type == 'password') {
            $('#cnfm_pwd').get(0).type = 'text';
            $('#show_confirm_password_btn').removeClass('ft-eye-off');
            $('#show_confirm_password_btn').addClass('ft-eye');
        } else {
            $('#cnfm_pwd').get(0).type = 'password';
            $('#show_confirm_password_btn').removeClass('ft-eye');
            $('#show_confirm_password_btn').addClass('ft-eye-off');
        }
    });
    $('#show_new_password_btn').click(function() {
        if ($('#pwd').get(0).type == 'password') {
            $('#pwd').get(0).type = 'text';
            $('#show_new_password_btn').removeClass('ft-eye-off');
            $('#show_new_password_btn').addClass('ft-eye');
        } else {
            $('#pwd').get(0).type = 'password';
            $('#show_new_password_btn').removeClass('ft-eye');
            $('#show_new_password_btn').addClass('ft-eye-off');
        }
    });
    $('#show_old_password_btn').click(function() {
        if ($('#old_pwd').get(0).type == 'password') {
            $('#old_pwd').get(0).type = 'text';
            $('#show_old_password_btn').removeClass('ft-eye-off');
            $('#show_old_password_btn').addClass('ft-eye');
        } else {
            $('#old_pwd').get(0).type = 'password';
            $('#show_old_password_btn').removeClass('ft-eye');
            $('#show_old_password_btn').addClass('ft-eye-off');
        }
    });
    $('#cnfm_pwd').keyup(function() {
        var password = $('#pwd').val();
        var c_password = $('#cnfm_pwd').val();
        if (password != c_password) {
            $('#change_compare_msg').html("Password Doesn't Match");
            $('#change_password_btn').prop('disabled', true);
        } else {
            $('#change_password_btn').removeClass('disabled_input');
            $('#change_password_btn').prop('disabled', false);
            $('#change_compare_msg').html("");
        }
    });
</script>

<!-- mfa js -->
<script>
    $('#msg').css('display', 'none');
    $('#genrate').click(function() {
        generateOTP();
        var code;
        $('#genrate').css('display', 'none');
        $('#msg-1').css('display', 'none');
        $('#msg').css('display', 'inline');
    });

    function generateOTP() {
        // Declare a digits variable
        // which stores all digits
        var digits = '0123456789';
        let BACKUP_CODE = '';
        for (let i = 0; i < 8; i++) {
            BACKUP_CODE += digits[Math.floor(Math.random() * 10)];
        }
        code = BACKUP_CODE;
        $('#code').text(code);

    }

    $('#box-1').click(function() {
        $('#box-1').addClass('active');
        $('#box-2').removeClass('active');
        $('#box-3').removeClass('active');
    });

    $('#box-2').click(function() {
        $('#box-2').addClass('active');
        $('#box-1').removeClass('active');
        $('#box-3').removeClass('active');
    });

    $('#box-3').click(function() {
        $('#box-3').addClass('active');
        $('#box-2').removeClass('active');
        $('#box-1').removeClass('active');
    });
</script>
<script>
    $('#reset_password').click(function() {
        $('#dynamic').addClass("dynamic_margin");
    })

    function resetPassword() {
        toastr.success("Password has been updated");
        setInterval(function() {
            window.location.href = "<?= base_url() ?>authentication/Authenticate/login?initial_login=true"
        }, 3000);
    }
    $("input#reset_password").blur(function() {
        $('#dynamic').removeClass("dynamic_margin");
        $("#reset_pwd_strength_wrap").fadeOut(400);
    });
    $("input#reset_password").on("focus keyup", function() {
        var score = 0;
        var a = $(this).val();
        var desc = new Array();

        // strength desc
        desc[0] = "Too short";
        desc[1] = "Weak";
        desc[2] = "Good";
        desc[3] = "Strong";
        desc[4] = "Best";

        $("#reset_pwd_strength_wrap").fadeIn(400);

        // password length
        if (a.length >= 6) {
            $("#length").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#length").removeClass("valid").addClass("invalid");
        }

        // at least 1 digit in password
        if (a.match(/\d/)) {
            $("#pnum").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#pnum").removeClass("valid").addClass("invalid");
        }

        // at least 1 capital & lower letter in password
        if (a.match(/[A-Z]/) && a.match(/[a-z]/)) {
            $("#capital").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#capital").removeClass("valid").addClass("invalid");
        }

        // at least 1 special character in password {
        if (a.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) {
            $("#spchar").removeClass("invalid").addClass("valid");
            score++;
        } else {
            $("#spchar").removeClass("valid").addClass("invalid");
        }
        if (score > 1) {
            $('#cnfm_reset_password').prop('disabled', false);
            $('#cnfm_reset_password').removeClass('disabled_input')
        } else if (score < 2) {
            $('#cnfm_reset_password').prop('disabled', true);
            $('#cnfm_reset_password').addClass('disabled_input');
            $('#cnfm_reset_password').val("");
        }
        if (a.length > 0) {
            //show strength text
            $("#passwordDescription").text(desc[score]);
            // show indicator
            $("#passwordStrength").removeClass().addClass("strength" + score);
        } else {
            $("#passwordDescription").text("Password not entered");
            $("#passwordStrength").removeClass().addClass("strength" + score);
        }
    });
    $('#reset_show_new_password_btn').click(function() {
        if ($('#reset_password').get(0).type == 'password') {
            $('#reset_password').get(0).type = 'text';
            $('#reset_show_new_password_btn').removeClass('ft-eye-off');
            $('#reset_show_new_password_btn').addClass('ft-eye');
        } else {
            $('#reset_password').get(0).type = 'password';
            $('#reset_show_new_password_btn').removeClass('ft-eye');
            $('#reset_show_new_password_btn').addClass('ft-eye-off');
        }
    });
    $('#reset_show_confirm_password_btn').click(function() {
        if ($('#cnfm_reset_password').get(0).type == 'password') {
            $('#cnfm_reset_password').get(0).type = 'text';
            $('#reset_show_confirm_password_btn').removeClass('ft-eye-off');
            $('#reset_show_confirm_password_btn').addClass('ft-eye');
        } else {
            $('#cnfm_reset_password').get(0).type = 'password';
            $('#reset_show_confirm_password_btn').removeClass('ft-eye');
            $('#reset_show_confirm_password_btn').addClass('ft-eye-off');
        }
    });
    $('#cnfm_reset_password').keyup(function() {
        var password = $('#reset_password').val();
        var c_password = $('#cnfm_reset_password').val();
        if (password != c_password) {
            $('#reset_compare_msg').html("Password Doesn't Match");
            $('#reset_password_btn').addClass('disabled_input')
            $('#reset_password_btn').prop('disabled', true)
        } else {
            $('#reset_compare_msg').html("");
            $('#reset_password_btn').prop('disabled', false)
            $('#reset_password_btn').removeClass('disabled_input')
        }
    });
</script>
<!-- profile view js -->
<script>
    $(document).ready(function() {
        var readURL = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.profile-pic').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".file-upload").on('change', function() {
            readURL(this);
        });

        $(".upload-button").on('click', function() {
            $(".file-upload").click();
        });
    });

    $('#edit').click(function() {
        var name = $('#p_name').html();
        var email = $('#email-address').html();
        var conntact = $('#contact').html();
        var dob = $('#dob').html();
        var country = $('#country').html();
        var lang = $('#lang').html();
        var divison = $('#division').html();
        var dept = $('#dept').html();
        var role = $('#role').html();
        var manager = $('#r-manager').html();
        var add = $('#add').html();
        var location = $('#lang').html();
        var lang = $('#location').html();
        var city = $('#city').html();
        var state = $('#state').html();
        var pincode = $('#pin-code').html();
        $('#name_field').val(name);
        $('#email_field').val(email);
        $('#contact_field').val(conntact);
        $('#dob_field').val(dob);
        $('#country_field').val(country);
        $('#language_field').val(lang);
        $('#dept_field').val(dept);
        $('#division_field').val(divison);
        $('#city_field').val(city);
        $('#state_field').val(state);
        $('#pin_field').val(pincode);
        $('#add_field').val(add);
        $('#location_field').val(location);
        $('#r_manager_field').val(manager);
        $('#role_field').val(role);
    });
</script>

<script>
    function validate() {
        var otp =
            document.getElementById("digit-1").value +
            document.getElementById("digit-2").value +
            document.getElementById("digit-3").value +
            document.getElementById("digit-4").value +
            document.getElementById("digit-5").value +
            document.getElementById("digit-6").value;
        if (otp == "123456") {
            alert(login)
        } else {
            attempt--; // Decrementing by one.
            alert("You have left " + attempt + " attempt;");
            // Disabling fields after 3 attempts.
            if (attempt == 0) {
                document.getElementById("username").disabled = true;
                document.getElementById("password").disabled = true;
                document.getElementById("submit").disabled = true;
                return false;
            }
        }
    }

    function showpassword(id, pwdid) {
        if ($('#' + pwdid).get(0).type == 'password') {
            $('#' + pwdid).get(0).type = 'text';
            $('#' + id).removeClass('ft-eye-off');
            $('#' + id).addClass('ft-eye');
        } else {
            $('#' + pwdid).get(0).type = 'password';
            $('#' + id).removeClass('ft-eye');
            $('#' + id).addClass('ft-eye-off');
        }
    }
</script>