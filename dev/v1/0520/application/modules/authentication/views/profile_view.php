<?php $this->load->view('header'); ?>
<?php $this->load->view('top'); ?>
<?php $this->load->view('vertical_navbar'); ?>
<?php $this->load->view('authentication_css') ?>
<div class="">
    <div class="col">
        <div>
            <div class="row mt-2">
                <div class="d-flex head p-1" style="margin-left: 17px;">
                    <img id="employee_img" src="<?= base_url() ?>app-assets/images/purushothaman_tr.png" alt="">
                    <div class="ml-1 mt-1 text-white">
                        <h3 class="text-white">Purushothaman TR.</h3>
                        <h5 class="text-white" id="user_email">purushothaman.tr@yashussunlimited.com</h5>
                    </div>
                </div>
                <div class="col-2"></div>
                <div class="col-2 d-flex align-items-center">
                    <button class="btn-yu primary media_btn" id="edit" data-toggle="modal" title="Add Employee" data-target=".datamodal"><i class="la la-edit"></i></button>
                </div>
            </div>
            <div class="card pb-1" style="border-radius:10px">

                <div class="profile">

                    <div class="row ml-5 mr-1">
                        <div class="col-md-6 mt-2 col-sm-6 col-xs-12">
                            <label style="font-size:16px" id="a" class="mb-0">Full Name</label>
                            <h4 style="font-size:15px" id="p_name">Purushothaman TR.</h4>
                        </div>

                        <div class="col-md-6 mt-2 col-sm-6 col-xs-12">
                            <label style="font-size:16px" class="mb-0">Email Address</label>
                            <h4 style="font-size:15px" id="email-address">purushothaman.tr@yashussunlimited.com</h4>
                        </div>
                        <div class="col-md-6 mt-2 col-sm-6 col-xs-12">
                            <label style="font-size:16px" class="mb-0">Contact</label>
                            <h4 style="font-size:15px" id="contact">+91 1234 567890</h4>
                        </div>
                        <div class="col-md-6 mt-2 col-sm-6">
                            <label style="font-size:16px" class="mb-0">Date of Birthday</label>
                            <h4 style="font-size:15px" id="dob">29/1/1980</h4>
                        </div>
                        <div class="col-md-6 mt-2 col-sm-6">
                            <label style="font-size:16px" class="mb-0">Division</label>
                            <h4 style="font-size:15px" id="division">Management</h4>
                        </div>
                        <div class="col-md-6 mt-2 col-sm-6">
                            <label style="font-size:16px" class="mb-0">Department</label>
                            <h4 style="font-size:15px" id="dept">Admin</h4>
                        </div>
                        <div class="col-md-6 mt-2 col-sm-6">
                            <label style="font-size:16px" class="mb-0">Role</label>
                            <h4 style="font-size:15px" id="role">CEO</h4>
                        </div>
                        <div class="col-md-6 mt-2 col-sm-6">
                            <label style="font-size:16px" class="mb-0">Reporting Manager</label>
                            <h4 style="font-size:15px" id="r-manager">-</h4>
                        </div>
                        <div class="col-md-6 mt-2 col-sm-6">
                            <label style="font-size:16px" class="mb-0">Address</label>
                            <h4 style="font-size:15px" id="add">24 - ABCD Charkop Industrial Area, Belimo Road</h4>
                        </div>
                        <div class="col-md-6 mt-2 col-sm-6">
                            <label style="font-size:16px" class="mb-0">Location</label>
                            <h4 style="font-size:15px" id="location">Kandivali</h4>
                        </div>
                        <div class="col-md-6 mt-2 col-sm-6">
                            <label style="font-size:16px" class="mb-0">City</label>
                            <h4 style="font-size:15px" id="city">Mumbai</h4>
                        </div>
                        <div class="col-md-6 mt-2 col-sm-6">
                            <label style="font-size:16px" class="mb-0">State</label>
                            <h4 style="font-size:15px" id="state">Maharashtra</h4>
                        </div>
                        <div class="col-md-6 mt-2 col-sm-6">
                            <label style="font-size:16px" class="mb-0">Pin Code</label>
                            <h4 style="font-size:15px" id="pin-code">400067</h4>
                        </div>
                        <div class="col-md-6 mt-2 col-sm-6">
                            <label style="font-size:16px" class="mb-0">Country</label>
                            <h4 style="font-size:15px" id="country">Indian</h4>
                        </div>
                        <div class="col-md-6 mt-2 col-sm-6">
                            <label style="font-size:16px" class="mb-0">Language</label>
                            <h4 style="font-size:15px" id="lang">English</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade text-left datamodal" id="xlarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel16">Edit Profile</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="name" style="margin-top:10px">Name</label>
                                    </div>
                                    <input type="text" class="form-control col-8" name="" id="name_field">
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-4">
                                        <label style="margin-top:10px" for="email_field">Email ID</label>
                                    </div>
                                    <input type="text" class="form-control col-8" name="" id="email_field">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="dob_field" style="margin-top:10px">Date of Birth</label>
                                    </div>
                                    <input type="date" class="form-control col-8" name="" id="dob_field">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="contact_field" style="margin-top:10px">Contact</label>
                                    </div>
                                    <input type="text" class="form-control col-8" name="" id="contact_field">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="dob_field" style="margin-top:10px">Division</label>
                                    </div>
                                    <input type="text" class="form-control col-8" name="" id="division_field">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="contact_field" style="margin-top:10px">Department</label>
                                    </div>
                                    <input type="text" class="form-control col-8" name="" id="dept_field">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row ">
                                    <div class="col-4">
                                        <label for="dob_field" style="margin-top:10px">Role</label>
                                    </div>
                                    <input type="text" class="form-control col-8" name="" id="role_field">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row ">
                                    <div class="col-4">
                                        <label for="contact_field" style="margin-top:10px">Reporting Manager</label>
                                    </div>
                                    <input type="text" class="form-control col-8" name="" id="r_manager_field">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12 ">
                                <div class="row ">
                                    <div class="col-4">
                                        <label for="dob_field" style="margin-top:10px">Address</label>
                                    </div>
                                    <textarea type="text" class="form-control col-8" name="" id="add_field" rowspan=10></textarea>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="contact_field" style="margin-top:10px">Location</label>
                                    </div>
                                    <input type="text" class="form-control col-8" name="" id="location_field">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row ">
                                    <div class="col-4">
                                        <label for="dob_field" style="margin-top:10px">City</label>
                                    </div>
                                    <input type="text" class="form-control col-8" name="" id="city_field">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row ">
                                    <div class="col-4">
                                        <label for="contact_field" style="margin-top:10px">State</label>
                                    </div>
                                    <input type="text" class="form-control col-8" name="" id="state_field">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row ">
                                    <div class="col-4">
                                        <label for="dob_field" style="margin-top:10px">Pin Code</label>
                                    </div>
                                    <input type="text" class="form-control col-8" name="" id="pin_field">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row ">
                                    <div class="col-4">
                                        <label for="contact_field" style="margin-top:10px">Country</label>
                                    </div>
                                    <input type="text" class="form-control col-8" name="" id="country_field">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row ">
                                    <div class="col-4">
                                        <label for="language_field" style="margin-top:10px">Language</label>
                                    </div>
                                    <input type="text" class="form-control col-8" name="" id="language_field">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-yu btn-width primary text-white" data-dismiss="modal">Save</button>
                    <button type="button" class="btn-yu btn-width primary text-white mr-2" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('footer') ?>
    <?php $this->load->view('authentication_js') ?>
    <script>
        if ($(window).width() < 550) {
            var emailArray = [];
            var email = $('#user_email').text();
            // alert(email.length)
            for (i = 0; i < email.length; i++) {
                emailArray[i] = email.charAt([i]);
            }
            $('#user_email').text(emailArray.splice(0, 28).join("").toString());

        }
    </script>