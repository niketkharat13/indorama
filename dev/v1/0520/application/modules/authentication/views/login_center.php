<html>
    <head>
    <link rel="icon" href="https://karmamgmt.com/wp-content/themes/karma-theme/images/fav.png" type="image/png" sizes="16x16">
        <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="icon" type="image/png" href="./assets/img/favicon.ico" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/ui-standard.css">
        <title>
            WeChecked
        </title>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no" name="viewport" />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.iconmonstr.com/1.3.0/css/iconmonstr-iconic-font.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
        <link href="./assets/css/material-kit.css?v=2.0.6" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="./assets/demo/demo.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="<?=base_url()?>app-assets/fonts/feather/style.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/ui-standard.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css">
        <style>
            
            .parent-div {
                width: 100%;
                height: 100vh;
                display:flex;
                justify-content:center;
                background-image: url(http://localhost/nf/basetemplate/dev/v1/1219/assets/images/login_bg.jpg);
                background-size:cover;
            
        }
        .digit-group input {
            width: 30px;
            height: 30px;
            background-color: #c5c5c5;
            border: none;
            line-height: 50px;
            text-align: center;
            font-size: x-large;
            font-weight: 200;
            color: white;
            /* margin: 0 2px; */
            background: none;
            outline: none;
            border-bottom: 2px solid #fff;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            /* margin-bottom: 0px; */
            padding-bottom: 0px;
        }

        .digit-group .splitter {
            padding: 0 5px;
            color: white;
            font-size: 24px;
        }
        .prompt {
            margin-bottom: 20px;
            font-size: 20px;
            color: #fff;
        }   
        .m-link {
            color: #fff;
        }
        .text {
            margin-top: -9px;
            font-size: 14px;
            margin-left: 22px;
        }
        </style>
    </head>
    <body style="">
    <div class="parent-div">
        <div class="">
       
            <div class="">
                <div class="row mt-0">
                    <div class="col-lg-4 col-md-6">
                        <div class="card card-login-dark " style="margin-top:160px !important;">
                            <div class=" card-header-primary text-center card-header-darkk card-body-dark">
                                <img id="yu-logo" src="http://simplifyhr.com/vcheck/assets/demo/media/img/logo/logo.png" style="width: 180px;" />
                            </div>
                            <div class="card-body mt-5">
                                <div id="box">
                                    <div class="prompt">
                                        <h2>
                                            <p class="login-para-dark" style="">Login</p>
                                        </h2>
                                    </div>
                                    <form method="get" data-group-name="digits" data-autosubmit="false" autocomplete="off">
                                        <div id="div1">
                                            <div class="row-yu">
                                                <i class="fa fa-user cust cust-dark"></i><input type="text" id="email_id" name="email_id" class="form-control username-style" style="" placeholder="Username" />
                                            </div>
                                            <div id="msg">
                                                <p class="text-left text" id="text"></p>
                                            </div>
                                        </div>
                                        <div class="row-yu">
                                            <i class="fa fa-lock cust-dark"></i><input type="password" id="password" name="password" class="form-control password-style" style="" placeholder="Password" />
                                        </div>
                                        <div class="input-group-prepend mt-4 login-div" style="">
                                            <input type="button" value="Log in" id="link" class="btn login-button-dark">
                                        </div>
                                    </form>
                                    <div class="input-group mt-2 forgot-div">
                                        <p class="forgot-password-dark" style="cursor:pointer" id="fp">Forgot Credential ? </p>
                                    </div>
                                </div>
                                <div id="otp">
                                    <div class="prompt ">

                                        <p style="font-size:14px;" class="otp-para-dark dark-text text-center" style="font-size: 14px;">OTP Sent</p>
                                        <div style="margin-left: 2.83rem;">
                                            <p style="font-size:14px;">We have just send you the verification code</p>
                                            <p style="font-size:14px;">The OTP will expire in <b><span id="timer"></span>
                                                </b></p>
                                        </div>

                                    </div>
                                    <form method="get" class="digit-group" data-group-name="digits" data-autosubmit="false" autocomplete="off" style="margin-left: -3px">
                                        <input type="text" style="margin-left:11px" id="digit-1" name="digit-1" data-next="digit-2" />
                                        <input type="text" id="digit-2" name="digit-2" data-next="digit-3" data-previous="digit-1" />
                                        <input type="text" id="digit-3" name="digit-3" data-next="digit-4" data-previous="digit-2" />
                                        <span class="splitter">&ndash;</span>
                                        <input type="text" id="digit-4" name="digit-4" data-next="digit-5" data-previous="digit-3" />
                                        <input type="text" id="digit-5" name="digit-5" data-next="digit-6" data-previous="digit-4" />
                                        <input type="text" id="digit-6" name="digit-6" data-previous="digit-5" onkeyup="validate()" style="margin-left:-2px ">
                                    </form>
                                    <div class="text-center" style="color:#fff; margin-top: 10px">
                                        <p style="margin-top:0;cursor:pointer " id="resend">Resend OTP?</p>
                                        <button class="btn login-button-dark" id="cancel_btn" style="margin-top:-10px; "> Cancel</button>
                                    </div>
                                </div>
                                <div id="forget_pwd">
                                    <div class="prompt ">

                                        <p style="font-size:20px;" class="otp-para-dark dark-text text-center">Forgot Password</p>
                                        <div>
                                            <p style="font-size:14px;margin-left:22px" id="forgot_username_link">Enter Your Username</p>
                                        </div>

                                    </div>
                                    <div class="row-yu">
                                        <i class="fa fa-user cust cust-dark"></i><input type="text" id="email_id" name="email_id" class="form-control username-style" style="" placeholder="Username" />
                                    </div>
                                    <div class="input-group-prepend mt-4 login-div" style="">
                                        <input type="button" value="Submit" id="forget-submit-btn" class="btn login-button-dark">
                                    </div>
                                    
                                    <div class="input-group mt-2 forgot-div">
                                        <p class="forgot-password-dark" style="cursor:pointer" id="fuser">Forgot Username ? </p>
                                    </div>
                                    <div class="input-group mt-2 forgot-div">
                                    <i class="ft-arrow-left mr-1" style="font-size:15px;margin-top:5px;color:#fff"></i>
                                        <p class="forgot-password-dark" style="cursor:pointer" id="log1">Back to Login</p>
                                    </div>

                                </div>

                                <div id="password_recovery">
                                    <p style="font-size:20px" class="text-white">Password Recovery</p>
                                    <p style="font-size:14px" class="text-white mb-0">We have Send Instruction in Your Registerd Email ID</p>
                                    <div class="input-group-prepend mt-1 login-div" style="">
                                    <a href="<?=base_url()?>authentication/authenticate/reset_password" style="font-size:14px;text-decoration:underline" class="text-white">Open in Gmail</a>
                                    </div>
                                    <div class="input-group-prepend mt-4 login-div" style="">
                                        <input type="button" value="Back to Login" id="backtologin" class="btn login-button-dark">
                                    </div>
                                </div>
                                <div id="forgot_username">
                                    <p style="font-size:20px" class="text-white text-center">Forgot Username</p>
                                    <p style="font-size:14px;" class="text-white mb-0">Send Recovery Options Via</p>
                                    <div class="d-flex">
                                        <div class="radio ml-0">
                                            <input id="email-option" name="radio" type="radio" >
                                            <label for="email-option" class="radio-label text-white">Email ID</label>
                                        </div>
                                        <div class="radio ml-0">
                                            <input id="mobile-option" name="radio" type="radio">
                                            <label for="mobile-option" class="radio-label text-white">Mobile #</label>
                                        </div>
                                    </div>
                                    <div id="mobile">
                                        <p style="font-size:14px;margin-left:26px" class="text-white">Enter Your Registered Mobile #</p>
                                        <div class="row-yu">
                                            <i class="ft-phone cust-dark"></i>
                                            <input type="text" id="contact" name="contact" class="form-control password-style" style="" placeholder="Contact #" />
                                        </div>
                                    </div>
                                    <div id="email">
                                        <p style="font-size:14px;margin-left:26px" class="text-white">Enter Your Registered Email ID</p>
                                        <div class="row-yu">
                                            <i class="ft-mail cust-dark"></i>
                                            <input type="text" id="contact" name="contact" class="form-control password-style" style="" placeholder="Email ID" />
                                        </div>
                                    </div>
                                    <div class="input-group-prepend mt-4 login-div" style="">
                                        <input type="button" value="Submit" id="username-submit" class="btn login-button-dark">
                                    </div>
                                    <div class="input-group mt-2 forgot-div">
                                    <i class="ft-arrow-left mr-1" style="font-size:15px;margin-top:5px;color:#fff"></i>
                                        <p class="forgot-password-dark" style="cursor:pointer;" id="log2">Back to Login</p>
                                    </div>
                                </div>
                                <div id="username_recovery">
                                    <p style="font-size:20px" class="text-white">Account Recovery</p>
                                    <p id="mail_msg" style="font-size:14px" class="text-white">We have Send Mail to Your Registerd Email ID</p>
                                    <p id="phone_msg" style="font-size:14px" class="text-white">We have Send SMS to Your Registerd Mobile #</p>
                                    <div class="input-group-prepend mt-4 login-div" style="">
                                        <input type="button" value="Back to Login" id="backtologin2" class="btn login-button-dark">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src=" assets/js/core/jquery.min.js" type="text/javascript"></script>
    <script src=" assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src=" assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
    <script src=" assets/js/plugins/moment.min.js"></script>
    <!-- Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
    <script src=" assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src=" assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
    <!--  Google Maps Plugin    -->
    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
    <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
    <script src=" assets/js/material-kit.js?v=2.0.6" type="text/javascript"></script>
    <script src="<?= base_url() ?>app-assets/js/custom-ui-standard.js" language="javascript"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
    <script>
         $('#email').hide();
        $('#mobile').hide();
        $('#phone_msg').hide();
        $('#mail_msg').hide();
        $('#email-option').click(function(){
            $('#email').show();
            $('#mobile').hide();
            $('#phone_msg').hide();
            $('#mail_msg').show();
        });
        $('#mobile-option').click(function(){
            $('#mobile').show();
            $('#email').hide();
            $('#mail_msg').hide();
            $('#phone_msg').show();
        });
        $('#otp').hide();
        $('#forgot_username').hide();
        $('#username_recovery').hide();

        $('#link').click(function() {
            if ($('#email_id').val() == "purushothaman.tr" && $("#password").val() == "We1c0me@123") {
                $('#text').text('');
                $('#box').hide(1000);
                $('#otp').show(500);
                otptimer();
            }
        });
        $('#cancel_btn').click(function() {
            $('#otp').hide(1000);
            $('#box').show(500);
        });
        $('#log1').click(function(){
            $('#forget_pwd').hide(1000);
            $('#box').show(500)
        });
        $('#log2').click(function(){
            $('#forgot_username').hide(1000);
            $('#box').show(500)
        });
        $('#fuser').click(function() {
            $('#forget_pwd').hide(1000);
            $('#forgot_username').show(500);
        });
        $('#username-submit').click(function() {
            $('#forgot_username').hide(1000);
            $('#username_recovery').show(500);
        });
        $('#forget_pwd').hide();
        $('#password_recovery').hide();
        $('#forget-submit-btn').click(function() {
            $('#forget_pwd').hide(1000);
            $('#password_recovery').show(500);
        });
        $('#fp').click(function() {
            $('#box').hide(1000);
            $('#forget_pwd').show(500)
        });
        $('#backtologin').click(function() {
            $('#password_recovery').hide(1000);
            $('#box').show(500);
        });
        $('#backtologin2').click(function() {

            $('#username_recovery').hide(1000);
            $('#box').show(500);
        });
        // $('#password').keydown(function(){
        //     var pass= $('#password').attr('type', 'text');
        //     if ($('#email_id').val() == "" && $('pass').val() == "") {
                
        //         $('#link').css('cursor','not-allowed');
        //    } else if ($('#email_id').val() !== "purushothaman.tr" && $('pass').val() !== "We1c0me@123") {
        //        alert($('pass').val());
        //     $('#link').css('cursor','not-allowed');
        //    }
        // })
        
        $('#link').click(function() {
            if ($('#email_id').val() == "" && $('#password').val() == "") {
               
                $('#text').text('Please Enter Fields');
            } else if ($('#email_id').val() !== "purushothaman.tr" && $('#password').val() !== "We1c0me@123") {
                $('#text').text('Please Enter Valid Username');
            }
        })
        $("#resend").click(function() {
            otptimer();
        })
        function otptimer() {
    //alert(1);
        document.getElementById('timer').innerHTML =
            03 + ":" + 00;
        startTimer();
    }   
function startTimer() {
    var presentTime = document.getElementById('timer').innerHTML;
    var timeArray = presentTime.split(/[:]+/);
    var m = timeArray[0];
    var s = checkSecond((timeArray[1] - 1));
    if (s == 59) { m = m - 1 }
    //if(m<0){alert('timer completed')}
    if (m == 0 && s == 0) {
        document.getElementById('timer').innerHTML = "";
        otpblink();
        return;
    }
    document.getElementById('timer').innerHTML =
        m + ":" + s;
    setTimeout(startTimer, 1000);
}function otpblink() {
    $("#timer").append(" <blink>0:00</blink>");
}
function checkSecond(sec) {
    if (sec < 10 && sec >= 0) { sec = "0" + sec }; // add zero in front of numbers < 10
    if (sec < 0) { sec = "59" };
    return sec;
}

        $(document).keypress(function(event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                if ($("#box").is(":visible")) {
                    $('#link').click();
                    
                } 
                else if($("#forget_pwd").is(":visible")){
                    $('#forget-submit-btn').click();
                }
                else if($("#password_recovery").is(":visible")){
                    $('#backtologin').click();
                }
                else if($("#forgot_username").is(":visible")){
                    $('username-submit').click();
                }
                else if($("#username_recovery").is(":visible")){
                    $('#backtologin2').click();
                }
                 
                $('#link').click();

                $("#digit-1").focus();
            }
        });
        if($("#otp").is(":visible")){
                    
        }
        $("#digit-6").on("keyup", function(e) {

            if (e.key != "Backspace") {

                var otp = $("#digit-1").val() + $("#digit-2").val() + $("#digit-3").val() + $("#digit-4").val() + $("#digit-5").val() + $("#digit-6").val();

                if (otp == '123456') {
                    window.location.href = "<?=base_url()?>dashboard/Dashboard_controller/dashboard_view";
                    $.alert({
                        title: 'LOGIN',
                        content: 'successfully',
                        type:'green',
                    });
                    confrim("Login successfully");

                } else {
                    $.alert({
                        title: '',
                        content: 'WRONG OTP!!',
                        type:'red',
                    });
                }
            }
        });
    </script>
    </body>
</html>