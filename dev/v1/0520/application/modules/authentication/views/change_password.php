<?php $this->load->view('header'); ?>
<?php $this->load->view('top'); ?>
<?php $this->load->view('vertical_navbar'); ?>
<?php $this->load->view('authentication_css') ?>
<div class="">
    <div class="col">
        <div class="container">
            <div class="card p-4" style="margin-top:2.5rem">
                <h4 style="font-size:18px">Change Password</h4>
                <div class="row  mt-5 ">
                    <div class="col-md-3 col-sm-12">
                        <h4 style="font-size:15px;margin-top:11px">Old Password</h4>
                    </div>
                    <div class="col-md-3 col-sm-12 pl-0 custom-input-div">
                        <input class="form-control" id="old_pwd" style="border:none" type="password" placeholder="Old Password"><i class="ft-eye-off" id="show_old_password_btn"></i>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-3 col-sm-12">
                        <h4 style="font-size:15px;margin-top:11px">New Password</h4>
                    </div>
                    <div class="col-md-3 col-sm-12 pl-0 custom-input-div password-field ">
                        <input class="form-control" id="change_pwd" style="border:none" type="password" placeholder="New Password">
                        <i class="ft-eye-off" id="show_new_password_btn"></i>
                        <div id="pwd_strength_wrap">
                            <div id="passwordDescription">Password not entered</div>
                            <div id="passwordStrength" class="strength0"></div>
                            <div id="pswd_info">
                                <strong>Strong Password Tips:</strong>
                                <ul>
                                    <li class="invalid" id="length">At least 6 characters</li>
                                    <li class="invalid" id="pnum">At least one number</li>
                                    <li class="invalid" id="capital">At least one lowercase &amp; one uppercase letter</li>
                                    <li class="invalid" id="spchar">At least one special character</li>
                                </ul>
                            </div><!-- END pwd_strength_wrap -->
                        </div>
                    </div>

                </div>
                <div class="row margin" id="dynamic">
                    <div class="col-md-3 col-sm-12">
                        <h4 style="font-size:15px;margin-top:11px">Confirm Password</h4>
                    </div>
                    <div class="col-md-3 col-sm-12 pl-0 custom-input-div">
                        <input disabled class="form-control disabled_input" id="cnfm_pwd" style="border:none" type="password" placeholder="Confirm Password"><i class="ft-eye-off" id="show_confirm_password_btn"></i>
                    </div>
                </div>
                <div class="row" id="">
                    <div class="col-md-3 col-sm-12"></div>
                    <div class="col-md-3 col-sm-12 pl-0">
                        <p id="change_compare_msg" class="compare-msg"></p>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-3"></div>
                    <div class="col-4">
                        <button class="btn-yu primary disabled_input" style="width:150px;margin-left:44px" onclick="changePassword()" id="change_password_btn" disabled>Change Password</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('authentication_js') ?>
<?php $this->load->view('footer') ?>