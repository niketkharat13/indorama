  </div>
  </div>
  </div>
  </div>

  <style type="text/css">
    /* dropdown multiselect styles` */
    .select2-container--classic .select2-selection--multiple .select2-selection__choice,
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
      margin-top: 0 !important;
      background-color: none !important;
      border-color: #512E90 !important;
      color: #FFFFFF;
      margin-right: 5px !important;
      background: none !important;
      color: black !important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
      color: #999 !important;
      cursor: pointer;
      display: inline-block;
      font-weight: bold;
    }

    .select2-container--classic .select2-results__options .select2-results__option[aria-selected=true],
    .select2-container--default .select2-results__options .select2-results__option[aria-selected=true] {
      background-color: #003b7e !important;
      color: #FFFFFF !important;
    }



    .select2-container--classic,
    .select2-container--default {
      width: 90% !important;
    }





    #floating-button {
      width: 55px;
      height: 55px;
      border-radius: 50%;
      background: #db4437;
      position: fixed;
      bottom: 60px;
      right: 30px;
      cursor: pointer;
      box-shadow: 0px 2px 5px #666;
    }

    #floating-button img {
      max-width: 100% !important;
      height: auto !important;
    }

    .plus {
      color: white;
      position: absolute;
      top: 0;
      display: block;
      bottom: 0;
      left: 0;
      right: 0;
      text-align: center;
      padding: 0;
      margin: 0;
      line-height: 55px;
      font-size: 38px;
      font-weight: 300;
      animation: plus-out 0.3s;
      transition: all 0.3s;
    }

    #container-floating {
      position: fixed;
      width: 70px;
      height: 70px;
      bottom: 30px;
      right: 30px;
      z-index: 127;
    }

    #container-floating:hover {
      height: 400px;
      width: 90px;
      padding: 30px;
    }

    #container-floating:hover .plus {
      animation: plus-in 0.15s linear;
      animation-fill-mode: forwards;
    }

    .edit {
      position: absolute;
      top: 0;
      display: block;
      bottom: 0;
      left: 0;
      display: block;
      right: 0;
      padding: 0;
      opacity: 0;
      margin: auto;
      line-height: 65px;
      transform: rotateZ(-70deg);
      transition: all 0.3s;
      animation: edit-out 0.3s;
    }

    #container-floating:hover .edit {
      animation: edit-in 0.2s;
      animation-delay: 0.1s;
      animation-fill-mode: forwards;
    }

    @keyframes edit-in {
      from {
        opacity: 0;
        transform: rotateZ(-70deg);
      }

      to {
        opacity: 1;
        transform: rotateZ(0deg);
      }
    }

    @keyframes edit-out {
      from {
        opacity: 1;
        transform: rotateZ(0deg);
      }

      to {
        opacity: 0;
        transform: rotateZ(-70deg);
      }
    }

    @keyframes plus-in {
      from {
        opacity: 1;
        transform: rotateZ(0deg);
      }

      to {
        opacity: 0;
        transform: rotateZ(180deg);
      }
    }

    @keyframes plus-out {
      from {
        opacity: 0;
        transform: rotateZ(180deg);
      }

      to {
        opacity: 1;
        transform: rotateZ(0deg);
      }
    }

    .nds {
      width: 40px;
      height: 40px;
      border-radius: 50%;
      position: fixed;
      z-index: 300;
      transform: scale(0);
      cursor: pointer;
    }

    .nd1 {
      background: #ffffff;
      right: 40px;
      bottom: 120px;
      animation-delay: 0.2s;
      animation: bounce-out-nds 0.3s linear;
      animation-fill-mode: forwards;
    }

    .nd3 {
      background: #3c80f6;
      right: 40px;
      bottom: 180px;
      animation-delay: 0.15s;
      animation: bounce-out-nds 0.15s linear;
      animation-fill-mode: forwards;
    }

    .nd4 {
      background: #ba68c8;
      right: 40px;
      bottom: 240px;
      animation-delay: 0.1s;
      animation: bounce-out-nds 0.1s linear;
      animation-fill-mode: forwards;
    }

    .nd5 {
      background-image: url('https://lh3.googleusercontent.com/-X-aQXHatDQY/Uy86XLOyEdI/AAAAAAAAAF0/TBEZvkCnLVE/w140-h140-p/fb3a11ae-1fb4-4c31-b2b9-bf0cfa835c27');
      background-size: 100%;
      right: 40px;
      bottom: 300px;
      animation-delay: 0.08s;
      animation: bounce-out-nds 0.1s linear;
      animation-fill-mode: forwards;
    }

    @keyframes bounce-nds {
      from {
        opacity: 0;
      }

      to {
        opacity: 1;
        transform: scale(1);
      }
    }

    @keyframes bounce-out-nds {
      from {
        opacity: 1;
        transform: scale(1);
      }

      to {
        opacity: 0;
        transform: scale(0);
      }
    }

    #container-floating:hover .nds {

      animation: bounce-nds 0.1s linear;
      animation-fill-mode: forwards;
    }

    #container-floating:hover .nd3 {
      animation-delay: 0.08s;
    }

    #container-floating:hover .nd4 {
      animation-delay: 0.15s;
    }

    #container-floating:hover .nd5 {
      animation-delay: 0.2s;
    }

    .letter {
      font-size: 23px;
      font-family: 'Roboto';
      color: white;
      position: absolute;
      left: 0;
      right: 0;
      margin: 0;
      top: 0;
      bottom: 0;
      text-align: center;
      line-height: 40px;
    }

    .reminder {
      position: absolute;
      left: 0;
      right: 0;
      margin: auto;
      top: 0;
      bottom: 0;
      line-height: 40px;
    }

    .profile_palli {
      border-radius: 50%;
      width: 40px;
      position: absolute;
      top: 0;
      bottom: 0;
      margin: auto;
      right: 20px;
    }
  </style>

  <style type="text/css">
    .ajax-file-upload-error {
      display: none !important;
      width: 65px;
    }

    .ajax-upload-dragdrop {
      border: 1px solid #00517cf5;
      width: 250px !important;
      color: #DADCE3;
      text-align: left;
      vertical-align: middle;
      padding: 10px 10px 0px 10px;
      border-radius: 6px;
    }

    .ajax-file-upload {
      background: #00517cf5 !important;
    }

    /* .ajax-file-upload-statusbar input {} */

    /* .extrahtml input,
    .extrahtml select */

    .document_master_select,
    .state_selection_footer_upload {
      display: block;
      width: 200px;
      height: -webkit-calc(1.25em + 1.5rem + 2px);
      height: -moz-calc(1.25em + 1.5rem + 2px);
      height: calc(1.25em + 1.5rem + 2px);
      padding: 0.75rem 1rem;
      font-size: 1rem;
      font-weight: 400;
      line-height: 1.25;
      color: #4E5154;
      background-color: #FFFFFF;
      background-clip: padding-box;
      border: 1px solid #BABFC7;
      border-radius: 0.25rem;
      -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
      transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
      -o-transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
      -moz-transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
      transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
      transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
    }

    .extrahtml textarea {
      width: 106%;
      margin-left: -15px;
      border-radius: 0.25rem;
    }

    .multi_upload_select_Site {
      width: 90%;
      border-radius: 6px;
      height: 50px;

    }

    .ajax-file-upload-container {
      margin: 20px 0px 20px 0px;
      width: 262px;
    }



    .ajax-file-upload-filename {
      width: auto;
    }

    @media(max-width:1400px) {
      .ajax-file-upload-statusbar {
        width: 560px !important;
        margin-left: 16px;

      }
    }

    @media(max-width:991px) {
      .ajax-file-upload-statusbar {
        width: 101% !important;

      }
    }

    .frequency_footer {
      margin-left: 14px;
    }

    .comment-position {
      position: absolute;
      top: 20%;
      left: 19%;
      font-size: 169% !important;
      color: white;
    }

    /* new  */
    .ajax-file-upload-progress {
      display: none !important;
    }

    .info-1 {
      color: #00517cf5;
      cursor: pointer;
    }

    .preview_text {
      margin-top: 10px;
      float: right;
      display: none;
      cursor: pointer;
    }

    .color-style {
      background-color: yellowgreen;
      border-radius: 50%;
    }
  </style>



  <div id="container-floating">

    <div class="nd1 nds" data-toggle="tooltip" data-placement="left" data-original-title="Request"><img class="reminder">
      <a class="nav_links" data-iframe="http://13.234.152.155/Forms/Home/StartProcess#/processes" data-iframe-title="Request">
        <i class="letter">
          <img src="<?= base_url() ?>app-assets/images/request.png" style="width: 24px; margin-top: -5px;">
        </i>
      </a>
    </div>

    <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Quick Actions">
      <p class="plus">+</p>
      <img class="edit" src="<?= base_url() ?>app-assets/images/float_button_edit.png">
    </div>
  </div>
  <script>
    function hide_img_div(uploader_class) {
      $("." + uploader_class).hide(2000);

      $.confirm({
        title: 'Replace!',
        type: 'orange',
        content: 'Do you want to replace existing file',
        buttons: {
          text: 'Done!',
          btnClass: 'btn-yu',
          update: function() {

            $.alert({
              title: '',
              content: '<div class="container"><div class="row"><div class="col-lg-4 col-md-4"><h4 class="text-center">Total Files</h4></div><div class="col-lg-4 col-md-4"><h4 class="text-success text-center">Uploaded</h4></div><div class="col-lg-4 col-md-4"><h4 class="text-danger text-center">Failed</h4></div></div><div class="row"><div class="col-lg-4 col-md-4"><h4 class="text-center count_fount">10</h4></div><div class="col-lg-4 col-md-4"><h4 class="text-success text-center count_fount">7</h4></div><div class="col-lg-4 col-md-4"><h4 class="text-danger text-center count_fount">3</h4></div></div></div>',
              type: 'green',
              buttons: {
                continue: {
                  text: 'Continue',
                  action: function() {
                    $.alert('File Uploaded Successfully');
                    start_upload();
                  }
                }
              }
            });

          },
          cancel: function() {
            $('#document_upload_modal').modal('show');
          }
        }
      });
    }

    function success_count_modal_show() {
      $('#success_count_modal').modal('show');
    }

    function start_upload() {
      window.location.href = "<?= base_url() ?>document/Document_controller/request_list?upload_confirmation=yes"
    }
  </script>

  <script>
    function handleUpdate(color, color_nav, icon_color, icon_nav, active_color) {
      localStorage.setItem('baseColor', color);
      localStorage.setItem('navColor', color_nav);
      localStorage.setItem('iconColor', icon_color);
      localStorage.setItem('inconColorNav', icon_nav);
      localStorage.setItem('activeColor', active_color);
      location.reload();
    }

    (function() {
      var baseC = localStorage.getItem('baseColor');
      document.documentElement.style.setProperty('--baseColor', baseC);
      var navC = localStorage.getItem('navColor');
      document.documentElement.style.setProperty('--navColor', navC);
      var iconC = localStorage.getItem('iconColor');
      document.documentElement.style.setProperty('--iconColor', iconC);
      var iconC_nav = localStorage.getItem('inconColorNav');
      document.documentElement.style.setProperty('--iconColorNav', iconC_nav);
      var active_Col = localStorage.getItem('activeColor');
      document.documentElement.style.setProperty('--activeColor', active_Col);
    })();
    window.onbeforeunload = function() {
      localStorage.removeItem(key);
      return '';
    };

    function check_frequency_footer() {
      var frequency_selected = $('input[name="frequency_footer"]:checked').val();
      if (frequency_selected == "annually") {
        $("#month_selection_footer").prop("disabled", true);
        $("#month_selection_footer").show();
        $("#half_year_selection_footer").hide();
        $("#quartly_selection_footer").hide();
      } else if (frequency_selected == "monthly") {
        $("#month_selection_footer").show();
        $("#month_selection_footer").prop("disabled", false);
        $("#half_year_selection_footer").hide();
        $("#quartly_selection_footer").hide();
      } else if (frequency_selected == "yearly") {

        $("#month_selection_footer").hide();
        $("#half_year_selection_footer").show();
        $("#quartly_selection_footer").hide();
      } else if (frequency_selected == "quartely") {
        $("#month_selection_footer").hide();
        $("#half_year_selection_footer").hide();
        $("#quartly_selection_footer").show()
      }
    }
  </script>



  <footer class="footer footer-static footer-light navbar-border">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2 ml-2"><span class="float-md-left d-block d-md-inline-block footer-test-size footer-color">Design & Developed by<a class="text-bold-800 grey darken-2" href="https://yashussunlimited.com/" target="_blank"> - Solution Doctors</a></span></p>
  </footer>

  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url() ?>app-assets/js/jquery.min.js"></script>
  <script src="<?= base_url() ?>app-assets/js/bootstrap.min.js"></script>
  <script src="<?= base_url() ?>app-assets/js/plugins/popper_js/popper.min.js"></script>
  <script src="<?= base_url() ?>app-assets/js/jquery-ui.js"></script>

  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/plugins/toastr_js/toastr.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/tweenmax.min.js"></script>

  <script type="text/javascript" src="<?= base_url() ?>app-assets/fonts/font-awesome/css/font-awesome.min.css"></script>
  <!-- BEGIN: Vendor JS-->
  <script type="text/javascript" src="<?= base_url() ?>app-assets/vendors/js/vendors.min.js"></script>
  <!-- BEGIN Vendor JS-->
  <!-- BEGIN: Page Vendor JS-->
  <script type="text/javascript" src="<?= base_url() ?>app-assets/vendors/js/charts/chartist.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js"></script>
  <!-- END: Page Vendor JS-->
  <!-- BEGIN: Theme JS-->
  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/core/app-menu.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/core/app.js"></script>
  <!-- END: Theme JS-->
  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/scripts/datatables/datatables.min.js"></script>

  <script type="text/javascript" src="<?= base_url() ?>app-assets/vendors/js/pickers/pickadate/picker.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/vendors/js/tables/jszip.min.js"></script>

  <!-- dashboard js -->
  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/dashboard.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>/app-assets/js/scripts/charts/c3/data/category-data.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>/app-assets/js/scripts/charts/c3/transform/to-bar.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>/app-assets/js/scripts/charts/c3/other/timeseries.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>/app-assets/js/scripts/charts/c3/bar-pie/pie.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>/app-assets/vendors/js/charts/d3.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>/app-assets/vendors/js/charts/c3.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>/app-assets/js/scripts/charts/c3/bar-pie/donut.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>/app-assets/js/loader.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>/app-assets/js/scripts/navs/navs.js"></script>
  <script src="<?= base_url() ?>app-assets/js/scripts/charts/c3/data/data-color.js"></script>


  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/jQuery.resizableColumns.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/jQuery.resizableColumns.min.js"></script>

  <script type="text/javascript" src='<?= base_url() ?>app-assets/fonts/font-awesome/js/a076d05399.js'></script>
  <!--DropZone-->
  <script src="<?= base_url() ?>app-assets/js/dropzone.js"></script>


  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/plugins/confirm_js/jquery-confirm.min.js"></script>

  <!-- imp for file upload to work------------------------------------ -->
  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/scripts/forms/custom-file-input.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/plugins/custom_file_uploader/jquery.uploadfile.min.js"></script>

  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/colResizable-1.6.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/colResizable-1.6.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/scripts/datatables/dataTables.fixedColumns.min.js"></script>


  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/forms/selects/select2.min.css">
  <script src="<?= base_url() ?>app-assets/vendors/js/forms/select/select2.full.min.js"></script>
  <script src="<?= base_url() ?>app-assets/js/scripts/forms/select/form-select2.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/plugins/jquery.multiselect.js"></script>

  <script type="text/javascript" src="<?= base_url() ?>app-assets/vendors/js/repeater/jquery.repeater.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>app-assets/js/form-repeater.js"></script>

  <script src="<?= base_url() ?>app-assets/vendors/js/extensions/datedropper.min.js"></script>
  <script src="<?= base_url() ?>app-assets/vendors/js/extensions/timedropper.min.js"></script>
  <script src="<?= base_url() ?>app-assets/js/scripts/extensions/date-time-dropper.js"></script>
  <script src="<?= base_url() ?>app-assets/vendors/js/extensions/moment.min.js"></script>
  <script src="<?= base_url() ?>app-assets/js/scripts/extensions/fullcalendar.js"></script>
  <script src="<?= base_url() ?>app-assets/vendors/js/extensions/fullcalendar.min.js"></script>
  <script src="<?= base_url() ?>app-assets/vendors/js/extensions/moment.min.js"></script>
  <script src="<?= base_url() ?>app-assets/js/scripts/extensions/fullcalendar.js"></script>


  <!-- Report view graph -->
  <script src="<?= base_url() ?>app-assets/js/scripts/charts/c3/bar-pie/stacked-bar.js"></script>

  <script src="<?= base_url() ?>app-assets/js/scripts/charts/c3/data/data-order.js"></script>
  <script src="<?= base_url() ?>app-assets/js/scripts/charts/c3/data/category-data.js"></script>

  <!-- Selectivity JS -->
  <script src="<?= base_url() ?>app-assets/vendors/js/forms/select/selectivity-full.min.js"></script>
  <script src="<?= base_url() ?>app-assets/js/scripts/forms/select/form-selectivity.js"></script>

  <!-- summernote editor JS-->
  <script src="<?= base_url() ?>app-assets/vendors/js/editors/summernote/summernote.js"></script>
  <script src="<?= base_url() ?>app-assets/js/scripts/editors/editor-summernote.js"></script>

  <!-- ckeditor-->
  <!-- <script src="<?= base_url() ?>app-assets/vendors/js/editors/ckeditor/ckeditor.js"></script>
  <script src="<?= base_url() ?>app-assets/js/scripts/editors/editor-ckeditor.js"></script> -->



  </body>
  <!-- END: Body-->

  </html>
  <script>
    //$('#search-input').hide();
    $('#s-icon').click(function() {
      $('#search-input').toggle();
    });

    function logout_session() {
      $.ajax({
        url: "<?= base_url() ?>authentication/Session_management/remove_session",
        type: "POST",
        dataType: 'JSON',
        data: "key=role",
        success: function(data) {
          window.location.href = "<?= base_url() ?>authentication/Authenticate/login?initial_login=enter";
        }
      });
    }
  </script>

  <script type="text/javascript">
    $(document).ready(function() {
      $(".multiple-input").attr("multiple", "multiple");
      $('.multiple-input').multiselect({
        columns: 0,
        placeholder: 'Select',
        search: true,
        searchOptions: {
          'default': 'Search'
        },
        selectAll: true
      });

      $(".multiple-input").multiselect('reset');


      $("#company_code_footer_dropdown").on('change', function() {

        if ($("#company_code_footer_dropdown").selectivity('val') == "1") {
          $("#company_code_footer").val("4001");
        } else if ($("#company_code_footer_dropdown").selectivity('val') == "2") {
          $("#company_code_footer").val("4002");

        }
      });

    });

    //    open  and close file upload on click and load its value in ip field start
    function open_file(uploader_class, input_id, preview_id) {
      $("." + uploader_class).click();
      $("." + uploader_class).change(function(e) {

        var fileName = e.target.files[0].name;
        $("#" + input_id).html(fileName);
        if ($("#" + input_id).html() != "") {
          $('#' + input_id).show();
          $('#' + preview_id).show();
          toastr.success('File upload successfully');
        }
      });
    }
    $(window).on("load", function() {
      setTimeout(function() {
        $("#menu_burger").click();
      }, 1000);
    });
    setTimeout(function() {
      $('.nav_links').click(function() {
        let iframe_value = $(this).attr('data-iframe');
        let iframe_title = $(this).attr('data-iframe-title');
        localStorage.setItem('iframe', iframe_value);
        localStorage.setItem('iframe_title', iframe_title);
        window.location.href = "<?= base_url() ?>dashboard/dashboard_controller/dynamic_content";
      });
    }, 200);
  </script>