<style type="text/css">
    @media (min-width: 425px) and (max-width: 768px) {
        #modal_multiupload_year {
            width: 283px;
            margin-left: -5px;
        }

    }

    .ajax-file-upload-error {
        display: none !important;
        width: 65px;
    }

    .ajax-upload-dragdrop {
        border: 1px solid #00517cf5;
        width: 250px !important;
        color: #DADCE3;
        text-align: left;
        vertical-align: middle;
        padding: 10px 10px 0px 10px;
        border-radius: 6px;
    }

    .ajax-file-upload {
        background: #00517cf5 !important;
    }

    .ajax-file-upload-container {
        margin: 20px 0px 20px 0px;
        width: 262px;
    }

    .ajax-file-upload-statusbar {
        width: 560px !important;
        margin-left: 16px;
        padding: 2rem;
    }

    .ajax-file-upload-filename {
        width: auto;
    }

    .frequency {
        margin-left: 14px;
    }

    .selectivity-input {

        border: 1px solid #BABFC7;
        width: 100%;
        border-radius: 0.25rem;

    }

    .client_selection_doc {
        width: 250px;
    }

    .width-cls {
        width: 250px;
        margin-left: -4px;
    }
</style>


<div class="modal-header">
    <h4 class="modal-title" id="myModalLabel17">Upload Documents</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<div class="modal-body">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                <div class="row mt-1">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="">Select Audit Type</label>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                        <input type="radio" name="audit_type" value="original" checked> Original
                        <input type="radio" name="audit_type" value="remediation"> Remediation
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                <div class="row align-items-baseline">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="select_region_doc">Select Region</label>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-12 search_dropdown_div">
                        <select class="selectivity_dropdown" id="select_region_doc">
                            <option>East</option>
                            <option>West</option>
                            <option>North</option>
                            <option>South</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                <div class="row align-items-baseline">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="client_selection_doc">Select Client</label>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-12 search_dropdown_div">
                        <select class="selectivity_dropdown" id="client_selection_doc">
                            <option>HDFC</option>
                            <option>SBI</option>
                            <option>RBL</option>
                            <option>JLL</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                <div class="row align-items-baseline">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="account_selection_doc">Select Account</label>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-12 search_dropdown_div">
                        <select class="selectivity_dropdown" id="account_selection_doc">
                            <option selected disabled>Select Account</option>
                            <option>JP Morgan Services India Pvt Ltd</option>
                            <option>Lehman Brothers</option>
                            <option>Accenture</option>
                            <option>HDFC Bank</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                <div class="row align-items-baseline">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="vendor_selection_doc">Select Vendor</label>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-12 search_dropdown_div">
                        <select class="selectivity_dropdown" id="vendor_selection_doc">
                            <option>Croma</option>
                            <option>Hewitt</option>
                            <option>Hotel Hyatt </option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                <div class="row align-items-baseline">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="modal_multiupload_year">Select Year</label>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                        <input type="text" class="form-control" id="modal_multiupload_year" placeholder="Select Year">
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                <div class="row align-items-baseline">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                        <label for="modal_multiupload_month">Select Month</label>
                    </div>

                    <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                        <input type="text" class="form-control" id="modal_multiupload_month" placeholder="Select Month">
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-1">
            <div class="col-lg-7">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        <label for="fileuploader2">Upload Documents</label>
                    </div>

                    <div class="">
                        <div class="col-lg-10">
                            <div class="row">
                                <fieldset class="form-group" style="z-index: 100;">

                                    <div id="fileuploader2" class="col-lg-12">Upload</div>
                                    <button id="extrabutton" class="ml-1 btn-yu primary btn-width" style="width: 90px;" onclick="hide_img_div('ajax-file-upload-statusbar')">Start Upload </button>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <script>
                        $(document).ready(function() {

                            var extraObj = $("#fileuploader2").uploadFile({
                            url:"<?=base_url()?>/upload.php",
                            fileName:"myfile",
                            extraHTML: function() {
                                    var html = "<div class='row'><div class='col'><b>Select State:</b><select name='state_name' class='selectivity_dropdown_upload'><option>Assam</option><option>Maharashtra</option><option>Goa</option><option>Gujarat</option></select></div><br>";
                                    html += "<div class='col'><b>Document Master</b>:<select name='category' class='selectivity_dropdown_upload'><option>Bonus - Bonus Register - Register</option><option>Bonus - Bonus Returns - Returns</option><option>ESI - ESI Registration Code - Registration</option><option>ESI - ESI challan - Challan</option><option>ESI - ESI challan date  of payment - Challan</option><option>ESI - ESIC ECR - Registration</option><option>ESI - ESI Cards - Preliminary</option></select></div></div>";

                                    html += "<b>Site Name:</b><select name='basic[]' multiple='multiple' class='3col active col-12 multi_upload_select_Site'><option value='an'>Andheri</option><option value='si'>Sion</option><option value='da'>Dadar</option><option value='vi'>Virar</option><option value='ca'>Charkop</option></select>";

                                    html += "<b>Comment:</b><div class='col-12'><textarea class='multi_upload_comment'></textarea></div>";
                                    var flag_count = localStorage.getItem("doc_count_flag");
                                    if (flag_count == '1') {
                                        html += '<div id="relpace_document_div"> <input type="checkbox" id="replace" class="toggle-check"><label title="Overwrite File" class="toggle-small" for="replace"></label> </div> <span>Do you want to overwrite existing file with this?</span>'
                                        localStorage.setItem("doc_count_flag", '2');
                                    } else {
                                        html += " ";
                                    }

                                    html += "</div>";

                                    return html;
                                },
                                onLoad:function(obj)
                                {
                                    $('.ajax-file-upload-container').bind('DOMNodeInserted DOMNodeRemoved', function() {
                                        $('select[multiple].active.3col').multiselect({
                                          columns: 0,
                                          placeholder: 'Select Sites',
                                          search: true,
                                          searchOptions: {
                                            'default': 'Search Sites'
                                          },
                                          selectAll: true
                                        });
                                        setTimeout(function() {

                                          $('.selectivity_dropdown_upload').selectivity({
                                            allowClear: true,
                                            placeholder: 'Select'
                                          });
                                        }, 1000);
                                    });
                                },
                            });

                            $("#extrabutton").click(function() {

                                extraObj.startUpload();

                            });

                        });

                        $(document).ready(function() {
                            localStorage.setItem("doc_count_flag", '1');
                        });
                    </script>
                </div>
            </div>

        </div>
    </div>

</div>
<div class="modal-footer">
    <button type="button" class="btn-yu btn-width primary text-white" data-dismiss="modal">Close</button>
</div>

<script type="text/javascript">
    check_frequency();

    function document_upload_modal_toggle() {
        $('#multiple_upload_modal').modal('show');
    }


    function check_frequency() {
        var frequency_selected = $('input[name="frequency_document"]:checked').val();
        if (frequency_selected == "annually") {
            $("#month_selection_document").prop("disabled", true);
            $("#month_selection_document").show();
            $("#half_year_selection_document").hide();
            $("#quartly_selection_document").hide();
        } else if (frequency_selected == "monthly") {
            $("#month_selection_document").show();
            $("#month_selection_document").prop("disabled", false);
            $("#half_year_selection_document").hide();
            $("#quartly_selection_document").hide();
        } else if (frequency_selected == "yearly") {

            $("#month_selection_document").hide();
            $("#half_year_selection_document").show();
            $("#quartly_selection_document").hide();
        } else if (frequency_selected == "quartely") {
            $("#month_selection_document").hide();
            $("#half_year_selection_document").hide();
            $("#quartly_selection_document").show()
        }
    }
</script>