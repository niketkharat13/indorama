<style type="text/css">
	.tdWidth {
		min-width: 180px !important;
	}

	.default_filter_element_div .table-responsive {
		height: auto;
	}

	.filter_table label {
		margin-bottom: 4px;
	}

	.default_filter_element_div table {
		height: 200px !important;
	}
</style>

<div class="row">


	<div class="default_filter_element_div col-lg-12" style="white-space: nowrap;overflow-x: auto !important;overflow-y: auto !important;">
		<table class="table table-responsive filter_table" style="padding: 1%;margin-bottom: 0%;">
			<tr>
				<td class="tdWidth default_filter_element">
					<h4>Filter 1</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option one
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option two
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option three
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth default_filter_element">
					<h4>Filter 2</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option one
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option two
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option three
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth default_filter_element">
					<h4>Filter 3</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option one
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option two
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option three
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option four
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth default_filter_element">
					<h4>Filter 4</h4>
					<div class="option_style" class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option one
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option two
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option three
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option four
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option five
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option six
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth default_filter_element">
					<h4>Filter 5</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option one
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option two
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option three
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option four
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Option five
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth default_filter_element">
					<h4>Filter 5</h4>
					<div class="form" style="float: right; width: 250px; margin-right: 15px;">
						<div class="form-group">
							<div class="input-group">
								<input type="text" class="form-control shawCalRanges" id="date_range" name="date_range">
								<div class="input-group-append">
									<span class="input-group-text">
										<span class="fa fa-filter"></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</td>



				<td class=" tdWidth document_filter_element">
					<h4>By Months</h4>
					<div class="option_style" id="document_filter_element_months">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								January
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								February
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								March
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								April
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								May
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								June
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								July
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								August
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								September
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								October
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								November
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								December
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth document_filter_element">
					<h4>By Act</h4>
					<div class="option_style" id="document_filter_element_acts">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Bonus
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								CLRA
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								MBenefit
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								MWages
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								HRA
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PWages
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PTax
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								LWF
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								S&E
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Others
							</label>
						</div>
					</div>
				</td>
				<td class=" tdWidth document_filter_element">
					<h4>By Title</h4>
					<div class="option_style" id="document_filter_element_title">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Bonus Register
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Bonus Returns
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI Registration Code
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI challan
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI challan date of payment
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESIC ECR
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI Cards
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF Registration Code
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF Challan
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF challan date of payment
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF ECR
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								EDLI Exemption
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								EDLI Monthly Return
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF Declaration Form 11 / UAN
							</label>
						</div>
					</div>
				</td>
				<td class=" tdWidth document_filter_element">
					<h4>By Document Type</h4>
					<div class="option_style" id="document_filter_element_doc_type">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Register
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Returns
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Challan
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Preliminary
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								License
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Head Count
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Bank statement/Mini statement
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Document
							</label>
						</div>
					</div>
				</td>

				<td class=" tdWidth document_filter_element">
					<form class="">
						<h4>By Audit Type</h4>
						<div class="option_style" id="document_filter_element_audit_type">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Original
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Remediation
								</label>
							</div>


						</div>
					</form>
				</td>
				<td class=" tdWidth document_filter_element">
					<form>
						<h4>By Frequency</h4>
						<div class="option_style" id="document_filter_element_frequency">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Annually
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Monthly
								</label>
							</div>


						</div>
					</form>
				</td>
				<td class=" tdWidth document_filter_element">
					<form>
						<h4>By Site Name</h4>
						<div class="option_style" id="document_filter_element_site_name">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Sion
								</label>
							</div>
						</div>
					</form>

				</td>
				<td class=" tdWidth document_filter_element">
					<form id="document_filter_element_region">
						<h4>By Region</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									East
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									West
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									South
								</label>
							</div>
						</div>
					</form>
				</td>
				<td class="tdWidth document_filter_element">
					<form id="document_filter_element_state">
						<h4>By State</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Maharashtra
							</div>

						</div>
					</form>
				</td>
				<td class="tdWidth document_filter_element">
					<form id="document_filter_element_state">
						<h4>By Year</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									2020
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									2019
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									2018
							</div>

						</div>
					</form>
				</td>

				<!-- dashboard filter ------------- -->
				<td class=" dashboard_filter_element">
					<h4>Filter By</h4>
					<div class="option_style" id="dashboard_filter_element_filter">
						<form id="dashboard_filter_radio">
							<div class="checkbox">
								<input id="filter_by_radio_1" name="filter_by_radio" value="monthly" type="radio" onclick="filter()">
								<label for="filter_by_radio_1" class="radio-label">Monthly</label>
							</div>

							<div class="checkbox">
								<input id="filter_by_radio_2" name="filter_by_radio" value="quarterly" type="radio" onclick="filter()">
								<label for="filter_by_radio_2" class="radio-label">Quarterly</label>
							</div>
							<div class="checkbox">
								<input id="filter_by_radio_3" name="filter_by_radio" value="biannual" type="radio" onclick="filter()">
								<label for="filter_by_radio_3" class="radio-label">Bi-Annual</label>

							</div>
							<div class="checkbox">

								<input id="filter_by_radio_4" name="filter_by_radio" value="annual" type="radio" onclick="filter()">
								<label for="filter_by_radio_4" class="radio-label">Annually</label>

							</div>
						</form>
					</div>
				</td>

				<td class="tdWidth  dashboard_filter_element_month_div" style="display: none">
					<h4>By Months</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="" name="dashboard_filter_month" onclick="onlyOne(this,'1')" id="dashboard_filter_element_month_1">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								January
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="" name="dashboard_filter_month" onclick="onlyOne(this,'2')" id="dashboard_filter_element_month_2">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								February
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="" name="dashboard_filter_month" onclick="onlyOne(this,'3')" id="dashboard_filter_element_month_3">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								March
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="" name="dashboard_filter_month" id="dashboard_filter_element_month_4" onclick="onlyOne(this,'4')">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								April
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="" name="dashboard_filter_month" onclick="onlyOne(this,'5')" id="dashboard_filter_element_month_5">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								May
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="" name="dashboard_filter_month" onclick="onlyOne(this,'6')" id="dashboard_filter_element_month_6">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								June
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="" name="dashboard_filter_month" onclick="onlyOne(this,'7')" id="dashboard_filter_element_month_7">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								July
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="" name="dashboard_filter_month" id="dashboard_filter_element_month_8" onclick="onlyOne(this,'8')">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								August
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="" name="dashboard_filter_month" id="dashboard_filter_element_month_9" onclick="onlyOne(this,'9')">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								September
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="" name="dashboard_filter_month" id="dashboard_filter_element_month_10" onclick="onlyOne(this,'10')">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								October
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="" name="dashboard_filter_month" id="dashboard_filter_element_month_11" onclick="onlyOne(this,'11')">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								November
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="" name="dashboard_filter_month" id="dashboard_filter_element_month_12" onclick="onlyOne(this,'12')">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								December
							</label>
						</div>
					</div>
				</td>

				<td class=" dashboard_filter_element">
					<h4>Client</h4>
					<div class="option_style" id="dashboard_filter_element_client">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								JLL
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								HDFC Bank
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Karma
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								SBI
							</label>
						</div>

					</div>
				</td>

				<td class=" dashboard_filter_element">
					<h4>Region</h4>
					<div class="option_style" id="dashboard_filter_element_region">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								West
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								East
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								North
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								South
							</label>
						</div>
					</div>
				</td>

				<td class="tdWidth dashboard_filter_element">
					<h4>Accounts</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								JP Morgan Services India Pvt Ltd
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Lehman Brothers
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								JP Morgan Services India Pvt Ltd
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Lehman Brothers
							</label>
						</div>
					</div>
				</td>

				<td class="tdWidth dashboard_filter_element">
					<h4>By Vendor</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Ami Sheth and Company
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Niket & Sons Company
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Soham Ui Design Solution
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Nayak Business Analyst Consultancy Services
							</label>
						</div>
					</div>
				</td>
				<td class="dashboard_filter_element">
					<h4>View By</h4>
					<div class="option_style" id="">
						<form id="dashboard_filter_radio_2">
							<!-- <div class="checkbox">
									<label class="d-flex">
										<input type="radio" value="vendor" id="dashboard_view_by_vendor" name="filter_by_radio_2" onclick="widget_1()">
										<span class=""><i class=""></i></span>
										Vendors
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="radio" id="dashboard_view_by_site" name="filter_by_radio_2" value="site" onclick="widget_1()">
										<span class=""><i class=""></i></span>
										Sites
									</label>
								</div> -->
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="" name="dashboard_filter_month" id="dashboard_filter_element_month_13" onclick="onlyOne(this,'13')">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Vendor
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="" name="dashboard_filter_month" id="dashboard_filter_element_month_14" onclick="onlyOne(this,'14')">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Site
								</label>
							</div>
						</form>

					</div>
				</td>
				<td class="  dashboard_filter_element">
					<form>
						<h4>By Site </h4>
						<div class="option_style" id="">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Sion
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Charkop
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Virar
								</label>
							</div>
						</div>
					</form>

				</td>

				<!-- calendar filter view -->
				<td class="tdWidth calendar_filter_element">
					<div class=" " id="calendar_filter_element_account">
						<h4>By Account</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									HDFC
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									SBI
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth calendar_filter_element">
					<div class=" " id="calendar_filter_element_site">
						<h4>By Site</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Charkop
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth calendar_filter_element">
					<div class=" " id="calendar_filter_element_region">
						<h4>By Region</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									East
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									West
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									South
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth calendar_filter_element">
					<div class=" " id="calendar_filter_element_client">
						<h4>By Client</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									JLL
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									HDFC
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth calendar_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Auditor</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham Patil
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth calendar_filter_element">
					<div class=" " id="calendar_filter_element_type">
						<h4>By Type</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Original
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Remediation
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth calendar_filter_element">
					<div class=" " id="calendar_filter_element_method">
						<h4>By Method</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online (AI)
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Physical
								</label>
							</div>

						</div>
					</div>
				</td>


				<!-- site filter-------------- -->

				<td class="tdWidth site_filter_element">
					<h4>By Accounts</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								JP Morgan Services India Pvt Ltd
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Accenture
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								HDFC Bank
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Lehman Brothers
							</label>
						</div>
					</div>
				</td>
				<td class=" tdWidth site_filter_element">

					<h4>By Site Name</h4>
					<div class="option_style" id="document_filter_element_site_name">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Andheri
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Charkop 2
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Dadar
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Gorai 2
							</label>
						</div>
					</div>


				</td>
				<td class="tdWidth site_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Site Status</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Active
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Inactive
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" tdWidth site_filter_element">
					<form id="document_filter_element_region">
						<h4>By Region</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									East
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									West
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									South
								</label>
							</div>
						</div>
					</form>
				</td>
				<td class="tdWidth site_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Location</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Borivali
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Kandivali
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth site_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By City</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Mumbai
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Delhi
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Patna
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth site_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By State</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Maharashtra
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Goa
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Gujarat
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth site_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Format</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online (AI)
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Physical
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth site_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Audit Frequency</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Regular
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Monthly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Quarterly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Half Yearly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Yearly
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth site_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Auditor</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Niket K
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham P
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Aniket Y
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth site_filter_element">
					<h4>Date Range</h4>
					<div class="form" style="float: right; width: 250px; margin-right: 15px;">
						<div class="form-group">
							<div class="input-group">
								<input type="text" class="form-control shawCalRanges" id="date_range_site" name="date_range">
								<div class="input-group-append">
									<span class="input-group-text">
										<span class="fa fa-filter"></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</td>


				<!-- audit history -->
				<td class="tdWidth audit_filter_element">
					<h4>By Accounts</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								JP Morgan Services India Pvt Ltd
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Accenture
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								HDFC Bank
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Lehman Brothers
							</label>
						</div>
					</div>
				</td>
				<td class=" tdWidth audit_filter_element">

					<h4>By Site Name</h4>
					<div class="option_style" id="document_filter_element_site_name">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Andheri
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Charkop 2
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Dadar
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Gorai 2
							</label>
						</div>
					</div>


				</td>
				<td class="tdWidth audit_filter_element">
					<h4>By Client</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								JLL
						</div>
						<!-- <div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Client 2
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Client 3
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Client 4
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Client 5
								</label>
							</div> -->
					</div>
				</td>
				<td class=" tdWidth audit_filter_element">

					<h4>By Region</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								East
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								West
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								North
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								South
							</label>
						</div>
					</div>

				</td>
				<td class="tdWidth  audit_filter_element">
					<h4>By Months</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								January
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								February
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								March
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								April
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								May
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								June
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								July
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								August
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								September
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								October
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								November
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								December
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth audit_filter_element">
					<h4>By Type</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Original
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Remediation
							</label>
						</div>

					</div>
				</td>
				<td class="tdWidth audit_filter_element">
					<h4>By Status</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Critical
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Non-Critical
							</label>
						</div>

					</div>
				</td>
				<td class="tdWidth audit_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Auditor</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham Patil
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth audit_filter_element">
					<h4>By Vendor</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Niket Kharat
						</div>


					</div>
				</td>
				<td class="tdWidth audit_filter_element">
					<div class=" " id="report_filter_element_audit_type">
						<h4>By Audit Type</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online (AI)
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Physical
								</label>
							</div>

						</div>
					</div>
				</td>

				<!-- audit table -->
				<td class="tdWidth audit_table_filter_element">
					<h4>By Accounts</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								JP Morgan Services India Pvt Ltd
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Accenture
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								HDFC Bank
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Lehman Brothers
							</label>
						</div>
					</div>
				</td>
				<td class=" tdWidth audit_table_filter_element">

					<h4>By Site Name</h4>
					<div class="option_style" id="document_filter_element_site_name">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Andheri
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Charkop 2
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Dadar
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Gorai 2
							</label>
						</div>
					</div>


				</td>
				<td class="tdWidth audit_table_filter_element">
					<h4>By Client</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								JLL
						</div>
						<!-- <div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Client 2
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Client 3
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Client 4
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										Client 5
									</label>
								</div> -->
					</div>
				</td>
				<td class=" tdWidth audit_table_filter_element">

					<h4>By Region</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								East
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								West
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								North
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								South
							</label>
						</div>
					</div>

				</td>
				<td class="tdWidth  audit_table_filter_element">
					<h4>By Months</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								January
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								February
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								March
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								April
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								May
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								June
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								July
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								August
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								September
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								October
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								November
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								December
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth audit_table_filter_element">
					<h4>By Type</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Original
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Remediation
							</label>
						</div>

					</div>
				</td>
				<td class="tdWidth audit_table_filter_element">
					<h4>By Audit Status</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								On Going
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Close
							</label>
						</div>

					</div>
				</td>
				<td class="tdWidth audit_table_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Auditor</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham Patil
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth audit_table_filter_element">
					<h4>By Vendor</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Niket Kharat
						</div>


					</div>
				</td>
				<td class="tdWidth audit_table_filter_element">
					<div class=" " id="">
						<h4>By Audit Type</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online (AI)
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Physical
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" audit_table_filter_element">
					<div class=" " id="report_filter_element_site">
						<h4>By # of Employees </h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>

				<!-- vendor team filter-->
				<td class="tdWidth vendor_team_filter_element">
					<div class=" " id="">
						<h4>By Name</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Purushothaman
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Aniket Yadav
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Niket Kharat
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham Patil
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Bhumika
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_team_filter_element">
					<div class=" " id="">
						<h4>By Username</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven1
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven2
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven3
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven4
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven5
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_team_filter_element">
					<div class=" " id="">
						<h4>By Email ID</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven1@gmail.com
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven2@gmail.com
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven3@gmail.com
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven4@gmail.com
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven5@gmail.com
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_team_filter_element">
					<div class=" " id="">
						<h4>By Phone #</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9821365441
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9821836917
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9820771885
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9820771887
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9820771888
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_team_filter_element">
					<div class=" " id="">
						<h4>By Sites</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Charkop 2
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Sion
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Borivali
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Virar
								</label>
							</div>

						</div>
					</div>
				</td>
				<!-- <td class="lol" style="display: none"> LOL </td> -->

				<td class=" tdWidth vendor_license_filter_element">

					<h4>By Site Name</h4>
					<div class="option_style" id="document_filter_element_site_name">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Charkop
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Andheri
							</label>
						</div>
					</div>
					</form>

				</td>
				<td class=" tdWidth vendor_license_filter_element">
					<form id="document_filter_element_region">
						<h4>By License #</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									CLA/L/11/SW-10
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									CLACT/ALC/2 NO 73/2010
								</label>
							</div>

						</div>

				</td>
				<td class="tdWidth vendor_license_filter_element">
					<form id="document_filter_element_state">
						<h4>By Expiration Date</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									31 Dec 2019
							</div>


						</div>
					</form>
				</td>
				<td class="tdWidth vendor_license_filter_element">

					<h4>By Expiration Date</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								31 Dec 2019
						</div>
					</div>

				</td>
				<td class="tdWidth vendor_license_filter_element">

					<h4>By RC</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Uploaded
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Not Uploaded
						</div>
					</div>

				</td>
				<td class="tdWidth vendor_license_filter_element">

					<h4>By VI A</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Uploaded
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Not Uploaded
						</div>
					</div>

				</td>
				<td class="tdWidth vendor_license_filter_element">

					<h4>By Renewal Status</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Applied
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Surrender
						</div>
					</div>

				</td>

				<!-- checklist menu client filter -->

				<td class="tdWidth checklist_menu_filter_element">
					<h4>By Act</h4>
					<div class="option_style" id="document_filter_element_acts">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Bonus
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								CLRA
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								MBenefit
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								MWages
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								HRA
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PWages
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PTax
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								LWF
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								S&E
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Others
							</label>
						</div>
					</div>
				</td>
				<td class=" tdWidth checklist_menu_filter_element">
					<h4>By Title</h4>
					<div class="option_style" id="document_filter_element_title">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Bonus Register
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Bonus Returns
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI Registration Code
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI challan
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI challan date of payment
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESIC ECR
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI Cards
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF Registration Code
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF Challan
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF challan date of payment
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF ECR
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								EDLI Exemption
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								EDLI Monthly Return
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF Declaration Form 11 / UAN
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth checklist_menu_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Status</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Critical
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Non-Critical
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth checklist_menu_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Audit Frequency</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Regular
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Monthly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Quarterly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Half Yearly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Yearly
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth checklist_menu_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Priority</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									High
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Medium
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Low
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth checklist_menu_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Impacts</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Books Register
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Return
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Licence
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Payments
								</label>
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth checklist_menu_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Category</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Central
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									State
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth checklist_menu_filter_element">
					<div class=" " id="report_filter_element_audit_type">
						<h4>By Mode</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online (AI)
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Physical
								</label>
							</div>

						</div>
					</div>
				</td>

				<!--client site filter-------------- -->

				<td class="tdWidth client_site_filter_element">
					<h4>By Accounts</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								JP Morgan Services India Pvt Ltd
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Accenture
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								HDFC Bank
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Lehman Brothers
							</label>
						</div>
					</div>
				</td>
				<td class=" tdWidth client_site_filter_element">

					<h4>By Site Name</h4>
					<div class="option_style" id="document_filter_element_site_name">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Andheri
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Charkop 2
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Dadar
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Gorai 2
							</label>
						</div>
					</div>


				</td>
				<td class="tdWidth client_site_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Site Status</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Active
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Inactive
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" tdWidth client_site_filter_element">
					<form id="document_filter_element_region">
						<h4>By Region</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									East
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									West
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									South
								</label>
							</div>
						</div>
					</form>
				</td>
				<td class="tdWidth client_site_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Location</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Borivali
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Kandivali
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth client_site_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By City</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Mumbai
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Delhi
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Patna
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth client_site_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By State</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Maharashtra
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Goa
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Gujarat
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth client_site_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Format</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online (AI)
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Physical
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth client_site_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Audit Frequency</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Regular
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Monthly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Quarterly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Half Yearly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Yearly
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth client_site_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Auditor</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Niket K
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham P
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Aniket Y
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth client_site_filter_element">
					<h4>Date Range</h4>
					<div class="form" style="float: right; width: 250px; margin-right: 15px;">
						<div class="form-group">
							<div class="input-group">
								<input type="text" class="form-control shawCalRanges" id="date_range_2" name="date_range">
								<div class="input-group-append">
									<span class="input-group-text">
										<span class="fa fa-filter"></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</td>

				<!-- report table filter view -->
				<td class="tdWidth report_filter_element">
					<div class=" " id="report_filter_element_site">
						<h4>By Site</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Charkop 2
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Sion
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Borivali
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth report_filter_element">
					<div class=" " id="report_filter_element_account">
						<h4>By Accounts</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									HDFC Bank
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									SBI
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth report_filter_element">
					<div class=" " id="report_filter_element_client">
						<h4>By Client</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									JLL
							</div>
							<!-- <div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										JLL
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										JLL
									</label>
								</div>
								<div class="checkbox">
									<label class="d-flex">
										<input type="checkbox" value="">
										<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
										JLL
									</label>
								</div> -->
						</div>
					</div>
				</td>
				<td class="tdWidth report_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By Region</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									East
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									West
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									South
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth report_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By State</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Maharashtra
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Goa
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Gujarat
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth report_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Location</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Borivali
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Kandivali
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth report_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Auditor</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham Patil
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth report_filter_element">
					<h4>By Vendor </h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Niket Kharat
						</div>


					</div>
				</td>
				<td class="tdWidth report_filter_element">
					<div class=" " id="">
						<h4>By Score</h4>

						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Below 60%
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Between 60% - 80%
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Above 80%
							</label>
						</div>

					</div>
				</td>
				<td class="tdWidth report_filter_element">
					<h4>By Months</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								January
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								February
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								March
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								April
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								May
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								June
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								July
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								August
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								September
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								October
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								November
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								December
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth report_filter_element">
					<div class=" " id="report_filter_element_type">
						<h4>By Type</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Original
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Remediation
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth report_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Status</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Critical
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Non-Critical
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth report_filter_element">
					<div class=" " id="report_filter_element_audit_type">
						<h4>By Audit Type</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online (AI)
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Physical
								</label>
							</div>

						</div>
					</div>
				</td>

				<!-- region wise table filter view -->
				<td class="tdWidth region_wise_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By Region</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									East
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									West
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									South
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth region_wise_filter_element">
					<div class=" " id="report_filter_element_site">
						<h4>By # of Site</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>

				<td class="tdWidth region_wise_filter_element">
					<div class=" " id="">
						<h4>By Critical</h4>

						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Above 60%
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Between 60% - 80%
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Above 80%
							</label>
						</div>

					</div>
				</td>

				<td class="tdWidth region_wise_filter_element">
					<h4>By full</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Below 60%
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Between 60% - 80%
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Above 80%
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth region_wise_filter_element">
					<div class=" " id="">
						<h4>By Net Critical</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Below 60%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Between 60% - 80%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Above 80%
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth region_wise_filter_element">
					<div class=" " id="">
						<h4>By Net Full</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Below 60%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Between 60% - 80%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Above 80%
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth region_wise_filter_element">
					<div class=" " id="report_filter_element_audit_type">
						<h4>By Vendors present for Audit</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>

				<!-- vendor compliance  table filter view -->
				<td class="tdWidth  vendor_compliance_filter_element">
					<h4>By Months</h4>
					<div class="option_style" id="document_filter_element_months">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								January
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								February
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								March
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								April
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								May
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								June
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								July
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								August
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								September
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								October
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								November
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								December
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_compliance_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By # of Sites</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_compliance_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By # of Sites Audited</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_compliance_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By # of Unique Vendor</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_compliance_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By # of Absent Vendors</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_compliance_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By Critical</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Below 60%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Between 60% - 80%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Above 80%
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_compliance_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By Full</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Below 60%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Between 60% - 80%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Above 80%
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_compliance_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By Net Critical </h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Below 60%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Between 60% - 80%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Above 80%
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_compliance_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By Net Full </h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Below 60%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Between 60% - 80%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Above 80%
								</label>
							</div>
						</div>
					</div>
				</td>

				<!-- vendor original table filter view -->
				<td class="tdWidth vendor_original_filter_element">
					<h4>By Months</h4>
					<div class="option_style" id="document_filter_element_months">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								January
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								February
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								March
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								April
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								May
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								June
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								July
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								August
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								September
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								October
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								November
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								December
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_original_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By # of Sites</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_original_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By # of Sites Audited</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_original_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By # of Unique Vendor</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_original_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By # of Absent Vendors</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_original_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By Critical</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Below 60%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Between 60% - 80%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Above 80%
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_original_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By Full</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Below 60%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Between 60% - 80%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Above 80%
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_original_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By Net Critical </h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Below 60%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Between 60% - 80%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Above 80%
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_original_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By Net Full </h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Below 60%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Between 60% - 80%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Above 80%
								</label>
							</div>
						</div>
					</div>
				</td>

				<!-- vendor Remediation table filter view -->
				<td class="tdWidth  vendor_remediation_filter_element">
					<h4>By Months</h4>
					<div class="option_style" id="document_filter_element_months">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								January
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								February
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								March
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								April
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								May
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								June
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								July
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								August
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								September
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								October
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								November
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								December
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_remediation_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By # of Sites</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_remediation_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By # of Sites Audited</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_remediation_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By # of Unique Vendor</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_remediation_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By # of Vendors Absent</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_remediation_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By Critical</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Below 60%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Between 60% - 80%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Above 80%
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_remediation_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By Full</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Below 60%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Between 60% - 80%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Above 80%
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_remediation_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By Net Critical </h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Below 60%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Between 60% - 80%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Above 80%
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth vendor_remediation_filter_element">
					<div class=" " id="report_filter_element_region">
						<h4>By Net Full </h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Below 60%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Between 60% - 80%
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Above 80%
								</label>
							</div>
						</div>
					</div>
				</td>

				<!-- client master filter -->
				<td class="tdWidth client_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Client Status</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Active
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Inactive
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth client_master_filter_element">
					<h4>By Client</h4>
					<div class="option_style" id="dashboard_filter_element_client">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								JLL
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								HDFC Bank
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Karma
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								SBI
							</label>
						</div>

					</div>
				</td>
				<td class="tdWidth client_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Auditor</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham Patil
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth client_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Key Account Owner</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>


						</div>
					</div>
				</td>
				<!-- User Filter View -->
				<td class="tdWidth user_filter_element">
					<div class=" " id="">
						<h4>By Name</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Purushothaman
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Aniket Yadav
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Niket Kharat
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham Patil
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Bhumika
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth user_filter_element">
					<div class=" " id="">
						<h4>By Username</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									<span id="user1">karcli1</span>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									<span id="user2">karcli2</span>
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									<span id="user3">karcli3</span>
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									<span id="user4">karcli4</span>
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									<span id="user5">karcli5</span>
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth user_filter_element">
					<div class=" " id="">
						<h4>By Email ID</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									<span id="email1">karcli1@gmail.com</span>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									<span id="email2">karcli2@gmail.com</span>
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									<span id="email3">karcli3@gmail.com</span>
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									<span id="email4">karcli4@gmail.com</span>
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									<span id="email5">karcli5@gmail.com</span>
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth user_filter_element">
					<div class=" " id="">
						<h4>By Phone #</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9821365441
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9821836917
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9820771885
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9820771887
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9820771888
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth user_filter_element">
					<div class=" " id="">
						<h4>By Sites</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Charkop 2
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Sion
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Borivali
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Virar
								</label>
							</div>

						</div>
					</div>
				</td>
				<!-- client master filter -->
				<td class="tdWidth client_user_filter_element">
					<div class=" " id="">
						<h4>By Name</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Purushothaman
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Aniket Yadav
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Niket Kharat
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham Patil
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Bhumika
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth client_user_filter_element">
					<div class=" " id="">
						<h4>By Username</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven1
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven2
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven3
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven4
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven5
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth client_user_filter_element">
					<div class=" " id="">
						<h4>By Email ID</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven1@gmail.com
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven2@gmail.com
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven3@gmail.com
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven4@gmail.com
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven5@gmail.com
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth client_user_filter_element">
					<div class=" " id="">
						<h4>By Phone #</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9821365441
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9821836917
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9820771885
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9820771887
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9820771888
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth client_user_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Address</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Charkop

							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Mumbai
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth client_user_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By State</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Maharashtra
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Goa
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Gujarat
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth client_user_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By City</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Mumbai
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Delhi
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Patna
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" tdWidth client_user_filter_element">
					<form id="document_filter_element_region">
						<h4>By Region</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									East
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									West
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									South
								</label>
							</div>
						</div>
					</form>
				</td>
				<td class=" tdWidth client_user_filter_element">
					<form id="document_filter_element_region">
						<h4>By Country</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									India
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Australia
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Japan
								</label>
							</div>
						</div>
					</form>
				</td>


				<!-- Account master filter -->

				<td class="tdWidth account_master_filter_element">
					<div class=" " id="calendar_filter_element_account">
						<h4>By Account</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									HDFC
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									SBI
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth account_master_filter_element">
					<h4>By Client</h4>
					<div class="option_style" id="dashboard_filter_element_client">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								JLL
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								HDFC Bank
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Karma
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								SBI
							</label>
						</div>

					</div>
				</td>
				<td class="tdWidth account_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>Appropriate Government</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Central
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									State
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth account_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Score</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Weighted Average
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Final Score
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Critical Score
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth account_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By TAT</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									00
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									01
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									02
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									07
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth account_master_filter_element">
					<div class=" " id="report_filter_element_audit_type">
						<h4>By Audit Type</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online (AI)
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Physical
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" tdWidth account_master_filter_element">
					<form>
						<h4>By Frequency</h4>
						<div class="option_style" id="document_filter_element_frequency">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Annually
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Monthly
								</label>
							</div>


						</div>
					</form>
				</td>
				<td class="tdWidth account_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Auditor</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham Patil
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth account_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Key Account Owner</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>
						</div>
					</div>
				</td>
				<td class=" account_master_filter_element">
					<div class=" " id="report_filter_element_site">
						<h4>By # of Site</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth account_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Amount</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									₹ 1200
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									₹ 1100
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" account_master_filter_element">
					<div class=" " id="report_filter_element_site">
						<h4>By # of Vendors</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class=" account_master_filter_element">
					<div class=" " id="report_filter_element_site">
						<h4>By # of Employees</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth account_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Duration of Contract</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									12
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									10
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" tdWidth account_master_filter_element">
					<h4>By Months</h4>
					<div class="option_style" id="document_filter_element_months">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								January
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								February
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								March
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								April
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								May
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								June
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								July
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								August
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								September
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								October
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								November
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								December
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth account_master_filter_element">
					<form id="document_filter_element_state">
						<h4>By Year</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									2020
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									2019
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									2018
							</div>

						</div>
					</form>
				</td>
				<td class="tdWidth account_master_filter_element">
					<form id="document_filter_element_state">
						<h4>By Checklist</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Checklist 1
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Checklist 2
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Checklist 3
							</div>

						</div>
					</form>
				</td>
				<td class="tdWidth account_master_filter_element">
					<form id="document_filter_element_state">
						<h4>By Audit Categorization</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Full Site
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Cluster
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Desk Audit
							</div>

						</div>
					</form>
				</td>


				<!-- site master filter -->
				<td class="tdWidth site_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Site Status</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Active
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Inactive
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" site_master_filter_element">
					<div class=" " id="">
						<h4>By Compliance Status</h4>

						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Below 60%
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Between 60% - 80%
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Above 80%
							</label>
						</div>

					</div>
				</td>
				<td class=" tdWidth site_master_filter_element">
					<form>
						<h4>By Site Name</h4>
						<div class="option_style" id="document_filter_element_site_name">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Sion
								</label>
							</div>
						</div>
					</form>

				</td>
				<td class="tdWidth site_master_filter_element">
					<div class=" " id="calendar_filter_element_account">
						<h4>By Account</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									HDFC
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									SBI
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth site_master_filter_element">
					<div class=" " id="calendar_filter_element_client">
						<h4>By Client</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									JLL
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									HDFC
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" tdWidth site_master_filter_element">
					<form id="document_filter_element_region">
						<h4>By Region</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									East
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									West
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									South
								</label>
							</div>
						</div>
					</form>
				</td>
				<td class="tdWidth site_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Location</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Borivali
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Kandivali
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth site_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By City</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Mumbai
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Delhi
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Patna
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth site_master_filter_element">
					<form id="document_filter_element_state">
						<h4>By State</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Maharashtra
							</div>

						</div>
					</form>
				</td>
				<td class="tdWidth site_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Format</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online (AI)
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Physical
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth site_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Audit Frequency</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Regular
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Monthly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Quarterly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Half Yearly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Yearly
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth site_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Auditor</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Niket K
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham P
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Aniket Y
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth site_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Site Contact's Person</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth site_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Site Contact's Email</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth site_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Site Contact's #</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth site_master_filter_element">
					<h4>Date Range</h4>
					<div class="form" style="float: right; width: 250px; margin-right: 15px;">
						<div class="form-group">
							<div class="input-group">
								<input type="text" class="form-control shawCalRanges" id="date_range_site_master" name="date_range">
								<div class="input-group-append">
									<span class="input-group-text">
										<span class="fa fa-filter"></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</td>


				<!-- vendor master filter -->
				<td class="tdWidth vendor_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Site Status</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Active
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Inactive
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" vendor_master_filter_element">
					<div class=" " id="">
						<h4>By Compliance Status</h4>

						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Below 60%
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Between 60% - 80%
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Above 80%
							</label>
						</div>

					</div>
				</td>
				<td class=" tdWidth vendor_master_filter_element">
					<form>
						<h4>By Site Name</h4>
						<div class="option_style" id="document_filter_element_site_name">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Sion
								</label>
							</div>
						</div>
					</form>

				</td>
				<td class="tdWidth vendor_master_filter_element">
					<div class=" " id="calendar_filter_element_account">
						<h4>By Account</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									HDFC
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									SBI
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_master_filter_element">
					<div class=" " id="calendar_filter_element_client">
						<h4>By Client</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									JLL
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									HDFC
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_master_filter_element">
					<div class=" " id="calendar_filter_element_client">
						<h4>By Vendor</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Croma
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									HDFC LTD
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" tdWidth vendor_master_filter_element">
					<form id="document_filter_element_region">
						<h4>By Region</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									East
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									West
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									South
								</label>
							</div>
						</div>
					</form>
				</td>
				<td class="tdWidth vendor_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Location</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Borivali
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Kandivali
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By City</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Mumbai
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Delhi
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Patna
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_master_filter_element">
					<form id="document_filter_element_state">
						<h4>By State</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Maharashtra
							</div>

						</div>
					</form>
				</td>
				<td class="tdWidth vendor_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Format</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online (AI)
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Physical
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Audit Frequency</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Regular
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Monthly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Quarterly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Half Yearly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Yearly
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Auditor</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Niket K
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham P
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Aniket Y
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Vendor Contact Person</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth vendor_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Vendor Contact Email</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth vendor_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Vendor Contact #</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth vendor_master_filter_element">
					<h4>Date Range</h4>
					<div class="form" style="float: right; width: 250px; margin-right: 15px;">
						<div class="form-group">
							<div class="input-group">
								<input type="text" class="form-control shawCalRanges" id="date_range_vendor_master" name="date_range">
								<div class="input-group-append">
									<span class="input-group-text">
										<span class="fa fa-filter"></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</td>


				<!-- vendor user master filter -->
				<td class="tdWidth vendor_user_filter_element">
					<div class=" " id="">
						<h4>By Name</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Purushothaman
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Aniket Yadav
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Niket Kharat
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham Patil
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Bhumika
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_user_filter_element">
					<div class=" " id="">
						<h4>By Username</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven1
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven2
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven3
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven4
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven5
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_user_filter_element">
					<div class=" " id="">
						<h4>By Email ID</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven1@gmail.com
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven2@gmail.com
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven3@gmail.com
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven4@gmail.com
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven5@gmail.com
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_user_filter_element">
					<div class=" " id="">
						<h4>By Phone #</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9821365441
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9821836917
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9820771885
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9820771887
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									9820771888
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_user_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Address</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Charkop

							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Mumbai
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_user_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By State</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Maharashtra
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Goa
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Gujarat
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth vendor_user_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By City</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Mumbai
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Delhi
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Patna
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" tdWidth vendor_user_filter_element">
					<form id="document_filter_element_region">
						<h4>By Region</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									East
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									West
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									South
								</label>
							</div>
						</div>
					</form>
				</td>
				<td class=" tdWidth vendor_user_filter_element">
					<form id="document_filter_element_region">
						<h4>By Country</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									India
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Australia
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Japan
								</label>
							</div>
						</div>
					</form>
				</td>

				<!-- cluster master filter -->
				<td class="tdWidth cluster_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Cluster Name</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Cluster 1
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Cluster 2
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Cluster 3
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" cluster_master_filter_element">
					<div class=" " id="">
						<h4>By Compliance Status</h4>

						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Below 60%
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Between 60% - 80%
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Above 80%
							</label>
						</div>

					</div>
				</td>
				<td class=" tdWidth cluster_master_filter_element">
					<form>
						<h4>By Cluster Type</h4>
						<div class="option_style" id="document_filter_element_site_name">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Vendor Cluster
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Site Cluster
								</label>
							</div>

						</div>
					</form>

				</td>
				<td class="tdWidth cluster_master_filter_element">
					<div class=" " id="calendar_filter_element_account">
						<h4>By Account</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									HDFC
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									SBI
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth cluster_master_filter_element">
					<div class=" " id="calendar_filter_element_client">
						<h4>By Client</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									JLL
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									HDFC
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" cluster_master_filter_element">
					<div class=" " id="report_filter_element_site">
						<h4>By # of Site</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>
				<td class=" cluster_master_filter_element">
					<div class=" " id="report_filter_element_site">
						<h4>By # of Vendor</h4>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								0 - 10
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								10 - 20
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								20 - 30
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								30 - 40
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								40 - 50
							</label>
						</div>
					</div>
				</td>


				<td class=" tdWidth cluster_master_filter_element">
					<form id="document_filter_element_region">
						<h4>By Region</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									East
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									West
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									South
								</label>
							</div>
						</div>
					</form>
				</td>
				<td class="tdWidth cluster_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Location</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Borivali
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Kandivali
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth cluster_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By City</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Mumbai
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Delhi
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Patna
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth cluster_master_filter_element">
					<form id="document_filter_element_state">
						<h4>By State</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Maharashtra
							</div>

						</div>
					</form>
				</td>
				<td class="tdWidth cluster_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Format</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online (AI)
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Physical
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth cluster_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Audit Frequency</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Regular
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Monthly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Quarterly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Half Yearly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Yearly
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth cluster_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Auditor</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Niket K
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham P
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Aniket Y
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth cluster_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Cluster Contact Person</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth cluster_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Cluster Contact Email</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth cluster_master_filter_element">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Cluster Contact #</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth cluster_master_filter_element">
					<h4>Date Range</h4>
					<div class="form" style="float: right; width: 250px; margin-right: 15px;">
						<div class="form-group">
							<div class="input-group">
								<input type="text" class="form-control shawCalRanges" id="date_range_cluster_master" name="date_range">
								<div class="input-group-append">
									<span class="input-group-text">
										<span class="fa fa-filter"></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</td>


				<!-- checklist master filter -->
				<td class="tdWidth checklist_master_filter_element">
					<div class=" " id="">
						<h4>By Checklist Name</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Checklist 1
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Checklist 2
								</label>
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth checklist_master_filter_element">
					<div class=" " id="">
						<h4>By Checklist Type</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Central
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									State
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class="tdWidth checklist_master_filter_element">
					<form id="document_filter_element_state">
						<h4>By State</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Goa
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Maharashtra
							</div>

						</div>
					</form>
				</td>
				<td class="tdWidth checklist_master_filter_element">
					<div class=" " id="">
						<h4>By Create Date</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									01 Dec 2019
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									01 Dec 2019
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth checklist_master_filter_element">
					<div class=" " id="">
						<h4>By Last Edit Date</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									29 Feb 2020
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									29 Feb 2020
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth checklist_master_filter_element">
					<div class=" " id="">
						<h4>Mapped to # of Accounts</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									0-10
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									11-20
								</label>
							</div>

						</div>
					</div>
				</td>


				<!-- user master filter -->
				<td class="tdWidth user_master_filter_element">
					<div class=" " id="">
						<h4>By First Name</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Purushothaman
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Aniket
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Niket
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Namrata
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth user_master_filter_element">
					<div class=" " id="">
						<h4>By Last Name</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Tr
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Yadav
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Kharat
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Patil
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Dushing
								</label>
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth user_master_filter_element">
					<div class=" " id="">
						<h4>By Email ID</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									soham@yashussunlimited.com
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									niket@solutiondoctors.com
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven3@gmail.com
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									aniket@solutiondoctors.com
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									namrata@solutiondoctors.com
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth user_master_filter_element">
					<div class=" " id="">
						<h4>By Username</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven1
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven2
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven3
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven4
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									karven5
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" tdWidth user_master_filter_element">
					<form id="document_filter_element_region">
						<h4>By Region</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									East
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									West
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									South
								</label>
							</div>
						</div>
					</form>
				</td>
				<td class="tdWidth user_master_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By City</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Mumbai
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Delhi
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Patna
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth user_master_filter_element">
					<form id="document_filter_element_state">
						<h4>By State</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Maharashtra
							</div>

						</div>
					</form>
				</td>
				<td class=" tdWidth user_master_filter_element">
					<form id="document_filter_element_region">
						<h4>By Country</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									India
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Australia
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Japan
								</label>
							</div>
						</div>
					</form>
				</td>
				<td class=" tdWidth user_master_filter_element">
					<form id="document_filter_element_region">
						<h4>By Pin Code</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									400067
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									400086
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									400088
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									400089
								</label>
							</div>
						</div>
					</form>
				</td>
				<td class=" tdWidth user_master_filter_element">
					<form id="document_filter_element_region">
						<h4>By Division</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Technical
							</div>

						</div>
					</form>
				</td>
				<td class=" tdWidth user_master_filter_element">
					<form id="document_filter_element_region">
						<h4>By Department</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									UI/UX
							</div>
						</div>
					</form>
				</td>
				<td class=" tdWidth user_master_filter_element">
					<form id="document_filter_element_region">
						<h4>By Reporting</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>
						</div>
					</form>
				</td>
				<td class=" tdWidth user_master_filter_element">
					<form id="document_filter_element_region">
						<h4>By Designation</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									UI/UX Lead
							</div>
						</div>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Front End Developer
							</div>
						</div>
					</form>
				</td>

				<!-- act wise report filter  -->
				<td class=" act_wsie_filter_element">
					<div class=" " id="calendar_filter_element_client">
						<h4>By Client</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									JLL
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									HDFC
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth act_wsie_filter_element">
					<div class=" " id="calendar_filter_element_account">
						<h4>By Account</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									HDFC
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									SBI
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="  act_wsie_filter_element">
					<form>
						<h4>By Site </h4>
						<div class="option_style" id="">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Sion
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Charkop
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Virar
								</label>
							</div>
						</div>
					</form>

				</td>
				<td class="tdWidth act_wsie_filter_element">
					<h4>By Vendor</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Ami Sheth and Company
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Niket & Sons Company
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Soham Ui Design Solution
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Nayak Business Analyst Consultancy Services
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth act_wsie_filter_element">
					<h4>By Status</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Compliant
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Non-Compliant
							</label>
						</div>

					</div>
				</td>
				<td class=" tdWidth act_wsie_filter_element">
					<form id="document_filter_element_region">
						<h4>By Region</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									East
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									West
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									South
								</label>
							</div>
						</div>
					</form>
				</td>
				<td class="tdWidth act_wsie_filter_element">
					<h4>By Act</h4>
					<div class="option_style" id="document_filter_element_acts">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Bonus
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								CLRA
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								MBenefit
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								MWages
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								HRA
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PWages
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PTax
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								LWF
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								S&E
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Others
							</label>
						</div>
					</div>
				</td>
				<td class=" tdWidth act_wsie_filter_element">
					<h4>By Title</h4>
					<div class="option_style" id="document_filter_element_title">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Bonus Register
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Bonus Returns
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI Registration Code
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI challan
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI challan date of payment
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESIC ECR
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI Cards
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF Registration Code
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF Challan
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF challan date of payment
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF ECR
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								EDLI Exemption
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								EDLI Monthly Return
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF Declaration Form 11 / UAN
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth act_wsie_filter_element">
					<h4>By Criticality</h4>
					<div class="option_style" id="">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Critical
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Non-Critical
							</label>
						</div>

					</div>
				</td>
				<td class=" tdWidth act_wsie_filter_element">
					<h4>By Document Type</h4>
					<div class="option_style" id="document_filter_element_doc_type">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Register
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Returns
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Challan
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Preliminary
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								License
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Head Count
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Bank statement/Mini statement
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Document
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth act_wsie_filter_element">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Impacts</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Monetary
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									License & Registration
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Procedures
								</label>
							</div>



						</div>
					</div>
				</td>
				<td class="tdWidth act_wsie_filter_element">
					<form id="document_filter_element_state">
						<h4>By Year</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									2020
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									2019
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									2018
							</div>

						</div>
					</form>
				</td>
				<td class=" tdWidth act_wsie_filter_element">
					<h4>By Months</h4>
					<div class="option_style" id="document_filter_element_months">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								January
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								February
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								March
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								April
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								May
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								June
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								July
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								August
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								September
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								October
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								November
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								December
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth audit_form_filter">
					<h4>By Act</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Bonus
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								CLRA
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								MBenefit
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								MWages
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								HRA
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PWages
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PTax
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								LWF
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								S&E
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Other
							</label>
						</div>
					</div>
				</td>
				<td class=" tdWidth audit_form_filter">
					<h4>By Title</h4>
					<div class="option_style" id="document_filter_element_doc_type">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Bonus Register
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Bonus Returns
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI Registration Code
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI challan
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI challan date of payment
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESIC ECR
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								ESI Cards
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								PF Registration Code
							</label>
						</div>
					</div>
				</td>
				<td class=" tdWidth audit_form_filter">
					<h4>By Document Type</h4>
					<div class="option_style" id="document_filter_element_doc_type">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Register
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Returns
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Challan
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Preliminary
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								License
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Head Count
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Bank statement/Mini statement
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Document
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth audit_form_filter">
					<h4>By Status</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Compliance (C)
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Non Compliance (NC)
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Not Produced (NP)
							</label>
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Not Availability (NA)
							</label>
						</div>
					</div>
				</td>
				<td class="tdWidth audit_form_filter">
					<h4>By Attachment</h4>
					<div class="option_style">
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								Yes
						</div>
						<div class="checkbox">
							<label class="d-flex">
								<input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
								No
							</label>
						</div>
					</div>
				</td>



				<!-- cluster list view filter -->
				<td class="  cluster_list_view_filter">
					<form>
						<h4>By Site </h4>
						<div class="option_style" id="">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Charkop 2
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Mind Space
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Chakala
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Virat Nagar
								</label>
							</div>
						</div>
					</form>

				</td>
				<td class="tdWidth cluster_list_view_filter">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Site Status</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Active
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Inactive
								</label>
							</div>
						</div>
					</div>
				</td>
				<td class=" cluster_list_view_filter">
					<div class=" " id="calendar_filter_element_client">
						<h4>By Client</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									JLL
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									HDFC
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth cluster_list_view_filter">
					<div class=" " id="calendar_filter_element_account">
						<h4>By Account</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									HDFC
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									SBI
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class=" tdWidth cluster_list_view_filter">
					<form id="document_filter_element_region">
						<h4>By Region</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									East
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									West
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									North
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									South
								</label>
							</div>
						</div>
					</form>
				</td>
				<td class="tdWidth cluster_list_view_filter">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Location</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Borivali
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Kandivali
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Andheri
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth cluster_list_view_filter">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By City</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Mumbai
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Delhi
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Patna
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth cluster_list_view_filter">
					<form id="document_filter_element_state">
						<h4>By State</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Maharashtra
							</div>

						</div>
					</form>
				</td>
				<td class="tdWidth cluster_list_view_filter">
					<div class=" " id="">
						<h4>By Last Audit Date</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									25 Dec 2019
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									15 Dec 2019
								</label>
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth cluster_list_view_filter">
					<div class=" " id="">
						<h4>By Next Audit Date</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									25 Jan 2020
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									15 Jan 2020
								</label>
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth cluster_list_view_filter">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Format</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online (AI)
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Online
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Physical
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth cluster_list_view_filter">
					<div class=" " id="report_filter_element_audit_status">
						<h4>By Audit Frequency</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Regular
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Monthly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Quarterly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Half Yearly
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Yearly
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth cluster_list_view_filter">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Auditor</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Niket K
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Soham P
								</label>
							</div>
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Aniket Y
								</label>
							</div>

						</div>
					</div>
				</td>
				<td class="tdWidth cluster_list_view_filter">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Site Contact's Person</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth cluster_list_view_filter">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Site Contact's Email</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>


						</div>
					</div>
				</td>
				<td class="tdWidth cluster_list_view_filter">
					<div class=" " id="calendar_filter_element_auditor">
						<h4>By Site Contact's #</h4>
						<div class="option_style">
							<div class="checkbox">
								<label class="d-flex">
									<input type="checkbox" value="">
									<span class="cr"><i class="cr-icon glyphicon ft-check"></i></span>
									Ami Sheth
							</div>


						</div>
					</div>
				</td>


			</tr>
		</table>
	</div>
</div>
<hr>
<div style="padding: 10px;">
	<button class="btn btn-sm primary filter_div_export_cls_1" style="width:135px;" id="filter_div_save">Apply and Export</button>
	<button class="btn btn-sm primary filter_div_apply_cls_1" style="width:135px" id="filter_div_apply">Apply</button>

	<button class="btn btn-sm btn-dark filter_div_close_cls_1" style="width:100px" id="filter_div_close">Close</button>

	<!-- <button class="btn btn-sm btn-dark" style="width:100px" id="listview_filter_div_close"  style="display: none;">Close</button> -->

</div>


<script type="text/javascript">
	$(document).ready(function() {
		display_filter_option_onurl();
	});

	function display_filter_option_onurl() {
		var value = window.location.href.substring(
			window.location.href.lastIndexOf("/") + 1
		);
		
		if (value == "document_view" || value == "document_unclassified") {
			$(".document_filter_element").show();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".dashboard_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".report_filter_element").hide();
			$(".site_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();

		} else if (value == "access_control" || value == "role1") { 
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".user_master_filter_element").show();

		} else if (value == "cluster_master") {
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".audit_form_filter").hide();
			$(".user_filter_element").hide();
			$(".cluster_master_filter_element").show();

		} else if (value == "checklist_master") {
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".checklist_master_filter_element").show();

		} else if (value == "vendor_list_view" || value == "my_team") {
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".audit_form_filter").hide();
			$(".vendor_master_filter_element").hide();
			$(".user_filter_element").hide();
			$(".vendor_user_filter_element").show();

		} else if (value == "client_user") {
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".audit_form_filter").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".user_filter_element").show();

		} else if (value == "site_main_list_view") {
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".site_master_filter_element").show();

		} else if (value == "vendor_main_list_view") {
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".vendor_master_filter_element").show();

		} else if (value == "client_main_list_view") {
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".client_master_filter_element").show();

		} else if (value == "account_main_list_view") {
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".account_master_filter_element").show();

		} else if (value == "dashboard_view") {
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".user_filter_element").hide();
			$(".dashboard_filter_element").show();

		} else if (value == "calendar_tab") {
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".report_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".calendar_filter_element").show();

		} else if (value == "report_table") {
			$(".report_filter_element").show();
			$(".region_wise_filter_element").show();
			$(".vendor_compliance_filter_element").show();
			$(".vendor_original_filter_element").show();
			$(".vendor_remediation_filter_element").show();
			$(".client_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();

			$(".calendar_filter_element").hide();
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();

			$(".act_wsie_filter_element").hide();
		} else if (value == "report") {
			$(".report_filter_element").show();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_master_filter_element").hide();

			$(".calendar_filter_element").hide();
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(". region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();

			$(".cluster_master_filter_element").hide();
			$(".user_master_filter_element").hide();
		} else if (value == "site_list") {
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".client_master_filter_element").hide();

			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();

			$(".site_filter_element").show();
		} else if (value == "audit_history") {
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();

			$(".audit_filter_element").show();
		} else if (value == "audit_table") {
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();

			$(".audit_table_filter_element").show();
		} else if (
			value == "licence_tracker_document" ||
			value == "my_license_tracker" ||
			value == "my_license"
		) {
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".report_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".filter_div_export_cls_1").hide();
			$(".vendor_team_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".vendor_license_filter_element").show();

		} else if (value == "list_view") {
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".report_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".filter_div_export_cls_1").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".client_user_filter_element").show();

		} else if (value == "menu" || value == "checklist") {
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".document_filter_element").hide();
			$(".site_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".checklist_menu_filter_element").show();

		} else if (value == "site_report") {
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".client_master_filter_element").hide();
			$(".document_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".client_site_filter_element").show();

		} else if (value == "act_wise") {
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".client_master_filter_element").hide();
			$(".document_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".act_wsie_filter_element").show();

		} else if (value == "audit_form") {
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".cluster_list_view_filter").hide();
			$(".client_master_filter_element").hide();
			$(".document_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".audit_form_filter").show();

		} else if (value == "cluster_list_view") {
			$(".report_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".dashboard_filter_element").hide();
			$(".default_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".document_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".audit_form_filter").hide();
			$(".user_filter_element").hide();
			$(".cluster_list_view_filter").show();

		} else {
			$(".document_filter_element").hide();
			$(".dashboard_filter_element").hide();
			$(".calendar_filter_element").hide();
			$(".client_master_filter_element").hide();
			$(".report_filter_element").hide();
			$(".site_filter_element").hide();
			$(".audit_table_filter_element").hide();
			$(".audit_filter_element").hide();
			$(".vendor_team_filter_element").hide();
			$(".vendor_license_filter_element").hide();
			$(".checklist_menu_filter_element").hide();
			$(".client_site_filter_element").hide();
			$(".client_user_filter_element").hide();
			$(".report_filter_element").hide();
			$(".region_wise_filter_element").hide();
			$(".vendor_compliance_filter_element").hide();
			$(".vendor_original_filter_element").hide();
			$(".vendor_remediation_filter_element").hide();
			$(".account_master_filter_element").hide();
			$(".site_master_filter_element").hide();
			$(".vendor_master_filter_element").hide();
			$(".vendor_user_filter_element").hide();
			$(".cluster_master_filter_element").hide();
			$(".checklist_master_filter_element").hide();
			$(".user_master_filter_element").hide();
			$(".act_wsie_filter_element").hide();
			$(".user_filter_element").hide();
			$(".cluster_list_view_filter").hide();
		}
	}

	// function onlyOne(checkbox, month_id) {
	// 	var checkboxes = document.getElementsByName('dashboard_filter_month')
	// 	checkboxes.forEach((item) => {
	// 		if (item !== checkbox) item.checked = false
	// 	});
	// 	if (month_id == "1") {
	// 		$(".widget_1_span").html("(Jan)");

	// 	} else if (month_id == "2") {
	// 		$(".widget_1_span").html("(Feb)");
	// 	} else if (month_id == "3") {
	// 		$(".widget_1_span").html("(Mar)");
	// 	} else if (month_id == "4") {
	// 		$(".widget_1_span").html("(Apr)");
	// 	} else if (month_id == "5") {
	// 		$(".widget_1_span").html("(May)");
	// 	} else if (month_id == "6") {
	// 		$(".widget_1_span").html("(Jun)");
	// 	} else if (month_id == "7") {
	// 		$(".widget_1_span").html("(Jul)");
	// 	} else if (month_id == "8") {
	// 		$(".widget_1_span").html("(Aug)");
	// 	} else if (month_id == "9") {
	// 		$(".widget_1_span").html("(Sep)");
	// 	} else if (month_id == "10") {
	// 		$(".widget_1_span").html("(Oct)");
	// 	} else if (month_id == "11") {
	// 		$(".widget_1_span").html("(Nov)");
	// 	} else if (month_id == "12") {
	// 		$(".widget_1_span").html("(Dec)");
	// 	} else if (month_id == "13") {
	// 		// $(".widget_1_span").html("(Dec)");
	// 		$(".vendor_container").show();
	// 		$(".site_container").hide();
	// 	} else if (month_id == "14") {
	// 		// $(".widget_1_span").html("(Dec)");
	// 		$(".vendor_container").hide();
	// 		$(".site_container").show();
	// 	}
	// }
</script>