<!-- theme css -->
<?php $this->load->view('header'); ?>
<?php $this->load->view('top'); ?>
<?php $this->load->view('vertical_navbar'); ?>
<style>
    .theme1 {
        height: 100px;
        flex-basis: 100px !important;
        border: 0.2px solid #c5c5c5;
    }

    .option-1 {
        background-color: #007bb5
    }

    .option-2 {
        background-color: #c11f36
    }

    .option-3 {
        background-color: #ff9928
    }

    .option-4 {
        background-color: #4b4bb2
    }

    .option-5 {
        background-color: whitesmoke
    }

    .sidebar_theme {
        width: 30px;
        height: 79px
    }

    .navbar_theme {
        height: 20px;
        width: 100%
    }

    .option-10 {
        background: #5d5c5c;

    }

    .option-10_sidebar {
        background-color: #5d5c5c;
    }

    .option-9 {
        background: #e91e63;
    }

    .option-9-sidebaar {
        background-color: #5d5c5c;
    }

    @media only screen and (max-width:824px) {
        .theme1{
            margin-top: 10px;
        }
    }
</style>

<div style="width:103% mt-5">
    <div class="col card-yu">
        <br>
        <div class="ml-2 mb-3">
            <h4 class="card-title mb-0" style="font-size:18px;display:inline">Themes</h4>
            <p>Pick a theme and pattern to apply for the background.</p>
            <div>
                <div class="d-flex justify-content-around flex-wrap">
                    <a class="mr-3 theme1" onclick="handleUpdate('#007bb5','#262c32','#fff','#fff','#007bb5');">
                        <div class="option-1 navbar_theme"></div>
                        <div class="bg-dark sidebar_theme"></div>
                    </a>
                    <a class="mr-3 theme1" onclick="handleUpdate('#c11f36','#262c32','#fff','#fff','#c11f36');">
                        <div class="option-2 navbar_theme"></div>
                        <div class="bg-dark sidebar_theme"></div>
                    </a>
                    <a class="mr-3 theme1" onclick="handleUpdate('#ff9928','#262c32','#fff','#fff','#ff9928');">
                        <div class="option-3 navbar_theme"></div>
                        <div class="bg-dark sidebar_theme"></div>
                    </a>
                    <a class="mr-3 theme1" onclick="handleUpdate('#4b4bb2','#262c32','#fff','#fff','#4b4bb2');">
                        <div class="option-4 navbar_theme"></div>
                        <div class="bg-dark sidebar_theme"></div>
                    </a>
                    <a class="mr-3 theme1" onclick="handleUpdate('#007bb5','#262c32','#fff','#fff','#007bb5');">
                        <div class="option-1 navbar_theme"></div>
                        <div class="bg-dark sidebar_theme"></div>
                    </a>
                    <a class="mr-3 theme1" onclick="handleUpdate('#e91e63','#5d5c5c','#fff','#fff','#e91e63');">
                        <div class="option-9 navbar_theme"></div>
                        <div class=" sidebar_theme option-9-sidebaar"></div>
                    </a>
                </div>
            </div>
            <div class="mt-5">
                <div class="d-flex justify-content-around flex-wrap">
                    <a class="mr-3 theme1" onclick="handleUpdate('#5d5c5c','#5d5c5c','#fff','#fff','#fff');">
                        <div class="option-10 navbar_theme"></div>
                        <div class=" option-10_sidebar sidebar_theme"></div>
                    </a>
                    <a class="mr-3 theme1" onclick="handleUpdate('#ff9928','#fff','#6b6f82','#fff','#ff9928')">
                        <div class="option-3 navbar_theme"></div>
                        <div class="option-5 sidebar_theme"></div>
                    </a>
                    <a class="mr-3 theme1" onclick="handleUpdate('#4b4bb2','#fff','#6b6f82','#fff','#4b4bb2');">
                        <div class="option-4 navbar_theme"></div>
                        <div class="option-5 sidebar_theme"></div>
                    </a>
                    <a class="mr-3 theme1" onclick="handleUpdate('#007bb5','#fff','#6b6f82','#fff','#007bb5')">
                        <div class="option-1 navbar_theme"></div>
                        <div class="option-5 sidebar_theme"></div>
                    </a>
                    <a class="mr-3 theme1" onclick="handleUpdate('#c11f36','#fff','#6b6f82','#fff','#c11f36')">
                        <div class="option-2 navbar_theme"></div>
                        <div class="option-5 sidebar_theme"></div>
                    </a>
                    <a class="mr-3 theme1" onclick="handleUpdate('#fff','#fff','#6b6f82','#6b6f82','#00517cf5');" data-toggle="tooltip" title="Default">
                        <div class="option-5 navbar_theme"></div>
                        <div class="option-5 sidebar_theme"></div>
                    </a>

                </div>

            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>