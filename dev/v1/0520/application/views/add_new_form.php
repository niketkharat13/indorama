    <style>
        .mt {
            margin-top: 5px
        }
    </style>
    <div class="container">
        <form id="add_new_record_form">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="row mt">
                        <div class="col-4">
                            <label for="name_field_first" style="margin-top:10px">First Name</label>
                        </div>
                        <input type="text" class="form-control col-8" name="" id="name_field_first">
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="row mt">
                        <div class="col-4">
                            <label for="name_field_last" style="margin-top:10px">Last Name</label>
                        </div>
                        <input type="text" class="form-control col-8" name="" id="name_field_last">
                    </div>
                </div>
            </div>
            <div class="row ">

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="row mt">
                        <div class="col-4">
                            <label for="email_field" style="margin-top:10px">Email ID</label>
                        </div>
                        <input type="text" class="form-control col-8" name="" id="email_field">
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="row mt">
                        <div class="col-4">
                            <label for="username_ip" style="margin-top:10px">Username</label>
                        </div>
                        <input type="text" class="form-control col-8" name="" id="username_ip">
                    </div>
                </div>
            </div>
            <div class="row  ">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="row mt">
                        <div class="col-4">
                            <label for="contact_field" style="margin-top:10px">Contact</label>
                        </div>
                        <input type="text" class="form-control col-8" name="" id="contact_field">
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="row mt">
                        <div class="col-4">
                            <label for="add_field" style="margin-top:10px">Address</label>
                        </div>
                        <textarea type="text" class="form-control col-8" name="" id="add_field" rowspan=10></textarea>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="row mt">
                        <div class="col-4" id="state-select">
                            <label for="state" style="margin-top:10px">State</label>
                        </div>
                        <div class="col-8 search_dropdown_div" style="margin-left: -1px;">
                            <select name="Select" class="selectivity_dropdown" id="state">
                                <option value="state-1" selected>Maharashtra
                                </option>
                                <option value="state-2">-- State Master --</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="row mt">
                        <div class="col-4">
                            <label for="city" style="margin-top:10px">City</label>
                        </div>
                        <div class="search_dropdown_div col-8" style="margin-left: -1px;">
                            <select name="Select" class="selectivity_dropdown" id="city">
                                <option value="location-1" selected>Mumbai
                                </option>
                                <option value="location-2"> -- City Master --</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="row mt">
                        <div class="col-4">
                            <label for="region" style="margin-top:10px">Region</label>
                        </div>
                        <div class="col-8 search_dropdown_div" style="margin-left: -1px;">
                            <select name="Select" class="selectivity_dropdown" id="region">
                                <option value="region-1" selected>East
                                </option>
                                <option value="region-2">West</option>
                                <option value="region-3">North</option>
                                <option value="region-4">South</option>
                            </select>

                        </div>

                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="row mt">
                        <div class="col-4">
                            <label for="country" style="margin-top:10px">Country</label>
                        </div>
                        <div class="search_dropdown_div col-8" style="margin-left: -1px;">
                            <select name="Select" class="selectivity_dropdown" id="country">
                                <option value="region-1">Select Country
                                </option>
                                <option value="region-2" selected>India</option>
                                <option value="region-3"> -- Country Master --</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>