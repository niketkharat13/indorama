$(document).ready(function(){	
		var url = $(location).attr('href');
		parts = url.split("/");
	    last_part = parts[parts.length-1];
	    
		var cookie_name=last_part;

		
	    if (typeof $.cookie(cookie_name) === 'undefined'){
	        var jsonEmployees = JSON.stringify({}); 
	        $.cookie(cookie_name, jsonEmployees);
	    }else{
	        var empString = $.cookie(cookie_name);
	        
	        //converting "empString" to an array. 
	        var empArr = $.parseJSON(empString);

	        $.each(empArr, function( key, value ) {
				if($('#'+key).is(":visible"))
				{
					$('#'+key).val(value);
				}
	        });
	    }  

		$(':input').change(function(){
	        var element_id=this.id;
	        var element_value=$('#'+element_id).val();
	    	reset_error_data(element_id);
	    	/*var required_fields = ["sd_cm_name","sd_supp_gln","sd_supp_legal_address","sd_fiscal_no","ad_article_no","ad_supp_no","sl_supplier_name","sl_email_address","sl_contact_person"];
            var zipcode_fields = ["sd_legal_address_zip","sd_postal_address_zip","sd_address_consignee_zip"];
            var telephone_fields = ["bd_supp_ceo_telephone","bd_supp_representative_telephone","bd_financial_info_telephone"];
            var email_fields = ["bd_supp_ceo_email","bd_supp_representative_email","bd_financial_info_email","bd_email_order"];
            var password = ["inputPassword"];*/

            if(element_value == "" && (element_id == "sd_cm_name" || element_id == "sd_supp_gln" || element_id == "sd_supp_legal_address" || element_id == "sd_fiscal_no" || element_id == "ad_article_no" || element_id == "ad_supp_no" || element_id == "sl_supplier_name" || element_id == "sl_email_address" || element_id == "sl_contact_person"))
	        {
	            check_required(element_value,element_id);
	        }

	        //if(jQuery.inArray("'"+element_id+"'", zipcode_fields))
	        if(element_id == "sd_legal_address_zip" || element_id == "sd_postal_address_zip" || element_id == "sd_address_consignee_zip")
	        {
	        	if(element_value.length != 6)
	        	{
	        		zipcode(element_value,element_id);
	        	}
	        }

	        if(element_id == "bd_supp_ceo_telephone" || element_id == "bd_supp_representative_telephone" || element_id == "bd_financial_info_telephone")
	        {
	        	if (element_value.length != 10 ) 
	        	{
	            	telephone(element_value,element_id);
	        	}
	        }

	        if(element_id == "bd_supp_ceo_email" || element_id == "bd_supp_representative_email"  || element_id == "bd_financial_info_email" || element_id == "bd_email_order")
	        {
	            isEmail(element_value,element_id);
	        }

	        var empString = $.cookie(cookie_name);

	        var empArr = $.parseJSON(empString);

	        empArr[this.id] = element_value;

	        //converting array into json string
	        var jsonEmployees = JSON.stringify(empArr);  

	        //storing it in a cookie 
	        $.cookie(cookie_name, jsonEmployees);
	        
	    });
	});

	function check_required(element_value,element_id){
		$("#error_div_reqired_"+element_id).remove();
        $("#"+element_id).css('border-color', 'red');
        $("#"+element_id).after("<div id='error_div_reqired_"+element_id+"' style='color:red;'>This Field is Required</div>");
    }

    function isEmail(element_value,element_id) {
	    return /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(element_value);
	    $("#error_div_email_"+element_id).remove();
        $("#"+element_id).css('border-color', 'red');
        $("#"+element_id).after("<div id='error_div_email_"+element_id+"' style='color:red;'>Please enter valid email address</div>");
     }

    function telephone(element_value,element_id){
       	$("#error_div_telephone_"+element_id).remove();
        $("#"+element_id).css('border-color', 'red');
        $("#"+element_id).after("<div id='error_div_telephone_"+element_id+"' style='color:red;'>Please enter valid Phone number</div>");
    }

    function zipcode(element_value,element_id){
      	$("#error_div_zipcode_"+element_id).remove();
        $("#"+element_id).css('border-color', 'red');
        $("#"+element_id).after("<div id='error_div_zipcode_"+element_id+"' style='color:red;'>Please enter valid zipcode</div>");
    }

    function reset_form_cookie()
    {
    	var url = $(location).attr('href');
		//alert(url);
		parts = url.split("/");
	    last_part = parts[parts.length-1];
	    
		var cookie_name=last_part;
		//alert(cookie_name);
        //$.cookie(cookie_name, null);
        $.removeCookie(cookie_name, null , {path:'/'});
    }

    function submit_validation(url)
    {
    	var check_required_var = true;
    	var zipcode_var = true;
    	var telephone_var = true;
    	var isEmail_var = true;
    	
    	reset_error_data();

    	/*
		$(":input").each(function(){
		    var element_id = $(this).attr("id"); 
		    var element_value = $(this).val(); 
		
	       		if(element_value == "" && (element_id == "sd_cm_name" || element_id == "sd_supp_gln" || element_id == "sd_supp_legal_address" || element_id == "sd_fiscal_no" || element_id == "ad_article_no" || element_id == "ad_supp_no" || element_id == "sl_supplier_name" || element_id == "sl_email_address" || element_id == "sl_contact_person"))
		        {
		            check_required_var = check_required(element_value,element_id);
		        }

		        //if(jQuery.inArray("'"+element_id+"'", zipcode_fields))
		        if(element_id == "sd_legal_address_zip" || element_id == "sd_postal_address_zip" || element_id == "sd_address_consignee_zip")
		        {
		        	if(element_value.length != 6)
		        	{
		        		zipcode_var = zipcode(element_value,element_id);
		        	}
		        }

		        if(element_id == "bd_supp_ceo_telephone" || element_id == "bd_supp_representative_telephone" || element_id == "bd_financial_info_telephone")
		        {
		        	if (element_value.length != 10 ) 
		        	{
		            	telephone_var = telephone(element_value,element_id);
		        	}
		        }

		        if(element_id == "bd_supp_ceo_email" || element_id == "bd_supp_representative_email"  || element_id == "bd_financial_info_email" || element_id == "bd_email_order")
		        {
		            isEmail_var = isEmail(element_value,element_id);
		        }
	        
	    });
        if(check_required_var && zipcode_var && telephone_var && isEmail_var){
            window.location.href = url;
        }else{
           toastr.error("Please Enter Valid Input", 'Success');
        }*/
		window.location.href = url;
    }

    function reset_error_data(element_id)
    {
    	if(element_id == undefined){
	    	$(":input").each(function(){
				var element_id = $(this).attr("id"); 
				if($("#"+element_id).length){
					$("#error_div_reqired_"+element_id).remove();
					$("#error_div_email_"+element_id).remove();
					$("#error_div_telephone_"+element_id).remove();
					$("#error_div_zipcode_"+element_id).remove();
					$("#"+element_id).removeAttr('style');
				}
	    	});
	    }else{
	    	$("#error_div_reqired_"+element_id).remove();
			$("#error_div_email_"+element_id).remove();
			$("#error_div_telephone_"+element_id).remove();
			$("#error_div_zipcode_"+element_id).remove();
			$("#"+element_id).removeAttr('style');
	    }
    }