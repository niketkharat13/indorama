	$('.selectivity_dropdown').selectivity({
		allowClear: true,
		placeholder: 'Select'
	});
	$(".selectivity_dropdown").selectivity('clear');


	$('.modal').click(function (evt) {
		// $('.selectivity_dropdown').selectivity("close");
		$(".selectivity_dropdown").each(function () {
			var id = $(this).attr("id");
			if ($(this).next(".selectivity-dropdown").length) {
				$('#' + id).selectivity("close");
			}
		});
		$(".multiple-input").each(function () {
			var id = $(this).attr("id");
			if ($(this).next(".selectivity-dropdown").length) {
				$('#' + id).selectivity("close");
			}
		});
	});

	function appended_Selectivity(id) {
		$('#' + id).selectivity({
			allowClear: true,
			placeholder: 'Select'
		});
		$("#" + id).selectivity('clear');
	}

	function month_dropdown_add_selectivity() {

		$('#yr_dropdown_regular').selectivity({
			allowClear: true,
			placeholder: 'Select'
		});

		$('#qtr_dropdown_regular').selectivity({
			allowClear: true,
			placeholder: 'Select'
		});

		$('#half_yr_dropdown_regular').selectivity({
			allowClear: true,
			placeholder: 'Select'
		});

		$('#year_dropdown_regular').selectivity({
			allowClear: true,
			placeholder: 'Select'
		});

		$('#bi_month_dropdown_regular').selectivity({
			allowClear: true,
			placeholder: 'Select'
		});



		// dropdown for cluster
		$('#yr_dropdown_regular_cluster').selectivity({
			allowClear: true,
			placeholder: 'Select'
		});

		$('#qtr_dropdown_regular_cluster').selectivity({
			allowClear: true,
			placeholder: 'Select'
		});

		$('#half_yr_dropdown_regular_cluster').selectivity({
			allowClear: true,
			placeholder: 'Select'
		});

		$('#year_dropdown_regular_cluster').selectivity({
			allowClear: true,
			placeholder: 'Select'
		});

		$('#bi_month_dropdown_regular_cluster').selectivity({
			allowClear: true,
			placeholder: 'Select'
		});

	// dropdown for Desk Audit
		$('#yr_dropdown_desk').selectivity({
			allowClear: true,
			placeholder: 'Select'
		});

		$('#qtr_dropdown_desk').selectivity({
			allowClear: true,
			placeholder: 'Select'
		});

		$('#half_yr_dropdown_desk').selectivity({
			allowClear: true,
			placeholder: 'Select'
		});

		$('#year_dropdown_desk').selectivity({
			allowClear: true,
			placeholder: 'Select'
		});

		$('#bi_month_dropdown_desk').selectivity({
			allowClear: true,
			placeholder: 'Select'
		});
	}

	// drpdown for cluster form
	$('#active_employee').selectivity({
		allowClear: true,
		placeholder: 'Select Active Employee'
	});

	$("#active_employee").selectivity('clear');

	// drpdown for audit form
	$('#active_employee_audit').selectivity({
		allowClear: true,
		placeholder: 'Select Active Employee'
	});

	$("#active_employee_audit").selectivity('clear');

	$('#select_event_dropdown').selectivity({
		allowClear: true,
		showSearchInputInDropdown: false,
		placeholder: 'Select'
	});

	$('#notification_dropdown').selectivity({
		allowClear: true,
		showSearchInputInDropdown: false,
		placeholder: 'Select'
	});

	$('#multi_select_account_for_planned_audit').selectivity({
		items: ['JP Morgan Services India Pvt Ltd', 'JLL', 'Accenture','Lehman Brothers'],
		multiple: true,
		tokenSeparators: [' '],
		placeholder: 'Select'

	});

	$('#multi_select_account_for_cluster_audit').selectivity({
		items: ['JP Morgan Services India Pvt Ltd', 'JLL', 'Accenture','Lehman Brothers'],
		multiple: true,
		tokenSeparators: [' '],
		placeholder: 'Select',
		allowClear:true

	});

	$("#select_event_dropdown").selectivity('value', 'annual_audit_selected');

	$("#auditor_edit_modal_regular").selectivity('value', 'auditor_2');

	$("#notification_dropdown").selectivity('value', 'notification_1');
	
	$("#cluster_master_add_client_edit").selectivity('value', 'client_1');

	$("#cluster_master_add_vendor_edit").selectivity('value', 'vendor_1');

	$("#cluster_master_edit_region").selectivity('value', 'region_1');

	$("#cluster_master_edit_state").selectivity('value', '4');
	
	// $("#multi_select_account_edit").selectivity('value', 'account_1');

	$("#cluster_master_edit_frequency").selectivity('value', 'monthly');

	$("#cluster_master_edit_audit_type").selectivity('value', 'audit_type_1');

	$("#cluster_master_edit_auditor").selectivity('value', 'type_2');

	// user access
	$("#role_user_edit").selectivity('value', 'value1');

	
	$("#state_user_edit").selectivity('value', 'state-1');
	$("#city_user_edit").selectivity('value', 'location-1');
	$("#region_user_edit").selectivity('value', 'region-1');
	
	$("#country_user_edit").selectivity('value', '2');
	$("#department_user_edit").selectivity('value', 'department-4');
	$("#reporting_user_edit").selectivity('value', 'value-1');
	$("#designation_user_edit").selectivity('value', 'value-1');

	$("#role_status_edit").selectivity('value', '1');
	$("#department_edit_name").selectivity('value', 'department-1');
	$("#department_status_edit").selectivity('value', '1');


	
	
	// Planned Audit Auditor Preselection
	$("#auditor_edit_modal_physical_edit").selectivity('value', 'auditor_2');
	$("#auditor_edit_modal_annual_edit").selectivity('value', 'auditor_3');
	$("#auditor_edit_modal_desk").selectivity('value', 'auditor_2');
	$("#auditor_edit_modal_planned_cluster").selectivity('value', 'auditor_3');

	// Calendar Event modal
	$("#auditor_edit_modal_cluster").selectivity('value', 'auditor_3');
	$("#location_edit_modal_planned_cluster").selectivity('value', 'site_2');
	$("#audit_manager_dropdown").selectivity('value', 'auditor_2');
	
	// $("#multi_select_account_edit").selectivity('value', [4,1]);

	// $('.modal').click(function (evt) {
	// 	$('#select_event_dropdown').selectivity("close");
	// });

	// site vendor mapping
	$("#vendor_mapping_site_edit").selectivity('value','site_1');
	$("#map_vendor_dropdown_edit").selectivity('value','vendor-1');
	$("#add_vendor_type_edit").selectivity('value','facility_manafment');
	$("#add_vendor_contract_type_edit").selectivity('value','direct');
	$("#add_vendor_group_edit").selectivity('value','group1');

	// Use this for Refrence
	// to clear selected option from sekect dropdown
	// $("#select_event_dropdown").selectivity('clear');


	(function (window, document, $) {
		'use strict';

		/* global $ */

		function escape(string) {
			return string ? String(string).replace(/[&<>"']/g, function (match) {
				return {
					'&': '&amp;',
					'<': '&lt;',
					'>': '&gt;',
					'"': '&quot;',
					'\'': '&#39;'
				} [match];
			}) : '';
		}



		// // Select more than one options
		// $('.multiple-input').selectivity({
		// 	multiple: true,
		// 	allowClear: true,
		// 	placeholder: 'Select',
		// 	// query: queryFunction
		// });
		// $(".multiple-input").selectivity('clear');


		// Multiple Select Box
		$('.multiple-select-box').selectivity();

	})(window, document, jQuery);