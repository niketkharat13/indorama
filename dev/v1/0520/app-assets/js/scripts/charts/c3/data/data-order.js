/*=========================================================================================
    File Name: data order.js
    Description: c3 data order chart
    ----------------------------------------------------------------------------------------
    Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
   Version: 3.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

// Data Order chart
// ------------------------------
$(window).on("load", function () {

    // Callback that creates and populates a data table, instantiates the data order chart, passes in the data and draws it.
    var dataOrder = c3.generate({
        bindto: '#data-order',
        size: {
            height: 300
        },
        color: {
            pattern: ['#99B898', '#FECEA8', '#FF847C', '#E84A5F', '#2A363B']
        },

        // Create the data table.
        data: {
            columns: [
                ['Yet to Start', 'TL Review', 'Submitted to TL','Sent to RO User','RO User Approved','Completed'],
                ['Yet to Start', 130, 200, 320, 400, 530, 750],
                ['TL Review', 130, 10, 130, 200, 150, 250],
                ['Submitted to TL', 130, 50, 10, 200, 250, 150],
                ['Sent to RO User', 1200, 1300, 1450, 1600, 1520, 1820],
                ['RO User Approved', 200, 300, 450, 600, 520, 820],
                ['Completed', 200, 300, 450, 600, 520, 820],
            ],
            type: 'bar',
            groups: [
                ['Yet to Start', 'TL Review', 'Submitted to TL','Sent to RO User','RO User Approved','Completed']
            ],
            order: 'desc' // stack order by sum of values descendantly. this is default.
            //      order: 'asc'  // stack order by sum of values ascendantly.
            //      order: null   // stack order by data definition.
        },
        bar: { width: { ratio: 0.5 } },
        grid: {
            x: {
                show: true
            }
        }
    });

   
    // Resize chart on sidebar width change
    $(".menu-toggle").on('click', function () {
        dataOrder.resize();
    });
});