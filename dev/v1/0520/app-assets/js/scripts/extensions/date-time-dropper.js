/*=========================================================================================
    File Name: date-time-dropper.js
    Description: Datepicker and Timepicker plugins based on jQuery
    ----------------------------------------------------------------------------------------
    Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
   Version: 3.0
    Author: GeeksLabs
    Author URL: http://www.themeforest.net/user/geekslabs
==========================================================================================*/
$(document).ready(function () {

    /********************************************
     *               Date Dropper                *
     ********************************************/

    // Options
    // Animate
    $('#animate').dateDropper({
        dropWidth: 200
    });

    // Init Animation
    $('#init_animation').dateDropper({
        dropWidth: 200,
        init_animation: 'bounce'
    });
    $('#format_1').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#desk_format_1').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#reschedule_Start_date').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#user_dob').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });


    $('#se_date_of_exp').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#se_date_of_reg').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#pf_Date').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#bocw_Date_exp').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#bocw_Date_reg').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#pt_Date').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#esi_Date').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#clra_date_reg').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#clra_date_exp').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#format_3').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#esi_Date').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#pf_Date').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    
    $('#shop_Date').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
     $('#shop_Date_exp').dateDropper({
         dropWidth: 200,
         format: 'j F,Y'
     });
    $('#bocw_Date').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#bocw_Date_exp').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#clra_Date_exp').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#clra_Date').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    
    $('#date_birth_field').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });

    // final audit
    $('#event_date_edit_final').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });

    $('#event_time_edit_final').timeDropper({
        meridians: true
    });
    
    

    // Add Account Module Date Widgets
    // Format
    $('#yr_dropdown_account_modal').dateDropper({
        dropWidth: 200,
        format: 'Y',
        minYear: '2010'
    });
    $('#month_dropdown_account_modal').dateDropper({
        dropWidth: 200,
        format: 'F'
    });

    $('#month_selection_prelimenary').dateDropper({
        dropWidth: 200,
        format: 'F,Y'
    });

    $('#retrival_month_selection_start').dateDropper({
        dropWidth: 200,
        format: 'F,Y'
    });

    $('#retrival_month_selection_end').dateDropper({
        dropWidth: 200,
        format: 'F,Y'
    });

    

    //END Add Account Module Date Widgets

    // Document Upload Module Date Widgets
    // Format
    $('#footer_multiupload_year').dateDropper({
        dropWidth: 200,
        format: 'Y',
        minYear: '2010'
    });
    $('#footer_multiupload_month').dateDropper({
        dropWidth: 200,
        format: 'F'
    });

    $('#modal_multiupload_year').dateDropper({
        dropWidth: 200,
        format: 'Y',
        minYear: '2010'
    });
    $('#modal_multiupload_month').dateDropper({
        dropWidth: 200,
        format: 'F'
    });
    //END Document Upload Module Date Widgets

    // calendar add event
    $('#cluster_month_selection').dateDropper({
        dropWidth: 200,
        format: 'F,Y'
    });

    $('#imt_act_yr').dateDropper({
        dropWidth: 200,
        format: 'Y',
        minYear: '2010'
    });
    $('#audit_end_date_selection').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });

    $('#imt_date_selection').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });

    $('#event_start_date_edit').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });


    // Format

    // annual edit modal
    $('#event_start_date_edit_annual').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });

    $('#event_start_date_edit_annual_cluster').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });

    $('#event_start_date_edit_annual_desk').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#event_start_time_edit_annual').timeDropper({
        meridians: true
    });
    $('#event_start_time_edit_annual_physical').timeDropper({
        meridians: true
    });

    $('#event_start_time_edit_annual_desk').timeDropper({
        meridians: true
    });
    $('#event_start_time_edit_annual_cluster').timeDropper({
        meridians: true
    });
    // regular edit modal
    $('#event_start_date_edit_regular').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });

    $('#event_start_time_edit_regular').timeDropper({
        meridians: true
    });

    // cluster edit modal
    $('#event_start_time_edit_cluster').timeDropper({
        meridians: true
    });

    $('#event_start_date_edit_annual_physical').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#event_start_date_edit_cluster').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });


    $('#holiday_date_selection').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });


    $('#event_start_time_edit').timeDropper({
        meridians: true
    });

    $('#imp_act_time').timeDropper({
        meridians: true
    });

    $('#imp_act_time_edit_modal').timeDropper({
        meridians: true
    });



    $('#imp_act_date_edit_modal').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });



    $('#client_date_slot_1').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });


    $('#client_time_slot_2').timeDropper({
        meridians: true
    });
    $('#client_date_slot_2').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });


    $('#client_time_slot_1').timeDropper({
        meridians: true
    });


    $('#audit_manager_time').timeDropper({
        meridians: true
    });
    $('#audit_manager_date').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });

    $('#audit_manager_time_2').timeDropper({
        meridians: true
    });
    $('#audit_manager_date_2').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });



    $('#format_4').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#format_5').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });
    $('#format_6').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });

    $('#format_7').dateDropper({
        dropWidth: 200,
        format: 'j F,Y'
    });

    $('#format_2').dateDropper({
        dropWidth: 200,
        format: 'j l, F, Y'
    });

    // Lang
    $('#lang').dateDropper({
        dropWidth: 200,
        lang: 'ar' // Arabic
    });

    // Lock
    $('#lock').dateDropper({
        dropWidth: 200,
        lock: 'from' // To select date after today, 'to' to select date before today
    });

    // Max Year
    $('#maxYear').dateDropper({
        dropWidth: 200,
        maxYear: '2020'
    });

    // Min Year
    $('#minYear').dateDropper({
        dropWidth: 200,
        minYear: '2001'
    });

    // Years Range
    $('#yearsRange').dateDropper({
        dropWidth: 200,
        yearsRange: '5'
    });


    // Styles

    // Drop Primary Color
    $('#dropPrimaryColor').dateDropper({
        dropWidth: 200,
        dropPrimaryColor: '#F6BB42',
        dropBorder: '1px solid #F6BB42'
    });

    // Drop Text Color
    $('#dropTextColor').dateDropper({
        dropWidth: 200,
        dropPrimaryColor: '#10617E',
        dropBorder: '1px solid #10617E',
        dropBackgroundColor: '#23b1e3',
        dropTextColor: '#FFF'
    });

    // Drop Background Color
    $('#dropBackgroundColor').dateDropper({
        dropWidth: 200,
        dropBackgroundColor: '#ACDAEC',
    });

    // Drop Border
    $('#dropBorder').dateDropper({
        dropWidth: 200,
        dropPrimaryColor: '#2fb594',
        dropBorder: '1px solid #2dad8d',
    });

    // Drop Border Radius
    $('#dropBorderRadius').dateDropper({
        dropWidth: 200,
        dropPrimaryColor: '#e8273a',
        dropBorder: '1px solid #e71e32',
        dropBorderRadius: '0'
    });

    // Drop Shadow
    $('#dropShadow').dateDropper({
        dropWidth: 200,
        dropPrimaryColor: '#fa4420',
        dropBorder: '1px solid #fa4420',
        dropBorderRadius: '20',
        dropShadow: '0 0 10px 0 rgba(250, 68, 32, 0.6)'
    });

    // Drop Width
    $('#dropWidth').dateDropper({
        dropWidth: 250
    });

    // Drop Text Weight
    $('#dropTextWeight').dateDropper({
        dropWidth: 200,
        dropTextWeight: 'normal'
    });

    $('#dropTextWeight_2').dateDropper({
        dropWidth: 200,
        dropTextWeight: 'normal'
    });


    /********************************************
     *               Time Dropper                *
     ********************************************/

    // Options


    // Auto Switch

    $('#reschedule_Start_date_time').timeDropper();
    $('#autoswitch').timeDropper();
    $('#desk_autoswitch').timeDropper();
    $('#autoswitch_2').timeDropper();
    $('#autoswitch_3').timeDropper();
    $('#autoswitch_4').timeDropper();


    // Meridians
    $('#meridians').timeDropper({
        meridians: true
    });

    // Format
    $('#timeformat').timeDropper({
        format: 'HH:mm A'
    });

    // Mousewheel
    $('#mousewheel').timeDropper({
        mousewheel: true
    });

    // Init Animation
    $('#time_init_animation').timeDropper({
        init_animation: 'dropDown',
        meridians: true
    });

    // Set Current Time
    $('#setCurrentTime').timeDropper();



    // Styles


    // Primary Color
    $('#primaryColor').timeDropper({
        primaryColor: '#2fb594',
        borderColor: '#2fb594'
    });

    // Text Color
    $('#textColor').timeDropper({
        primaryColor: '#2fb594',
        textColor: '#e8273a'
    });

    // Background Color
    $('#backgroundColor').timeDropper({
        primaryColor: '#FFF',
        backgroundColor: '#fa4420',
        borderColor: '#781602',
        textColor: '#781602'
    });

    // Border Color
    $('#borderColor').timeDropper({
        primaryColor: '#FFF',
        backgroundColor: '#23b1e3',
        borderColor: '#FFF',
        textColor: '#FFF'
    });

});