
//  datepicker js-----------------------------
    $(document).ready(function() {
        //cb(start, end);
        get_dashboard_data();
        //set_dashboard_data();
    });
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        // $('.shawCalRanges').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#date_range').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        get_dashboard_data(start.format('YYYY-MM-DD') + " - " + end.format('YYYY-MM-DD'));
        console.log("A new date selection was made: " + start.format('YYYY:MM:DD') + ' to ' + end.format('YYYY:MM:DD'));
    }

    $('#date_range').daterangepicker({
        autoApply: true,
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        alwaysShowCalendars: true,
        locale: {
            format: 'MMM D, YYYY'
            // format: 'YYYY:MM:D'
        }
    }, cb);

    cb(start, end);

    function get_dashboard_data(date_range) {
        var base_url = $('#base_url').val();
        $.ajax({
            // url: , 
            type: "POST",
            dataType: 'JSON',
            data: 'date=' + date_range,
            beforeSend: function() {
                $(".loader_container").show();
            },
            success: function(data) {
                //alert(data);
                set_dashboard_data(data);
                $(".loader_container").fadeOut('slow');
                //swal('Data Updated Successfully');
            }
        });
    }


    // datepicker js ends-----------------------------------------------------------------------



    //  datatable js-----------------------------------------------------------------------------

     $('#upload_file').modal('hide');
		$('#upload').click(function() {
			$('#upload_file').slideToggle(200);
		})
		$('#save_toggle_row').click(function() {
			$('#modal_toggle_row').hide();
		})


		$(document).ready(function() {
			// Setup - add a text input to each footer cell
			$('#site_list_grid thead tr').clone(true).appendTo('#site_list_grid thead');
			$('#site_list_grid thead tr:eq(1) th').each(function(i) {
				var title = $(this).text();
				if (title.trim() == "" || title == "Action")
					$(this).html('');
				else
					$(this).html('<input type="text" placeholder="Search ' + title + '" />');

				$('input', this).on('keyup change', function() {
					if (table.column(i).search() !== this.value) {
						table
							.column(i)
							.search(this.value)
							.draw();
					}
				});
			});


			// $('table').wrap('<div class="table-responsive"></div>');

			var table = $('#vendor_list_grid_table').DataTable({
				orderCellsTop: true,
				fixedHeader: true,
				//			scrollX: true,
				//  sScrollXInner: "100%",
				"columnDefs": [{
					"targets": 0,
					"orderable": false,
				}],
				"bStateSave": true,
				'aaSorting': [
					[1, 'asc']
				],
				dom: 'RBfr<"table-responsive"t>ip',
				// scrollX: true,
				//  sScrollXInner: "100%",
				buttons: [{
						className: 'excel_button',
						text: '<i class="la la-trash"></i>',
						titleAttr: 'Delete All',

					},
					{
						className: 'filter_export_button_table',
						text: '<i class="la la-download"></i>',
						titleAttr: 'Filter and Export',

					},

					{
						className: 'hide_show_button_table',
						text: '<i class="la la-eye-slash" title="Hide/Unhide Columns"></i>',
						titleAttr: 'Hide or Show Columns',

					},
					{
						extend: 'pageLength',
						text: '<i class="la la-list-ol" title="Toggle Page Length"></i>',
						className: 'excel_button ',
						titleAttr: 'Toggle Page Length'
					},
				],

			});
			new $.fn.dataTable.FixedColumns(table, {
				leftColumns: 1
			});
			$(function() {
				// $('#vendor_list_grid').resizableColumns();

				$("#vendor_list_grid").colResizable({
					liveDrag: true,
					gripInnerHtml: "<div class='grip'></div>",
					draggingClass: "dragging",
					resizeMode: 'overflow',
					sScrollXInner: "100%"

				});
			});
        });


        var fixHelperModified = function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        },
        updateIndex = function(e, ui) {
            $('td.index', ui.item.parent()).each(function(i) {
                $(this).html(i + 1);
            });
            $('input[type=text]', ui.item.parent()).each(function(i) {
                $(this).val(i + 1);
            });
        };

    $("#mytable tbody").sortable({
        helper: fixHelperModified,
        stop: updateIndex
    }).disableSelection();

    $(".toggle-div").sortable({
        distance: 5,
        delay: 100,
        opacity: 0.6,
        cursor: 'move',
        update: function() {}
    });
        
    // datatable js ends here----------------------------------------------------------------------------------------------------


    // confrim js ---------------------------------------------------------------------------------------------------------------
    $("#alert").on("click", function() {
        $.confirm({
            title: 'Delete Record!',
            content: 'Do You Want To delete selected records?',
            buttons: {
                text: 'Done!',
                btnClass: 'btn-yu',
                confirm: function() {
                    $.alert('Record Deleted!');
                },
                cancel: function() {
                    $.alert('Canceled!');
                }

            }
        });
    });

    $("#alert1").on("click", function() {
        $.confirm({
            title: 'Delete Record!',
            content: 'Do You Want To delete selected records?',
            buttons: {
                text: 'Done!',
                btnClass: 'btn-yu',
                confirm: function() {
                    $.alert('Record Deleted!');
                },
                cancel: function() {
                    $.alert('Canceled!');
                }

            }
        });
    });
    $("#alert2").on("click", function() {
        $.confirm({
            title: 'Delete Record!',
            content: 'Do You Want To delete selected records?',
            buttons: {
                text: 'Done!',
                btnClass: 'btn-yu',
                confirm: function() {
                    $.alert('Record Deleted!');
                },
                cancel: function() {
                    $.alert('Canceled!');
                }

            }
        });
    });
    // confrim ends here---------------------------------------------------------------------------------------------------------------


    // checkbox js---------------------------------------------------------------------------------------------------------------------
    $('#close').click(function() {
        $('#upload_file').hide();
    })
    $("#select_all").change(function() { //"select all" change 
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function() { //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function() { //".checkbox" change 
        //uncheck "select all", if one of the listed checkbox item is unchecked
        if (this.checked == false) { //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

        //check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length) {
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });
    // $('input[type="checkbox"]').click(function () {
    //             if ($('#check1').is(":checked") && $('#check2').is(":checked") && $('#check3').is(":checked")) {
    //                 $('#select_all').prop("checked", true);
    //             } 
    // })







    // modal js -----------------------------------------------------------------------------
    $(document).ready(function() {
        $('[data-toggle="popover"]').popover({
            trigger: 'hover'
        });
        $('.hide_show_button_table').click(function(e) {
            e.preventDefault();
            e.stopPropagation(); 
            $('#modal_toggle_row').toggle();

        });

        $('#modal_toggle_row').click(function(e) {
            e.stopPropagation(); 
        });

        $('body').click(function() {
            $('#modal_toggle_row').hide();
        });
	});



    // filter div show hide js -------------------------------------------------------------------------------------
    $('#filter_div_save').click(function() {
        $('#filter_div').css('display', 'none');
    });
    $('#filter_div_close').click(function() {
        $('#filter_div').css('display', 'none');
    });



    //  inline editing js ---------------------------------------starts---------------------------------------------------------------

    $('#continuebtn').click(function() {
        $('#upload_file').modal('hide');
    });
    $('#reset_toggle_row').click(function(){
        $('input[type="checkbox"]').each(function() {
        this.checked = false;
        });
        })
    $('.text').dblclick(function() {
        var OriginalContent = $(this).text();
        $(this).addClass("cellEditing");
        $(this).html("<input type='text' value='" + OriginalContent + "' />");
        $(this).children().first().focus();
        $(this).children().first().keypress(function(e) {
            if (e.which == 13) {
                var newContent = $(this).val();
                $(this).parent().text(newContent);
                $(this).parent().removeClass("cellEditing");
            }
        });
        $(this).children().first().blur(function() {
            $(this).parent().text(OriginalContent);
            $(this).parent().removeClass("cellEditing");
        });
    });
    $('.date').dblclick(function() {
        var OriginalContent = $(this).text();
        $(this).addClass("cellEditing");
        $(this).html("<input type='date' data-date-format=' MMMM YYYY' value='" + OriginalContent + "' />");
        $(this).children().first().focus();
        $(this).children().first().keypress(function(e) {
            if (e.which == 13) {
                
                var newContent = $(this).val();
                var d=new Date(newContent);
                var month=d.getMonth()+1;
                var date=d.getDate();
                var year=d.getFullYear();
                newContent=date+'/'+month+'/'+year;
                $(this).parent().text(newContent);
                $(this).parent().removeClass("cellEditing");
            }
        });
        $(this).children().first().blur(function() {
            $(this).parent().text(OriginalContent);
            $(this).parent().removeClass("cellEditing");
        });
    });
    $('.number').dblclick(function() {
        var OriginalContent = $(this).text();
        $(this).addClass("cellEditing");
        $(this).html("<input type='number' value='" + OriginalContent + "' />");
        $(this).children().first().focus();
        $(this).children().first().keypress(function(e) {
            if (e.which == 13) {
                
                var newContent = $(this).val();
                var d=new Date(newContent);
                var month=d.getMonth()+1;
                var date=d.getDate();
                var year=d.getFullYear();
                newContent=date+'/'+month+'/'+year;
                $(this).parent().text(newContent);
                $(this).parent().removeClass("cellEditing");
            }
        });
        $(this).children().first().blur(function() {
            $(this).parent().text(OriginalContent);
            $(this).parent().removeClass("cellEditing");
        });
    });
    $('.dropdown').dblclick(function() {
        var OriginalContent = $(this).text();
        $(this).addClass("cellEditing");
        $(this).html("<select><option>Charkop (O)</option><option>Boriwali</option><option>Andheri</option><option>Virar</option></select>");
        $(this).children().first().focus();
        $(this).children().first().keypress(function(e) {
            if (e.which == 13) {	
                var newContent = $(this).val();
                $(this).parent().text(newContent);
                $(this).parent().removeClass("cellEditing");
            }
        });
        $(this).children().first().blur(function() {
            $(this).parent().text(OriginalContent);
            $(this).parent().removeClass("cellEditing");
        });
    });


   

// show checkbox true of custom download div -------------------------------------------------------------------------------------------------------------------
        $('#mytable input[type=checkbox]').attr('checked', true);













